/**
 * @format
 */

import {AppRegistry} from 'react-native';
import React from 'react';
import App from './App';
import {name as appName} from './app.json';
import { ModalPortal } from 'react-native-modals';
//redux

import {Provider} from 'react-redux';

//reducer Config
import configureStore from './src/store/store';

const store = configureStore();
const app = () => (
  <Provider store={store}>
    <App />
    <ModalPortal />
  </Provider>
);

AppRegistry.registerComponent(appName, () => app);
