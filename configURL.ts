import axios, {AxiosError} from 'axios';
import {useDispatch} from 'react-redux';
import {REACT_APP_API} from '@env';
import {URL} from 'react-native-url-polyfill';
import AsyncStorage from '@react-native-community/async-storage';

import {
  setAuthToken,
  setAuthUser,
  setAuthUserRole,
} from './src/screens/auth/store/actions';

import {getTutor} from './src/common/api/tutor';
import {Role} from './src/common/enums/role';
import {User} from './src/common/contracts/user';

// const REACT_APP_API = 'http://13.233.159.169/api';
export class Bootstrap {
  static init() {
    Bootstrap.configureAxios();
    Bootstrap.setupAuthToken();
  }

  private static configureAxios = async () => {
    // Automatically append the authorization token for all the requests
    // if the user is authenticated.
    axios.interceptors.request.use(async function (config) {
      const authToken = await AsyncStorage.getItem('accessToken');
      const serverURL = new URL(REACT_APP_API as string);
      const requestURL = new URL(config.url as string);

      if (serverURL.host === requestURL.host) {
        config.headers.Authorization =
          authToken && authToken.length > 0 ? `Bearer ${authToken}` : null;
      }

      return config;
    });

    // Dispatch must be defined here to work because it can not be
    // initialized conditionally.
    const dispatch = useDispatch();

    axios.interceptors.response.use(
      (response) => response,
      (error: AxiosError) => {
        const serverURL = new URL(REACT_APP_API as string);
        const requestURL = new URL(error.config.url as string);
        // Expire the session for the user if the server responds with the
        // 401 status code, i.e. Unauthenticated user.
        if (
          serverURL.host === requestURL.host &&
          error.response?.status === 401
        ) {
          // dispatch(setAuthToken(''));
          AsyncStorage.setItem('accessToken', '');
          // dispatch(setAuthUser({}));
          AsyncStorage.setItem('authUser', JSON.stringify({}));
          // dispatch(setAuthUserRole(''));
          AsyncStorage.setItem('userRole', '');
        }

        throw error;
      },
    );
  };

  private static async setupAuthToken() {
    const accessToken = await AsyncStorage.getItem('accessToken');
    const authUser = await AsyncStorage.getItem('authUser');
    const authUserRole = await AsyncStorage.getItem('authUserRole');

    const dispatch = useDispatch();

    AsyncStorage.setItem('accessToken', '');
    AsyncStorage.setItem('authUser', JSON.stringify({}));
    AsyncStorage.setItem('userRole', '');

    if (!accessToken || !authUser || !authUserRole) {
      return;
    }

    const user: User = JSON.parse(authUser);

    // dispatch(setAuthToken(accessToken));
    // dispatch(setAuthUser(user));
    // dispatch(setAuthUserRole(authUserRole as Role));
    AsyncStorage.setItem('accessToken', accessToken);
    AsyncStorage.setItem('authUser', JSON.stringify(user));
    AsyncStorage.setItem('userRole', authUserRole as Role);
    dispatch(setAuthToken(accessToken));
    dispatch(setAuthUser(user));
    dispatch(setAuthUserRole(authUserRole as Role));
    // AsyncStorage.setItem('accessToken', accessToken);
    // AsyncStorage.setItem('authUser', JSON.stringify(user));
    // AsyncStorage.setItem('userRole', authUserRole as Role);

    if (authUserRole === Role.TUTOR || authUserRole === Role.ORG_TUTOR) {
      try {
        const tutor = await getTutor();

        AsyncStorage.setItem('authUser', JSON.stringify(tutor));
      } catch (error) {
        // Expire the session for the user if the server responds with the
        // 401 status code, i.e. Unauthenticated user.
        if (error.response?.status === 401) {
          AsyncStorage.setItem('accessToken', '');
          AsyncStorage.setItem('authUser', JSON.stringify({}));
          AsyncStorage.setItem('userRole', '');
        }

        throw error;
      }
    }
  }
}
