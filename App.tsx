import React, {Component} from 'react';
import {StatusBar} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Login from './src/screens/auth/login';
import PreRegister from './src/screens/auth/preRegister';
import Register from './src/screens/auth/register';
import Otp_verification from './src/screens/auth/otpVerification';
import ForgotPassword from './src/screens/auth/forgotPassword';
import ResetPassword from './src/screens/auth/resetPassword';
import {Bootstrap} from './configURL';

//redux
import {connect} from 'react-redux';
import * as actions from './src/store/actions/index';

//AuthStack Screens
import SplashScreen from './src/screens/auth/splashScreen';
import ProfileProcessTutor from './src/screens/app/tutor/processForms/profileProcessTutor';
import ProfileProcessOrg from './src/screens/app/institute/processForms/profileProcessOrg';
import IntroSlider from './src/screens/app/tutor/introSlider';

//HomeStack Screens
import appStackTutor from './src/screens/app/tutor/appStackTutor';
import appStackStudent from './src/screens/app/student/appStackStudent';
import appStackOrg from './src/screens/app/institute/appStackOrg';
import appStackParent from './src/screens/app/parent/appStackParent';
import PdfContent from './src/screens/app/tutor/contentManagement/pdfContent'
import {Colors} from './src/utility/utilities';

const App = () => {
  Bootstrap.init();
  const authStack = createStackNavigator();
  const mainStack = createStackNavigator();
  const profileDetailStack = createStackNavigator();

  const createProfileDetailsStackTutor = () => (
    <profileDetailStack.Navigator initialRouteName="ProfileDetailsScreen">
      <mainStack.Screen
        name={'ProfileDetailsScreen'}
        component={ProfileProcessTutor}
        options={{headerShown: false}}
      />
      <mainStack.Screen
        name="IntroSlider"
        component={IntroSlider}
        options={{headerShown: false}}
      />
    </profileDetailStack.Navigator>
  );

  const createProfileDetailsStackOrg = () => (
    <profileDetailStack.Navigator initialRouteName="ProfileDetailsScreen">
      <mainStack.Screen
        name={'ProfileProcessOrg'}
        component={ProfileProcessOrg}
        options={{headerShown: false}}
      />
      <mainStack.Screen
        name="IntroSlider"
        component={IntroSlider}
        options={{headerShown: false}}
      />
    </profileDetailStack.Navigator>
  );

  const createAuthStack = () => (
    <authStack.Navigator initialRouteName="SplachScreen">
      <authStack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{headerShown: false, title: 'EDUMATICA'}}
      />
      <authStack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false, title: 'EDUMATICA'}}
      />
      <authStack.Screen
        name="PreRegister"
        component={PreRegister}
        options={{title: 'EDUMATICA'}}
      />
      <authStack.Screen
        name="Register"
        component={Register}
        options={{title: 'EDUMATICA'}}
      />
      <authStack.Screen
        name="OTP"
        component={Otp_verification}
        options={{title: 'EDUMATICA'}}
      />
      <authStack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{title: 'EDUMATICA'}}
      />
      <authStack.Screen
        name="ResetPassword"
        component={ResetPassword}
        options={{title: 'EDUMATICA'}}
      />
    </authStack.Navigator>
  );

  const createMainStack = () => (
    <mainStack.Navigator initialRouteName="authStack">
      <mainStack.Screen
        name={'authStack'}
        component={createAuthStack}
        options={{headerShown: false}}
      />
      <mainStack.Screen
        name={'appStackTutor'}
        component={appStackTutor}
        options={{headerShown: false}}
      />
      <mainStack.Screen
        name={'appStackStudent'}
        component={appStackStudent}
        options={{headerShown: false}}
      />
      <mainStack.Screen
        name={'appStackOrg'}
        component={appStackOrg}
        options={{headerShown: false}}
      />
      <mainStack.Screen
        name={'appStackParent'}
        component={appStackParent}
        options={{headerShown: false}}
      />
      <mainStack.Screen
        name={'createProfileDetailsStackTutor'}
        component={createProfileDetailsStackTutor}
        options={{headerShown: false}}
      />
      <mainStack.Screen
        name={'createProfileDetailsStackOrg'}
        component={createProfileDetailsStackOrg}
        options={{headerShown: false}}
      />
    </mainStack.Navigator>
  );

  return (
    <NavigationContainer>
      <StatusBar backgroundColor={Colors.lightGrey} barStyle={'dark-content'} />
      {createMainStack()}
    </NavigationContainer>
  );
};

// const mapStateToProps = state => {
//   return {
//     tokenValue: state.auth.setTokenValue
//   };
// }

// const mapDispatchToProps = dispatch => {
//   return {
//     setData: (setTokenValue) => dispatch(actions.setToken(setTokenValue))
//   }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(App);
export default App;
