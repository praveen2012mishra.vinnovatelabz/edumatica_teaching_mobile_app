import React, {FunctionComponent} from 'react';
import {Box, Grid, IconButton} from '@material-ui/core';
import {RemoveCircleOutline as RemoveCircleIcon} from '@material-ui/icons';
import {BoardClassSubjectsMap} from '../../common/academics/contracts/board_class_subjects_map';
import {
  StyleSheet,
  Text,
  Image,
  View,
  Linking,
  Button,
  TouchableOpacity,
  LogBox,
  SafeAreaView,
  Dimensions,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Colors} from '../../utility/utilities';
import HeightWidth from '../../utility/HeightWidth';
interface BoardClassSubjectRowProps {
  item: BoardClassSubjectsMap;
  handleRemoveItem: () => any;
}

const BoardClassSubjectsRow: FunctionComponent<BoardClassSubjectRowProps> = ({
  item,
  handleRemoveItem,
}) => {
  return (
    <View style={styles.__tableBox}>
      <View style={styles.__bodyBox}>
        <Text style={styles.__title3}>{item.boardname}</Text>
      </View>
      <View style={styles.__bodyBox}>
        <Text style={styles.__title3}>{item.classname}</Text>
      </View>
      <View style={styles.__bodyBox2}>
        <View style={styles.__bodyBox3}>
          <Text style={styles.__title3}>{item.subjects.join(', ')}</Text>
        </View>
        <View style={styles.__bodyBox4}>
          <TouchableOpacity onPress={handleRemoveItem}>
            <Icon name="minus-circle" size={24} color="red" />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

interface Props {
  boardClassSubjectsMap: BoardClassSubjectsMap[];
  handleRemoveItem: (map: BoardClassSubjectsMap) => any;
}

const CourseTable: FunctionComponent<Props> = ({
  boardClassSubjectsMap,
  handleRemoveItem,
}) => {
  return (
    <View>
      <View style={styles.__tableBox}>
        <View style={styles.__headerBox}>
          <Text style={styles.__title2}>Boards</Text>
        </View>
        <View style={styles.__headerBox}>
          <Text style={styles.__title2}>Classes</Text>
        </View>
        <View style={styles.__headerBox2}>
          <Text style={styles.__title2}>Subjects</Text>
        </View>
      </View>
      {boardClassSubjectsMap.map((map, index) => (
        <BoardClassSubjectsRow
          key={index}
          item={map}
          handleRemoveItem={() => handleRemoveItem(map)}
        />
      ))}
    </View>
  );
};
const styles = StyleSheet.create({
  __table: {
    flexDirection: 'row',
    // justifyContent: 'space-between',
    // alignItems: 'stretch',
  },
  __table2: {
    flexDirection: 'row',
    // justifyContent: 'space-between',
    // alignItems: 'stretch',
    fontWeight: '900',
  },
  __text: {
    fontWeight: 'bold',
  },
  __title2: {
    color: Colors.veryDarkGray,
    fontSize: HeightWidth.getResFontSize(12),
    textAlign: 'center',
    // marginVertical: 12,
  },
  __tableBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  __headerBox: {
    flex: 0.25,
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: Colors.lightBlue,
    height: 50,
  },
  __headerBox2: {
    flex: 0.5,
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: Colors.lightBlue,
    height: 50,
  },
  __bodyBox: {
    flex: 0.25,
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.primaryBlue,
    height: 50,
  },
  __bodyBox2: {
    flexDirection: 'row',
    flex: 0.5,
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.primaryBlue,
    height: 50,
  },
  __bodyBox3: {
    flex: 0.8,
    justifyContent: 'center',
    // borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.primaryBlue,
    height: 50,
  },
  __bodyBox4: {
    flex: 0.2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  __title3: {
    color: Colors.primaryGrey,
    fontSize: HeightWidth.getResFontSize(12),
    textAlign: 'center',
    // marginVertical: 12,
  },
});

export default CourseTable;
