import React, {Component} from 'react';
import {View} from 'react-native';
import Modal from 'react-native-modal';

//utility
import {Colors} from '../../utility/colors';
import {height, StatusBarHeight} from '../../utility/utilities';

//components

export default class CustomModel extends Component {
  render() {
    let {isModalVisible, onBackButtonPress, children} = this.props;
    return (
      <View>
        <Modal
          isVisible={isModalVisible}
          onBackButtonPress={onBackButtonPress}
          backdropColor={Colors.lightBlue}
          backdropOpacity={0.5}
          deviceHeight={height + StatusBarHeight}
          coverScreen={true}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={600}
          animationOutTiming={600}
          backdropTransitionInTiming={600}
          backdropTransitionOutTiming={600}>
          {children}
        </Modal>
      </View>
    );
  }
}
