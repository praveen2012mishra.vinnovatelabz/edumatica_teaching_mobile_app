/* eslint-disable react-native/no-inline-styles */
/* eslint-disable eqeqeq */
import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';
import {Colors} from '../../utility/utilities';

function Loading(props) {
  const styleView = props.loading ? 2 : -1;
  if (props.loading) {
    return (
      //, backgroundColor: 'rgba(255, 255, 255, 0.5)'
      <View
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          alignItems: 'center',
          zIndex: styleView,
          justifyContent: 'center',
          backgroundColor: 'rgba(0, 0, 0, 0.7)',
        }}>
        {/* <Spinner color='#5195FF'/> */}
        <ActivityIndicator size="large" color={Colors.white} />
      </View>
    );
  } else {
    return null;
  }
}

function mapStateToProps(state) {
  return {
    loading: state.home.loading,
  };
}

export default connect(mapStateToProps)(Loading);
