import React from 'react';
import { StyleSheet, ScrollView, KeyboardAvoidingView, Platform } from 'react-native'

const CustomScrollView = ({ children, nestedScrollEnabled = false }) => {
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : null}
            keyboardVerticalOffset={Platform.OS === 'ios' ? 50 : 0}
            style={[styles.__container]}>
            <ScrollView
                keyboardDismissMode={'on-drag'}
                keyboardShouldPersistTaps="always"
                nestedScrollEnabled
            >
                {children}
            </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default CustomScrollView;

const styles = StyleSheet.create({
    __container: {
        flex: 1
    }
})
