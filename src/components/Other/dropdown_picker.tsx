/* eslint-disable react-native/no-inline-styles */
// import DropDownPicker from 'react-native-dropdown-picker';
import DropDownPicker from 'react-native-picker-select';
import React, {useState} from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {connect} from 'react-redux';
import HeightWidth from '../../utility/HeightWidth';
import {Colors} from '../../utility/utilities';
import Icon from 'react-native-vector-icons/Feather';




function DropdownPicker(props) {
  return (
    <View style={[styles(props).SelectView, { ...props.otherStyle }]}>
      <Icon name="arrow-down" size={25} color={Colors.lightBlack} style={styles.IconStyle} />
      <DropDownPicker
        items={props.items}
        value={props.value}
        placeholder={{
          label: props.placeholder,
          value: '',
          color: Colors.darkBlue,
        }}
        style={pickerSelectStyles(props)}
        onValueChange={item => props.onValueChange(item)}
        disabled={props.disabled}
      />
    </View>
  );
}

export default (DropdownPicker);


const styles = props =>
  StyleSheet.create({
    SelectView: {
      borderBottomWidth: 1,
      borderBottomColor: Colors.primaryRed,
      flexDirection: 'row',
      alignItems: 'center',
    },
    IconStyle: {
      height: HeightWidth.getResHeight(30),
      width: HeightWidth.getResWidth(20),
      marginTop: HeightWidth.getResHeight(20),
    },
  });
const pickerSelectStyles = props =>
  StyleSheet.create({
    inputIOS: {
      fontSize: HeightWidth.getResFontSize(0.16),
      color: Colors.primaryRed,
      paddingVertical: HeightWidth.getResHeight(20),
      paddingHorizontal: HeightWidth.getResWidth(10),
      backgroundColor: 'transparent',
    },
    inputAndroid: {
      fontSize: HeightWidth.getResFontSize(0.16),
      color: Colors.primaryRed,
      paddingVertical: HeightWidth.getResHeight(8),
      paddingHorizontal: HeightWidth.getResWidth(100),
      backgroundColor: 'transparent',
    },
  });
