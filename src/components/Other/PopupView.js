/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, View, TouchableOpacity, Image} from 'react-native';
import {connect} from 'react-redux';
import { width, Colors, images, height } from '../../utility/utilities';
import Modal from 'react-native-modal';

import HeightWidth from '../../utility/HeightWidth';

import Icon from 'react-native-vector-icons/Feather';

PopupView = (props) => {
  return (
    <Modal
        isVisible={props.visible}
        backdropOpacity={0.5}
        animationIn="slideInUp"
        animationOut="slideOutDown"
        animationInTiming={600}
        animationOutTiming={600}
        backdropTransitionInTiming={200}
        backdropTransitionOutTiming={200}
        onBackdropPress={props.closePopUp}
    >
      <View style={styles(props).modalView}>
        <View style={styles(props).backView}>
          <TouchableOpacity
            style={styles(props).closeButton}
            onPress={props.closePopUp}>
            <Icon name={"x"} size={20} color={Colors.lightBlack} />
          </TouchableOpacity>
          {props.PopUp}
        </View>
      </View>
    </Modal>
  );
}

export default PopupView;

const styles = props =>
  StyleSheet.create({
    modalView: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: Colors.white,
      borderRadius:10
    },
    backView: {
      width: '95%',
      paddingBottom: HeightWidth.getResHeight(10),
    },
    closeButton: {
      right: 0,
      marginRight: HeightWidth.getResWidth(10),
      marginLeft: 'auto',
      marginTop: HeightWidth.getResHeight(10),
      marginBottom: HeightWidth.getResHeight(20)
    },
  });
