/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import {width, Colors} from '../../utility/utilities';

const CheckBoxUI = ({label, width}) => {
  const [toogleCheckBox, settoogleCheckBox] = useState(false);
  return (
    <View
      style={[
        styles.__checkboxView,
        {
          flexDirection: 'row',
          alignItems: 'center',
          width:
            width === 'sm'
              ? __inpSmall
              : width === 'md'
              ? __inpMid
              : width === 'lg'
              ? __inpLarge
              : width,
        },
      ]}>
      <CheckBox
        disabled={false}
        value={toogleCheckBox}
        tintColors={{true: '#212121'}}
        tintColor={{true: '#212121'}}
        onValueChange={(value) => settoogleCheckBox(value)}
      />
      <Text style={{fontSize: 16, fontWeight: '500'}}> {label} </Text>
    </View>
  );
};

export default CheckBoxUI;

const __inpSmall = width / 2 + 10;

const __inpMid = width / 2 + 60;

const __inpLarge = width - 40;

const styles = StyleSheet.create({
  __checkboxView: {
    flexDirection: 'column',
    alignItems: 'center',
    overflow: 'hidden',
    paddingLeft: 8,
  },
});
