import React from 'react'
import { StyleSheet, Text, View } from 'react-native';

//utilities
import { Colors, width } from '../../../utility/utilities';
import HeightWidth from '../../../utility/HeightWidth';

import { FlatList, TouchableOpacity } from 'react-native-gesture-handler'

export default function Dropdown(props) {
    let { data, onItemPress, Width, maxHeight } = props;
    return (
        <View style={[styles.__dropDownView, { width: Width }]}>
            <View style={[styles.__dropDownInnerView, { maxHeight: maxHeight }]}>
                <FlatList
                    data={data}
                    nestedScrollEnabled
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity activeOpacity={0.5} key={index} onPress={() => onItemPress(item.value)} style={[styles.__dropDownItem, { width: Width - 10 }]}>
                                <Text numberOfLines={1} style={styles.__dropDownItemText}>{item.label}</Text>
                            </TouchableOpacity>
                        )
                    }}
                    keyExtractor={(_, index) => index.toString()}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({

    __dropDownView: {
        top: HeightWidth.getResWidth(70),
        zIndex: 100,
        position: "absolute",
        backgroundColor: Colors.veryLightGray,
        borderRadius: 10,
        overflow: "hidden"
    },
    __dropDownInnerView: {
        maxHeight: HeightWidth.getResWidth(200),

    },
    __dropDownItem: {
        marginVertical: HeightWidth.getResWidth(6),
        paddingVertical: HeightWidth.getResWidth(7),
        borderRadius: 10,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        backgroundColor: Colors.fullWhite,
    },
    __dropDownItemText: {
        fontSize: 17,
        color: Colors.primaryBlue,
        paddingHorizontal: HeightWidth.getResWidth(8)
    },
})
