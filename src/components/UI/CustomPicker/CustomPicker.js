import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, LogBox } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import PropTypes from 'prop-types';

//utilities
import { Colors, width } from '../../../utility/utilities';
import HeightWidth from '../../../utility/HeightWidth';
import Dropdown from './Dropdown';

//ToDo: LogBox.ignoreAllLogs();

const CustomPicker = ({
    data,
    label,
    labelSize,
    labelColor,
    labelStyle,
    value,
    inputBackground,
    textInputStyles,
    placeholder,
    placeholderTextColor,
    selectedOnChange,
    rightIconSize,
    rightIconColor,
    otherStyle,
    Width,
    maxHeight
}) => {
    const [inputValue, setInputValue] = useState('');
    const [showDropdown, setShowDropdown] = useState(false);
    const [filteredList, setFilteredList] = useState([]);
    const [showFilteredData, setShowFilteredData] = useState(false);
    const toggle = React.useCallback(() => setShowDropdown(!showDropdown));

    useEffect(() => {
        if (value !== inputValue) {
            setInputValue(value);
        }
    }, [value])

    const onChangeText = (text) => {
        if (text) {
            setShowFilteredData(true)
        }
        setFilteredList(data.filter(
            (item) => {
                return item.value.toLowerCase().indexOf(text?.toLowerCase()) !== -1;
            }
        ))
        setShowDropdown(true)
        setInputValue(text)
        selectedOnChange(text)
    }

    const setValueOnClick = (value) => {
        setShowDropdown(false)
        setInputValue(value)
        selectedOnChange(value)
    }


    return (
        <View>
            {label ? (
                <Text
                    style={[{ fontSize: labelSize, color: labelColor }, { ...labelStyle }]}>
                    {label}
                </Text>
            ) : null}
            <View style={[styles.__inputView, { width: Width }, { ...otherStyle }]}>
                <TextInput
                    value={inputValue}
                    style={[
                        styles.__input,
                        { backgroundColor: inputBackground },
                        { ...textInputStyles },
                    ]}
                    onChangeText={onChangeText}
                    placeholder={placeholder}
                    placeholderTextColor={placeholderTextColor}
                    onFocus={() => setShowDropdown(true)}
                    onBlur={() => setShowDropdown(false)}
                />
                <TouchableOpacity onPress={toggle} style={styles.__rightIconView}>
                    <Icon
                        size={rightIconSize}
                        color={rightIconColor}
                        name={showDropdown ? 'chevron-up' : 'chevron-down'}
                    />
                </TouchableOpacity>
            </View>
            {showDropdown &&
                <Dropdown
                    onItemPress={(item) => { setValueOnClick(item) }}
                    data={showFilteredData ? filteredList : data}
                    Width={Width}
                    maxHeight={maxHeight}
                />
            }
        </View>
    )
}

CustomPicker.propTypes = {
    data: PropTypes.array,
    label: PropTypes.string,
    labelSize: PropTypes.number,
    labelColor: PropTypes.string,
    labelStyle: PropTypes.object,
    value: PropTypes.string,
    inputBackground: PropTypes.string,
    textInputStyles: PropTypes.object,
    placeholder: PropTypes.string,
    placeholderTextColor: PropTypes.string,
    selectedOnChange: PropTypes.any,
    rightIconSize: PropTypes.number,
    rightIconColor: PropTypes.string,
    otherStyle: PropTypes.object,
    maxHeight: PropTypes.number
}

CustomPicker.defaultProps = {
    inputBackground: Colors.transparent,
    labelSize: 16,
    labelColor: Colors.lightGrayColor,
    placeholder: "Input",
    rightIconSize: 20,
    rightIconColor: Colors.veryGrayishBlack,
}

const styles = StyleSheet.create({
    __inputView: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: HeightWidth.getResWidth(width - 40),
        height: HeightWidth.getResWidth(50),
        overflow: 'visible',
    },
    __input: {
        fontSize: 17,
        flex: 1
    },
    __rightIconView: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: HeightWidth.getResWidth(8)
    },

})

export default CustomPicker;