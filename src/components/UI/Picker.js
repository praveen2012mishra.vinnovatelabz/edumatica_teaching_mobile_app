/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import Icon from 'react-native-vector-icons/Ionicons';

const Picker = ({
  label,
  data,
  rightIconName,
  rightIconSize,
  rightIconColor,
  value,
  docTypeHandler,
  onRightIconPress,
  labelSize,
  labelColor,
  showLabel = true,
  otherStyle = {}
}) => {
  return (
    <View style={{ marginVertical: 10, marginHorizontal: 3 }}>
      {showLabel ? (
        <Text
          style={[
            {
              fontSize: labelSize ? labelSize : 16,
              color: labelColor,
              marginHorizontal: 10,
            },
            { ...otherStyle }
          ]}>
          {label}
        </Text>
      ) : null}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={{ width: rightIconName ? '90%' : '100%' }}>
          <RNPickerSelect
            placeholder={{
              label: `Select ${label}`,
              value: null,
              color: '#000',
            }}
            value={value}
            onValueChange={(value) => docTypeHandler(value)}
            items={data}
            Icon={() => {
              return <Icon size={20} color="black" name="chevron-down" />;
            }}
            style={{
              inputAndroid: {
                backgroundColor: 'transparent',
                color: '#000',
              },
              iconContainer: {
                top: 15,
                right: 10,
              },
            }}
          />
        </View>
        {rightIconName ? (
          <View style={styles.rightIconView}>
            <Icon
              size={rightIconSize}
              color={rightIconColor}
              name={rightIconName}
              onPress={onRightIconPress}
            />
          </View>
        ) : null}
      </View>

      <View
        style={{
          borderBottomColor: '#a1a1a1',
          borderBottomWidth: 1,
          marginHorizontal: 10,
        }}
      />
    </View>
  );
};

export default Picker;
const styles = StyleSheet.create({
  rightIconView: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    paddingLeft: 8,
    paddingRight: 8,
  },
});
