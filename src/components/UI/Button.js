import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Feather';

//utilities
import {width, Colors} from '../../utility/utilities';

const __btnsm = {
  width: width / 2,
  height: 50,
};
const __btnmd = {
  width: width / 2 + 50,
  height: 55,
};
const __btnlg = {
  width: width - 50,
  height: 60,
};

class ButtonUI extends Component {
  render() {
    const {
      title,
      size,
      width,
      backgroundColor,
      elevation,
      borderRadius,
      leftText,
      leftIconSize,
      leftIconColor,
      leftIconName,
      leftIconStyle,
      rightIconSize,
      rightIconColor,
      rightIconName,
      textStyles,
      height,
      onPress,
      otherStyle,
      activityIndicator,
      disabled,
      leftTextStyle,
    } = this.props;
    return (
      <TouchableOpacity
        disabled={disabled}
        style={[
          styles.__buttonContainer,
          size === 'sm'
            ? __btnsm
            : size === 'md'
            ? __btnmd
            : size === 'lg'
            ? __btnlg
            : null,
          {
            backgroundColor: backgroundColor,
            elevation: elevation,
            borderRadius: borderRadius,
          },
          width ? {width: width} : null,
          height ? {height: height} : null,
          {...otherStyle},
        ]}
        onPress={onPress}>
        {!activityIndicator ? (
          <>
            {leftIconName ? (
              <View style={[styles.__leftIconView, {...leftIconStyle}]}>
                <Icon
                  size={leftIconSize}
                  color={leftIconColor}
                  name={leftIconName}
                />
              </View>
            ) : null}
            {leftText ? (
              <Text style={[styles.__leftTextView, {...leftTextStyle}]}>
                {leftText}
              </Text>
            ) : null}
            <View style={styles.__textView}>
              <Text numberOfLines={1} style={[styles.__text, {...textStyles}]}>
                {' '}
                {title}{' '}
              </Text>
            </View>
            {rightIconName ? (
              <View style={styles.__rightIconView}>
                <Icon
                  size={rightIconSize}
                  color={rightIconColor}
                  name={rightIconName}
                />
              </View>
            ) : null}
          </>
        ) : (
          <ActivityIndicator size={'small'} color={Colors.white} />
        )}
      </TouchableOpacity>
    );
  }
}

ButtonUI.propTypes = {
  leftIconSize: PropTypes.number,
  leftText: PropTypes.string,
  leftIconColor: PropTypes.string,
  leftIconName: PropTypes.string,
  leftIconStyle: PropTypes.object,
  rightIconSize: PropTypes.number,
  rightIconColor: PropTypes.string,
  rightIconName: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  backgroundColor: PropTypes.string,
  elevation: PropTypes.number,
  size: PropTypes.string,
  borderRadius: PropTypes.number,
  title: PropTypes.string.isRequired,
  textStyles: PropTypes.any,
  onPress: PropTypes.any,
  otherStyle: PropTypes.object,
  activityIndicator: PropTypes.bool,
  disabled: PropTypes.bool,
  leftTextStyle: PropTypes.object,
};

ButtonUI.defaultProps = {
  size: 'sm',
  backgroundColor: Colors.blue,
  elevation: 1,
  borderRadius: 2,
  activityIndicator: false,
  disabled: false,
};

const styles = StyleSheet.create({
  __buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  __leftIconView: {
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  __leftTextView: {
    paddingLeft: 10,
    paddingRight: 10,
    color: Colors.primaryBlue,
    fontSize: 16,
  },
  __textView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  __text: {
    fontSize: 17,
  },
  __rightIconView: {
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default ButtonUI;
