import React from 'react'
import {View, Text, TouchableOpacity, Alert} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import DocumentPicker from 'react-native-document-picker';

const UploadFile = ({iconColor, iconName}) => {
    
    const fileUploader =()=>{
        try {
        const res = await DocumentPicker.pick({
            type: [DocumentPicker.types.images],
        });
        console.log(
            res.uri,
            res.type, // mime type
            res.name,
            res.size
        );
        } catch (err) {
        if (DocumentPicker.isCancel(err)) {
            // User cancelled the picker, exit any dialogs or menus and move on
            Alert.alert('Upload Cancel')
        } else {
            throw err;
        }
        }
    }
  return (
    <View>
      <TouchableOpacity onPress={(e) => fileUploader(e)}>
        <Feather size={25} color={iconColor} name={iconName} />
      </TouchableOpacity>
    </View>
  );
};

export default UploadFile
