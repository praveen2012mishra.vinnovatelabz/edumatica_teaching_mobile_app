import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Feather';
import { TextInput } from 'react-native-paper';


//utilities
import { width, Colors } from '../../utility/utilities';

class FloatingTextInputUI extends Component {
    render() {
        let {
            value,
            onChangeText,
            placeholder,
            placeholderTextColor,
            editable,
            password,
            keyboardType,
            maxLength,
            returnKeyTypes,
            returnKeyLabels,
            leftIconSize,
            leftIconColor,
            leftIconName,
            rightIconSize,
            rightIconColor,
            rightIconName,
            width,
            inputBackground,
            leftText,
            onRightIconPress,
            backgroundColor,
            leftTextColor,
            elevation,
            height,
            leftTextSize,
            inputBorderR,
            textInputStyles,
            otherStyle,
            leftViewStyle,
            label,
            labelSize,
            labelColor,
            labelStyle,
            multiline,
            autoFocus,
            blurOnSubmit,
            onSubmitEditing,
            refData,
            mode,
            onBlur,
            onChange
        } = this.props;
        return (
            <>
                <View style={[styles.__inputView,{...otherStyle},]}>
                    <View style={styles.__textInputView}>
                        <TextInput
                            label={label}
                            mode={mode}
                            ref={refData}
                            value={value}
                            style={[styles.__input, { backgroundColor: inputBackground }, { ...textInputStyles }]}
                            onChangeText={onChangeText}
                            onChange={onChange}
                            placeholder={placeholder}
                            placeholderTextColor={placeholderTextColor}
                            editable={editable}
                            secureTextEntry={password}
                            keyboardType={keyboardType}
                            maxLength={maxLength}
                            returnKeyType={returnKeyTypes}
                            returnKeyLabel={returnKeyLabels}
                            multiline={multiline}
                            blurOnSubmit={blurOnSubmit}
                            onSubmitEditing={onSubmitEditing}
                            autoFocus={autoFocus}
                            onBlur={onBlur}
                        />
                    </View>
                </View>
            </>
        )
    }
}


FloatingTextInputUI.propTypes = {
    value: PropTypes.string.isRequired,
    onChangeText: PropTypes.func,
    onChange: PropTypes.onChange,
    onBlur: PropTypes.func,
    placeholder: PropTypes.string,
    placeholderTextColor: PropTypes.string,
    editable: PropTypes.bool,
    password: PropTypes.bool,
    keyboardType: PropTypes.string,
    maxLength: PropTypes.number,
    returnKeyTypes: PropTypes.string,
    returnKeyLabels: PropTypes.string,
    leftIconSize: PropTypes.number,
    leftIconColor: PropTypes.string,
    leftIconName: PropTypes.string,
    rightIconSize: PropTypes.number,
    rightIconColor: PropTypes.string,
    rightIconName: PropTypes.string,
    width: PropTypes.any,
    inputBackground: PropTypes.string,
    leftText: PropTypes.string,
    onRightIconPress: PropTypes.any,
    backgroundColor: PropTypes.string,
    elevation: PropTypes.number,
    leftTextSize: PropTypes.number,
    height: PropTypes.number,
    inputBorderR: PropTypes.number,
    textInputStyles: PropTypes.any,
    otherStyle: PropTypes.object,
    leftViewStyle: PropTypes.object,
    labelSize: PropTypes.number,
    labelColor: PropTypes.string,
    labelStyle: PropTypes.object,
    multiline: PropTypes.bool,
    autoFocus: PropTypes.bool,
    blurOnSubmit: PropTypes.bool,
    onSubmitEditing: PropTypes.any,
    label: PropTypes.string,
    mode: PropTypes.string
}

FloatingTextInputUI.defaultProps = {
    editable: true,
    password: false,
    keyboardType: "default",
    returnKeyTypes: "default",
    returnKeyLabels: "",
    maxLength: 200,
    // backgroundColor: Colors.transparent,
    // inputBackground: Colors.transparent,
    // elevation: 0,
    // height: 50,
    // inputBorderR: 1,
    labelSize: 16,
    labelColor: Colors.darkDBlue,
    // multiline: false,
    // autoFocus: false,
    // blurOnSubmit: true,
    mode: "flat",
    onSubmitEditing: () => { }
}

const styles = StyleSheet.create({
    __inputView: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        overflow: "hidden",
    },
    __leftIconView: {
        alignItems: "center",
        justifyContent: "center",
        paddingLeft: 8,
        paddingRight: 8,
    },
    __textInputView: {
        flex: 1,
        justifyContent: "center",
    },
    __input: {
        fontSize: 17
    },
});

export default FloatingTextInputUI;
