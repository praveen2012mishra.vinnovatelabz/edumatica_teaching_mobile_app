/* eslint-disable no-shadow */
/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {Text, View} from 'react-native';
import {width} from '../../utility/utilities';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import Feather from 'react-native-vector-icons/Feather';

const DatePickerUI = ({label, placeholder, iconName, iconColor}) => {
  const [date, setDate] = useState('');

  const _handleDateChange = (date) => {
    setDate(moment(date).format('DD/MM/YYYY'));
  };

  return (
    <View>
      {label && <Text> {label} </Text>}
      <DatePicker
        style={{width: width - 35}}
        date={date}
        mode="date"
        placeholder={placeholder}
        format="DD/MM/YYYY"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        iconComponent={<Feather size={25} color={iconColor} name={iconName} />}
        customStyles={{
          dateInput: {
            borderTopWidth: 0,
            borderLeftWidth: 0,
            borderRightWidth: 0,
            borderBottomWidth: 0,
            justifyContent: 'center',
            alignItems: 'flex-start',
            paddingHorizontal: 5,
            marginLeft: 10,
          },
          placeholderText: {
            fontSize: 16,
          },
          dateText: {
            fontSize: 16,
          },
        }}
        onDateChange={(date) => _handleDateChange(date)}
      />
      <View
        style={{
          borderBottomColor: '#a1a1a1',
          borderBottomWidth: 1,
          marginHorizontal: 10,
        }}
      />
    </View>
  );
};

export default DatePickerUI;
