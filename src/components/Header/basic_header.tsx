import React, { FunctionComponent } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
// import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Feather';
import PropTypes from 'prop-types'

//Utilities
import { width, Colors, images } from '../../utility/utilities';
import HeightWidth from '../../utility/HeightWidth';


interface Props  { title,
    showRightIcon,
    onRightIconClick,
    showLeftIcon,
    onLeftIconClick,
    profileData,
    userName,
    userMobileNo
} 

const BasicHeader: FunctionComponent <Props> = ({ title,
    showRightIcon,
    onRightIconClick,
    showLeftIcon,
    onLeftIconClick,
    profileData,
    userName,
    userMobileNo }) => {
    // render() {
        return (
            <View style={[styles.__container, showLeftIcon ? null : { justifyContent: "center", alignItems: "center" }]}>
                <View style={[styles.__attachedElement]}>
                    {showLeftIcon ? <View style={styles.__leftElement}>
                        <Icon onPress={onLeftIconClick} style={styles.__leftElementIcon} name={"arrow-left"} size={25} color={Colors.white} />
                    </View> : null}
                    {title ? <View style={[styles.__centerElement, showLeftIcon ? null : { alignItems: "center" }]}>
                        <Text style={styles.__centerElementText}>
                            {title}
                        </Text>
                    </View> : null}
                    {profileData ?
                        <View style={styles.__profileView}>
                            <View style={styles.__profileImageView}>
                                <Image source={images.avatar} style={styles.__profileImage} />
                            </View>
                            <View style={styles.__profileDetails}>
                                <Text numberOfLines={1} style={styles.__profileTopDetails}>
                                    {userName}
                                </Text>
                                <Text numberOfLines={1} style={styles.__profileBottomDetails}>
                                    +91 {userMobileNo}
                                </Text>
                            </View>
                        </View>
                        : null}
                </View>
                {showRightIcon ? <View style={styles.__rightElement}>
                    <Icon onPress={onRightIconClick} name={"power"} size={20} color={Colors.white} />
                </View> : null}
            </View>
        )
    }

// onPress={this.props.route.params.onGoBack()}
BasicHeader.propTypes = {
    title: PropTypes.string,
    showRightIcon: PropTypes.bool,
    onRightIconClick: PropTypes.func,
    showLeftIcon: PropTypes.bool,
    onLeftIconClick: PropTypes.func,
    profileData: PropTypes.bool
}

BasicHeader.defaultProps = {
    title: null,
    showLeftIcon: false,
    showRightIcon: false,
    profileData: false
}

const styles = StyleSheet.create({
    __container: {
        width: width,
        height: 65,
        justifyContent: "space-between",
        flexDirection: "row",
        alignItems: "center",
        padding: 10
    },
    __attachedElement: {
        justifyContent: "flex-start",
        flexDirection: "row",

    },
    __leftElement: {
        padding: 10,
        borderRightColor: Colors.veryDarkRed,
        borderRightWidth: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    __leftElementIcon:{
        padding:3
    },
    __centerElement: {
        justifyContent: "center",
        paddingLeft: 10,
        paddingRight: 10
    },
    __centerElementText: {
        color: Colors.white,
        fontSize: 18,
        fontWeight: "bold"
    },
    __rightElement: {
        padding: 10,
        justifyContent: "center"
    },
    __profileView: {
        marginLeft: 20,
        justifyContent: "space-around",
        alignItems: "center",
        flexDirection: "row",
    },
    __profileImageView: {
        width: HeightWidth.getResWidth(35),
        height: HeightWidth.getResWidth(35),
        borderRadius: HeightWidth.getResWidth(35),
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: Colors.white,
        overflow:"hidden"
    },
    __profileImage: {
        width: HeightWidth.getResWidth(34),
        height: HeightWidth.getResWidth(34),
        borderRadius: HeightWidth.getResWidth(35),
        resizeMode: 'contain'
    },
    __profileDetails: {
        justifyContent: "space-around",
        marginLeft: 10
    },
    __profileTopDetails: {
        color: Colors.smokeWhite,
        fontSize: 15,
    },
    __profileBottomDetails: {
        color: Colors.lightGrey,
        fontSize: 14
    },
})

export default BasicHeader;
