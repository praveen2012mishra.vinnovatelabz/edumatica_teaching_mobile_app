import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

//utilities
import { width, Colors, images } from '../../utility/utilities';
import HeightWidth from '../../utility/HeightWidth';

interface Props {
  title?: string;
  onPress?: any;
  showNotification?: boolean,
  showMenu?: boolean,
  showBackButton?: boolean,
  onNotificationPress?: any;
  onMenuPress?: any;
}

const BaseHeader = ({
  title = 'EDUMATICA',
  onPress,
  showNotification = false,
  showMenu = true,
  showBackButton = true,
  onNotificationPress,
  onMenuPress,
}: Props) => {
  return (
    <View style={styles.__container}>
      { showBackButton && <TouchableOpacity onPress={onPress} style={styles.__backIconButtonView}>
        <Icon name={'arrow-left'} color={Colors.fullWhite} size={30} />
      </TouchableOpacity>}
      <Text numberOfLines={1} style={styles.__headerTitle}>
        {title}
      </Text>
      <View style={styles.__headerRightContentView}>
        {showNotification && <TouchableOpacity onPress={onNotificationPress} style={styles.__leftIconView}>
          <Icon name={'bell'} color={Colors.fullWhite} size={24} />
        </TouchableOpacity>}
        {showMenu && <TouchableOpacity onPress={onMenuPress} style={styles.__rightIconView}>
          <Image source={images.menu} style={styles.__menuIcon} />
        </TouchableOpacity>}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  __container: {
    height: HeightWidth.getResWidth(65),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.primaryBlue,
    elevation: 5,
  },
  __backIconButtonView: {
    marginLeft: HeightWidth.getResWidth(10),
    marginRight: HeightWidth.getResWidth(4),
    padding: HeightWidth.getResWidth(6),
    justifyContent: 'center',
    alignItems: 'center',
  },
  __headerRightContentView: {
    flexDirection: 'row',
    marginHorizontal: HeightWidth.getResWidth(8),
    alignItems: 'center',

  },
  __headerTitle: {
    fontSize: 19,
    color: Colors.fullWhite,
    alignSelf: 'center',
    fontWeight: 'bold',
    flex: 1,
    paddingHorizontal: HeightWidth.getResWidth(10),
  },
  __leftIconView: {
    padding: HeightWidth.getResWidth(4),
    marginRight: HeightWidth.getResWidth(10)
  },
  __menuIcon: {
    alignSelf: "center",
    width: HeightWidth.getResWidth(30),
    height: HeightWidth.getResWidth(30),
  },
  __rightIconView: {
    padding: HeightWidth.getResWidth(4)
  },
});

export default BaseHeader;
