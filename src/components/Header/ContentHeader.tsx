import React from 'react';
import { StyleSheet,Dimensions, Text, View, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

//utilities
import { width, Colors, images } from '../../utility/utilities';
import HeightWidth from '../../utility/HeightWidth';

interface Props {
  title?: string;
  onPress?: any;
  showNotification?: boolean,
  showMenu?: boolean,
  showBackButton?: boolean,
  onNotificationPress?: any;
  onMenuPress?: any;
  rightContent?:any;
  boardName?:any;
  className?:any;
}

const ContentHeader = ({
  title = 'EDUMATICA',
  onPress,
  showNotification = false,
  showMenu = true,
  showBackButton = true,
  onNotificationPress,
  onMenuPress,
  rightContent=false,
  boardName,
  className
}: Props) => {
  return (
    <View style={styles.__container}>
     <View style={styles.documentDisplayHeaderContainer}>
                    <TouchableOpacity style={styles.contentHeaderContainer}>
                        <Icon name={'arrow-left'} color={Colors.fullWhite} size={30} />
                        {/* <Image source={images.VerticalLine} style={styles.LargeVerticleLine} /> */}
                        <Text style={styles.headerContentUpload}>{title}</Text>
                    </TouchableOpacity>
                    <View style={styles.spaceHeaderContainer}>
                        <Text style={styles.headerContent}>{boardName}</Text>
                        <Text style={styles.headerContent}>|</Text>
                        <Text style={styles.headerContent}>{className}</Text>
                    </View>
                </View>
    </View>
  );
};

const styles = StyleSheet.create({
    LargeVerticleLine: {
        width: HeightWidth.getResWidth(3),
        height: HeightWidth.getResWidth(17),
    },
    headerContentUpload: {
        fontSize: HeightWidth.getResFontSize(18),
        color: Colors.fullWhite,
    },
    contentHeaderContainer: {
        width: '60%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems:'center'
    },
    documentDisplayHeaderContainer: {
        width: Dimensions.get('window').width,
        //height: 100,
        backgroundColor: Colors.contentDocumentDisplayHeader,
        paddingTop: 25,
        //justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
  __container: {
    height: HeightWidth.getResWidth(90),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.primaryBlue,
    elevation: 5,
  },
  __backIconButtonView: {
    marginLeft: HeightWidth.getResWidth(10),
    marginRight: HeightWidth.getResWidth(4),
    padding: HeightWidth.getResWidth(6),
    justifyContent: 'center',
    alignItems: 'center',
  },
  __headerRightContentView: {
    flexDirection: 'row',
    marginHorizontal: HeightWidth.getResWidth(8),
    alignItems: 'center',

  },
  __headerTitle: {
    fontSize: 19,
    color: Colors.fullWhite,
    alignSelf: 'center',
    fontWeight: 'bold',
    flex: 1,
    paddingHorizontal: HeightWidth.getResWidth(10),
  },
  __leftIconView: {
    padding: HeightWidth.getResWidth(4),
    marginRight: HeightWidth.getResWidth(10)
  },
  __menuIcon: {
    alignSelf: "center",
    width: HeightWidth.getResWidth(30),
    height: HeightWidth.getResWidth(30),
  },
  __rightIconView: {
    padding: HeightWidth.getResWidth(4)
  },
  spaceHeaderContainer: {
    width: '40%',
    justifyContent: 'space-around',
    flexDirection: 'row'
},
headerContent: {
  color: 'rgba(255, 255, 255, 0.31)',
  fontSize: HeightWidth.getResFontSize(15),
  fontWeight: '500'
},
VerticleLine: {
  width: HeightWidth.getResWidth(3),
  height: HeightWidth.getResWidth(14),
  alignSelf: 'center',
  marginRight: 10
},
});

export default ContentHeader;
