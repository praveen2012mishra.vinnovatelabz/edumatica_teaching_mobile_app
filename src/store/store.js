import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {authReducer} from '../screens/auth/store/reducers';
import homeReducer from './reducers/home';

const rootReducer = combineReducers({
  auth: authReducer,
  home: homeReducer,
});

const configureStore = () => createStore(rootReducer, applyMiddleware(thunk));

export default configureStore;
