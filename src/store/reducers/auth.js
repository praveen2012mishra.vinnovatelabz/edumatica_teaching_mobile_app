import * as actionTypes from '../actions/actionsTypes';
import {updateObject} from '../utility';

const initialState = {
  setTokenValue: null,
  setUserType: null,
  setUserMobile: null,
  setRole: null,
};

const setToken = (state, action) => {
  console.log('we are in reducers----------------------->', state, action)
  return updateObject(state, {
    setTokenValue: action.setTokenValue,
  });
};

const setUserMobileNo = (state, action) => {
  return updateObject(state, {
    setUserMobile: action.setUserMobile,
  });
};

const setAppLanguage = (state, action) => {
  return updateObject(state, {
    setAppLanguageValue: action.setAppLanguageValue,
  });
};

const setRole = (state, action) => {
  return updateObject(state, {
    setRoleValue: action.setRoleValue,
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_TOKEN:
      return setToken(state, action);
    case actionTypes.SET_ROLE:
      return setRole(state, action);
    case actionTypes.SET_USER_MOBILE:
      return setUserMobileNo(state, action);
    case actionTypes.SET_APP_LANGUAGE:
      return setAppLanguage(state, action);
    default:
      return state;
  }
};

export default reducer;
