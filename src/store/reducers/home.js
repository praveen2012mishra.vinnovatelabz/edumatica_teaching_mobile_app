import * as actionTypes from '../actions/actionsTypes';
import { updateObject } from '../utility';

const initialState = {
    c_id: null,
    userProfileData: null,
    chat_id: null,
    userChatProfile: {},
    allContactList: null,
    allAsyncContactList: null,
    singleBankDetail: null,
    loading: false
};

const tempDataSet = (state, action) => {
    return updateObject(state, {
        c_id: action.appTempData
    });
}

const setUserProfile = (state, action) => {
    return updateObject(state, {
        userProfileData: action.userProfileData
    });
}

const setChatId = (state, action) => {
    return updateObject(state, {
        chat_id: action.chat_id
    });
}

const setChatProfile = (state, action) => {
    return updateObject(state, {
        userChatProfile: action.userChatProfile
    });
}

const getAllContactsSync = (state, action) => {
    return updateObject(state, {
        allContactList: action.contactData
    })
}

const getAllContactsAsync = (state, action) => {
    return updateObject(state, {
        allAsyncContactList: action.contactAsyncData
    })
}

const setTransferBankDetails = (state, action) => {
    return updateObject(state, {
        singleBankDetail: action.singleBankDetail
    })
}

const setLoading = (state, action) => {
    return updateObject(state, {
        loading: action.loading
    })
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_TEMPDATA: return tempDataSet(state, action);
        case actionTypes.SET_USER_PROFILE: return setUserProfile(state, action);
        case actionTypes.SET_CHAT_ID: return setChatId(state, action);
        case actionTypes.SET_CHAT_PROFILE: return setChatProfile(state, action);
        case actionTypes.GET_ALLCONTACTS_SYNC: return getAllContactsSync(state, action);
        case actionTypes.GET_ALLCONTACTS_ASYNC: return getAllContactsAsync(state, action);
        case actionTypes.SET_TRANSFER_BANK_DETAIL: return setTransferBankDetails(state, action);
        case actionTypes.SET_LOADING: return setLoading(state, action);
        default: return state;
    }
};

export default reducer;