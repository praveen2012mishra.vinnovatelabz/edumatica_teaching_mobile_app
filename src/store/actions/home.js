import * as actionTypes from './actionsTypes';


export const setUserProfile = (userProfileData) => {
    return {
        type: actionTypes.SET_USER_PROFILE,
        userProfileData: userProfileData
    };
}

