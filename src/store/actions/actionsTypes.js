export const SET_TOKEN = 'SET_TOKEN';
export const SET_USER_MOBILE = 'SET_USER_MOBILE';
export const SET_APP_LANGUAGE = 'SET_APP_LANGUAGE';
export const SET_ROLE = 'SET_ROLE';

