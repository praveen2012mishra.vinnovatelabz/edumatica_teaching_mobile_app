import * as actionTypes from './actionsTypes';

export const setToken = (setTokenValue) => {
  return {
    type: actionTypes.SET_TOKEN,
    setTokenValue: setTokenValue,
  };
};

export const setAuthUser = (setUserMobile) => {
  return {
    type: actionTypes.SET_USER_MOBILE,
    setUserMobile: setUserMobile,
  };
};

export const setAppLanguage = (setAppLanguageValue) => {
  return {
    type: actionTypes.SET_USER_MOBILE,
    setAppLanguageValue: setAppLanguageValue,
  };
};

export const setAuthUserRole = (setRoleValue) => {
  return {
    type: actionTypes.SET_ROLE,
    setRole: setRoleValue,
  };
};
