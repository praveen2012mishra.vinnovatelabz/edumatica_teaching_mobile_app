import {Dimensions, StatusBar} from 'react-native';
import {connect} from 'react-redux';
import * as actions from '../store/actions/index';

export const {width, height} = Dimensions.get('window');

export const StatusBarHeight = StatusBar.currentHeight;

export {strings} from './Language';

export {images} from './images';

export {Colors} from './colors';

export const currency = '\u20B9';

export const postFetch = async (fetchURL, fetchBodydata, dataType) => {
  console.log('FetchBody--------->', fetchBodydata);
  console.log('FetchURL---------->', fetchURL);
  let headers = null;
  let fetchData = null;

  if (dataType === 'form') {
    headers = {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
      'x-FUNDPAY': 'fundpay123*#',
    };
    fetchData = fetchBodydata;
  } else {
    headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'x-FUNDPAY': 'fundpay123*#',
    };
    fetchData = JSON.stringify(fetchBodydata);
  }

  const fetchCall = await fetch(fetchURL, {
    method: 'POST',
    headers: headers,
    body: fetchData,
  });
  console.log('Header---------->', fetchCall);
  const responseData = await fetchCall.json();
  return responseData;
};

export const getFetch = async (fetchURL) => {
  console.log('URL---------->', fetchURL);
  let headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'x-FUNDPAY': 'fundpay123*#',
  };

  const fetchCall = await fetch(fetchURL, {
    method: 'GET',
    headers: headers,
  });
  console.log('Header---------->', fetchCall);
  const responseData = await fetchCall.json();
  return responseData;
};
