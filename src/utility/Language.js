import LocalizedStrings from 'react-native-localization';
export const DEFAULT_LANGUAGE = 'english';

export const strings = new LocalizedStrings({
  english: {
    reminder: 'Reminder',
  },
  marathi: {
    reminder: 'स्मरणपत्र',
  },
});
