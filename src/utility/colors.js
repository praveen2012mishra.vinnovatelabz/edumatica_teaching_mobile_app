export const Colors = {
  transparent: 'transparent',
  primaryOrange: '#EF7F1A',
  brightOrange: '#f9bd33', //3rd
  screenBG: '#d9d9d9',
  fullWhite: '#ffffff',
  primaryBlue: '#4c8bf5', //1st
  softBlue: '#4ca7f5',
  primaryBlueOpacity: '#4c8bf580',
  inActiveIcon: '#212121', // changed to veryGrayishBlack
  veryGrayishBlack: '#212121',
  pureCyanBlue: '#00B9F5', //2nd
  veryDarkGray: '#444444',
  veryLightGray: '#e6e6e6',
  primaryRed: '#cf2d33',
  primaryMustard: '#e1ad01',
  primaryGrey: '#808080',
  lightMustard: '#ffd880',
  lightBlue: '#C8DCFF',
  lightGreen: '#cdf29d',
  primaryGreen: '#80C12C',
  verySoftCyan: '#c4f1ff',
  softRed: '#f0716e',
  verySoftRed: '#f7a3a1',
  veryDarkDesaturatedBlue: "#20304a",
  contentDocumentDisplayHeader:'#4C8BF5',
  ChapterNameGold:'#F9BD33',
  countItemDocumentColor:'#FDE8B7',
  documentTypeContainer:'rgba(197, 216, 248, 0.16)',
  documentTypeColor:'#4C8BF5',
  circlePlusGreen:'#80C12C',
  veryDarkDesaturatedBlue: '#20304a',
  fullBlack:'#000',
  greenSave:'#80C12C',
  arrowCircleContentColor:'rgba(76, 139, 245, 0.53)'
};
