import React, {FunctionComponent, useState, useEffect} from 'react';

import {StyleSheet, Text, View, Dimensions, ScrollView} from 'react-native';
//utilities
import HeightWidth from '../../../../utility/HeightWidth';
import ButtonUI from '../../../../components/UI/Button';
import {Organization, User} from '../../../../common/contracts/user';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {connect} from 'react-redux';

interface Props {
  profile: Organization;
  navigation: any;
}

const OrgBusinessInformation: FunctionComponent<Props> = ({
  profile,
  navigation,
}) => {
  const [openModal, setOpenModal] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');

  interface BusinessInformationProps {
    user: Organization;
  }

  const BusinessInformation: FunctionComponent<BusinessInformationProps> = ({
    user,
  }) => {
    useEffect(() => {}, [user]);

    if (user.kycDetails && user.kycDetails.length > 0) {
      return (
        <View>
          <View>
            <Text style={styles.title}>Documents </Text>
            <Text style={styles.title}>{user.kycDetails.length}</Text>

            <View>
              {profile.kycDetails.map((document, index) => (
                <Text>{document.kycDocType}</Text>
              ))}
            </View>
          </View>
        </View>
      );
    }

    return (
      <View>
        <Text>No documents added.</Text>
      </View>
    );
  };

  return (
    <ScrollView style={styles.form}>
      <View style={{flexDirection: 'row', padding: 3, alignItems: 'center'}}>
        <View style={{flex: 0.8}}>
          <View
            style={{
              flexDirection: 'row',
              padding: 3,
              alignItems: 'center',
            }}>
            <MaterialCommunityIcons
              name="book-account-outline"
              color={'grey'}
              size={50}
            />
            <Text
              style={{
                color: '#0000cd',
                fontWeight: 'bold',
                fontSize: 25,
                textAlign: 'center',
                marginVertical: 12,
              }}>
              Business Info
            </Text>
          </View>
          <Text style={styles.title3}>
            View &amp; Edit Your Business Details
          </Text>
        </View>
        <View style={{flex: 0.2}}>
          <ButtonUI
            title="Edit"
            width={80}
            backgroundColor="#4C8BF5"
            onPress={() => {
              navigation.navigate('EditBusinessInfo');
            }}
            borderRadius={5}
            textStyles={{
              color: 'white',
              fontSize: HeightWidth.getResFontSize(18),
              fontWeight: '400',
            }}
          />
        </View>
      </View>

      <View>
        <View style={{borderWidth: 3, borderColor: 'red', borderRadius: 8}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 20,
            }}>
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  padding: 3,
                  alignItems: 'center',
                }}>
                <MaterialCommunityIcons
                  name="rename-box"
                  color={'grey'}
                  size={25}
                />
                <Text style={styles.title2}>Business Type</Text>
              </View>
              <Text style={styles.title3}>
                {profile.businessType ? profile.businessType : '-'}
              </Text>
            </View>
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  padding: 3,
                  alignItems: 'center',
                }}>
                <MaterialCommunityIcons
                  name="rename-box"
                  color={'grey'}
                  size={25}
                />
                <Text style={styles.title2}>Date of Incorporation</Text>
              </View>
              <Text style={styles.title3}>
                {profile.dob ? profile.dob : '-'}
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 20,
            }}>
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  padding: 3,
                  alignItems: 'center',
                }}>
                <MaterialCommunityIcons
                  name="card-account-mail"
                  color={'grey'}
                  size={25}
                />
                <Text style={styles.title2}>Business PAN</Text>
              </View>
              <Text style={styles.title3}>
                {profile.businessPAN ? profile.businessPAN : '-'}
              </Text>
            </View>
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  padding: 3,
                  alignItems: 'center',
                }}>
                <MaterialCommunityIcons
                  name="cellphone"
                  color={'grey'}
                  size={25}
                />
                <Text style={styles.title2}>Business Name</Text>
              </View>
              <Text style={styles.title3}>
                {profile.businessName ? profile.businessName : '-'}
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 20,
            }}>
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  padding: 3,
                  alignItems: 'center',
                }}>
                <MaterialCommunityIcons
                  name="calendar"
                  color={'grey'}
                  size={25}
                />
                <Text style={styles.title2}>GSTIN</Text>
              </View>
              <Text style={styles.title3}>
                {profile.gstin ? profile.gstin : '-'}
              </Text>
            </View>
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  padding: 3,
                  alignItems: 'center',
                }}>
                <MaterialCommunityIcons
                  name="calendar"
                  color={'grey'}
                  size={25}
                />
                <Text style={styles.title2}>Owner PAN</Text>
              </View>
              <Text style={styles.title3}>
                {profile.ownerPAN ? profile.ownerPAN : '-'}
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 20,
            }}>
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  padding: 3,
                  alignItems: 'center',
                }}>
                <MaterialCommunityIcons
                  name="calendar"
                  color={'grey'}
                  size={25}
                />
                <Text style={styles.title2}>Owner Name</Text>
              </View>
              <Text style={styles.title3}>
                {profile.ownerName ? profile.ownerName : '-'}
              </Text>
            </View>
          </View>
        </View>
        <View style={{paddingVertical: HeightWidth.getResHeight(12)}}></View>

        <View
          style={{
            borderWidth: 3,
            borderColor: 'green',
            borderRadius: 8,
          }}>
          <BusinessInformation user={profile} />
        </View>
        <View style={{paddingVertical: HeightWidth.getResHeight(20)}}></View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('screen').width,
    padding: 14,
  },
  input: {
    padding: 10,
    margin: 10,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 12,
    color: 'black',
    fontSize: 18,
    overflow: 'hidden',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 30,
    textAlign: 'center',
    marginVertical: 12,
  },
  title2: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    marginVertical: 8,
    padding: 4,
  },
  title3: {
    fontSize: 16,
    textAlign: 'center',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  footer: {
    flex: 0.2,
    marginVertical: 8,
    alignItems: 'center',
    marginBottom: 50,
  },
  body: {
    padding: 20,
    margin: 20,
    marginTop: 0,
    paddingTop: 0,
    flex: 0.8,
    marginVertical: 8,
    borderBottomColor: '#737373',
    width: Dimensions.get('screen').width * 1,
  },
  form: {
    borderRadius: 18,
    backgroundColor: 'white',
    padding: 18,
    textAlign: 'center',
    width: '100%',
  },
  logo: {
    width: 50,
    height: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
  link1: {
    color: 'blue',
    fontSize: 12,
  },
  link2: {
    color: 'blue',
    fontSize: 18,
    padding: 20,
  },
  text: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
    margin: 5,
    alignItems: 'center',
  },
  btn: {
    backgroundColor: '#1e90ff',
    borderRadius: 8,
    padding: 20,
    margin: 20,
    textAlign: 'center',
  },
  button: {
    backgroundColor: '#1e90ff',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  button2: {
    backgroundColor: '#1e90ff',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  img: {
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  pin: {
    color: 'blue',
  },
  input_role: {
    paddingBottom: 1,
    marginBottom: 10,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 18,
  },
  lnktxt: {
    fontSize: 20,
  },
  input_role_body: {},
});

const mapStateToProps = (state) => ({
  profile: state.auth.authUser as User,
});

export default connect(mapStateToProps)(OrgBusinessInformation);
