import React, {Component, useState, useEffect, FunctionComponent} from 'react';
import {
  Link,
  Route,
  Redirect,
  NativeRouter,
  RouteComponentProps,
} from 'react-router-native';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Icon} from 'react-native-elements';
import {connect, useDispatch} from 'react-redux';
//utilities
import {Colors} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';
import TextInputUI from '../../../../components/UI/TextInput';
import ButtonUI from '../../../../components/UI/Button';
import {Organization, User} from '../../../../common/contracts/user';
import {updateOrganization} from '../../../../common/api/organization';
import {setAuthUser} from '../../../auth/store/actions';

interface Props {
  profile: Organization;
  navigation: any;
}
const PersonalDetails: FunctionComponent<Props> = ({profile, navigation}) => {
  useEffect(() => {}, [profile]);

  //todo we need to dispatch profile photo from this component
  //! need to work om profile picture part once will get design
  const dispatch = useDispatch();

  return (
    <View>
      <SafeAreaView>
        {/* <SampleUI name={profile.OrganizationName} imagePath=''>
  <View>
  <Text style={styles.title}>
  Welcome, {profile.OrganizationName}!
  </Text>
  <ProfileImage
    profileUpdated={(profile) => dispatch(setAuthUser(profile))}
    profile={profile}
    name={profile.OrganizationName}
    />
  </View>
  </SampleUI>
     */}

        <View style={styles.__header}>
          <View>
            <View style={styles.__header}>
              <Icon
                name="person-outline"
                size={25}
                type="material"
                color={Colors.primaryMustard}
              />
              <Text style={styles.__title}>Personal Details</Text>
            </View>
          </View>

          <View style={styles.__header2}>
            <TouchableOpacity
              style={styles.__icon}
              onPress={() => {
                navigation.navigate('EditPersonalInfo');
              }}>
              <Icon
                name="pencil-circle"
                size={40}
                type="material-community"
                color={Colors.primaryBlue}
              />
            </TouchableOpacity>

            <Text style={styles.__title3}>
              View &amp; Edit Your Personal Details
            </Text>
          </View>
        </View>

        <ScrollView>
          <View style={styles.__box}>
            <View style={styles.__insideBox}>
              <View>
                <View style={styles.__insideBox2}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>Your Name</Text>
                </View>
                <Text style={styles.__title3}>{profile.organizationName}</Text>
              </View>
              <View>
                <View style={styles.__insideBox2}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>Email Address</Text>
                </View>
                <Text style={styles.__title3}>{profile.emailId}</Text>
              </View>
            </View>

            <View style={styles.__insideBox}>
              <View>
                <View style={styles.__insideBox2}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>Phone Number</Text>
                </View>
                <Text style={styles.__title3}>{profile.mobileNo}</Text>
              </View>
            </View>
          </View>

          <View style={styles.__box}>
            <View style={styles.__insideBox}>
              <View>
                <View style={styles.__insideBox2}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>Package</Text>
                </View>
                <Text style={styles.__title3}>
                  {profile.package && profile.package.name}
                </Text>
              </View>
            </View>
          </View>

          <View style={styles.__box}>
            <View style={styles.__insideBox}>
              <View>
                <View style={styles.__insideBox2}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>Address</Text>
                </View>
                <Text style={styles.__title3}>{profile.address}</Text>
              </View>
              <View>
                <View style={styles.__insideBox2}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>City</Text>
                </View>
                <Text style={styles.__title3}>{profile.city}</Text>
              </View>
            </View>
            <View style={styles.__insideBox}>
              <View>
                <View style={styles.__insideBox2}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>State</Text>
                </View>
                <Text style={styles.__title3}>{profile.stateName}</Text>
              </View>
              <View>
                <View style={styles.__insideBox2}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>Pin Code</Text>
                </View>
                <Text style={styles.__title3}>{profile.pinCode}</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  __title: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(16),
    textAlign: 'center',
    marginVertical: 12,
  },
  __title2: {
    color: Colors.veryDarkGray,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(12),
    textAlign: 'center',
    // marginVertical: 12,
  },
  __title3: {
    color: Colors.primaryGrey,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(12),
    textAlign: 'center',
    // marginVertical: 12,
  },
  __header: {
    flexDirection: 'row',
    padding: 3,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  __header2: {
    flexDirection: 'column',
    padding: 3,
    alignItems: 'center',
  },
  __icon: {
    alignSelf: 'flex-end',
  },
  __box: {
    borderWidth: 1,
    borderColor: Colors.pureCyanBlue,
    borderRadius: 5,
    marginVertical: HeightWidth.getResWidth(5),
    marginHorizontal: HeightWidth.getResHeight(5),
  },
  __insideBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
  },
  __insideBox2: {
    flexDirection: 'row',
    padding: 3,
    alignItems: 'center',
  },
});

const mapStateToProps = (state) => ({
  profile: state.auth.authUser as User,
});

export default connect(mapStateToProps)(PersonalDetails);
