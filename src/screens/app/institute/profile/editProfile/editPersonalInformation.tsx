import React, {useEffect, useState, FunctionComponent} from 'react';
import {View, Text, Dimensions, StyleSheet, SafeAreaView} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Button from '../../../../../components/UI/Button';
import TextInput from '../../../../../components/UI/TextInput';
import Picker from '../../../../../components/UI/Picker';
import {Organization, User} from '../../../../../common/contracts/user';
import {updateOrganization} from '../../../../../common/api/organization';
import {setAuthUser} from '../../../../auth/store/actions';

import {
  EMAIL_PATTERN,
  PIN_PATTERN,
} from '../../../../../common/validations/patterns';
import CustomStatusBar from '../../../../../components/Header/custom_status_bar';
import * as Animatable from 'react-native-animatable';
import {connect, useDispatch} from 'react-redux';

import {
  fetchQualificationsList,
  fetchCitySchoolsList,
  fetchCitiesByPinCode,
} from '../../../../../common/api/academics';

//utilities
import {
  width,
  Colors,
  height,
  images,
  strings,
} from '../../../../../utility/utilities';
import HeightWidth from '../../../../../utility/HeightWidth';

interface Props {
  user: Organization;
  //   saveUser: (data: Organization) => any;
  navigation: any;
}

const EditPersonalDetails: FunctionComponent<Props> = ({user, navigation}) => {
  const [name, setName] = useState(user.organizationName || '');
  const [email, setEmail] = useState(user.emailId || '');
  const [pinCode, setPinCode] = useState(user.pinCode || '');
  const [qualification, setQualification] = useState(0);
  const [school, setSchool] = useState(0);
  const [cityName, setCityName] = useState(user.city || 'City Name');
  const [stateName, setStateName] = useState(user.stateName || 'State Name');
  const [address, setAddress] = useState(user.address || '');

  // validation states
  const [nameError, setNameError] = useState('');
  const [pinError, setPinError] = useState('');
  const [emailError, setEmailError] = useState('');
  const [addressError, setAddressError] = useState('');
  const [schoolError, setSchoolError] = useState('');
  const [qualificationError, setQualificationError] = useState('');
  const [submitError, setSubmitError] = useState('');
  const dispatch = useDispatch();

  const savePersonalInformation = async (data: Organization) => {
    const User = Object.assign({}, user, data);
    try {
      dispatch(setAuthUser(User));
      await updateOrganization(User);
    } catch (error) {
      console.log('catching error', error);
      if (error.response?.status === 401) {
        navigation.navigate('Login');
      } else {
        // profileUpdated(profile);
      }
    }
  };

  // validation
  const validateName = (value) => {
    if (value.length > 4) {
      setNameError('');
    } else if (value.lenth == 0) {
      setNameError('Name field cannot be empty');
    } else {
      setNameError('Name must contains 5 characters');
    }
  };

  const validateAddress = (value) => {
    if (value.length > 4) {
      setAddressError('');
    } else if (value.lenth == 0) {
      setAddressError('Address field cannot be empty');
    } else {
      setAddressError('Address must contains 5 characters');
    }
  };

  const validateEmail = (value) => {
    if (EMAIL_PATTERN.test(value)) {
      setEmailError('');
    } else if (value.lenth == 0) {
      setEmailError('Email field cannot be empty');
    } else {
      setEmailError('Email is invalid');
    }
  };

  const validatePin = (value) => {
    if (PIN_PATTERN.test(value)) {
      setPinError('');
    } else if (value.lenth == 0) {
      setPinError('PIN field cannot be empty');
    } else {
      setPinError('Invalid PIN');
    }
  };

  const submitPersonalInformation = async () => {
    try {
      await savePersonalInformation({
        ...user,
        organizationName: name,
        emailId: email,
        address: address,
        city: cityName,
        pinCode: pinCode,
        stateName: stateName,
      });
    } catch (error) {
      alert(error);
    }
  };

  const onPinCodeChange = async (pin: string) => {
    setPinCode(pin);
    if (PIN_PATTERN.test(pin)) {
      try {
        const getCityArr = await fetchCitiesByPinCode({pinCode: pin});
        setCityName(getCityArr[0].cityName);
        setStateName(getCityArr[0].stateName);

        const schoolsListResponse = await fetchCitySchoolsList({
          cityName: getCityArr[0].cityName,
        });
      } catch (error) {
        if (error.response?.status === 401) {
          navigation.navigate('Login');
        } else {
          alert('Service not available in this area');
          setPinCode('');
        }
      }
    } else {
      setCityName('');
      setStateName('');
    }
  };
  const handelSubmit = () => {
    if (
      pinCode.length == 0 ||
      email.length == 0 ||
      name.length == 0 ||
      address.length == 0
    ) {
      setSubmitError('All fields are required field');
      setNameError('Name field cannot be empty');
      setEmailError('Email field cannot be empty');
      setPinError('PIN cannot be empty');
    } else if (
      emailError.length > 0 ||
      nameError.length > 0 ||
      pinError.length > 0 ||
      addressError.length > 0
    ) {
      setSubmitError('All fields are required field');
    } else {
      submitPersonalInformation();
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />
      <View style={styles.body}>
        <View style={styles.form}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <TextInput
                width="lg"
                style={styles.input}
                // label="Name"
                labelColor="#a1a1a1"
                placeholder="Enter Your Name"
                autoFocus={true}
                value={name}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setName(value);
                  validateName(value);
                }}
              />
              {nameError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>{nameError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <View
                style={{
                  paddingVertical: HeightWidth.getResHeight(4),
                  marginVertical: HeightWidth.getResHeight(4),
                }}></View>

              <TextInput
                width="lg"
                style={styles.input}
                // label="Email Address"
                labelColor="#a1a1a1"
                placeholder="Enter Your Email Address"
                value={email}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setEmail(value);
                  validateEmail(value);
                }}
              />
              {emailError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>{emailError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <TextInput
                width="lg"
                style={styles.input}
                // label="Email Address"
                labelColor="#a1a1a1"
                placeholder="Enter Your Address"
                value={address}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setAddress(value);
                  validateAddress(value);
                }}
              />
              {addressError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>
                    {addressError}
                  </Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <View
                style={{
                  paddingVertical: HeightWidth.getResHeight(4),
                  marginVertical: HeightWidth.getResHeight(4),
                }}></View>

              <TextInput
                width="lg"
                style={styles.input}
                // label="Email Address"
                labelColor="#a1a1a1"
                placeholder="PIN CODE"
                value={pinCode}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setPinCode(value);
                  validatePin(value);
                  if (value.length > 5) {
                    onPinCodeChange(value);
                  }
                  // validatePassword(value)
                }}
              />
              {pinError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>{pinError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}
              <View
                style={{
                  paddingVertical: HeightWidth.getResHeight(4),
                  marginVertical: HeightWidth.getResHeight(4),
                }}></View>

              <View
                style={{
                  paddingVertical: HeightWidth.getResHeight(4),
                  marginVertical: HeightWidth.getResHeight(4),
                }}></View>
              <TextInput
                width="lg"
                style={styles.input}
                labelColor="#a1a1a1"
                placeholder="City"
                value={cityName}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
              />
              <View
                style={{
                  paddingVertical: HeightWidth.getResHeight(4),
                  marginVertical: HeightWidth.getResHeight(4),
                }}></View>
              <TextInput
                width="lg"
                style={styles.input}
                labelColor="#a1a1a1"
                placeholder="State"
                value={stateName}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
              />
              <View
                style={{
                  paddingVertical: HeightWidth.getResHeight(4),
                  marginVertical: HeightWidth.getResHeight(4),
                }}></View>
              <Button
                backgroundColor={'#4C8BF5'}
                size="lg"
                borderRadius={5}
                title="Save"
                textStyles={{
                  fontSize: 18,
                  fontStyle: 'normal',
                  fontWeight: '400',
                  color: '#fff',
                }}
                onPress={() => {
                  const values = {
                    name: name,
                    email: email,
                    pinCode: pinCode,
                    qualification: qualification,
                    school: school,
                    city: cityName,
                    state: stateName,
                  };
                  handelSubmit();
                }}
              />
            </ScrollView>

            <View style={{padding: 50}}></View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginLeft: HeightWidth.getResWidth(2),
    marginRight: HeightWidth.getResWidth(2),
    height: '100%',
  },
  input: {
    padding: 10,
    margin: 10,
    fontSize: 12,
    overflow: 'hidden',
  },
  title: {
    fontWeight: '500',
    fontSize: HeightWidth.getResFontSize(24),
    textAlign: 'center',
    marginVertical: 8,
    paddingBottom: '5%',
  },
  title2: {
    fontWeight: '400',
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'left',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footer: {
    flex: 0.2,
    // marginVertical: 8,
    alignItems: 'center',
    // marginBottom: 50,
  },
  body: {},
  form: {
    borderRadius: 18,
    backgroundColor: 'white',
    //  padding:20,
    textAlign: 'center',
    //  paddingTop:"10%",
    //  marginTop:'10%'
  },
  logo: {
    width: 50,
    height: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
  link1: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(12),
    backgroundColor: 'white',
  },
  link2: {
    fontWeight: '400',
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(16),
    paddingTop: '5%',
    paddingBottom: '8%',
    marginLeft: '60%',
    borderRadius: 5,
    backgroundColor: 'white',
  },
  link3: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(18),
    paddingVertical: 10,
    backgroundColor: 'white',
    // paddingHorizontal: 20,
  },
  text: {
    fontSize: HeightWidth.getResFontSize(14),
    color: 'grey',
  },
  btn: {
    backgroundColor: '#f5c242',
    borderRadius: 8,
    padding: 20,
    margin: 20,
    textAlign: 'center',
  },
  button: {
    fontWeight: '400',
    backgroundColor: '#4C8BF5',
    borderRadius: 5,
    color: 'white',
    fontSize: HeightWidth.getResFontSize(18),
    overflow: 'hidden',
    padding: '5%',
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  img: {
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  linknav: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

const mapStateToProps = (state) => ({
  user: state.auth.authUser as User,
});

export default connect(mapStateToProps)(EditPersonalDetails);
