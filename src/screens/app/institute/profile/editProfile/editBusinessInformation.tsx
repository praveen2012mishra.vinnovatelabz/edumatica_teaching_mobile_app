import React, {useState, FunctionComponent} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ToastAndroid,
  PermissionsAndroid,
  TouchableOpacity,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Button from '../../../../../components/UI/Button';
import TextInput from '../../../../../components/UI/TextInput';
import Picker from '../../../../../components/UI/Picker';
import {Organization, User} from '../../../../../common/contracts/user';
import {updateOrganization} from '../../../../../common/api/organization';
import DatePicker from 'react-native-datepicker';
import {List} from 'react-native-paper';
import {KycDocument} from '../../../../../common/contracts/kyc_document';
import CustomStatusBar from '../../../../../components/Header/custom_status_bar';
import * as Animatable from 'react-native-animatable';
import {connect} from 'react-redux';
import {DocumentType} from '../../../../../common/enums/document_type';

//utilities
import {width, Colors} from '../../../../../utility/utilities';
import HeightWidth from '../../../../../utility/HeightWidth';
import {BusinessType} from '../../../../../common/enums/business_types';
import {
  IFSC_PATTERN,
  ACCOUNT_NO_PATTERN,
} from '../../../../../common/validations/patterns';
import CustomModel from '../../../../../components/Other/customModal';

import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {
  fetchUploadUrlForKycDocument,
  uploadKycDocument,
} from '../../../../../common/api/document';
interface Props {
  user: Organization;
  //   saveUser: (data: Organization) => any;
  navigation: any;
}
interface FormDataValue {
  uri: string;
  name: string;
  type: string;
}

const data = [
  {label: 'Private Limited', value: BusinessType.PVT_LTD},
  {label: 'Proprietorship', value: BusinessType.PROPRIETORSHIP},
  {label: 'Partnership', value: BusinessType.PARTNERSHIP},
  {label: 'Public Limited', value: BusinessType.PUBLIC_LTD},
  {label: 'LLP', value: BusinessType.LLP},
  {label: 'SOCIETY', value: BusinessType.SOCIETY},
  {label: 'Trust', value: BusinessType.TRUST},
  {label: 'NGO', value: BusinessType.NGO},
  {label: 'Not Registered', value: BusinessType.NOT_REG},
];

const EditBusinessInformation: FunctionComponent<Props> = ({user}) => {
  const [businessType, setBusinessType] = useState(user.businessType || '0');
  const [dob, setDob] = useState(user.dob || '09-10-2020');
  const [businessPAN, setBusinessPAN] = useState(user.businessPAN || '');
  const [businessName, setBusinessName] = useState(user.businessName || '');
  const [ownerPAN, setOwnerPAN] = useState(user.ownerPAN || '');
  const [ownerName, setOwnerName] = useState(user.ownerName || '');
  const [gstin, setGstin] = useState(user.gstin || '');
  const [documentType, setDocumentType] = useState(DocumentType.AADHAR);
  const [documentNumber, setDocumentNumber] = useState('');
  const [dropzoneKey, setDropzoneKey] = useState(0);
  // validation states
  const [formData2, setFormData] = useState(new FormData());
  const [ownerNameError, setOwnerNameError] = useState('');
  const [pinError, setPinError] = useState('');
  const [emailError, setEmailError] = useState('');
  const [businessTypeError, setBusinessTypeError] = useState('');
  const [businessPanError, setBusinessPanError] = useState('');
  const [businessNameError, setBusinessNameError] = useState('');
  const [ownerPanError, setOwnerPanError] = useState('');
  const [submitError, setSubmitError] = useState('');
  const [aadharExpanded, setAadharExpanded] = React.useState(false);
  const [bankExpanded, setBankExpanded] = React.useState(false);
  const [ownerPanExpanded, setOwnerPanExpanded] = React.useState(false);
  const [businessPanExpanded, setBusinessPanExpanded] = React.useState(false);
  const [gstExpanded, setGstExpanded] = React.useState(false);
  const [showSelectImageSourceModel, setShowSelectImageSourceModel] = useState(
    false,
  );

  const [bankIfsc, setBankIfsc] = useState(user.bankIfsc || '');
  const [currentBankDetails, setCurrentBankDetails] = useState({
    accountNo: '',
    ifsc: '',
  });
  const [documents, setDocuments] = useState<KycDocument[]>(
    user.kycDetails || [],
  );
  const [droppedFiles, setDroppedFiles] = useState<File[]>([]);
  const [aadhaar, setAadhaar] = useState(user.aadhaar || '');
  const [bankAccount, setBankAccount] = useState(user.bankAccount || '');

  const savePersonalInformation = async (data: Organization) => {
    const User = Object.assign({}, user, data);
    try {
      //   profileUpdated(user);
      await updateOrganization(User);
    } catch (error) {
      console.log('catching error', error);
      if (error.response?.status === 401) {
        alert("setRedirectTo('/login');");
      } else {
        // profileUpdated(profile);
      }
    }
  };

  // validation
  const validateBusinessType = (value) => {
    console.log(value);
    if (value == '0' || value == null) {
      setBusinessTypeError('Please select a valid business type');
    } else {
      setBusinessTypeError('');
    }
  };

  const validateBusinessPan = (value) => {
    if (value.lenth == 0 || value == '') {
      setBusinessPanError('Business PAN field cannot be empty');
    } else {
      setBusinessPanError('');
    }
  };
  const validateOwnerPan = (value) => {
    if (value.lenth == 0 || value == '') {
      setOwnerPanError('PAN field cannot be empty');
    } else {
      setOwnerPanError('');
    }
  };

  const validateBusinessName = (value) => {
    if (value.lenth == 0 || value == '') {
      setBusinessNameError('Business Name field cannot be empty');
    } else {
      setBusinessNameError('');
    }
  };
  const validateOwnerName = (value) => {
    if (value.lenth == 0 || value == '') {
      setOwnerNameError('Name field cannot be empty');
    } else {
      setOwnerNameError('');
    }
  };

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'App Camera Permission',
          message: 'App needs access to your camera ',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Camera permission given');
      } else {
        console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };
  const openCamera = () => {
    requestCameraPermission();
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: true,
        maxHeight: 2000,
        maxWidth: 2000,
      },
      (response) => {
        if (response.fileName != null || response.fileName != undefined) {
          // console.log("Image Capture Response---", response)
          setShowSelectImageSourceModel(false);
          // console.log(response.uri);
          // console.log(response.fileName);
          // console.log(response.base64);
          let localUri = response.uri;
          let filename = localUri.split('/').pop();
          let match = /\.(\w+)$/.exec(filename);
          let type = match ? `image/${match[1]}` : `image`;
          let formData = new FormData();
          formData.append('photo', {uri: localUri, name: filename, type});
          setFormData(formData);
          setDroppedFiles(localUri);
          // console.log(formData._parts[0][1]);
        } else {
          setShowSelectImageSourceModel(false);
          ToastAndroid.show('Cancelled Adding Image', ToastAndroid.LONG);
        }
      },
    );
  };
  const openImageLibrary = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: true,
        maxWidth: 2000,
        maxHeight: 2000,
      },
      (response) => {
        if (response.fileName != null || response.fileName != undefined) {
          setShowSelectImageSourceModel(false);
          let localUri = response.uri;
          let filename = localUri.split('/').pop();
          let match = /\.(\w+)$/.exec(filename);
          let type = match ? `image/${match[1]}` : `image`;
          let formData = new FormData();
          formData.append('photo', {uri: localUri, name: filename, type});
          setFormData(formData);
          setDroppedFiles(localUri);
          // console.log(formData._parts[0][1]);
        } else {
          setShowSelectImageSourceModel(false);
          ToastAndroid.show('Cancelled Adding Image', ToastAndroid.LONG);
        }
        // console.log("Image Response---", response)
      },
    );
  };

  const addDocument = async () => {
    console.log(droppedFiles);
    if (droppedFiles.length < 1) return;
    if (aadhaar !== '') {
    }
    if (ownerPAN !== '') {
    }
    if (documentType === DocumentType.BANK) {
      if (!ACCOUNT_NO_PATTERN.test(bankAccount)) {
        // alert(
        //   'bankAccount' + 'Invalid account number' + 'Invalid account number',
        // );
        // return;
      } else {
      }
      if (!IFSC_PATTERN.test(bankIfsc)) {
        // alert('bankIfsc' + 'Invalid IFSC code' + 'Invalid IFSC code');
        // return;
      } else {
      }
    }
    const file = formData2._parts[0][1];
    const clonedDocuments = [...documents];

    const documentIndex = clonedDocuments.findIndex(
      (document) =>
        document.kycDocType.toLowerCase() === documentType.toLowerCase(),
    );

    if (documentIndex > -1) {
      clonedDocuments.splice(documentIndex, 1);
    }

    setDocuments([
      ...clonedDocuments,
      {
        kycDocFormat: file.type,
        kycDocType: documentType,
        kycDocLocation: file.name,
      },
    ]);
    console.log(documents);
    setDroppedFiles([]);
    setDocumentNumber('');
    if (bankAccount && bankIfsc) {
      setCurrentBankDetails({accountNo: bankAccount, ifsc: bankIfsc});
    }
    setBankAccount('');
    setBankIfsc('');
    setDropzoneKey(dropzoneKey + 1);

    const formData = formData2;

    // formData.append('document', file);

    try {
      const awsBucket = await fetchUploadUrlForKycDocument({
        fileName: file.name,
        contentType: file.type,
        contentLength: file.size,
      });
      await uploadKycDocument(awsBucket.url, formData);
      console.log(documents.length);
      if (documents.length < 1) return;
      saveOtherInformation({
        ...user,
        kycDetails: documents,
        //   dob: dob.toString(),
        aadhaar: aadhaar ? aadhaar : undefined,
        ownerPAN: ownerPAN ? ownerPAN : undefined,
        bankAccount: currentBankDetails.accountNo
          ? currentBankDetails.accountNo
          : undefined,
        bankIfsc: currentBankDetails.ifsc ? currentBankDetails.ifsc : undefined,
      });
    } catch (error) {
      console.log('cathing here ', error);
      if (error.response?.status === 401) {
        //
      }
    }
  };

  const saveOtherInformation = async (data: Organization) => {
    const User = Object.assign({}, user, data);

    try {
      // profileUpdated(user);
      console.log(User);
      await updateOrganization(User);
      console.log('tutor updated locally');
    } catch (error) {
      console.log('error aa rha h tutor updatte karne me ', error);
      if (error.response?.status === 401) {
        // setRedirectTo('/login');
      } else {
        // profileUpdated(profile);
      }
    }
  };

  const submitPersonalInformation = async () => {
    try {
      await savePersonalInformation({
        ...user,
        businessType: businessType,
        dob: dob,
        businessPAN: businessPAN,
        businessName: businessName,
        ownerPAN: ownerPAN,
        ownerName: ownerName,
        gstin: gstin,
      });
    } catch (error) {
      alert(error);
    }
  };

  const handelSubmit = () => {
    if (
      businessPAN.length == 0 ||
      ownerName.length == 0 ||
      ownerPAN.length == 0 ||
      businessName.length == 0 ||
      businessType == '0' ||
      businessType == null
    ) {
      setSubmitError('All fields are required field');
      setBusinessTypeError('Please select a valid business type');
      setOwnerNameError('Owner name field cannot be empty');
      setBusinessNameError('Business name field cannot be empty');
      setOwnerPanError('Owner PAN field cannot be empty');
      setBusinessPanError('Business PAN field cannot be empty');
    } else {
      submitPersonalInformation();
    }
  };

  const BankDetails = () => {
    return (
      <View>
        <Text>Bank account details</Text>

        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            setDocumentType(DocumentType.BANK);
            setShowSelectImageSourceModel(true);
          }}>
          <View>
            <Text style={styles.__uploadText}>
              Upload cancelled cheque leaf
            </Text>
          </View>
        </TouchableOpacity>

        <TextInput
          width="lg"
          otherStyle={{
            borderBottomColor: Colors.verylightGrey,
            borderBottomWidth: 1,
          }}
          name="bankAccount"
          style={styles.input}
          placeholder="Your bank account number"
          // onChangeText={handleChange('bankAccount')}
          onChangeText={(item) => {
            // setTouchedBank(true);
            setBankAccount(item);
          }}
          value={bankAccount}
        />

        <TextInput
          width="lg"
          otherStyle={{
            borderBottomColor: Colors.verylightGrey,
            borderBottomWidth: 1,
          }}
          name="bankIfsc"
          style={styles.input}
          placeholder="Your bank IFSC code"
          onChangeText={(item) => {
            // setTouchedIfsc(true);
            setBankIfsc(item);
          }}
          value={bankIfsc}
        />

        <View style={styles.__smallButton}>
          <Button
            title="Add"
            width={80}
            backgroundColor="#4C8BF5"
            onPress={() => {
              addDocument();
              // setShowSelectImageSourceModel(true);
            }}
            borderRadius={5}
            textStyles={{
              color: 'white',
              fontSize: HeightWidth.getResFontSize(18),
              fontWeight: '400',
            }}
          />
        </View>
      </View>
    );
  };

  const AadharDetails = () => {
    return (
      <View>
        <Text>Addhar Details</Text>

        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            setDocumentType(DocumentType.AADHAR);
            setShowSelectImageSourceModel(true);
          }}>
          <View style={styles.__uploadText}>
            <Text style={styles.title3}> Upload Aadhar Card</Text>
          </View>
        </TouchableOpacity>

        <TextInput
          width="lg"
          otherStyle={{
            borderBottomColor: Colors.verylightGrey,
            borderBottomWidth: 1,
          }}
          name="aadhar"
          style={styles.input}
          placeholder={'Your aadhar Number'}
          onChangeText={(item) => {
            // setTouchedDoc(true);
            // handleBlur('documentNumber');
            setAadhaar(item);
          }}
          onBlur={() => {}}
          value={aadhaar}
        />

        <View style={styles.gap}></View>

        <View style={styles.__smallButton}>
          <Button
            title="Add"
            width={80}
            backgroundColor="#4C8BF5"
            onPress={() => {
              addDocument();
              // setShowSelectImageSourceModel(true);
            }}
            borderRadius={5}
            textStyles={{
              color: 'white',
              fontSize: HeightWidth.getResFontSize(18),
              fontWeight: '400',
            }}
          />
        </View>
      </View>
    );
  };

  const OwnerPanDetails = () => {
    return (
      <View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            setDocumentType(DocumentType.OWNER_PAN);
            setShowSelectImageSourceModel(true);
          }}>
          <View style={styles.__uploadText}>
            <Text style={styles.title3}> Owner PAN Card</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.__smallButton}>
          <Button
            title="Add"
            width={80}
            backgroundColor="#4C8BF5"
            onPress={() => {
              addDocument();
              // setShowSelectImageSourceModel(true);
            }}
            borderRadius={5}
            textStyles={{
              color: 'white',
              fontSize: HeightWidth.getResFontSize(18),
              fontWeight: '400',
            }}
          />
        </View>
      </View>
    );
  };

  const BusinessPanDetails = () => {
    return (
      <View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            setDocumentType(DocumentType.BUSINESS_PAN);
            setShowSelectImageSourceModel(true);
          }}>
          <View style={styles.__uploadText}>
            <Text style={styles.title3}> Business PAN Card</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.__smallButton}>
          <Button
            title="Add"
            width={80}
            backgroundColor="#4C8BF5"
            onPress={() => {
              addDocument();
              // setShowSelectImageSourceModel(true);
            }}
            borderRadius={5}
            textStyles={{
              color: 'white',
              fontSize: HeightWidth.getResFontSize(18),
              fontWeight: '400',
            }}
          />
        </View>
      </View>
    );
  };

  const GSTDetails = () => {
    return (
      <View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            setDocumentType(DocumentType.GST);
            setShowSelectImageSourceModel(true);
          }}>
          <View style={styles.__uploadText}>
            <Text style={styles.title3}> GST Certificate</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.__smallButton}>
          <Button
            title="Add"
            width={80}
            backgroundColor="#4C8BF5"
            onPress={() => {
              addDocument();
              // setShowSelectImageSourceModel(true);
            }}
            borderRadius={5}
            textStyles={{
              color: 'white',
              fontSize: HeightWidth.getResFontSize(18),
              fontWeight: '400',
            }}
          />
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />
      <View style={styles.body}>
        <View style={styles.form}>
          <View>
            <ScrollView>
              <View>
                <Text style={styles.title2}>Mandatory</Text>
              </View>

              <Picker
                label="Business Type"
                labelSize={16}
                labelColor={Colors.lightGrayColor}
                data={data}
                rightIconName=""
                rightIconSize=""
                rightIconColor=""
                value={businessType}
                docTypeHandler={async (item) => {
                  setBusinessType(item);
                  validateBusinessType(item);
                }}
                onRightIconPress=""
                showLabel={true}
              />
              {businessTypeError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={styles.__anim}>
                  <Text style={styles.error}>{businessTypeError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}
              <View style={styles.gap}></View>

              <Text style={styles.text}>Date Of Incorporation :</Text>
              <View>
                <DatePicker
                  style={styles.dateInput}
                  date={dob} // Initial date from state
                  mode="date" // The enum of date, datetime and time
                  placeholder="select date"
                  format="DD-MM-YYYY"
                  useNativeDriver="true"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  onDateChange={(dob) => {
                    setDob(dob);
                    console.log(dob);
                  }}
                />
              </View>
              <View style={styles.gap}></View>

              <TextInput
                width="lg"
                style={styles.input}
                // label="Name"
                labelColor="#a1a1a1"
                placeholder="PAN of the company"
                autoFocus={true}
                value={businessPAN}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setBusinessPAN(value);
                  console.log('value is =>', value);
                  validateBusinessPan(value);
                }}
              />
              {businessPanError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={styles.error}>{businessPanError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}
              <View style={styles.gap}></View>

              <TextInput
                width="lg"
                style={styles.input}
                // label="Name"
                labelColor="#a1a1a1"
                placeholder="Company Name as per PAN"
                autoFocus={true}
                value={businessName}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setBusinessName(value);
                  validateBusinessName(value);
                }}
              />
              {businessNameError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={styles.error}>{businessNameError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}
              <View style={styles.gap}></View>

              <TextInput
                width="lg"
                style={styles.input}
                // label="Name"
                labelColor="#a1a1a1"
                placeholder="PAN of one of the directors"
                autoFocus={true}
                value={ownerPAN}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setOwnerPAN(value);
                  validateOwnerPan(value);
                }}
              />
              {ownerPanError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={styles.error}>{ownerPanError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}
              <View style={styles.gap}></View>

              <TextInput
                width="lg"
                style={styles.input}
                // label="Name"
                labelColor="#a1a1a1"
                placeholder="Owner name as per PAN"
                autoFocus={true}
                value={ownerName}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setOwnerName(value);
                  validateOwnerName(value);
                }}
              />
              {ownerNameError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={styles.error}>{ownerNameError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <View style={styles.gap}></View>

              <TextInput
                width="lg"
                style={styles.input}
                // label="Name"
                labelColor="#a1a1a1"
                placeholder="Gst number"
                autoFocus={true}
                value={gstin}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setGstin(value);
                  // validateOwnerName(value);
                }}
              />
              {/* {ownerNameError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={styles.error}>
                    {ownerNameError}
                  </Text>
                </Animatable.View>
              ) : (
                <View></View>
              )} */}
              <View style={styles.gap}></View>

              <View>
                <Text style={styles.title2}>Optional</Text>
              </View>
              <View>
                <List.Section>
                  <List.Accordion
                    expanded={bankExpanded}
                    onPress={() => {
                      setBankExpanded(!bankExpanded);
                      setGstExpanded(false);
                      setBusinessPanExpanded(false);
                      setOwnerPanExpanded(false);
                      setAadharExpanded(false);
                    }}
                    children={<BankDetails />}
                    title="Bank Account Derails"></List.Accordion>
                </List.Section>
              </View>

              <View>
                <List.Section>
                  <List.Accordion
                    expanded={aadharExpanded}
                    onPress={() => {
                      setAadharExpanded(!aadharExpanded);
                      setGstExpanded(false);
                      setBusinessPanExpanded(false);
                      setOwnerPanExpanded(false);
                      setBankExpanded(false);
                    }}
                    children={<AadharDetails />}
                    title="AAdhar Derails"></List.Accordion>
                </List.Section>
              </View>

              <View>
                <List.Section>
                  <List.Accordion
                    expanded={ownerPanExpanded}
                    onPress={() => {
                      setOwnerPanExpanded(!ownerPanExpanded);
                      setGstExpanded(false);
                      setBusinessPanExpanded(false);
                      setBankExpanded(false);
                      setAadharExpanded(false);
                    }}
                    children={<OwnerPanDetails />}
                    title="Owner Pan Card"></List.Accordion>
                </List.Section>
              </View>

              <View>
                <List.Section>
                  <List.Accordion
                    expanded={businessPanExpanded}
                    onPress={() => {
                      setBusinessPanExpanded(!businessPanExpanded);
                      setGstExpanded(false);
                      setOwnerPanExpanded(false);
                      setBankExpanded(false);
                      setAadharExpanded(false);
                    }}
                    children={<BusinessPanDetails />}
                    title="Business Pan Card"></List.Accordion>
                </List.Section>
              </View>
              <View>
                <List.Section>
                  <List.Accordion
                    expanded={gstExpanded}
                    onPress={() => {
                      setGstExpanded(!gstExpanded);
                      setBusinessPanExpanded(false);
                      setOwnerPanExpanded(false);
                      setBankExpanded(false);
                      setAadharExpanded(false);
                    }}
                    children={<GSTDetails />}
                    title="GST Certificate"></List.Accordion>
                </List.Section>
              </View>
              <View style={styles.gap}></View>

              <Button
                backgroundColor={'#4C8BF5'}
                width={HeightWidth.getResWidth(375)}
                borderRadius={5}
                title="Save"
                textStyles={{
                  fontSize: 18,
                  fontStyle: 'normal',
                  fontWeight: '400',
                  color: '#fff',
                }}
                onPress={() => {
                  handelSubmit();
                }}
              />
            </ScrollView>
            <View style={styles.gap}></View>
          </View>
        </View>
      </View>

      <CustomModel
        isModalVisible={showSelectImageSourceModel}
        onBackButtonPress={() => {
          setShowSelectImageSourceModel(false);
        }}>
        <View>
          <Text>Select Image From</Text>
          <View>
            <Button
              title={'Camera'}
              borderRadius={50}
              backgroundColor={Colors.lightBlue}
              leftIconName={'camera'}
              leftIconSize={20}
              leftIconColor={Colors.white}
              elevation={10}
              textStyles={{
                fontSize: 16,
                color: Colors.white,
                fontFamily: 'Montserrat-Medium',
                textAlign: 'center',
              }}
              width={width / 2 - 50}
              height={50}
              onPress={() => {
                openCamera();
              }}
            />
            <Button
              title={'Image Library'}
              borderRadius={50}
              backgroundColor={Colors.lightBlue}
              leftIconName={'image'}
              leftIconSize={20}
              leftIconColor={Colors.white}
              elevation={10}
              textStyles={{
                fontSize: 16,
                color: Colors.white,
                fontFamily: 'Montserrat-Medium',
                textAlign: 'center',
              }}
              width={width / 2 - 30}
              height={50}
              onPress={() => {
                openImageLibrary();
              }}
            />
          </View>
        </View>
      </CustomModel>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginLeft: HeightWidth.getResWidth(2),
    marginRight: HeightWidth.getResWidth(2),
    height: '100%',
  },
  input: {
    padding: 10,
    margin: 10,
    fontSize: 12,
    overflow: 'hidden',
  },

  title2: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'center',
  },
  title3: {
    color: 'white',
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'center',
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footer: {
    flex: 0.2,
    // marginVertical: 8,
    alignItems: 'center',
    // marginBottom: 50,
  },
  body: {},
  form: {
    borderRadius: 18,
    backgroundColor: 'white',
    //  padding:20,
    textAlign: 'center',
    paddingBottom: HeightWidth.getResHeight(12),
  },
  logo: {
    width: 50,
    height: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
  link1: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(12),
    backgroundColor: 'white',
  },
  link2: {
    fontWeight: '400',
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(16),
    paddingTop: '5%',
    paddingBottom: '8%',
    marginLeft: '60%',
    borderRadius: 5,
    backgroundColor: 'white',
  },
  link3: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(18),
    paddingVertical: 10,
    backgroundColor: 'white',
    // paddingHorizontal: 20,
  },
  text: {
    fontSize: HeightWidth.getResFontSize(14),
    color: 'grey',
  },
  btn: {
    backgroundColor: '#f5c242',
    borderRadius: 8,
    padding: 20,
    margin: 20,
    textAlign: 'center',
  },
  button: {
    fontWeight: '400',
    backgroundColor: '#4C8BF5',
    borderRadius: 5,
    color: 'white',
    fontSize: HeightWidth.getResFontSize(18),
    overflow: 'hidden',
    padding: '5%',
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  img: {
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  linknav: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dateInput: {
    padding: 2,
    margin: 2,
    fontSize: 12,
    overflow: 'hidden',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    marginTop: '2%',
    width: '100%',
  },
  gap: {
    paddingVertical: HeightWidth.getResHeight(12),
  },
  error: {color: 'red', fontSize: 15},
  __smallButton: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingTop: HeightWidth.getResHeight(10),
  },
  __uploadText: {flexDirection: 'row', justifyContent: 'space-between'},
  __anim: {alignSelf: 'flex-start'},
});

const mapStateToProps = (state) => ({
  user: state.auth.authUser as User,
});

export default connect(mapStateToProps)(EditBusinessInformation);
