import {connect, useDispatch} from 'react-redux';
import React, {FunctionComponent, useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  ScrollView,
} from 'react-native';
import {
  Link,
  Route,
  Redirect,
  NativeRouter,
  Switch,
  RouteComponentProps,
  withRouter,
} from 'react-router-native';
import {Colors} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';
import TextInputUI from '../../../../components/UI/TextInput';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {DataTable} from 'react-native-paper';
import ButtonUI from '../../../../components/UI/Button';
import {Organization, User} from '../../../../common/contracts/user';
import {updateOrganization} from '../../../../common/api/organization';

interface BoardClassSubjectsMap {
  boardname: string;
  classname: string;
  subjects: string[];
}

interface Props {
  profile: Organization;
  navigation: any;
}

const CourseDetails: FunctionComponent<Props> = ({profile, navigation}) => {
  const [openModal, setOpenModal] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const saveSubjectInformation = async (data: Organization) => {
    const user = Object.assign({}, profile, data);

    try {
      await updateOrganization(user);
      // profileUpdated(user);
    } catch (error) {
      if (error.response?.status === 401) {
        setRedirectTo('/login');
      } else {
        //profileUpdated(profile);
      }
    }
  };

  useEffect(() => {}, [profile]);

  const OrganizationSubjects = [...profile.courseDetails];

  const boardClassSubjectsMap: BoardClassSubjectsMap[] = [];

  for (let i = 0; i < OrganizationSubjects.length; ++i) {
    const subject = OrganizationSubjects[i];

    let boardClassSubjectIndex = boardClassSubjectsMap.findIndex(
      (boardClassSubject) =>
        boardClassSubject.boardname.toLowerCase() ===
          subject.board.toLowerCase() &&
        boardClassSubject.classname.toString().toLowerCase() ===
          subject.className.toString().toLowerCase(),
    );

    if (boardClassSubjectIndex === -1) {
      boardClassSubjectsMap.push({
        boardname: subject.board,
        classname: subject.className,
        subjects: [],
      });

      boardClassSubjectIndex = boardClassSubjectsMap.length - 1;
    }

    boardClassSubjectsMap[boardClassSubjectIndex].subjects.push(
      subject.subject,
    );
  }

  const boards = Array.from(
    new Set(OrganizationSubjects.map((subject) => subject.board)),
  );
  return (
    // <View profile={profile as Organization}>
    <ScrollView style={styles.form}>
      <View style={{flexDirection: 'row', padding: 3, alignItems: 'center'}}>
        <View style={{flex: 0.8}}>
          <View
            style={{flexDirection: 'row', padding: 5, alignItems: 'center'}}>
            <MaterialCommunityIcons
              name="book-open-page-variant"
              color={'grey'}
              size={50}
            />
            <Text
              style={{
                color: '#0000cd',
                fontWeight: 'bold',
                fontSize: 25,
                textAlign: 'center',
                marginVertical: 12,
                paddingLeft: '20%',
              }}>
              Course Details
            </Text>
          </View>
          <Text style={styles.title3}>
            View &amp; Edit Your Personal &amp; Contact Details
          </Text>
        </View>
      </View>
      <View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingBottom: HeightWidth.getResHeight(100),
          }}>
          <Text style={styles.title2}>Courses</Text>

          <ButtonUI
            title="Add"
            width={80}
            backgroundColor="#4C8BF5"
            onPress={() => {
              navigation.navigate('EditCourseInfo');
            }}
            borderRadius={5}
            textStyles={{
              color: 'white',
              fontSize: HeightWidth.getResFontSize(18),
              fontWeight: '400',
            }}
          />
        </View>

        {boards &&
          boards.length > 0 &&
          boards.map((board) => (
            <View>
              <Text style={styles.title}>{board}</Text>

              <View>
                <DataTable>
                  <DataTable.Header>
                    <DataTable.Title>
                      {' '}
                      <Text style={styles.title2}>Classes</Text>{' '}
                    </DataTable.Title>
                    <DataTable.Title>
                      <Text style={styles.title2}>Subjects</Text>
                    </DataTable.Title>
                  </DataTable.Header>

                  {boardClassSubjectsMap
                    .filter(
                      (boardClassSubjectMap) =>
                        boardClassSubjectMap.boardname === board,
                    )
                    .map((boardClassSubjectMap) => (
                      <DataTable.Row>
                        <DataTable.Title>
                          {boardClassSubjectMap.classname}
                        </DataTable.Title>
                        <DataTable.Title>
                          {boardClassSubjectMap.subjects.join(', ')}
                        </DataTable.Title>
                      </DataTable.Row>
                    ))}
                </DataTable>
              </View>
            </View>
          ))}
      </View>
      <View style={{height: '100%'}}></View>
      {/* <OrganizationSubjectInformationModal
                openModal={openModal}
                onClose={() => setOpenModal(false)}
                saveUser={saveSubjectInformation}
                user={profile as Organization}
              /> */}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('screen').width,
    padding: 14,
  },
  input: {
    padding: 10,
    margin: 10,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 12,
    color: 'black',
    fontSize: 18,
    overflow: 'hidden',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 30,
    textAlign: 'center',
    marginVertical: 12,
  },
  title2: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    marginVertical: 8,
    padding: 4,
  },
  title3: {
    fontSize: 16,
    textAlign: 'center',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  footer: {
    flex: 0.2,
    marginVertical: 8,
    alignItems: 'center',
    marginBottom: 50,
  },
  body: {
    padding: 20,
    margin: 20,
    marginTop: 0,
    paddingTop: 0,
    flex: 0.8,
    marginVertical: 8,
    borderBottomColor: '#737373',
    width: Dimensions.get('screen').width * 1,
  },
  form: {
    borderRadius: 18,
    backgroundColor: 'white',
    padding: 8,
    textAlign: 'center',
  },
  logo: {
    width: 50,
    height: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
  link1: {
    color: 'blue',
    fontSize: 12,
  },
  link2: {
    color: 'blue',
    fontSize: 18,
    padding: 20,
  },
  text: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
    margin: 5,
    alignItems: 'center',
  },
  btn: {
    backgroundColor: '#1e90ff',
    borderRadius: 8,
    padding: 20,
    margin: 20,
    textAlign: 'center',
  },
  button: {
    backgroundColor: '#1e90ff',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  button2: {
    backgroundColor: '#1e90ff',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  img: {
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  pin: {
    color: 'blue',
  },
  input_role: {
    paddingBottom: 1,
    marginBottom: 10,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 18,
  },
  lnktxt: {
    fontSize: 20,
  },
  input_role_body: {},
});

const mapStateToProps = (state) => ({
  profile: state.auth.authUser as User,
});

export default connect(mapStateToProps)(CourseDetails);
