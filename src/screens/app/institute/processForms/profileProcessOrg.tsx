import React, {FunctionComponent, useState, useEffect} from 'react';
import {connect, useDispatch} from 'react-redux';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import OrgPersonalInformation from '../components/orgPersonalInformation';
import OrgCourseInformation from '../components/orgCourseInformation';
import OrgBusinessInformation from '../components/orgBusinessInformation';
import Icon from 'react-native-vector-icons/Feather';
import Header from '../components/profile_details_header';
import {Organization, User} from '../../../../common/contracts/user';
import {updateOrganization} from '../../../../common/api/organization';
import {setAuthUser} from '../../../auth/store/actions';

//utilities
import {
  width,
  Colors,
  height,
  images,
  strings,
} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';
import AsyncStorage from '@react-native-community/async-storage';
interface Props {
  navigation: any;
  authUser: User;
}

const ProfileDetailScreen: FunctionComponent<Props> = ({
  navigation,
  authUser,
}) => {
  // const [authUser,setAuthUser] = useState(AuthUser)
  const dispatch = useDispatch();
  const MAX_STEPS = 3;
  const [profile, setProfile] = useState(authUser);
  const [redirectTo, setRedirectTo] = useState('');
  const [step, setstep] = useState(1);

  const nextButtonHandler = () => {
    if (step == 1 || step == 2) {
      setstep(step + 1);
    } else {
      alert(step);
      // navigation.navigate('IntroSlider')
    }
  };

  const backButtonHandler = () => {
    console.log('back click', step);
    if (step == 2 || step == 3) {
      setstep(step - 1);
    }
  };

  const saveUser = async (data: Organization) => {
    setProfile(data);
    console.log('profile', profile);
    if (step < MAX_STEPS) {
      return;
    }
    try {
      console.log(data);
      // AsyncStorage.setItem('authUser', JSON.stringify(data));
      dispatch(setAuthUser(data));
      await updateOrganization(data);

      console.log('success');
      navigation.navigate('appStackOrg');
      // history.push(`/profile/${authUser.mobileNo}/dashboard`);
    } catch (error) {
      console.log(error);
      if (error.response?.status === 401) {
        // setRedirectTo(`/`);
      }
    }
  };

  return (
    <View style={styles.contianer}>
      <View style={{paddingTop: '15%'}}>
        {step === 1 ? (
          <Header
            title={'CONTACT DETAILS'}
            iconName={'information-outline'}
            iconBackName={''}
            onBack={''}
          />
        ) : (
          <View></View>
        )}
        {step === 2 ? (
          <Header
            title={'COURSE DETAILS'}
            iconName={'bank'}
            iconBackName={'chevron-left'}
            onBack={backButtonHandler}
          />
        ) : (
          // <Text style={styles.title}>COURSE DETAILS</Text>
          <View></View>
        )}
        {step === 3 ? (
          <Header
            title={'OTHERS'}
            iconName={'shield-search'}
            iconBackName={'chevron-left'}
            onBack={backButtonHandler}
          />
        ) : (
          <View></View>
        )}
      </View>
      <View
        style={{
          paddingTop: '5%',
          justifyContent: 'space-between',
          flexDirection: 'row',
        }}>
        {step === 1 ? (
          <View style={styles.activeBar}></View>
        ) : (
          <View style={styles.inActiveBar}></View>
        )}
        {step === 2 ? (
          <View style={styles.activeBar}></View>
        ) : (
          <View style={styles.inActiveBar}></View>
        )}
        {step === 3 ? (
          <View style={styles.activeBar}></View>
        ) : (
          <View style={styles.inActiveBar}></View>
        )}
      </View>
      <View style={{marginTop: 0, flex: 2}}>
        {step === 1 && (
          <OrgPersonalInformation
            user={profile as Organization}
            nextButtonHandler={nextButtonHandler}
            saveUser={saveUser}
          />
        )}
        {step === 2 && (
          <OrgCourseInformation
            user={profile as Organization}
            nextButtonHandler={nextButtonHandler}
            saveUser={saveUser}
          />
        )}
        {step === 3 && (
          <OrgBusinessInformation
            user={profile as Organization}
            nextButtonHandler={nextButtonHandler}
            saveUser={saveUser}
          />
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  contianer: {
    flex: 1,
    backgroundColor: Colors.fullWhite,
  },

  title: {
    fontWeight: '400',
    fontSize: 18,
    textAlign: 'center',
    marginVertical: 8,
    color: Colors.darkColor,
  },
  activeBar: {
    borderBottomColor: '#F9BD33',
    borderBottomWidth: 5,
    flex: 0.33,
  },
  inActiveBar: {
    borderBottomColor: 'grey',
    borderBottomWidth: 5,
    flex: 0.33,
  },
});
import {authReducer} from '../../../auth/store/reducers';

const mapStateToProps = (state) => ({
  authUser: state.auth.authUser as User,
});

export default connect(mapStateToProps)(ProfileDetailScreen);
