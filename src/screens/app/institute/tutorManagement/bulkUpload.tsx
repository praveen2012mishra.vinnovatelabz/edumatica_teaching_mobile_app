import React, { FunctionComponent, useState } from 'react'
import { StyleSheet, Text, View, SafeAreaView, ToastAndroid, TouchableOpacity, PermissionsAndroid, ActivityIndicator } from 'react-native'
import XLSX from 'xlsx';
import Icon from 'react-native-vector-icons/Feather';
import RNFS from 'react-native-fs';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob'


//utilities
import { Colors, } from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';
import { addTutorsForOrganization } from '../../../../common/api/organization';
import { Tutor } from '../../../../common/contracts/user';
import { EMAIL_PATTERN, PHONE_PATTERN, PIN_PATTERN, NAME_PATTERN } from '../../../../common/validations/patterns';
import { GET_TUTORS_TEMPLATES } from '../../../../common/api/routes';

interface Props {
    route: any,
    navigation: any
}

interface FileProps {
    uri: string;
    type: string;
    name: string;
    size: number;
}

const BulkUpload: FunctionComponent<Props> = ({ route, navigation }) => {

    const [tutors, setTutors] = useState<Tutor[]>([]);
    const [fileName, setFileName] = useState('');
    const [loading, setLoading] = useState(false);

    const downloadTemplate = async () => {
        let dirs = RNFetchBlob.fs.dirs;
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
            title: "Storage",
            message: "This app would like to store some files on your phone",
            buttonPositive: "ok"
        }).then(async () => {
            setLoading(true);
            await RNFetchBlob
                .config({
                    fileCache: true,
                    addAndroidDownloads: {
                        notification: true,
                        useDownloadManager: true,
                        title: 'tutor.xlsx',
                        description: 'Tutor Enrollment Template',
                        mime: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        mediaScannable: true,
                        path: dirs.DownloadDir + '/tutor.xlsx',
                    }
                })
                .fetch('GET', GET_TUTORS_TEMPLATES)
                .then((res) => {
                    console.log('The file saved to ', res.path())
                    setLoading(false)
                    ToastAndroid.show("Template Downloaded successfully", ToastAndroid.LONG);
                }).catch((error) => {
                    ToastAndroid.show("Error Downloading" + error, ToastAndroid.LONG);
                    setLoading(false)
                    console.log("error" + error)
                })
        })
    }

    const uploadFile = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.xlsx],
            });
            console.log(
                res.uri,
                res.type,
                res.name,
                res.size
            );
            await readExcel(res);
            setFileName(res.name)
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                ToastAndroid.show("Cancelled Picking Document", ToastAndroid.LONG)
            } else {
                throw err;
            }
        }
    }

    const isUniqueMobile = (mobile: string[]) => {
        let mobileNo,
            unique = true;
        mobile.sort().sort((a, b) => {
            if (a === b) {
                mobileNo = a;
                unique = false;
            }
            return 0;
        });
        return { mobileNo: mobileNo, isUnique: unique };
    };

    const validateTutors = (structuredTutors: Tutor[]) => {
        let uploadError = '';

        const tutorMobile = isUniqueMobile(
            structuredTutors.map((tutor) => tutor.mobileNo)
        );
        if (!tutorMobile.isUnique) {
            uploadError = `Tutor mobile number "${tutorMobile.mobileNo}" must be unique`;
        }

        structuredTutors &&
            structuredTutors.every((tutor) => {
                if (!uploadError) {
                    if (!tutor.tutorName) {
                        uploadError = `Tutor name cannot be empty`;
                        return false;
                    }
                    if (tutor.tutorName.length < 5) {
                        uploadError = `Tutor name "${tutor.tutorName}" cannot be less than 5 character`;
                        return false;
                    }

                    if (!NAME_PATTERN.test(tutor.tutorName)) {
                        uploadError = `Invalid tutor name "${tutor.tutorName}"`;
                        return false;
                    }

                    if (!tutor.mobileNo) {
                        uploadError = `Mobile number cannot be empty`;
                        return false;
                    }
                    if (!tutor.mobileNo.toString().match(PHONE_PATTERN)) {
                        uploadError = `Invalid mobile number "${tutor.mobileNo}"`;
                        return false;
                    }

                    if (!tutor.emailId) {
                        uploadError = 'Email id cannot be empty';
                        return false;
                    }
                    if (!EMAIL_PATTERN.test(tutor.emailId.toLowerCase())) {
                        uploadError = 'Invalid Email';
                        return false;
                    }

                    if (tutor.qualifications == null) {
                        uploadError = 'Qualification cannot be empty';
                        return false;
                    }
                    if (tutor.qualifications.length < 3) {
                        uploadError = 'Qualification must be minimum 3 characters long';
                        return false;
                    }

                    if (!tutor.pinCode) {
                        uploadError = 'Pin Code cannot be empty';
                        return false;
                    }

                    if (!PIN_PATTERN.test(tutor.pinCode.toString())) {
                        uploadError = 'Invalid Pin Code';
                        return false;
                    }

                    if (!tutor.cityName) {
                        uploadError = 'City cannot be empty';
                        return false;
                    }

                    if (!tutor.stateName) {
                        uploadError = 'State cannot be empty';
                        return false;
                    }

                    if (tutor.schoolName == null) {
                        uploadError = 'School cannot be empty';
                        return false;
                    }

                    if (
                        !tutor.courseDetails.every((course, index) => {
                            if (course.board && course.className && course.subject) {
                                if (
                                    course.board.length >= 4 &&
                                    course.className.length >= 7 &&
                                    course.subject.length > 3
                                )
                                    return true;
                            }
                            uploadError = `Invalid Course Detail for ${tutor.tutorName} `;
                            return false;
                        })
                    ) {
                        return false;
                    }

                    const filteredTutors = tutors.filter(function (el) {
                        return el.mobileNo === tutor.mobileNo;
                    });

                    if (filteredTutors.length > 0) {
                        uploadError = `Mobile number "${tutor.mobileNo}" already used`;
                        return false;
                    }
                }
                return true;
            });
        if (uploadError === '') {
            setTutors([...tutors, ...structuredTutors]);
        } else {
            setFileName('')
            ToastAndroid.show(uploadError, ToastAndroid.SHORT);
        }
    };

    const readExcel = (file: FileProps | null) => {
        RNFS.readFile(file.uri, 'ascii').then(async (res) => {
            const workbook = XLSX.read(res, { type: 'binary' });
            const wsname = workbook.SheetNames[0];
            const ws = workbook.Sheets[wsname];
            const tutorArr = XLSX.utils.sheet_to_json(ws);

            let structuredTutors: Tutor[] = await Promise.all(tutorArr.map(async (tutor: any) => {
                const getCourseDetails = () => {
                    const courseDetailsArr =
                        tutor.Course_Details && tutor.Course_Details.split(' | ');
                    const courseDetails = courseDetailsArr.map((course: any) => {
                        const arr = course.split(', ');
                        return {
                            board: arr[0],
                            className: arr[1],
                            subject: arr[2]
                        };
                    });
                    return courseDetails;
                };
                let tutorObj = {
                    mobileNo: tutor.Mobile_No,
                    enrollmentId: tutor.Enrollment_Id,
                    emailId: tutor.Email_Id,
                    qualifications: tutor.Qualifications,
                    schoolName: tutor.School_Name,
                    tutorName: tutor.Tutor_Name,
                    pinCode: tutor.Pin_Code,
                    cityName: tutor.City,
                    stateName: tutor.State,
                    courseDetails: getCourseDetails()
                }
                return tutorObj;
            }))
            validateTutors(structuredTutors);
        });
    };

    const submitTutorList = async () => {
        setLoading(true)
        try {
            await addTutorsForOrganization(tutors);
            setLoading(false)
            navigation.navigate('tutorManagement')
        } catch (error) {
            console.log("Error" + error)
            setLoading(false)
            if (error.response?.data.message) {
                console.log("error", error.response?.data.message)
                ToastAndroid.show('Error Fetching Data', ToastAndroid.LONG)
            }
        }
    }

    return (
        <SafeAreaView style={styles.__container}>
            <TouchableOpacity onPress={() => { downloadTemplate() }} style={styles.__templateButton}>
                <Text style={styles.__templateText}>Download Template</Text>
            </TouchableOpacity>
            <View style={styles.__innerContent}>
                <Text style={styles.__innerContentTitle}>Upload Tutor List</Text>
                <Text style={styles.__innerContentNotice}>Note: Upload file as per the given format (Template)</Text>
                <TouchableOpacity
                    onPress={() => { fileName != '' && tutors.length != 0 ? submitTutorList() : uploadFile() }} style={[styles.__uploadButton,
                    { backgroundColor: fileName === '' ? Colors.primaryBlue : Colors.verySoftRed }]}>
                    {!loading ? <>
                        {fileName === '' ?
                            <Icon name="upload" size={26} color={Colors.fullWhite} />
                            : null}
                    </> :
                        <ActivityIndicator size="small" color={Colors.fullWhite} />
                    }
                    <Text style={styles.__uploadText}>{fileName === '' ? "Upload" : "Submit"}</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    __container: {
        flex: 1
    },
    __templateButton: {
        borderRadius: 40,
        paddingHorizontal: HeightWidth.getResWidth(20),
        paddingVertical: HeightWidth.getResWidth(10),
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        backgroundColor: Colors.primaryBlue,
        marginVertical: HeightWidth.getResWidth(30),
    },
    __templateText: {
        fontSize: 17,
        color: Colors.fullWhite,
    },
    __uploadButton: {
        borderRadius: 20,
        padding: HeightWidth.getResWidth(30),
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        marginVertical: HeightWidth.getResWidth(50),
        backgroundColor: Colors.primaryBlue,
    },
    __uploadText: {
        fontSize: 17,
        color: Colors.fullWhite,
    },
    __innerContent: {
        padding: HeightWidth.getResWidth(15),
        borderRadius: 10,
        marginVertical: HeightWidth.getResWidth(15),
        marginHorizontal: HeightWidth.getResWidth(10),
        backgroundColor: Colors.fullWhite
    },
    __innerContentTitle: {
        alignSelf: "center",
        fontSize: 18,
        color: Colors.primaryBlue,
        marginVertical: HeightWidth.getResWidth(10)
    },
    __innerContentNotice: {
        alignSelf: "center",
        textAlign: "center",
        fontSize: 14,
        color: Colors.primaryRed
    },
})

export default BulkUpload;
