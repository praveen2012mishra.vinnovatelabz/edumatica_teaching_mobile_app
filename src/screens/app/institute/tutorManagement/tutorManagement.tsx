import React, { FunctionComponent, useState, useEffect, useCallback } from 'react'

import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, FlatList, ActivityIndicator, ToastAndroid, Alert } from 'react-native'
import Icon from 'react-native-vector-icons/Feather';
import AsyncStorage from '@react-native-community/async-storage';

//components
import BaseHeader from '../../../../components/Header/BaseHeader';
import CustomStatusBar from '../../../../components/Header/custom_status_bar';

//utilities
import { width, Colors, height, images, strings } from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';
import { fetchOrgTutorsList, deleteOrganizationTutors } from '../../../../common/api/organization';

interface Props {
    navigation: any;
    route: any
}

interface TutorData {
    item: {
        __v: number;
        _id: string;
        cityName: string;
        mobileNo: string;
        ownerId: string;
        pinCode: string;
        schoolName: string;
        stateName: string;
        status: number;
        updatedby: string;
        updatedon: string;
        qualifications: any;
        studentList: any;
        batchList: any;
        scheduleList: any;
        tutorName: string;
        emailId: string;
        courseDetails: any;
        kycDetails: any;
    }
}

const TutorManagement: FunctionComponent<Props> = ({ navigation, route }) => {
    const [tutorList, setTutorList] = useState([]);
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getTutorList();
        });
        return unsubscribe;

    }, [])

    const getTutorList = async () => {
        setLoading(true);
        let teacherList = [];
        try {
            teacherList = await fetchOrgTutorsList();
            console.log("Tutor data-----------", teacherList)
            setTutorList(teacherList);
            setLoading(false);
        } catch (error) {
            ToastAndroid.show('Error Fetching Data', ToastAndroid.LONG)
        }
    }

    const deleteTutor = async (deleteId: string) => {
        try {
            await deleteOrganizationTutors([deleteId]);
            getTutorList();
        } catch (error) {
            if (error.response?.status === 401) {
                ToastAndroid.show("Login Again", ToastAndroid.LONG)
            }
            if (error.response?.status === 422) {
                ToastAndroid.show("Error invalid input", ToastAndroid.LONG)
            }
        }

    }

    const renderItem = ({ item }: TutorData) => {
        return (
            <View key={item._id} style={styles.__cardContainer}>
                <View style={styles.__leftCardContent}>
                    <Text style={styles.__cardTitle}>
                        {item.tutorName}
                    </Text>
                    <View style={styles.__cardTopContent}>
                        <Text style={styles.__cardTopTitle}>School Name:</Text>
                        <Text style={styles.__cardTopData}>{item.schoolName}</Text>
                    </View>
                    <View style={styles.__cardMiddleContent}>
                        <Text style={styles.__cardMiddleLeftTitle}>Email Id:</Text>
                        <Text style={styles.__cardMiddleLeftData}>{item.emailId}</Text>
                    </View>
                    <View style={styles.__cardBottomContent}>
                        <View style={styles.__leftCardBottomContent}>
                            <Text style={styles.__cardBottomContentTitle}>PhoneNo:</Text>
                            <Text style={styles.__cardBottomContentData}>{item.mobileNo}</Text>
                        </View>
                        <View style={styles.__rightBottomCardContent}>
                            <TouchableOpacity
                                onPress={() => { navigation.navigate('tutorEnterDetails', { formType: "edit", currentTutor: item }) }}
                                style={[styles.__rightBottomCardContentButton, { backgroundColor: Colors.primaryBlueOpacity }]}>
                                <Icon name="edit-2" size={16} color={Colors.fullWhite} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {
                                Alert.alert(
                                    "Delete",
                                    "Are you sure you want to Delete this Tutor?",
                                    [
                                        { text: "OK", onPress: () => { deleteTutor(item.mobileNo) } },
                                        {
                                            text: "Cancel",
                                            onPress: () => console.log("Cancel Pressed"),
                                            style: "cancel"
                                        },
                                    ],
                                    { cancelable: true }
                                );
                            }} style={[styles.__rightBottomCardContentButton, { backgroundColor: Colors.verySoftRed }]}>
                                <Icon name="trash" size={16} color={Colors.fullWhite} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.__container}>
            <CustomStatusBar
                backgroundColor={Colors.primaryBlue}
                barStyle="light-content"
            />
            <BaseHeader
                onPress={() => {
                    navigation.goBack();
                }}
                title={'EDUMATICA'}
            />
            {!loading ? <View style={styles.__container}>
                {tutorList.length != 0 ? <FlatList
                    keyExtractor={(_: any, index: number) => index.toString()}
                    renderItem={renderItem}
                    data={tutorList}
                    style={styles.__flatList}
                /> : <Text style={styles.__noContentText}>
                    No Data Present
                    </Text>}
            </View> :
                <View style={styles.__activityIndicator}>
                    <ActivityIndicator size={"large"} color={Colors.primaryBlue} />
                </View>
            }
            <TouchableOpacity onPress={() => { navigation.navigate('AddTutor') }} style={styles.__bottomAddButton}>
                <Icon name={"plus"} size={35} color={Colors.fullWhite} />
            </TouchableOpacity>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    __container: {
        flex: 1
    },
    __cardContainer: {
        marginHorizontal: HeightWidth.getResWidth(10),
        borderRadius: 10,
        marginVertical: HeightWidth.getResWidth(4),
        padding: HeightWidth.getResWidth(6),
        elevation: 4,
        backgroundColor: Colors.fullWhite,
        flexDirection: "row",
    },
    __leftCardContent: {
        flex: 1,
    },
    __rightBottomCardContent: {
        paddingHorizontal: HeightWidth.getResWidth(5),
        alignItems: "center",
        justifyContent: 'space-around',
        flexDirection: "row",
        marginLeft: HeightWidth.getResWidth(10),
    },
    __cardTitle: {
        fontSize: 18,
        color: Colors.primaryBlue
    },
    __cardTopContent: {
        flexDirection: "row",
        alignItems: "center",
    },
    __cardTopTitle: {
        fontSize: 16,
        color: Colors.primaryBlue
    },
    __cardTopData: {
        fontSize: 16,
        color: Colors.veryGrayishBlack,
        marginLeft: HeightWidth.getResWidth(10)
    },
    __cardMiddleContent: {
        flexDirection: "row",
        alignItems: "center",
        marginVertical: HeightWidth.getResWidth(4)
    },
    __cardMiddleLeftContent: {
        flexDirection: "row",
        // justifyContent: "center",
        alignItems: "center",
    },
    __cardMiddleLeftTitle: {
        fontSize: 16,
        color: Colors.veryGrayishBlack
    },
    __cardMiddleLeftData: {
        fontSize: 16,
        color: Colors.veryGrayishBlack,
        marginLeft: HeightWidth.getResWidth(10)
    },
    __cardMiddleRightContent: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    __cardMiddleRightTitle: {
        fontSize: 16,
        color: Colors.veryGrayishBlack
    },
    __cardMiddleRightData: {
        fontSize: 16,
        color: Colors.veryGrayishBlack,
        marginLeft: HeightWidth.getResWidth(10)
    },
    __cardBottomContent: {
        flexDirection: "row",
        alignItems: "center",
        marginVertical: HeightWidth.getResWidth(4)
    },
    __leftCardBottomContent: {
        flexDirection: "row",
        alignItems: "center",
    },
    __cardBottomContentTitle: {
        fontSize: 16,
        color: Colors.veryGrayishBlack
    },
    __cardBottomContentData: {
        fontSize: 16,
        color: Colors.veryGrayishBlack,
        marginLeft: HeightWidth.getResWidth(10)
    },
    __bottomAddButton: {
        bottom: 0,
        right: HeightWidth.getResWidth(15),
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        backgroundColor: Colors.primaryBlue,
        elevation: 10,
        marginBottom: HeightWidth.getResWidth(15),
        width: HeightWidth.getResWidth(50),
        height: HeightWidth.getResWidth(50),
        borderRadius: 30,
    },
    __activityIndicator: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    __rightBottomCardContentButton: {
        borderRadius: 30,
        width: HeightWidth.getResWidth(30),
        height: HeightWidth.getResWidth(30),
        marginHorizontal: HeightWidth.getResWidth(10),
        justifyContent: "center",
        alignItems: "center",
    },
    __noContentText: {
        fontSize: 20,
        color: Colors.veryGrayishBlack,
        alignSelf: "center",
        marginVertical: HeightWidth.getResWidth(40)
    },
    __flatList: {
        marginBottom: HeightWidth.getResWidth(15)
    }
})

export default TutorManagement;


