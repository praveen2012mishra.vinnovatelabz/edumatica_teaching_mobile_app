import React, { FunctionComponent, useState, useEffect } from 'react'
import { StyleSheet, Text, View, SafeAreaView, ToastAndroid, ActivityIndicator } from 'react-native'

//components
import TextInputUI from '../../../../components/UI/TextInput';
import BaseHeader from '../../../../components/Header/BaseHeader';
import CustomStatusBar from '../../../../components/Header/custom_status_bar';
import CustomScrollView from '../../../../components/Other/CustomScrollView';
import Picker from '../../../../components/UI/Picker';
import ButtonUI from '../../../../components/UI/Button';


//utilities
import { width, Colors } from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';
import { fetchCitiesByPinCode, fetchCitySchoolsList, fetchQualificationsList } from '../../../../common/api/academics';
import { addTutorsForOrganization, updateOrgTutor } from '../../../../common/api/organization';
import { PHONE_PATTERN, EMAIL_PATTERN, PIN_PATTERN } from '../../../../common/validations/patterns';
import CustomPicker from '../../../../components/UI/CustomPicker/CustomPicker';

interface Props {
    route: any,
    navigation: any
}

const EnterTutorDetails: FunctionComponent<Props> = ({ route, navigation }) => {
    let { formType } = route.params;
    const [tutorName, setTutorName] = useState('');
    const [enrollmentId, setEnrollmentId] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [emailId, setEmailId] = useState('');
    const [pinCode, setPinCode] = useState('');
    const [cityName, setCityName] = useState('');
    const [stateName, setStateName] = useState('');
    const [schoolName, setSchoolName] = useState('');
    const [citySchoolList, setCitySchoolList] = useState([]);
    const [qualifications, setQualifications] = useState('');
    const [qualificationList, setQualificationList] = useState([]);
    const [showInput, setShowInput] = useState(false);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const unsubscribe = navigation.addListener('blur', () => {
            setTutorName('')
            setEnrollmentId('')
            setMobileNo('')
            setEmailId('')
            setPinCode('')
            setCityName('')
            setStateName('')
            setSchoolName('')
            setCitySchoolList([])
            setQualifications('')
            setQualificationList([])
        });
        return unsubscribe;
    }, [navigation]);

    if (formType === 'add') {
        useEffect(() => {
            getQualification();
        }, [])
    }

    if (formType === 'edit') {
        let { currentTutor } = route.params;
        useEffect(() => {
            setLoading(true);
            (async () => {
                try {
                    await getQualification()
                    await getCitiesFromPINCode(currentTutor.pinCode)
                    await getSchools(currentTutor.cityName)
                    Promise.all([
                        setTutorName(currentTutor.tutorName || ''),
                        setEnrollmentId(currentTutor.enrollmentId || ''),
                        setMobileNo(currentTutor.mobileNo || ''),
                        setSchoolName(currentTutor.schoolName || ''),
                        setEmailId(currentTutor.emailId || ''),
                        setPinCode(currentTutor.pinCode || ''),
                        setCityName(currentTutor.cityName || ''),
                        setStateName(currentTutor.stateName || ''),
                        setQualifications(currentTutor.qualifications[0] || ''),
                    ])
                    console.log("qualification", currentTutor.qualifications[0])
                    setLoading(false);

                } catch (error) {
                    if (error.response?.status === 401) {
                        ToastAndroid.show("Login Again", ToastAndroid.LONG);
                        setLoading(false);
                    }
                }
            })();
        }, [currentTutor]);
    }

    const getCitiesFromPINCode = async (pinCode: string) => {
        try {
            const getStateCity = await fetchCitiesByPinCode({ pinCode });
            setCityName(getStateCity[0].cityName);
            setStateName(getStateCity[0].stateName);
            getSchools(getStateCity[0].cityName)
        } catch (error) {
            ToastAndroid.show('Error Fetching Data', ToastAndroid.LONG)

        }
    }

    const getSchools = async (city: string) => {
        try {
            const [getCitySchoolList] = await Promise.all([fetchCitySchoolsList({ cityName: city })]);
            const structuredSchoolsList = getCitySchoolList.map((school) => ({
                label: `${school.schoolName} (${school.schoolAddress})`,
                value: school.schoolName,
            }));
            const schoolListData = [
                { label: 'other', value: 'other' },
                ...structuredSchoolsList
            ]
            setCitySchoolList(schoolListData)
        } catch (error) {
            ToastAndroid.show('Error Fetching Data', ToastAndroid.LONG)

        }
    }

    const getQualification = async () => {
        try {
            const [getQualificationList] = await Promise.all([fetchQualificationsList()]);
            const structuredQualifList = getQualificationList.map((qualification) => ({
                label: `${qualification.degree} (${qualification.subjectName})`,
                value: qualification.degree,
            }));
            await setQualificationList(structuredQualifList)
        } catch (error) {
            if (error.response?.data.message) {
                ToastAndroid.show('Error Fetching Data', ToastAndroid.LONG)
            }
        }
    }

    const postTutorDetails = async () => {
        if (tutorName === '') {
            ToastAndroid.show("Please Enter Tutor Name", ToastAndroid.LONG)
            return;
        } else if (tutorName.length < 5) {
            ToastAndroid.show("Tutor name cannot be less than 5 character", ToastAndroid.LONG)
            return;
        }
        if (!PHONE_PATTERN.test(mobileNo)) {
            ToastAndroid.show("Please Enter Mobile No", ToastAndroid.LONG)
            return;
        } else if (mobileNo.length == 0) {
            ToastAndroid.show('mobile Number field cannot be empty', ToastAndroid.LONG)
            return;
        }
        if (!EMAIL_PATTERN.test(emailId.trim())) {
            ToastAndroid.show("Please Enter EmailId", ToastAndroid.LONG)
            return;
        } else if (emailId.length === 0) {
            ToastAndroid.show('Please Enter EmailId', ToastAndroid.LONG)
            return;
        }
        if (qualifications === '') {
            ToastAndroid.show("Board is not selected", ToastAndroid.LONG)
            return;
        }
        if (!PIN_PATTERN.test(pinCode)) {
            ToastAndroid.show("PIN Code is Invalid", ToastAndroid.LONG)
            return;
        } else if (pinCode.length === 0) {
            ToastAndroid.show("Please Enter PIN Code", ToastAndroid.LONG)
            return;
        }
        if (stateName === '') {
            ToastAndroid.show("State is not selected", ToastAndroid.LONG)
            return;
        }
        if (cityName === '') {
            ToastAndroid.show("City is not selected", ToastAndroid.LONG)
            return;
        }
        if (schoolName === '') {
            ToastAndroid.show("School is not selected", ToastAndroid.LONG)
            return;
        }

        let tutorDetails = {
            tutorName,
            mobileNo,
            emailId,
            qualifications: [qualifications],
            pinCode,
            stateName,
            cityName,
            schoolName,
            courseDetails: []
        };

        let structuredTutorDetails = []
        let structuredObjectTutorDetails = {};

        if (enrollmentId === '') {
            if (formType === 'add') {
                structuredTutorDetails = [
                    { ...tutorDetails, }
                ]
            } else {
                structuredObjectTutorDetails = {
                    ...tutorDetails
                }
            }
        } else {
            if (formType === 'add') {
                structuredTutorDetails = [
                    { ...tutorDetails, enrollmentId }
                ]
            } else {
                structuredObjectTutorDetails = {
                    ...tutorDetails,
                    enrollmentId
                }
            }
        }

        try {
            if (formType === 'edit') {
                await updateOrgTutor(structuredObjectTutorDetails);
            } else {
                await addTutorsForOrganization(structuredTutorDetails);
            }
            navigation.navigate('tutorManagement');
        } catch (error) {
            console.log("Error" + error)
            if (error.response?.data.message) {
                console.log("error", error.response?.data.message)
                ToastAndroid.show(error.response?.data.message, ToastAndroid.LONG)
            }
        }
    }

    return (
        <SafeAreaView style={styles.__container}>
            {formType === 'edit' ? <>
                <CustomStatusBar
                    backgroundColor={Colors.primaryBlue}
                    barStyle="light-content"
                />
                <BaseHeader
                    onPress={() => {
                        navigation.goBack();
                    }}
                    title={'EDUMATICA'}
                />
            </> : null}
            {!loading ? <CustomScrollView
                nestedScrollEnabled={true}
            >
                <View style={styles.__innerContent}>
                    <Text style={styles.__contentHeader}>Enroll Tutor</Text>
                    <View style={styles.__textInput}>
                        <TextInputUI
                            label={"Tutor Name:"}
                            labelStyle={styles.__inputLabelStyle}
                            width="lg"
                            style={styles.__textInput}
                            placeholder="Enter Tutor Name"
                            onChangeText={(text) => { setTutorName(text) }}
                            value={tutorName}
                            otherStyle={styles.__inputContainerStyle}
                        />
                    </View>
                    <View style={styles.__textInput}>
                        <TextInputUI
                            label={"Enrollment ID:"}
                            labelStyle={styles.__inputLabelStyle}
                            width="lg"
                            style={styles.__textInput}
                            placeholder="Enter Enrollment ID"
                            onChangeText={(text) => { setEnrollmentId(text) }}
                            value={enrollmentId}
                            otherStyle={styles.__inputContainerStyle}
                        />
                    </View>
                    <View style={styles.__textInput}>
                        <TextInputUI
                            label={"Tutor Mobile No:"}
                            labelStyle={styles.__inputLabelStyle}
                            width="lg"
                            style={styles.__textInput}
                            placeholder="Enter Tutor Mobile No"
                            onChangeText={(text) => { setMobileNo(text) }}
                            value={mobileNo}
                            maxLength={10}
                            keyboardType={'numeric'}
                            otherStyle={styles.__inputContainerStyle}
                        />
                    </View>
                    <View style={styles.__textInput}>
                        <TextInputUI
                            label={"Email Address"}
                            labelStyle={styles.__inputLabelStyle}
                            width="lg"
                            style={styles.__textInput}
                            placeholder="Enter Email Address"
                            onChangeText={(text) => { setEmailId(text.trim()) }}
                            value={emailId}
                            otherStyle={styles.__inputContainerStyle}
                        />
                    </View>
                    <View style={styles.__customDropDown}>
                        <CustomPicker
                            data={qualificationList}
                            label="Qualifications"
                            labelSize={16}
                            labelColor={Colors.lightGrayColor}
                            value={qualifications}
                            Width={HeightWidth.getResWidth(width - 60)}
                            otherStyle={styles.__inputContainerStyle}
                            placeholder={"Enter Qualification"}
                            selectedOnChange={(item) => { setQualifications(item) }}
                        />
                    </View>
                    <View style={styles.__textInput}>
                        <TextInputUI
                            label={"PIN Code"}
                            labelStyle={styles.__inputLabelStyle}
                            width="lg"
                            style={styles.__textInput}
                            placeholder="Enter PIN Code"
                            onChangeText={(text) => {
                                setPinCode(text);
                                text.length > 5 ?
                                    getCitiesFromPINCode(text) :
                                    null
                            }}
                            value={pinCode}
                            maxLength={6}
                            keyboardType={'numeric'}
                            otherStyle={styles.__inputContainerStyle}
                        />
                    </View>
                    <View style={styles.__textContent}>
                        <View style={styles.__textLeftContent}>
                            <Text style={styles.__textHeader}>City</Text>
                            <Text style={styles.__textData}>{cityName === '' ? "None" : cityName}</Text>
                        </View>
                        <View style={styles.__divider} />
                        <View style={styles.__textRightContent}>
                            <Text style={styles.__textHeader}>State</Text>
                            <Text style={styles.__textData}>{stateName === '' ? "None" : stateName}</Text>
                        </View>
                    </View>
                    <View style={styles.__customDropDown}>
                        <CustomPicker
                            data={citySchoolList}
                            label="School Name"
                            labelSize={16}
                            labelColor={Colors.lightGrayColor}
                            value={schoolName}
                            Width={HeightWidth.getResWidth(width - 60)}
                            otherStyle={styles.__inputContainerStyle}
                            placeholder={"Enter School Name"}
                            selectedOnChange={(item) => { setSchoolName(item) }}
                            maxHeight={HeightWidth.getResWidth(90)}
                        />
                    </View>
                    <View style={styles.__inputButton}>
                        <ButtonUI
                            title={"Submit"}
                            borderRadius={10}
                            backgroundColor={Colors.primaryBlue}
                            elevation={0}
                            textStyles={styles.__buttonTextStyle}
                            width={width / 2 - 70}
                            height={50}
                            onPress={() => { postTutorDetails() }}
                        />
                    </View>
                </View>
            </CustomScrollView> :
                <SafeAreaView style={styles.__activityIndicator}>
                    <ActivityIndicator size={"large"} color={Colors.primaryBlue} />
                </SafeAreaView>
            }
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    __container: {
        flex: 1
    },
    __innerContent: {
        marginHorizontal: HeightWidth.getResWidth(10),
        marginVertical: HeightWidth.getResWidth(15),
        borderRadius: 10,
        backgroundColor: Colors.fullWhite,
        padding: HeightWidth.getResWidth(10)
    },
    __contentHeader: {
        alignSelf: "center",
        fontWeight: "bold",
        fontSize: 18,
        color: Colors.veryDarkDesaturatedBlue
    },
    __textInput: {
        marginTop: HeightWidth.getResWidth(18),
        alignSelf: "center"
    },
    __textContent: {
        marginTop: HeightWidth.getResWidth(10),
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around",
        backgroundColor: Colors.primaryBlueOpacity,
        paddingVertical: HeightWidth.getResWidth(8),
        borderRadius: 10
    },
    __textLeftContent: {
        justifyContent: "space-around",
        alignItems: "center",
        marginHorizontal: HeightWidth.getResWidth(2),
    },
    __textRightContent: {
        justifyContent: "space-around",
        alignItems: "center",
        marginHorizontal: HeightWidth.getResWidth(2),
    },
    __textHeader: {
        fontSize: 16,
        color: Colors.veryDarkDesaturatedBlue,
        marginBottom: HeightWidth.getResWidth(8)
    },
    __textData: {
        fontSize: 17,
        color: Colors.veryDarkDesaturatedBlue
    },
    __divider: {
        marginVertical: HeightWidth.getResWidth(2),
        width: HeightWidth.getResWidth(1),
        backgroundColor: Colors.fullWhite,
        height: HeightWidth.getResWidth(28)
    },
    __dropDown: {
        marginTop: HeightWidth.getResWidth(8),
        alignSelf: "center"
    },
    __customDropDown: {
        marginTop: HeightWidth.getResWidth(20),
        alignSelf: "center"
    },
    __inputButton: {
        marginVertical: HeightWidth.getResWidth(20),
        alignSelf: "center",
        justifyContent: "center",
        alignItems: "center"
    },
    __activityIndicator: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1
    },
    __inputLabelStyle: {
        marginLeft: HeightWidth.getResWidth(6),
        color: Colors.veryDarkDesaturatedBlue
    },
    __inputContainerStyle: {
        borderBottomColor: Colors.primaryBlue,
        borderBottomWidth: 1,
    },
    __pickerContainerStyle: {
        marginHorizontal: HeightWidth.getResWidth(4)
    },
    __buttonTextStyle: {
        fontSize: 16,
        color: Colors.fullWhite
    }
})

export default EnterTutorDetails;
