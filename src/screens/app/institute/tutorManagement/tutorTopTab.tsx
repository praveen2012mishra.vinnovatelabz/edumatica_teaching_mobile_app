import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'

// Screens
import EnterTutorDetails from './enterTutorDetails';
import BulkUpload from './bulkUpload';

const TutorTopTabs = createMaterialTopTabNavigator();

export default function TutorTopTab() {
    return (
        <TutorTopTabs.Navigator initialRouteName={"tutorEnterDetails"}>
            <TutorTopTabs.Screen name="tutorEnterDetails" options={{ title: 'Enter Details' }} component={EnterTutorDetails} initialParams={{ formType: "add" }} />
            <TutorTopTabs.Screen name="tutorBulkUpload" options={{ title: 'Bulk Upload' }} component={BulkUpload} />
        </TutorTopTabs.Navigator>
    )
}

const styles = StyleSheet.create({})
