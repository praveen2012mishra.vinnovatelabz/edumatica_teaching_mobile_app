import React, { FunctionComponent, useState, useEffect } from 'react'
import { StyleSheet, Text, View, SafeAreaView } from 'react-native'

//components
import BaseHeader from '../../../../components/Header/BaseHeader';
import CustomStatusBar from '../../../../components/Header/custom_status_bar';

import TutorTopTab from './tutorTopTab';


//utilities
import { Colors} from '../../../../utility/utilities';

interface Props {
    route: any,
    navigation: any
}

const AddTutor: FunctionComponent<Props> = ({ route, navigation }) => {
    return (
        <SafeAreaView style={styles.__container}>
            <CustomStatusBar
                backgroundColor={Colors.primaryBlue}
                barStyle="light-content"
            />
            <BaseHeader
                onPress={() => {
                    navigation.goBack();
                }}
                title={'EDUMATICA'}
            />
            <TutorTopTab />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    __container: {
        flex: 1
    },
})

export default AddTutor;