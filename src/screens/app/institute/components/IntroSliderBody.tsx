import React, {FunctionComponent} from 'react';
import {Text, StyleSheet, View, Image, Dimensions} from 'react-native';
import PropTypes from 'prop-types';
import AppIntroSlider from 'react-native-app-intro-slider';

//Utilities
import {
  width,
  Colors,
  height,
  images,
  strings,
} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';

//Components

const slides = [
  {
    key: '1',
    title: 'Add student',
    text: 'Invite student though notification',
    image: images.slide1,
    backgroundColor: '#fff',
  },
  {
    key: '2',
    title: 'Create batch',
    text: 'Make batch based on course and subject',
    image: images.slide2,
    backgroundColor: '#fff',
  },
  {
    key: '3',
    title: 'Take Virtual class',
    text: 'Take class from anywhere and anytime',
    image: images.slide3,
    backgroundColor: '#fff',
  },
  {
    key: '4',
    title: 'Upload Course Content',
    text: 'Upload study materials for students',
    image: images.slide4,
    backgroundColor: '#fff',
  },
];

const IntroSliderBody: FunctionComponent = ({}) => {
  const [showRealApp, setShowRealApp] = React.useState(false);

  const RenderItem = ({item}) => {
    return (
      <View key={item.key} style={styles.__bodyContainer}>
        <Text numberOfLines={1} style={styles.__titleStyle}>
          {item.title}
        </Text>
        <Image style={styles.__introImageStyle} source={item.image} />
        <Text style={styles.__description}>{item.text}</Text>
      </View>
    );
  };

  const renderDoneButton = () => {
    return (
      <View style={styles.__inputButton}>
        <Text style={styles.__inputButtonText}>Get Started</Text>
      </View>
    );
  };

  const onDone = () => {};

  const renderNextButton = () => {
    return (
      <View style={styles.__inputButton}>
        <Text style={styles.__inputButtonText}>Next</Text>
      </View>
    );
  };

  return (
    <View style={styles.__appIntroContainer}>
      <AppIntroSlider
        data={slides}
        renderItem={RenderItem}
        onDone={onDone}
        activeDotStyle={styles.__activeDotStyle}
        renderDoneButton={renderDoneButton}
        bottomButton={true}
        renderNextButton={renderNextButton}
      />
    </View>
  );
};

IntroSliderBody.propTypes = {
  title: PropTypes.string,
};

IntroSliderBody.defaultProps = {
  title: null,
};

const styles = StyleSheet.create({
  __appIntroContainer: {
    flex: 1,
  },
  __bodyContainer: {
    backgroundColor: Colors.fullWhite,
    width: width,
  },
  __titleStyle: {
    alignSelf: 'center',
    fontWeight: '300',
    fontSize: 24,
    color: Colors.pureCyanBlue,
    marginVertical: HeightWidth.getResWidth(26),
    textTransform: 'uppercase',
  },
  __description: {
    fontSize: 16,
    fontWeight: 'normal',
    color: Colors.softBlue,
    textAlign: 'center',
    marginTop: 25,
    marginBottom: 10,
  },
  __introImageStyle: {
    resizeMode: 'contain',
    alignSelf: 'center',
    height: HeightWidth.getResWidth(260),
  },
  __inputButton: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    backgroundColor: Colors.primaryBlue,
    width: HeightWidth.getResWidth(width - 80),
    paddingVertical: HeightWidth.getResWidth(10),
  },
  __inputButtonText: {
    fontSize: 18,
    color: Colors.fullWhite,
  },
  __activeDotStyle: {
    backgroundColor: Colors.primaryBlue,
  },
});

export default IntroSliderBody;
