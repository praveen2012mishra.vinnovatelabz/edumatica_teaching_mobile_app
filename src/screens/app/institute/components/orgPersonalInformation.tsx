import React, {useEffect, useState, FunctionComponent} from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Button from '../../../../components/UI/Button';
import TextInput from '../../../../components/UI/TextInput';
import Picker from '../../../../components/UI/Picker';
import {Organization} from '../../../../common/contracts/user';
import {
  EMAIL_PATTERN,
  PIN_PATTERN,
} from '../../../../common/validations/patterns';
import CustomStatusBar from '../../../../components/Header/custom_status_bar';
import * as Animatable from 'react-native-animatable';

import {
  fetchQualificationsList,
  fetchCitySchoolsList,
  fetchCitiesByPinCode,
} from '../../../../common/api/academics';

//utilities
import {Colors} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';

interface Props {
  user: Organization;
  nextButtonHandler: any;
  saveUser: (data: Organization) => any;
}

const OrgPersonalInformation: FunctionComponent<Props> = ({
  user,
  saveUser,
  nextButtonHandler,
}) => {
  const [orgName, setOrgName] = useState('');
  const [email, setEmail] = useState('');
  const [pin, setPin] = useState('');
  const [address, setAddress] = useState('');
  const [school, setSchool] = useState(0);
  const [pinCode, setPinCode] = useState('');
  const [cityName, setCityName] = useState('City Name');
  const [stateName, setStateName] = useState('State Name');
  // validation states
  const [nameError, setNameError] = useState('');
  const [pinError, setPinError] = useState('');
  const [emailError, setEmailError] = useState('');
  const [addressError, setAddressError] = useState('');
  const [submitError, setSubmitError] = useState('');

  useEffect(() => {
    (async () => {
      try {
      } catch (error) {
        if (error.response?.status === 401) {
          alert("setRedirectTo('/')");
        }
      }
    })();
  }, []);

  // validation
  const validateName = (value) => {
    if (value.length > 4) {
      setNameError('');
    } else if (value.lenth == 0) {
      setNameError('Name field cannot be empty');
    } else {
      setNameError('Name must contains 5 characters');
    }
  };

  const validateEmail = (value) => {
    if (EMAIL_PATTERN.test(value)) {
      setEmailError('');
    } else if (value.lenth == 0) {
      setEmailError('Email field cannot be empty');
    } else {
      setEmailError('Email is invalid');
    }
  };

  const validateAddress = (value) => {
    if (value.lenth == 0) {
      setAddressError('Address field cannot be empty');
    } else {
      setAddressError('');
    }
  };

  const validatePin = (value) => {
    if (PIN_PATTERN.test(value)) {
      setPinError('');
    } else if (value.lenth == 0) {
      setPinError('PIN field cannot be empty');
    } else {
      setPinError('Invalid PIN');
    }
  };

  const submitPersonalInformation = async (values) => {
    try {
      await saveUser({
        ...user,
        organizationName: orgName,
        emailId: email,
        address: address,
        city: cityName,
        pinCode: pinCode,
        stateName: stateName,
      });
    } catch (error) {
      alert(error);
    }
  };

  const onPinCodeChange = async (pin: string) => {
    setPinCode(pin);
    if (PIN_PATTERN.test(pin)) {
      try {
        const getCityArr = await fetchCitiesByPinCode({pinCode: pin});
        setCityName(getCityArr[0].cityName);
        setStateName(getCityArr[0].stateName);

        const schoolsListResponse = await fetchCitySchoolsList({
          cityName: getCityArr[0].cityName,
        });

        const structuredSchoolsList = schoolsListResponse.map((school) => ({
          label: `${school.schoolName} (${school.schoolAddress})`,
          value: school.schoolName,
        }));
      } catch (error) {
        if (error.response?.status === 401) {
          // setRedirectTo('/login');
        } else {
          alert('Service not available in this area');
          setPinCode('');
        }
      }
    } else {
      setCityName('');
      setStateName('');
    }
  };
  const handelSubmit = (values) => {
    if (pin.length == 0 || email.length == 0 || orgName.length == 0) {
      setSubmitError('All fields are required field');
      setNameError('Name field cannot be empty');
      setEmailError('Email field cannot be empty');
      setPinError('PIN cannot be empty');
      setAddressError('Address field cannot be empty');
    } else if (
      emailError.length > 0 ||
      nameError.length > 0 ||
      pinError.length > 0
    ) {
      setSubmitError('All fields are required field');
    } else {
      submitPersonalInformation(values);
      nextButtonHandler();
    }
  };

  // const handelSubmit = (values)=>{

  // }
  return (
    <SafeAreaView style={styles.container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />
      <View style={styles.body}>
        <View style={styles.form}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <TextInput
                width="lg"
                style={styles.input}
                // label="Name"
                labelColor="#a1a1a1"
                placeholder="Enter Your Name"
                autoFocus={true}
                value={orgName}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setOrgName(value);
                  validateName(value);
                }}
              />
              {nameError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>{nameError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}
              <TextInput
                width="lg"
                style={styles.input}
                // label="Email Address"
                labelColor="#a1a1a1"
                placeholder="Enter Your Email Address"
                value={email}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setEmail(value);
                  validateEmail(value);
                }}
              />
              {emailError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>{emailError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <TextInput
                width="lg"
                style={styles.input}
                // label="Email Address"
                labelColor="#a1a1a1"
                placeholder="Enter Your Address"
                value={address}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setAddress(value);
                  validateAddress(value);
                }}
              />
              {addressError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>
                    {addressError}
                  </Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <TextInput
                width="lg"
                style={styles.input}
                labelColor="#a1a1a1"
                placeholder="PIN CODE"
                value={pin}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setPin(value);
                  validatePin(value);
                  if (value.length > 5) {
                    onPinCodeChange(value);
                  }
                  // validatePassword(value)
                }}
              />
              {pinError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>{pinError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <View>
                <TextInput
                  width="lg"
                  style={styles.input}
                  labelColor="#a1a1a1"
                  placeholder="City"
                  value={cityName}
                  otherStyle={{
                    borderBottomColor: Colors.verylightGrey,
                    borderBottomWidth: 1,
                  }}
                />

                <TextInput
                  width="lg"
                  style={styles.input}
                  labelColor="#a1a1a1"
                  placeholder="State"
                  value={stateName}
                  otherStyle={{
                    borderBottomColor: Colors.verylightGrey,
                    borderBottomWidth: 1,
                  }}
                />
              </View>
              <View
                style={{
                  marginTop: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}></View>
            </ScrollView>
            <Button
              backgroundColor={'#4C8BF5'}
              size="lg"
              borderRadius={5}
              title="Next"
              textStyles={{
                fontSize: 18,
                fontStyle: 'normal',
                fontWeight: '400',
                color: '#fff',
              }}
              onPress={() => {
                const values = {
                  orgName: orgName,
                  email: email,
                  pinCode: pinCode,
                  school: school,
                  city: cityName,
                  state: stateName,
                };
                handelSubmit(values);
              }}
            />
            <View style={{padding: 50}}></View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default OrgPersonalInformation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginLeft: HeightWidth.getResWidth(2),
    marginRight: HeightWidth.getResWidth(2),
  },
  input: {
    padding: 10,
    margin: 10,
    fontSize: 12,
    overflow: 'hidden',
  },
  title: {
    fontWeight: '500',
    fontSize: HeightWidth.getResFontSize(24),
    textAlign: 'center',
    marginVertical: 8,
    paddingBottom: '5%',
  },
  title2: {
    fontWeight: '400',
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'left',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footer: {
    flex: 0.2,
    // marginVertical: 8,
    alignItems: 'center',
    // marginBottom: 50,
  },
  body: {},
  form: {
    borderRadius: 18,
    backgroundColor: 'white',
    //  padding:20,
    textAlign: 'center',
    //  paddingTop:"10%",
    //  marginTop:'10%'
  },
  logo: {
    width: 50,
    height: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
  link1: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(12),
    backgroundColor: 'white',
  },
  link2: {
    fontWeight: '400',
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(16),
    paddingTop: '5%',
    paddingBottom: '8%',
    marginLeft: '60%',
    borderRadius: 5,
    backgroundColor: 'white',
  },
  link3: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(18),
    paddingVertical: 10,
    backgroundColor: 'white',
    // paddingHorizontal: 20,
  },
  text: {
    fontSize: HeightWidth.getResFontSize(14),
    color: 'grey',
  },
  btn: {
    backgroundColor: '#f5c242',
    borderRadius: 8,
    padding: 20,
    margin: 20,
    textAlign: 'center',
  },
  button: {
    fontWeight: '400',
    backgroundColor: '#4C8BF5',
    borderRadius: 5,
    color: 'white',
    fontSize: HeightWidth.getResFontSize(18),
    overflow: 'hidden',
    padding: '5%',
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  img: {
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  linknav: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
