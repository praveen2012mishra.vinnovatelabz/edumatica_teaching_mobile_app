import React, {FunctionComponent} from 'react';
import {Text, StyleSheet, View, Image, ImageBackground} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import PropTypes from 'prop-types';

//Utilities
import {Colors, images, width} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';

const IntroSliderHeader: FunctionComponent = ({}) => {
  return (
    <View style={styles.__HeaderContent}>
      <ImageBackground
        source={images.sliderHeader}
        imageStyle={styles.__contentImageStyle}
        style={styles.__backgroundImage}>
        <View style={styles.__headerContentLeftBox}>
          <Text style={styles.__headerTitle}>Hello !</Text>
          <Text style={styles.__headerSubtitle1}>Mr. Ashish</Text>
          <Text style={styles.__headerSubtitle2}>Warm welcome to</Text>
          <View style={styles.__headerIconView}>
            <View style={styles.__headerIconInnerView}>
              <Icon name={'book-open'} color={Colors.fullWhite} size={20} />
            </View>
            <Text style={styles.__headerSubtitle3}>Edumatica</Text>
          </View>
        </View>
        <View style={styles.__headerContentRightBox}>
          <Image source={images.tempUser} style={styles.__avatarImage} />
        </View>
      </ImageBackground>
    </View>
  );
};

IntroSliderHeader.propTypes = {
  title: PropTypes.string,
};

IntroSliderHeader.defaultProps = {
  title: null,
};

const styles = StyleSheet.create({
  __HeaderContent: {
    backgroundColor: Colors.fullWhite,
  },
  __contentImageStyle: {
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
  },
  __backgroundImage: {
    width: width,
    height: HeightWidth.getResWidth(160),
    alignItems: 'center',
    flexDirection: 'row',
  },
  __headerContentLeftBox: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginTop: HeightWidth.getResWidth(26),
    marginLeft: HeightWidth.getResWidth(26),
    flex: 1,
  },
  __headerContentRightBox: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  __avatarImage: {
    width: HeightWidth.getResWidth(80),
    height: HeightWidth.getResWidth(80),
    borderRadius: 45,
  },
  __headerTitle: {
    color: Colors.veryGrayishBlack,
    fontWeight: '700',
    fontSize: 30,
  },
  __headerSubtitle1: {
    color: Colors.veryGrayishBlack,
    fontWeight: '700',
    fontSize: 20,
  },
  __headerSubtitle2: {
    color: Colors.primaryBlue,
    fontWeight: '400',
    fontSize: 18,
  },
  __headerIconView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  __headerIconInnerView: {
    borderRadius: 5,
    backgroundColor: Colors.brightOrange,
    justifyContent: 'center',
    alignItems: 'center',
    padding: HeightWidth.getResWidth(3),
  },
  __headerSubtitle3: {
    color: Colors.primaryBlue,
    fontWeight: '700',
    fontSize: 22,
    marginLeft: HeightWidth.getResWidth(10),
  },
});

export default IntroSliderHeader;
