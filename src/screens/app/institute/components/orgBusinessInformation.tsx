import React, {useEffect, useState, FunctionComponent} from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Button from '../../../../components/UI/Button';
import TextInput from '../../../../components/UI/TextInput';
import Picker from '../../../../components/UI/Picker';
import {Organization} from '../../../../common/contracts/user';
import {BusinessType} from '../../../../common/enums/business_types';
import DatePicker from 'react-native-datepicker';

import {
  EMAIL_PATTERN,
  PIN_PATTERN,
} from '../../../../common/validations/patterns';
import CustomStatusBar from '../../../../components/Header/custom_status_bar';
import * as Animatable from 'react-native-animatable';

import {
  fetchQualificationsList,
  fetchCitySchoolsList,
  fetchCitiesByPinCode,
} from '../../../../common/api/academics';

//utilities
import {Colors} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';

const data = [
  {label: 'Private Limited', value: BusinessType.PVT_LTD},
  {label: 'Proprietorship', value: BusinessType.PROPRIETORSHIP},
  {label: 'Partnership', value: BusinessType.PARTNERSHIP},
  {label: 'Public Limited', value: BusinessType.PUBLIC_LTD},
  {label: 'LLP', value: BusinessType.LLP},
  {label: 'SOCIETY', value: BusinessType.SOCIETY},
  {label: 'Trust', value: BusinessType.TRUST},
  {label: 'NGO', value: BusinessType.NGO},
  {label: 'Not Registered', value: BusinessType.NOT_REG},
];

interface Props {
  user: Organization;
  nextButtonHandler: any;
  saveUser: (data: Organization) => any;
}

const OrgBusinessInformation: FunctionComponent<Props> = ({
  user,
  saveUser,
  nextButtonHandler,
}) => {
  const [businessType, setBusinessType] = useState(user.businessType || '0');
  const [dob, setDob] = useState(user.dob || '09-10-2020');
  const [businessPAN, setBusinessPAN] = useState(user.businessPAN || '');
  const [businessName, setBusinessName] = useState(user.businessName || '');
  const [ownerPAN, setOwnerPAN] = useState(user.ownerPAN || '');
  const [ownerName, setOwnerName] = useState(user.ownerName || '');
  // validation states
  const [ownerNameError, setOwnerNameError] = useState('');
  const [pinError, setPinError] = useState('');
  const [emailError, setEmailError] = useState('');
  const [businessTypeError, setBusinessTypeError] = useState('');
  const [businessPanError, setBusinessPanError] = useState('');
  const [businessNameError, setBusinessNameError] = useState('');
  const [ownerPanError, setOwnerPanError] = useState('');
  const [submitError, setSubmitError] = useState('');

  useEffect(() => {
    (async () => {
      try {
      } catch (error) {
        if (error.response?.status === 401) {
          alert("setRedirectTo('/')");
        }
      }
    })();
  }, []);

  // validation
  const validateBusinessType = (value) => {
    console.log(value);
    if (value == '0' || value == null) {
      setBusinessTypeError('Please select a valid business type');
    } else {
      setBusinessTypeError('');
    }
  };

  const validateBusinessPan = (value) => {
    if (value.lenth == 0 || value == '') {
      setBusinessPanError('Business PAN field cannot be empty');
    } else {
      setBusinessPanError('');
    }
  };
  const validateOwnerPan = (value) => {
    if (value.lenth == 0 || value == '') {
      setOwnerPanError('PAN field cannot be empty');
    } else {
      setOwnerPanError('');
    }
  };

  const validateBusinessName = (value) => {
    if (value.lenth == 0 || value == '') {
      setBusinessNameError('Business Name field cannot be empty');
    } else {
      setBusinessNameError('');
    }
  };
  const validateOwnerName = (value) => {
    if (value.lenth == 0 || value == '') {
      setOwnerNameError('Name field cannot be empty');
    } else {
      setOwnerNameError('');
    }
  };

  const submitPersonalInformation = async (values) => {
    try {
      saveUser({
        ...user,
        businessType: businessType,
        dob: dob,
        businessPAN: businessPAN,
        businessName: businessName,
        ownerPAN: ownerPAN,
        ownerName: ownerName,
        //kycDetails: documents
      });
    } catch (error) {
      alert(error);
    }
  };

  const handelSubmit = (values) => {
    if (
      businessPAN.length == 0 ||
      ownerName.length == 0 ||
      ownerPAN.length == 0 ||
      businessName.length == 0 ||
      businessType == '0' ||
      businessType == null
    ) {
      setSubmitError('All fields are required field');
      setBusinessTypeError('Please select a valid business type');
      setOwnerNameError('Owner name field cannot be empty');
      setBusinessNameError('Business name field cannot be empty');
      setOwnerPanError('Owner PAN field cannot be empty');
      setBusinessPanError('Business PAN field cannot be empty');
    } else {
      submitPersonalInformation(values);
      nextButtonHandler();
    }
  };

  // const handelSubmit = (values)=>{

  // }
  return (
    <SafeAreaView style={styles.container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />
      <View style={styles.body}>
        <View style={styles.form}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Picker
                label="Business Type"
                labelSize={16}
                labelColor={Colors.lightGrayColor}
                data={data}
                rightIconName=""
                rightIconSize=""
                rightIconColor=""
                value={businessType}
                docTypeHandler={async (item) => {
                  setBusinessType(item);
                  validateBusinessType(item);
                }}
                onRightIconPress=""
                showLabel={true}
              />
              {businessTypeError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>
                    {businessTypeError}
                  </Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <Text style={styles.text}>Date Of Incorporation :</Text>
              <View>
                <DatePicker
                  style={styles.dateInput}
                  date={dob} // Initial date from state
                  mode="date" // The enum of date, datetime and time
                  placeholder="select date"
                  format="DD-MM-YYYY"
                  useNativeDriver="true"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  onDateChange={(dob) => {
                    setDob(dob);
                    console.log(dob);
                  }}
                />
              </View>

              <TextInput
                width="lg"
                style={styles.input}
                // label="Name"
                labelColor="#a1a1a1"
                placeholder="PAN of the company"
                autoFocus={true}
                value={businessPAN}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setBusinessPAN(value);
                  console.log('value is =>', value);
                  validateBusinessPan(value);
                }}
              />
              {businessPanError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>
                    {businessPanError}
                  </Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <TextInput
                width="lg"
                style={styles.input}
                // label="Name"
                labelColor="#a1a1a1"
                placeholder="Company Name as per PAN"
                autoFocus={true}
                value={businessName}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setBusinessName(value);
                  validateBusinessName(value);
                }}
              />
              {businessNameError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>
                    {businessNameError}
                  </Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <TextInput
                width="lg"
                style={styles.input}
                // label="Name"
                labelColor="#a1a1a1"
                placeholder="PAN of one of the directors"
                autoFocus={true}
                value={ownerPAN}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setOwnerPAN(value);
                  validateOwnerPan(value);
                }}
              />
              {ownerPanError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>
                    {ownerPanError}
                  </Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <TextInput
                width="lg"
                style={styles.input}
                // label="Name"
                labelColor="#a1a1a1"
                placeholder="Owner name as per PAN"
                autoFocus={true}
                value={ownerName}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setOwnerName(value);
                  validateOwnerName(value);
                }}
              />
              {ownerNameError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>
                    {ownerNameError}
                  </Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}
            </ScrollView>
            <Button
              backgroundColor={'#4C8BF5'}
              size="lg"
              borderRadius={5}
              title="Next"
              textStyles={{
                fontSize: 18,
                fontStyle: 'normal',
                fontWeight: '400',
                color: '#fff',
              }}
              onPress={() => {
                const values = {
                  businessType: businessType,
                  dob: dob,
                  businessPAN: businessPAN,
                  businessName: businessName,
                  ownerPAN: ownerPAN,
                  ownerName: ownerName,
                };
                handelSubmit(values);
              }}
            />
            <View style={{padding: 50}}></View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default OrgBusinessInformation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginLeft: HeightWidth.getResWidth(2),
    marginRight: HeightWidth.getResWidth(2),
  },
  input: {
    padding: 10,
    margin: 10,
    fontSize: 12,
    overflow: 'hidden',
  },
  title: {
    fontWeight: '500',
    fontSize: HeightWidth.getResFontSize(24),
    textAlign: 'center',
    marginVertical: 8,
    paddingBottom: '5%',
  },
  title2: {
    fontWeight: '400',
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'left',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footer: {
    flex: 0.2,
    // marginVertical: 8,
    alignItems: 'center',
    // marginBottom: 50,
  },
  body: {},
  form: {
    borderRadius: 18,
    backgroundColor: 'white',
    //  padding:20,
    textAlign: 'center',
    //  paddingTop:"10%",
    //  marginTop:'10%'
  },
  logo: {
    width: 50,
    height: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
  link1: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(12),
    backgroundColor: 'white',
  },
  link2: {
    fontWeight: '400',
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(16),
    paddingTop: '5%',
    paddingBottom: '8%',
    marginLeft: '60%',
    borderRadius: 5,
    backgroundColor: 'white',
  },
  link3: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(18),
    paddingVertical: 10,
    backgroundColor: 'white',
    // paddingHorizontal: 20,
  },
  text: {
    fontSize: HeightWidth.getResFontSize(14),
    color: 'grey',
  },
  btn: {
    backgroundColor: '#f5c242',
    borderRadius: 8,
    padding: 20,
    margin: 20,
    textAlign: 'center',
  },
  button: {
    fontWeight: '400',
    backgroundColor: '#4C8BF5',
    borderRadius: 5,
    color: 'white',
    fontSize: HeightWidth.getResFontSize(18),
    overflow: 'hidden',
    padding: '5%',
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  img: {
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  linknav: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dateInput: {
    padding: 2,
    margin: 2,
    fontSize: 12,
    overflow: 'hidden',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    marginTop: '2%',
    width: '100%',
  },
});
