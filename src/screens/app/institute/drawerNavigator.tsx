import React, {Component} from 'react';
import {Text, StyleSheet, View, SafeAreaView} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Profile from './drawerScreens/profile';
//TutorStackScreens
import Calendar from './bottomTabScreens/calendar';
import Dashboard from './bottomTabScreens/dashboard';
import Notification from './bottomTabScreens/notification';
import Student from './bottomTabScreens/student';
import StudentManagement from '../../../common/screens/profileManagement/studentManagement';
import AddStudents from '../../../common/screens/profileManagement/addStudents';
import EnterStudentDetails from '../../../common/screens/profileManagement/enterStudentDetails';
import TutorManagement from '../institute/tutorManagement/tutorManagement';
import AddTutor from './tutorManagement/addTutor';
import EnterTutorDetails from './tutorManagement/enterTutorDetails';
import Course from './AssignmentManagement/course';
import Chapters from './AssignmentManagement/chapters';
import TempLogout from './tempLogout';

// utilities
import {Colors} from '../../../utility/colors';

// BottomTab
const Drawer = createDrawerNavigator();
const StudentManagementStack = createStackNavigator();
const TutorManagementStack = createStackNavigator();
const AssignmentManagementStack = createStackNavigator();

const StudentOrgManagementStack = () => {
  return (
    <StudentManagementStack.Navigator initialRouteName="StudentManagement">
      <StudentManagementStack.Screen
        name="StudentManagement"
        component={StudentManagement}
        options={{headerShown: false}}
      />
      <StudentManagementStack.Screen
        name="AddStudents"
        component={AddStudents}
        options={{headerShown: false}}
      />
      <StudentManagementStack.Screen
        name="Enter Details"
        component={EnterStudentDetails}
        options={{headerShown: false}}
        initialParams={{formType: 'add'}}
      />
    </StudentManagementStack.Navigator>
  );
};

const AssignmentManagement = () => {
  return (
    <AssignmentManagementStack.Navigator initialRouteName="Course">
      <AssignmentManagementStack.Screen
        name="Course"
        component={Course}
        options={{headerShown: false}}
      />
      <AssignmentManagementStack.Screen
        name="Chapters"
        component={Chapters}
        options={{headerShown: false}}
      />
    </AssignmentManagementStack.Navigator>
  );
};

const TutorOrgManagementStack = () => {
  return (
    <TutorManagementStack.Navigator initialRouteName="tutorManagement">
      <TutorManagementStack.Screen
        name="tutorManagement"
        component={TutorManagement}
        options={{headerShown: false}}
      />
      <TutorManagementStack.Screen
        name="AddTutor"
        component={AddTutor}
        options={{headerShown: false}}
      />
      <StudentManagementStack.Screen
        name="tutorEnterDetails"
        component={EnterTutorDetails}
        options={{headerShown: false}}
        initialParams={{formType: 'add'}}
      />
    </TutorManagementStack.Navigator>
  );
};

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator drawerPosition={'right'} initialRouteName="Home">
      <Drawer.Screen name="Home" component={Dashboard} />
      <Drawer.Screen
        name="Assignment Management"
        component={AssignmentManagement}
      />
      <Drawer.Screen name="Schedule Calender" component={Calendar} />
      <Drawer.Screen
        name="Student Management"
        component={StudentOrgManagementStack}
      />
      <Drawer.Screen
        name="Tutor Management"
        component={TutorOrgManagementStack}
      />
      <Drawer.Screen name="Students" component={Student} />
      <Drawer.Screen name="Profile" component={Profile} />
      <Drawer.Screen name="Logout" component={TempLogout} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;

const styles = StyleSheet.create({
  __container: {
    flex: 1,
  },
});
