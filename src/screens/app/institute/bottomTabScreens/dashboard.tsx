import React, {FunctionComponent} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';

//components
import BaseHeader from '../../../../components/Header/BaseHeader';
import CustomStatusBar from '../../../../components/Header/custom_status_bar';

//utilities
import HeightWidth from '../../../../utility/HeightWidth';
import {Colors} from '../../../../utility/utilities';

interface Props {
  navigation: any;
  route: any;
}

const Dashboard: FunctionComponent<Props> = ({route, navigation}) => {
  return (
    <SafeAreaView style={styles.__container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />
      <BaseHeader
        showBackButton={false}
        title={'Dashboard'}
        onMenuPress={() => {
          navigation.openDrawer();
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  __container: {
    flex: 1,
  },
});

export default Dashboard;
