import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import EditPersonalInfo from './profile/editProfile/editPersonalInformation';
import EditCourseInfo from './profile/editProfile/editCourseInformation';
import EditBusinessInfo from './profile/editProfile/editBusinessInformation';

//mainAppStack
import DrawerNavigator from './drawerNavigator';

// import Temp from './temp';
import IntroSlider from './../tutor/introSlider';
import {Colors} from '../../../utility/utilities';

// StackNavigation
const appStack = createStackNavigator();

const navigationOptions = {
  headerTintColor: Colors.primaryMustard,
  headerStyle: {
    backgroundColor: '#c2dfff',
    borderBottomColor: '#ffffff',
    borderBottomWidth: 3,
  },
  headerTitleStyle: {
    fontSize: 18,
  },
};

const createAppStackOrg = () => {
  return (
    <appStack.Navigator initialRouteName="DrawerNavigator">
      <appStack.Screen
        name="IntroSlider"
        component={IntroSlider}
        options={{headerShown: false}}
      />
      <appStack.Screen
        name="DrawerNavigator"
        component={DrawerNavigator}
        options={{headerShown: false}}
      />
      <appStack.Screen
        name="EditPersonalInfo"
        component={EditPersonalInfo}
        options={{...navigationOptions, title: 'Personal Information'}}
      />
      <appStack.Screen
        name="EditCourseInfo"
        component={EditCourseInfo}
        options={{...navigationOptions, title: 'Course Information'}}
      />
      <appStack.Screen
        name="EditBusinessInfo"
        component={EditBusinessInfo}
        options={{...navigationOptions, title: 'Business Information'}}
      />
    </appStack.Navigator>
  );
};

export default createAppStackOrg;

const styles = StyleSheet.create({
  __container: {
    flex: 1,
  },
});
