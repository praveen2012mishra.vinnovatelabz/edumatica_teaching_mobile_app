import React, {FunctionComponent, useState, useEffect} from 'react';
import {StyleSheet, Text, View, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

//components
import BaseHeader from '../../../components/Header/BaseHeader';
import CustomStatusBar from '../../../components/Header/custom_status_bar';

//utilities
import {Colors} from '../../../utility/utilities';

interface Props {
  route: any;
  navigation: any;
}

const TempLogout: FunctionComponent<Props> = ({route, navigation}) => {
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      logout();
    });
    return unsubscribe;
  }, []);

  const logout = async () => {
    await AsyncStorage.removeItem('accessToken');
    await AsyncStorage.removeItem('userRole');

    navigation.reset({
      index: 0,
      routes: [{name: 'authStack'}],
    });
  };

  return (
    <SafeAreaView style={styles.__container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />
      <BaseHeader
        onPress={() => {
          navigation.goBack();
        }}
        title={'EDUMATICA'}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  __container: {
    flex: 1,
  },
});

export default TempLogout;
