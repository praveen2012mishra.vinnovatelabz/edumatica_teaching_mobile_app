import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import EditPersonalInfo from './profile/editProfile/editPersonalInformation';
import EditCourseInfo from './profile/editProfile/editCourseInformation';
import EditOtherInformation from './profile/editProfile/editOtherInformation';

//mainAppStack
import DrawerNavigator from './drawerNavigator';
// import Temp from './temp';
import ProfileDetailsScreen from './processForms/profileProcessTutor';
import IntroSlider from './../tutor/introSlider';
import {Colors} from '../../../utility/utilities';
import HeightWidth from '../../../utility/HeightWidth';

// StackNavigation
const appStack = createStackNavigator();

// todo We are using edit screens direct in this stack !

const navigationOptions = {
  headerTintColor: Colors.primaryMustard,
  headerStyle: {
    backgroundColor: '#c2dfff',
    borderBottomColor: '#ffffff',
    borderBottomWidth: 3,
  },
  headerTitleStyle: {
    fontSize: HeightWidth.getResFontSize(16),
  },
};

const createAppStack = () => {
  return (
    <appStack.Navigator initialRouteName="DrawerNavigator">
      <appStack.Screen
        name="IntroSlider"
        component={IntroSlider}
        options={{headerShown: false}}
      />
      <appStack.Screen
        name="DrawerNavigator"
        component={DrawerNavigator}
        options={{headerShown: false}}
      />

      <appStack.Screen
        name="EditPersonalInfo"
        component={EditPersonalInfo}
        options={{...navigationOptions, title: 'Personal Information'}}
      />

      <appStack.Screen
        name="EditCourseInfo"
        component={EditCourseInfo}
        options={{...navigationOptions, title: 'Course Information'}}
      />

      <appStack.Screen
        name="EditOtherInformation"
        component={EditOtherInformation}
        options={{...navigationOptions, title: 'Other Information'}}
      />
      {/* <appStack.Screen name="Temp" component={Temp} options={{ headerShown: false }} /> */}
    </appStack.Navigator>
  );
};

export default createAppStack;

const styles = StyleSheet.create({
  __container: {
    flex: 1,
  },
});
