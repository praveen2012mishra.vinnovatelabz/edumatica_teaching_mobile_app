import React, {Component} from 'react';
import {Text, StyleSheet, View, SafeAreaView} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Profile from './drawerScreens/profile';
//TutorStackScreens
import Temp from './temp';
import calendarScheduleScreen from './topTabScreens/calendarScheduleScreen';
import AddScheduleScreen from './topTabScreens/AddScheduleScreen'
import ContentManagement from './topTabScreens/contentManagement';
import AllContentType from './contentManagement/allContentType';
import PdfContent from './contentManagement/pdfContent';
import PdfContentAdd from './contentManagement/pdfContentAdd';
import Course from './contentManagement/course'
import Chapters from './contentManagement/chapter'
import Calendar from './drawerScreens/calendar';
import Dashboard from './drawerScreens/dashboard';
import Notification from './drawerScreens/notification';
import Student from './drawerScreens/student';
import StudentManagement from '../../../common/screens/profileManagement/studentManagement';
import AddStudents from '../../../common/screens/profileManagement/addStudents';
import EnterStudentDetails from '../../../common/screens/profileManagement/enterStudentDetails';
import ImageContent from './contentManagement/ImageContent';
import ImageContentAdd from './contentManagement/ImageContentAdd';
import TempLogout from '../institute/tempLogout';
import VideoContent from './contentManagement/VideoContent';
import VideoContentAdd from './contentManagement/VideoContentAdd';

// utilities
import {Colors} from '../../../utility/colors';

const Drawer = createDrawerNavigator();
const CalenderStack=createStackNavigator();

const calendarScheduleScreenStack=()=>{
  return (
    <CalenderStack.Navigator initialRouteName="Schedule Calender">
      <CalenderStack.Screen name="AddScheduleScreen" component={AddScheduleScreen} options={{headerShown: false}}/>
      <CalenderStack.Screen name="Schedule Calender" component={calendarScheduleScreen} options={{headerShown: false}}/>
    </CalenderStack.Navigator>
  )
}

const contentManagementStack = createStackNavigator();

const contentStackTutor = () => {
  return (
    <contentManagementStack.Navigator initialRouteName="Course">
      <contentManagementStack.Screen
        name="ContentManagement"
        component={ContentManagement}
        options={{headerShown: false}}
      />
      <contentManagementStack.Screen
        name="Course"
        component={Course}
        options={{headerShown: false}}
      />
       <contentManagementStack.Screen
        name="Chapters"
        component={Chapters}
        options={{headerShown: false}}
      />
      <contentManagementStack.Screen
        name="AllContentType"
        component={AllContentType}
        options={{headerShown: false}}
      />
      <contentManagementStack.Screen
        name="PdfContent"
        component={PdfContent}
        options={{headerShown: false}}
      />
      <contentManagementStack.Screen
        name="ImageContent"
        component={ImageContent}
        options={{headerShown: false}}
      />
      <contentManagementStack.Screen
        name="PdfContentAdd"
        component={PdfContentAdd}
        options={{headerShown: false}}
      />
      <contentManagementStack.Screen
        name="ImageContentAdd"
        component={ImageContentAdd}
        options={{headerShown: false}}
      />
      <contentManagementStack.Screen
        name="VideoContent"
        component={VideoContent}
        options={{headerShown: false}}
      />
      <contentManagementStack.Screen
        name="VideoContentAdd"
        component={VideoContentAdd}
        options={{headerShown: false}}
      />
    </contentManagementStack.Navigator>
  );
};
const StudentManagementStack = createStackNavigator();

const StudentTutoragementStack = () => {
  return (
    <StudentManagementStack.Navigator initialRouteName="StudentManagement">
      <StudentManagementStack.Screen
        name="StudentManagement"
        component={StudentManagement}
        options={{headerShown: false}}
      />
      <StudentManagementStack.Screen
        name="AddStudents"
        component={AddStudents}
        options={{headerShown: false}}
      />
      <StudentManagementStack.Screen
        name="Enter Details"
        component={EnterStudentDetails}
        options={{headerShown: false}}
        initialParams={{formType: 'add'}}
      />
    </StudentManagementStack.Navigator>
  );
};
// drawer
const CreateDrawerNavigator = () => {
  return (
    <Drawer.Navigator backBehavior="initialRoute" initialRouteName="Home">
      <Drawer.Screen name="Home" component={Dashboard} />
      <Drawer.Screen name="Schedule Calender" component={Calendar} />
      <Drawer.Screen
        name="Student Management"
        component={StudentTutoragementStack}
      />
      <Drawer.Screen name="Students" component={Student} />
      <Drawer.Screen name="Profile" component={Profile} />
      <Drawer.Screen name="Manage Content" component={contentStackTutor} />
      {/* <Drawer.Screen name="Logout" component={Dashboard} /> */}
      <Drawer.Screen name="Logout" component={TempLogout} />
    </Drawer.Navigator>
  );
};

export default CreateDrawerNavigator;

const styles = StyleSheet.create({
  __container: {
    flex: 1,
  },
});
