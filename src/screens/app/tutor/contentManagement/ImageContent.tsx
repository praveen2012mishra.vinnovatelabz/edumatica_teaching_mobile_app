// All content Type screen
import React, { useEffect, FunctionComponent, useState } from 'react';
import {
    StyleSheet,
    View,
    SafeAreaView,
    FlatList,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    Image,
    ImageBackground,
    Text
} from 'react-native';
import {
    fetchContentDetails,
    fetchContents,
} from '../../../../common/api/academics';
import { Icon } from "react-native-elements";
import ButtonUI from '../../../../components/UI/Button';
import HeightWidth from '../../../../utility/HeightWidth';
import { Colors, width, images } from '../../../../utility/utilities';
import { Modal, ModalContent } from 'react-native-modals';
import ContentHeader from '../../../../components/Header/ContentHeader'
import TextInputUI from '../../../../components/UI/TextInput'
interface Props {
    navigation: any;
    route: any;
}

const documentTypeNumber = ['Documents', 'Images', 'Videos', 'Links', 'Quiz']

const ImageContent: FunctionComponent<Props> = ({ navigation, route }) => {
    useEffect(() => {
    }, []);
    const [imageUpdateStatus, setImageUpdateStatus] = useState(false);
    const [deleteDocument, setDeleteDocument] = useState(false);
    const [renameDocument, setRenameDocument] = useState(false);

    const getUploadedDocument = (item, id) => (
        <TouchableOpacity
            style={{ flexDirection: 'row', marginBottom: 20 }}
            onPress={() => {
                setImageUpdateStatus(true);
            }}
            key={id}
        >
            <ImageBackground source={images.cbsc_1} style={styles.__imageBox}>
                <Text>{' '}</Text>
                <Icon
                    iconStyle={{}}
                    name="ellipsis-v"
                    color={'black'}
                    size={15}
                    type="font-awesome"
                />
            </ImageBackground>

        </TouchableOpacity>
    )

    return (
        <SafeAreaView style={[styles.__container, { }]}>
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                <ContentHeader title={'Images'} rightContent={true} boardName={route.params.boardName} className={route.params.className} />
                <View style={styles.__bodyContainer}>
                    <View style={styles.__titleContainer}>
                        <View style={styles.__chapterContainer} >
                            {/* <Image source={images.GoldVerticalLine} style={styles.__VerticleLine} /> */}
                            <Text style={styles.chapterName}>{route.params.chapterName}</Text>
                        </View>
                        <View style={styles.__subjectContainer}>
                            <Text style={styles.__subjectBox}>{route.params.subjectName}</Text>
                        </View>
                    </View>

                    <View style={styles.__documnetContainer}>
                        <View style={styles.__documentBox}>
                            <View style={styles.__countItem}>
                                <Text style={styles.__countItemContent}>4 Items</Text>
                            </View>
                            {documentTypeNumber.length > 0 && (<View style={styles.__documnetContainerBox}>
                                {documentTypeNumber.map((item, id) => getUploadedDocument(item, id))}

                            </View>)}
                        </View>                        
                    </View>                   
                    
                    {imageUpdateStatus && !deleteDocument && !renameDocument && (<Modal
                        visible={imageUpdateStatus}
                        onTouchOutside={() => {
                            setImageUpdateStatus(false)
                        }}
                    >
                        <ModalContent  style={styles.__modalContainer}>
                            <View style={styles._documentUpdateStatusContainer}>
                                <Icon
                                    iconStyle={{ textAlign: 'center', width: 30, height: 30 }}
                                    name="edit"
                                    color={'black'}
                                    size={15}
                                    type="font-awesome"
                                    onPress={() => {
                                        setRenameDocument(true)
                                    }}
                                />
                                <Icon
                                    iconStyle={{ textAlign: 'center', width: 30, height: 30 }}
                                    name="trash"
                                    color={'black'}
                                    size={15}
                                    type="font-awesome"
                                    onPress={() => {
                                        setDeleteDocument(true)
                                    }}
                                />
                                <Icon
                                    iconStyle={{ textAlign: 'center', width: 30, height: 30 }}
                                    name="share"
                                    color={'black'}
                                    size={15}
                                    type="font-awesome"
                                />
                            </View>
                        </ModalContent>
                    </Modal>
                    )}

                    {deleteDocument && (

                        <Modal
                            visible={deleteDocument}
                            onTouchOutside={() => {
                                setDeleteDocument(false)
                            }}
                        >
                            <ModalContent  style={styles.__modalContainer}>
                                <View style={styles._deleteDocumentContainer}>
                                    <Text style={styles._deleteDocumentContent}>
                                        Are you sure you want to delete?
                                     </Text>
                                    <Text style={styles._deleteHorizontalLine}>
                                    </Text>
                                    <View style={styles.__cancelBox}>
                                        <ButtonUI
                                            title="Cancel"
                                            width={HeightWidth.getResWidth(100)}
                                            backgroundColor="#fff"
                                            onPress={async () => {

                                                //console.log(getItemState,'----------------------------->');
                                            }}
                                            style={styles.__buttonCancel}
                                            borderRadius={5}
                                            textStyles={{
                                                color: '#000',
                                                fontSize: HeightWidth.getResFontSize(18),
                                                fontWeight: '400',
                                            }}
                                            otherStyle={{
                                                borderWidth: 2,
                                                borderColor: '#4C8BF5',
                                                shadowColor: "#000",
                                                shadowOffset: {
                                                    width: 0,
                                                    height: 1,
                                                },
                                                shadowOpacity: 0.22,
                                                shadowRadius: 2.22,
                                                elevation: 3,
                                            }}
                                        />
                                        
                                        <Text style={styles.__yesDeleteContent}>Yes, Delete</Text>
                                    </View>

                                </View>
                            </ModalContent>
                        </Modal>
                    )}
                    {renameDocument && (
                        <Modal
                            visible={renameDocument}
                            onTouchOutside={() => {
                                setRenameDocument(false)
                            }}
                            
                        >
                            <ModalContent style={styles.__modalContainer}>
                                <View style={styles._deleteDocumentContainer}>
                                <TextInputUI 
                                        //label={"Enter New Name"}
                                        //labelStyle={styles._renameDocumentContent}
                                        width="md"
                                        style={styles.__textInput}
                                        placeholder="Enter New Name"
                                        onChangeText={(text) => {
                                             //setStudentName(text)
                                             }}
                                        //value={''}
                                        placeholderTextColor={Colors.fullBlack}
                                        otherStyle={styles.__inputContainerStyle}
                                        />
                                 
                                    <View style={styles.__cancelBox}>
                                        <ButtonUI
                                            title="Cancel"
                                            width={HeightWidth.getResWidth(100)}
                                            backgroundColor="#fff"
                                            onPress={async () => {
                                                navigation.navigate('ImageContent', { courseDetails: route.params })
                                                //console.log(getItemState,'----------------------------->');
                                            }}
                                            style={styles.__buttonCancel}
                                            borderRadius={5}
                                            textStyles={{
                                                color: '#000',
                                                fontSize: HeightWidth.getResFontSize(18),
                                                fontWeight: '400',
                                            }}
                                            otherStyle={{
                                                borderWidth: 2,
                                                borderColor: '#4C8BF5',
                                                shadowColor: "#000",
                                                shadowOffset: {
                                                    width: 0,
                                                    height: 1,
                                                },
                                                shadowOpacity: 0.22,
                                                shadowRadius: 2.22,
                                                elevation: 3,
                                            }}
                                        />
                                        <ButtonUI
                                            title="Save"
                                            width={HeightWidth.getResWidth(100)}
                                            backgroundColor={Colors.contentDocumentDisplayHeader}
                                            onPress={async () => {
                                                navigation.navigate('ImageContent', { courseDetails: route.params })
                                                //console.log(getItemState,'----------------------------->');
                                            }}
                                            style={styles.__buttonCancel}
                                            borderRadius={5}
                                            textStyles={{
                                                color: '#fff',
                                                fontSize: HeightWidth.getResFontSize(18),
                                                fontWeight: '400',
                                            }}
                                            otherStyle={{
                                                borderWidth: 2,
                                                borderColor: '#4C8BF5',
                                                shadowColor: "#000",
                                                shadowOffset: {
                                                    width: 0,
                                                    height: 1,
                                                },
                                                shadowOpacity: 0.22,
                                                shadowRadius: 2.22,
                                                elevation: 3,
                                            }}
                                        />
                                    </View>

                                </View>
                            </ModalContent>
                        </Modal>
                        
                    )}
                    <TouchableOpacity style={styles.__plusTouchBox}
                        onPress={() =>{navigation.navigate('ImageContentAdd', { courseDetails: route.params })
                        }}
                    >
                        <View style={styles.__plusCircle}>
                            <Text
                                style={styles.__plusText}>+</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                
            </ScrollView >
        </SafeAreaView>
    );
};

export default ImageContent;

const styles = StyleSheet.create({
    __documnetContainerBox: {
        paddingVertical: HeightWidth.getResWidth(20),
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: Colors.fullWhite,
        //backgroundColor:'green'
    },
    __container: {
        flex: 1,

    },
    __VerticleLine: {
        width: HeightWidth.getResWidth(3),
        height: HeightWidth.getResHeight(18),
        alignSelf: 'center',
        marginRight: HeightWidth.getResWidth(10)
    },
    __bodyContainer: {
        width: Dimensions.get('window').width,
        paddingVertical: HeightWidth.getResHeight(20),
        backgroundColor: Colors.fullWhite,
        paddingHorizontal: HeightWidth.getResWidth(10),
        minHeight: HeightWidth.getResHeight(900),
        position:'relative',
    },
    __titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#4C8BF5',
        borderBottomWidth: HeightWidth.getResHeight(2),
        paddingBottom: HeightWidth.getResHeight(20),
    },
    __chapterContainer: {
        flexDirection: 'row',
        width: '60%',
        paddingRight: HeightWidth.getResWidth(5)
    },
    chapterName: {
        fontSize: HeightWidth.getResFontSize(15),
        color: Colors.ChapterNameGold
    },
    __subjectContainer: {
        width: '40%',
        height: '100%'
    },
    __subjectBox: {
        //width: HeightWidth.getResWidth(100),
        //height: HeightWidth.getResWidth(22),
        backgroundColor: Colors.ChapterNameGold,
        borderRadius: HeightWidth.getResWidth(5),
        padding: HeightWidth.getResWidth(5),
        fontSize: HeightWidth.getResFontSize(18),
        textAlign: 'center'
    },
    __plusCircle: {
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width * 0.15,
        height: Dimensions.get('window').width * 0.15,
        // borderRadius: 50,
        // width: HeightWidth.getResWidth(50),
        // height: HeightWidth.getResHeight(50),
        backgroundColor: Colors.circlePlusGreen,
        alignItems: 'center',
        justifyContent: 'center',
    },
    __documnetContainer: {
        //width: '100%',
        paddingVertical: HeightWidth.getResHeight(20),
        flexWrap: 'wrap',
        flexDirection: 'row',
        minHeight: HeightWidth.getResHeight(700),
    },
    __documentBox: {
        marginVertical: HeightWidth.getResHeight(10),
        //width: HeightWidth.getResWidth(160),
        //marginRight:10,
        width: '100%',
        //backgroundColor:'red'
    },
    __countItem: {
        height: HeightWidth.getResHeight(50),
    },
    __countItemContent: {
        width: HeightWidth.getResWidth(80),
        minHeight: HeightWidth.getResHeight(20),
        backgroundColor: Colors.countItemDocumentColor,
        paddingVertical: HeightWidth.getResHeight(5),
        paddingHorizontal: HeightWidth.getResWidth(10),
        borderRadius: HeightWidth.getResWidth(5),
        //marginLeft: '75%',
        //justifyContent:'flex-end',
        left: '78%',
        marginBottom: HeightWidth.getResHeight(5),
        height: '100%'
    },
    __backIconButtonView: {
        marginLeft: HeightWidth.getResWidth(10),
        marginRight: HeightWidth.getResWidth(4),
        padding: HeightWidth.getResWidth(6),
        //justifyContent: 'center',
        //alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    __imageBox: {
        width: HeightWidth.getResWidth(160),
        minHeight: HeightWidth.getResHeight(160),
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: HeightWidth.getResWidth(10)
    },
    _documentUpdateStatusContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: HeightWidth.getResWidth(200)
    },
    _deleteDocumentContainer: {
        width: HeightWidth.getResWidth(300),
        height: HeightWidth.getResHeight(500),
        alignItems: 'center',
        justifyContent: 'space-around',
        //marginVertical: HeightWidth.getResHeight(20),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: HeightWidth.getResHeight(8),
        },
        shadowOpacity: 0.44,
        shadowRadius: 10.32,
        elevation: HeightWidth.getResHeight(16),
    },
    _deleteDocumentContent: {
        fontSize: HeightWidth.getResFontSize(25),
        fontWeight: '500',
        color: Colors.fullBlack,
        textAlign: 'center',
    },
    _renameDocumentContent: {
        fontSize: HeightWidth.getResFontSize(25),
        width: HeightWidth.getResWidth(300),
        fontWeight: '500',
        //backgroundColor:'red'
    },
    _deleteHorizontalLine: {
        // marginHorizontal:'30%',
        // marginVertical:'5%',
        // borderBottomWidth:2,
        // borderBottomColor:'rgba(76, 139, 245, 0.29)'
        borderBottomWidth: HeightWidth.getResHeight(2),
        borderBottomColor: 'rgba(76, 139, 245, 0.29)',
        width: HeightWidth.getResWidth(250)
    },
    __buttonCancel: {
        borderWidth: 1,
        borderColor: '#4C8BF5'
    },
    __yesDeleteContent: {
        fontSize: HeightWidth.getResFontSize(20),
        fontWeight: 'bold',
        color: '#4C8BF5'
    },
    __cancelBox: {
        flexDirection: 'row',
        marginVertical: HeightWidth.getResHeight(40),
        width: '100%',
        justifyContent: 'space-between'
    },
    __textInput: {
        //marginTop: HeightWidth.getResWidth(18),
        //alignSelf: "center",
        fontSize: HeightWidth.getResFontSize(25),
        fontWeight: '500',
        color: Colors.fullBlack,
    },
    __inputContainerStyle: {
        borderBottomColor: Colors.documentTypeColor,
        borderBottomWidth: HeightWidth.getResWidth(2),
        //paddingBottom: 5,
        fontSize: HeightWidth.getResFontSize(25),
        width: HeightWidth.getResWidth(300),
        fontWeight: '500',
        color: Colors.fullBlack,
    },
    __modalContainer:{
        borderColor:Colors.contentDocumentDisplayHeader,
        borderWidth:HeightWidth.getResWidth(3),
        //backgroundColor:'red',
        borderRadius:5
    },
    __plusTouchBox:{
         position:'absolute',
         zIndex:100,
         bottom:HeightWidth.getResWidth(10),
         right:HeightWidth.getResWidth(10)
    },
    __plusText:{
        color: '#fff',
         fontSize: HeightWidth.getResFontSize(25) 
    }
});
