// All content Type screen
import React, { useEffect, FunctionComponent, useState } from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  Text,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Alert,
  Image,
  ImageBackground,
} from 'react-native';
import {
  createChapterContent,
  fetchChapterContentUploadUrl,
} from '../../../../common/api/academics';
import ButtonUI from '../../../../components/UI/Button';
import DocumentPicker from 'react-native-document-picker';
import { Icon } from "react-native-elements";
import HeightWidth from '../../../../utility/HeightWidth';
import { Modal, ModalContent } from 'react-native-modals';
import { Colors, width, images } from '../../../../utility/utilities';
import ContentHeader from '../../../../components/Header/ContentHeader'
import TextInputUI from '../../../../components/UI/TextInput'

interface Props {
  navigation: any;
  route: any;
  boardname: string;
  classname: string;
  subjectname: string;
  chaptername: string;
}

const choosedItem = [1, 2]

const PdfContentAdd: FunctionComponent<Props> = ({ navigation, route }) => {
  useEffect(() => {
    //console.log('navigation-----', navigation);
    console.log(documentType, documentSize, contentname);

    if (documentType && documentSize && contentname) {
      //getDocumentUploadURL();
    }
  }, []);
  //console.log('routeeee------>', route);

  const [documentType, setDocumentType] = useState('');
  const [documentSize, setDocumentSize] = useState(null);
  const [contentname, setContentname] = useState('');
  const [getSaveStatus, setSaveStatus] = useState(false);
  //console.log(route.params,'---------------------------');

  const fileUploader = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      });
      // console.log(
      //   res.uri,
      //   res.type, // mime type
      //   res.name,
      //   res.size,
      // );
      setDocumentType(res.type);
      setDocumentSize(res.size);
      setContentname(res.name);
      console.log(documentType, documentSize, contentname);

      if (documentType && documentSize && contentname) {
        getDocumentUploadURL();
      }

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
        Alert.alert('Error', 'You have canceled uploading');
      } else {
        throw err;
      }
    }
  };

  const getDocumentUploadURL = async () => {
    try {
      const documentUploadURL = await fetchChapterContentUploadUrl({
        boardname: route.params.courseDetails.boardName,
        classname: route.params.courseDetails.className,
        subjectname: route.params.courseDetails.subjectName,
        chaptername: route.params.courseDetails.chapterName,
        contentlength: documentSize,
        contenttype: documentType,
      });
      //console.log('documentUploadURL----->', documentUploadURL);

      uploadDocument(documentUploadURL);
    } catch (error) {
      // if (error.response?.status === 401) {
      //   setRedirectTo('/login');
      // } else {
      //   alert('Service not available in this area');
      //   setPinCode('');
      // }

      console.log('Error----->Not getting documnet url', error);
    }
  };

  const uploadDocument = async (uploadDocDetails) => {
    try {
      console.log(uploadDocDetails, route.params.courseDetails, documentSize, documentType, contentname, uploadDocDetails.uuid, documentSize);
      const documentUpload = await createChapterContent({
        boardname: route.params.courseDetails.boardName,
        classname: route.params.courseDetails.className,
        subjectname: route.params.courseDetails.subjectName,
        chaptername: route.params.courseDetails.chapterName,
        contenttype: documentType,
        contentname: contentname,
        title: contentname,
        uuid: uploadDocDetails.uuid,
        contentlength: documentSize
      });
      //console.log('documentUpload----->', documentUpload);
    } catch (error) {
      //console.log('documentUpload-----=================================================================>');
      // if (error.response?.status === 401) {
      //   setRedirectTo('/login');
      // } else {
      //   alert('Service not available in this area');
      //   setPinCode('');
      // }
      //console.log('documentUpload-----=================================================================>');
      console.log('Error----->', error);
    }
  };

  return (
    <SafeAreaView style={styles.__container}>
      <ScrollView>
      <ContentHeader title={'Uploaded Documents'} rightContent={true} boardName={route.params.courseDetails.boardName} className={route.params.courseDetails.className} />

        <View style={styles.__bodyContainer}>
          <View style={styles.__titleContainer}>
            <View style={styles.__chapterContainer} >
              {/* <Image source={images.GoldVerticalLine} style={styles.__VerticleLine} /> */}
              <Text style={styles.__chapterName}>{route.params.courseDetails.chapterName}</Text>
            </View>
            <View style={styles.__subjectContainer}>
              <Text style={styles.__subjectBox}>{route.params.courseDetails.subjectName}</Text>
            </View>
          </View>
          <View style={styles.__uploadContainer}>
            <View>
              <ImageBackground source={images.flower_doc} style={styles._backgroundImage}>
                
              </ImageBackground>
              {/* <Image source={images.flower}  /> */}
            </View>
            <View><Text style={styles.__subContent}>You can select multiple files</Text></View>
            <View style={styles.__uploadBox}>
              <Icon
                iconStyle={{ textAlign: 'center' }}
                name="upload"
                color={Colors.fullWhite}
                size={35}
                type="font-awesome"
                onPress={() => fileUploader()}
              />
            </View>
            <Text style={styles.__uploadContent}>Select file to upload</Text>
          </View>
          {choosedItem.map((item, id) => (<View style={styles.__displayImageChooseContainer}>
            {/* <Text style={styles._renameDocumentContent}>
              Enter New Name
            </Text> */}
            <TextInputUI 
                                        //label={"Enter New Name"}
                                        //labelStyle={styles._renameDocumentContent}
                                        width="md"
                                        style={styles.__textInput}
                                        placeholder="Enter New Name"
                                        onChangeText={(text) => {
                                             //setStudentName(text)
                                             }}
                                        //value={''}
                                        placeholderTextColor={Colors.fullBlack}
                                        otherStyle={styles.__inputContainerStyle}
                                        />
            <Text style={styles._removeText}>
              -
            </Text>
          </View>))}

          <View style={styles.__buttonContainer}>
            <ButtonUI
              title="Cancel"
              width={HeightWidth.getResWidth(100)}
              backgroundColor="#fff"
              onPress={async () => {

                //console.log(getItemState,'----------------------------->');
              }}
              style={styles.__buttonCancel}
              borderRadius={5}
              textStyles={{
                color: '#000',
                fontSize: HeightWidth.getResFontSize(18),
                fontWeight: '400',
              }}
              otherStyle={{
                borderWidth: 2,
                borderColor: '#4C8BF5',
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,
                elevation: 3,
              }}
            />
            <ButtonUI
              title="Save"
              width={HeightWidth.getResWidth(100)}
              backgroundColor={Colors.contentDocumentDisplayHeader}
              onPress={async () => {
                setSaveStatus(true)
                //console.log(getItemState,'----------------------------->');
              }}
              style={styles.__buttonCancel}
              borderRadius={5}
              textStyles={{
                color: '#000',
                fontSize: HeightWidth.getResFontSize(18),
                fontWeight: '400',
              }}
              otherStyle={{
                borderWidth: 2,
                borderColor: '#4C8BF5',
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,
                elevation: 3,
              }}
            />
          </View>
        </View>
        {getSaveStatus && (
          <Modal
            visible={getSaveStatus}
            onTouchOutside={() => {
              setSaveStatus(false)
            }}
          >
            <ModalContent>
              <View style={styles.__pdfContainerResponse}>
                <View style={styles.__pdfBorderBox}>
                  <ImageBackground source={images.flower_correct} style={styles.__pdfResponseFlower}>
                   
                  </ImageBackground>
                </View>
                <Text style={styles.__addSuccessFulyContainer}>
                You have successfully added
                </Text>
                <ButtonUI
              title="Ok"
              width={HeightWidth.getResWidth(100)}
              backgroundColor={Colors.contentDocumentDisplayHeader}
              onPress={async () => {
                setSaveStatus(false)
                //console.log(getItemState,'----------------------------->');
              }}
              style={styles.__buttonCancel}
              borderRadius={5}
              textStyles={{
                color: Colors.fullWhite,
                fontSize: HeightWidth.getResFontSize(18),
                fontWeight: '400',
              }}
              otherStyle={{
                borderWidth: 2,
                borderColor: '#4C8BF5',
                marginVertical:10,
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,
                elevation: 3,
              }}
            />
              </View>
            </ModalContent>
          </Modal>
        )}
      </ScrollView>
      {/* <TouchableOpacity activeOpacity={0.5} onPress={() => fileUploader()}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 1,
            elevation: 5,
            backgroundColor: Colors.fullWhite,
            padding: HeightWidth.getResWidth(10),
          }}>
          <Text>Select file</Text>
        </View>
      </TouchableOpacity> */}

      {/* <View
        style={{justifyContent: 'center', alignItems: 'center', padding: 10}}>
        <ButtonUI
          title={'Upload'}
          backgroundColor={Colors.primaryBlue}
          borderRadius={5}
          elevation={5}
          textStyles={{fontSize: 17, color: Colors.fullWhite}}
          width={width / 3}
          height={HeightWidth.getResWidth(43)}
          onPress={() => {
            //navigation.navigate('PdfContentAdd')
        }}
          // disabled={loading}
          // activityIndicator={loading}
        />
      </View> */}
    </SafeAreaView>
  );
};

export default PdfContentAdd;

const styles = StyleSheet.create({
  __container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  __VerticleLine: {
    width: HeightWidth.getResWidth(3),
    height: HeightWidth.getResHeight(18),
    alignSelf: 'center',
    marginRight: HeightWidth.getResWidth(10)
  },
  __bodyContainer: {
    width: Dimensions.get('window').width,
    paddingVertical: HeightWidth.getResHeight(20),
    backgroundColor: Colors.fullWhite,
    paddingHorizontal: HeightWidth.getResWidth(10),
    minHeight: HeightWidth.getResHeight(800),
    //alignItems: 'flex-start'
    alignItems: 'center'
  },
  __titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#4C8BF5',
    borderBottomWidth: HeightWidth.getResWidth(2),
    paddingBottom: HeightWidth.getResHeight(20),
  },
  __chapterContainer: {
    flexDirection: 'row',
    width: '60%',
    paddingRight: HeightWidth.getResWidth(5)
  },
  __chapterName: {
    fontSize: HeightWidth.getResFontSize(15),
    color: Colors.ChapterNameGold
  },
  __subjectContainer: {
    width: '40%',
    height: '100%'
  },
  __subjectBox: {
    backgroundColor: Colors.ChapterNameGold,
    borderRadius: HeightWidth.getResWidth(5),
    padding: HeightWidth.getResWidth(5),
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'center'
  },
  __uploadContainer: {
    width: HeightWidth.getResWidth(350),
    minHeight: HeightWidth.getResHeight(600),
    //marginHorizontal:HeightWidth.getResWidth(50),
    //backgroundColor:'red',
    marginTop: HeightWidth.getResHeight(20),
    borderColor: Colors.contentDocumentDisplayHeader,
    borderWidth: HeightWidth.getResWidth(2),
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: HeightWidth.getResHeight(20),
    borderRadius: HeightWidth.getResWidth(10),
  },
  __uploadBox: {
    width: HeightWidth.getResWidth(200),
    minHeight: HeightWidth.getResHeight(70),
    backgroundColor: Colors.contentDocumentDisplayHeader,
    borderRadius: HeightWidth.getResWidth(10),
    alignItems: 'center',
    justifyContent: 'center'
  },
  __subContent: {
    fontSize: HeightWidth.getResFontSize(10),
    fontWeight: 'normal',
    color: Colors.fullBlack,
  },
  __uploadContent: {
    fontSize: HeightWidth.getResFontSize(15),
    fontWeight: 'bold',
    color: Colors.fullBlack,
  },
  _backgroundImage: {
    width: HeightWidth.getResWidth(300),
    minHeight: HeightWidth.getResHeight(400),
  },
  __pdfBox: {
    width: HeightWidth.getResWidth(130),
    minHeight: HeightWidth.getResHeight(40),
    borderWidth: HeightWidth.getResWidth(2),
    borderColor: Colors.fullBlack,
    borderRadius: HeightWidth.getResWidth(10),
    alignItems: 'center',
    justifyContent: 'center'
  },
  __pdfContent: {
    fontSize: HeightWidth.getResFontSize(25),
    fontWeight: 'bold',
    color: Colors.fullWhite,
  },
  __displayImageChooseContainer: {
    borderBottomColor: '#4C8BF5',
    borderBottomWidth: HeightWidth.getResHeight(2),
    paddingBottom: HeightWidth.getResWidth(5),
    width: '100%',
    marginTop: HeightWidth.getResHeight(40),
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  _renameDocumentContent: {
    fontSize: HeightWidth.getResFontSize(15),
    fontWeight: '500',
    color: Colors.fullBlack,
  },
  _removeText: {
    width: HeightWidth.getResWidth(25),
    minHeight: HeightWidth.getResHeight(25),
    borderRadius: HeightWidth.getResWidth(25),
    backgroundColor: Colors.lightBlue,
    textAlign: 'center',
    alignSelf: 'center'
  },
  __buttonCancel: {
    borderWidth: HeightWidth.getResWidth(1),
    borderColor: '#4C8BF5'
  },
  __buttonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: HeightWidth.getResHeight(40),
  },
  __addSuccessFulyContainer: {
    fontSize: HeightWidth.getResFontSize(15),
    fontWeight: '500',
    color:Colors.greenSave,
    paddingVertical: HeightWidth.getResHeight(5)
  },
  __textInput: {
    //marginTop: HeightWidth.getResWidth(18),
    //alignSelf: "center",
    fontSize: HeightWidth.getResFontSize(25),
    fontWeight: '500',
    color: Colors.fullBlack,
},
__inputContainerStyle: {
    fontSize: HeightWidth.getResFontSize(25),
    width: HeightWidth.getResWidth(300),
    fontWeight: '500',
    color: Colors.fullBlack,
},
__pdfContainerResponse:{
  justifyContent: 'space-between', 
  alignItems: 'center' 
},
__pdfBorderBox:{
  paddingBottom: HeightWidth.getResHeight(50), 
  borderBottomWidth: HeightWidth.getResHeight(2), 
  borderBottomColor: 'rgba(76, 139, 245, 0.29)' 
},
__pdfResponseFlower:{
  width:HeightWidth.getResWidth(200), 
  height:HeightWidth.getResHeight(200),
  alignItems:'center',
  justifyContent:'center' 
}
});
