// All content Type screen
import React, { useEffect, FunctionComponent, useState } from 'react';
import {
    StyleSheet,
    View,
    SafeAreaView,
    Text,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    Alert,
    Image,
    ImageBackground,
} from 'react-native';
import {
    createChapterContent,
    fetchChapterContentUploadUrl,
} from '../../../../common/api/academics';
import ButtonUI from '../../../../components/UI/Button';
import DocumentPicker from 'react-native-document-picker';
import { Icon } from "react-native-elements";
import HeightWidth from '../../../../utility/HeightWidth';
import { Modal, ModalContent } from 'react-native-modals';
import { Colors, width, images } from '../../../../utility/utilities';
import ContentHeader from '../../../../components/Header/ContentHeader'

interface Props {
    navigation: any;
    route: any;
    boardname: string;
    classname: string;
    subjectname: string;
    chaptername: string;
}

const choosedItem = [1, 2]

const ImageContentAdd: FunctionComponent<Props> = ({ navigation, route }) => {
    useEffect(() => {
        //console.log('navigation-----', navigation);
        console.log(documentType, documentSize, contentname);

        if (documentType && documentSize && contentname) {
            //getDocumentUploadURL();
        }
    }, []);
    //console.log('routeeee------>', route);

    const [documentType, setDocumentType] = useState('');
    const [documentSize, setDocumentSize] = useState(null);
    const [contentname, setContentname] = useState('');
    const [getSaveStatus, setSaveStatus] = useState(false);
    //console.log(route.params,'---------------------------');

    const fileUploader = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf],
            });
            setDocumentType(res.type);
            setDocumentSize(res.size);
            setContentname(res.name);
            console.log(documentType, documentSize, contentname);

            if (documentType && documentSize && contentname) {
                getDocumentUploadURL();
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
                Alert.alert('Error', 'You have canceled uploading');
            } else {
                throw err;
            }
        }
    };

    const getDocumentUploadURL = async () => {
        try {
            const documentUploadURL = await fetchChapterContentUploadUrl({
                boardname: route.params.courseDetails.boardName,
                classname: route.params.courseDetails.className,
                subjectname: route.params.courseDetails.subjectName,
                chaptername: route.params.courseDetails.chapterName,
                contentlength: documentSize,
                contenttype: documentType,
            });
            uploadDocument(documentUploadURL);
        } catch (error) {

            console.log('Error----->Not getting documnet url', error);
        }
    };

    const documentTypeNumber = ['cbsc_4', 'cbsc_1', 'cbsc_2', 'cbsc_3', 'cbsc_4']

    const uploadDocument = async (uploadDocDetails) => {
        try {
            console.log(uploadDocDetails, route.params.courseDetails, documentSize, documentType, contentname, uploadDocDetails.uuid, documentSize);
            const documentUpload = await createChapterContent({
                boardname: route.params.courseDetails.boardName,
                classname: route.params.courseDetails.className,
                subjectname: route.params.courseDetails.subjectName,
                chaptername: route.params.courseDetails.chapterName,
                contenttype: documentType,
                contentname: contentname,
                title: contentname,
                uuid: uploadDocDetails.uuid,
                contentlength: documentSize
            });
        } catch (error) {
            console.log('Error----->', error);
        }
    };

    const getUploadedDocument = (item, id) => (
        <TouchableOpacity
            style={styles.__imageTouchBox}
            onPress={() => {
                //setDocumentUpdateStatus(true);
            }}
        >
            <View style={styles.__menuBox}>
                <Text>{item}</Text>
                <Icon
                    iconStyle={{}}
                    name="ellipsis-v"
                    color={'black'}
                    size={15}
                    type="font-awesome"
                />
            </View>
            <ImageBackground source={images.cbsc_1} style={styles.__imageBox}>

            </ImageBackground>

        </TouchableOpacity>
    )

    return (
        <SafeAreaView style={styles.__container}>
            <ScrollView>
                <ContentHeader title={'Uploaded Documents'} rightContent={true} boardName={route.params.courseDetails.boardName} className={route.params.courseDetails.className} />

                <View style={styles.__bodyContainer}>
                    <View style={styles.__titleContainer}>
                        <View style={styles.__chapterContainer} >
                            {/* <Image source={images.GoldVerticalLine} style={styles.__VerticleLine} /> */}
                            <Text style={styles.chapterName}>{route.params.courseDetails.chapterName}</Text>
                        </View>
                        <View style={styles.__subjectContainer}>
                            <Text style={styles.__subjectBox}>{route.params.courseDetails.subjectName}</Text>
                        </View>
                    </View>
                    {documentTypeNumber.length > 0 && (<View style={styles.__documnetContainerBox}>
                        {documentTypeNumber.map((item, id) => getUploadedDocument(item, id))}

                    </View>)}
                    <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <TouchableOpacity style={styles.__arrowIcon} onPress={() => {
                            navigation.navigate('ImageContent', { courseDetails: route.params })
                        }}><Icon
                            iconStyle={{}}
                            name="long-arrow-left"
                            color={Colors.fullWhite}
                            size={25}
                            type="font-awesome"
                        /></TouchableOpacity>
                        <TouchableOpacity style={styles.__arrowIcon}
                        onPress={() => {
                            navigation.navigate('ImageContentAdd', { courseDetails: route.params })
                        }}><Icon
                            iconStyle={{}}
                            name="long-arrow-right"
                            color={Colors.fullWhite}
                            size={25}
                            type="font-awesome"
                        /></TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

export default ImageContentAdd;

const styles = StyleSheet.create({
    __container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    __VerticleLine: {
        width: HeightWidth.getResWidth(3),
        height: HeightWidth.getResHeight(18),
        alignSelf: 'center',
        marginRight: HeightWidth.getResWidth(10)
    },
    __bodyContainer: {
        width: Dimensions.get('window').width,
        paddingVertical: HeightWidth.getResHeight(20),
        backgroundColor: Colors.fullWhite,
        paddingHorizontal: HeightWidth.getResWidth(10),
        minHeight: HeightWidth.getResHeight(800),
        //alignItems: 'flex-start'
        alignItems: 'center'
    },
    __titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#4C8BF5',
        borderBottomWidth: HeightWidth.getResHeight(2),
        paddingBottom: HeightWidth.getResHeight(20),
    },
    __chapterContainer: {
        flexDirection: 'row',
        width: '60%',
        paddingRight: HeightWidth.getResWidth(5)
    },
    chapterName: {
        fontSize: HeightWidth.getResFontSize(15),
        color: Colors.ChapterNameGold
    },
    __subjectContainer: {
        width: '40%',
        height: '100%'
    },
    __subjectBox: {
        backgroundColor: Colors.ChapterNameGold,
        borderRadius: HeightWidth.getResWidth(5),
        padding: HeightWidth.getResHeight(5),
        fontSize: HeightWidth.getResFontSize(18),
        textAlign: 'center'
    },
    __documnetContainerBox: {
        paddingVertical: HeightWidth.getResHeight(20),
        flexWrap: 'wrap',
        //flexDirection: 'row',
        //justifyContent: 'space-between',
        backgroundColor: Colors.fullWhite,
        //backgroundColor:'green'
        width: '100%',
    },
    __imageBox: {
        width: '100%',
        minHeight: HeightWidth.getResHeight(160),
    },
    __menuBox: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingVertical: HeightWidth.getResHeight(10),
    },
    __arrowIcon: {
        width: HeightWidth.getResWidth(50),
        minHeight: HeightWidth.getResHeight(50),
        borderRadius: HeightWidth.getResWidth(100),
        backgroundColor: Colors.arrowCircleContentColor,
        justifyContent:'center',
        marginRight:HeightWidth.getResWidth(10)
    },
    __imageTouchBox:{
        flexDirection: 'column',
         marginBottom: HeightWidth.getResHeight(20),
          width: '100%', 
    }
});
