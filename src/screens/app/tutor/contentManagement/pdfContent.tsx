// All content Type screen
import React, { useEffect, FunctionComponent, useState } from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Image,
  Text
} from 'react-native';
import {
  fetchContentDetails,
  fetchContents,
} from '../../../../common/api/academics';
import ButtonUI from '../../../../components/UI/Button';
import { Icon } from "react-native-elements";
import HeightWidth from '../../../../utility/HeightWidth';
import { Colors, width, images } from '../../../../utility/utilities';
import { Modal, ModalContent } from 'react-native-modals';
import ContentHeader from '../../../../components/Header/ContentHeader'
import TextInputUI from '../../../../components/UI/TextInput'
interface Props {
  navigation: any;
  route: any;
}

const documentTypeNumber = ['Documents', 'Images', 'Videos', 'Links', 'Quiz']


const PdfContent: FunctionComponent<Props> = ({ navigation, route }) => {
  useEffect(() => {
    getContent();
  }, []);

  const [contentDetails, setContentDetails] = useState([]);
  const [documentUpdateStatus, setDocumentUpdateStatus] = useState(false);
  const [deleteDocument, setDeleteDocument] = useState(false);
  const [renameDocument, setRenameDocument] = useState(false);
  const getContent = async () => {
    try {
      const getCourseList = await fetchContents({
        boardname: route.params.boardName,
        classname: route.params.className,
        subjectname: route.params.subjectName,
        chaptername: route.params.chapterName,
      });
      await setContentDetails(getCourseList);

      //getContentDetails();
    } catch (error) {
    }
  };

  const getContentDetails = async () => {
    try {
      console.log(route.params, '==============================>', contentDetails);

      const getEachContentDetails = await fetchContentDetails({
        boardname: route.params.boardName,
        classname: route.params.className,
        subjectname: route.params.subjectName,
        chaptername: route.params.chapterName,
        contentname: contentDetails[0].contentname,
      });
      //console.log('getEachContentDetails----->', getEachContentDetails);
    } catch (error) {
      console.log('Error----->', error);
    }
  };

  const getUploadedDocument = (item, id) => (
    <TouchableOpacity
      style={styles.__touchDocument}
      onPress={() => {
        setDocumentUpdateStatus(true);
      }}
    >
      <View style={styles.__documnetTypeCircle}>
        <Text
          style={styles.__pdfText}>Pdf
        </Text>
      </View>

      <View style={styles.__documentBoxType}>
        <Text style={styles.__documentFileName}>Document Name</Text>
        <Icon
          iconStyle={{ width: 10, height: '100%' }}
          name="ellipsis-v"
          color={'black'}
          size={15}
          type="font-awesome"
        />
      </View>
    </TouchableOpacity>
  )

  return (
    <SafeAreaView style={styles.__container}>
      <ScrollView>
        <ContentHeader title={'Uploaded Documents'} rightContent={true} boardName={route.params.boardName} className={route.params.className} />
        <View style={styles.__bodyContainer}>
          <View style={styles.__titleContainer}>
            <View style={styles.__chapterContainer} >
              {/* <Image source={images.GoldVerticalLine} style={styles.__VerticleLine} /> */}
              <Text style={styles.__chapterName}>{route.params.chapterName}</Text>
            </View>
            <View style={styles.__subjectContainer}>
              <Text style={styles.__subjectBox}>{route.params.subjectName}</Text>
            </View>
          </View>

          <View style={styles.__documnetContainer}>
            <View style={styles.__documentBox}>
              <View style={styles.__countItem}>
                <Text style={styles.__countItemContent}>{contentDetails.length} Items</Text>
              </View>
              {contentDetails.length > 0 && (<View style={styles.__documnetContainerBox}>
                {contentDetails.map((item, id) => getUploadedDocument(item, id))}

              </View>)}
            </View>
            {documentUpdateStatus && !deleteDocument && (

              <Modal
                visible={documentUpdateStatus}
                onTouchOutside={() => {
                  setDocumentUpdateStatus(false)
                }}
              >
                <ModalContent  style={styles.__modalContainer}>
                  <View style={styles._documentUpdateStatusContainer}>
                    <Icon
                      iconStyle={styles.__editIcon}
                      name="edit"
                      color={'black'}
                      size={15}
                      type="font-awesome"
                      onPress={() => {
                        setRenameDocument(true)
                      }}
                    />
                    <Icon
                      iconStyle={styles.__editIcon}
                      name="trash"
                      color={'black'}
                      size={15}
                      type="font-awesome"
                      onPress={() => {
                        setDeleteDocument(true)
                      }}
                    />
                    <Icon
                      iconStyle={styles.__editIcon}
                      name="share"
                      color={'black'}
                      size={15}
                      type="font-awesome"
                    />
                  </View>
                </ModalContent>
              </Modal>
            )}

            {deleteDocument && (

              <Modal
                visible={deleteDocument}
                onTouchOutside={() => {
                  setDeleteDocument(false)
                }}
              >
                <ModalContent  style={styles.__modalContainer}>
                  <View style={styles._deleteDocumentContainer}>
                    <Text style={styles._deleteDocumentContent}>
                      Are you sure you want to delete?
                     </Text>
                    <Text style={styles._deleteHorizontalLine}>
                    </Text>
                    <View style={styles.__cancelBox}>
                      <ButtonUI
                        title="Cancel"
                        width={HeightWidth.getResWidth(100)}
                        backgroundColor="#fff"
                        onPress={async () => {

                          //console.log(getItemState,'----------------------------->');
                        }}
                        style={styles.__buttonCancel}
                        borderRadius={5}
                        textStyles={{
                          color: '#000',
                          fontSize: HeightWidth.getResFontSize(18),
                          fontWeight: '400',
                        }}
                        otherStyle={styles.__cancelButton}
                      />
                      <Text style={styles.__yesDeleteContent}>Yes, Delete</Text>
                    </View>

                  </View>
                </ModalContent>
              </Modal>
            )}

            {renameDocument && (
              <Modal
                visible={renameDocument}
                onTouchOutside={() => {
                  setRenameDocument(false)
                }}
              >
                <ModalContent  style={styles.__modalContainer}>
                  <View style={styles._deleteDocumentContainer}>
                  <TextInputUI 
                                        //label={"Enter New Name"}
                                        //labelStyle={styles._renameDocumentContent}
                                        width="md"
                                        style={styles.__textInput}
                                        placeholder="Enter New Name"
                                        onChangeText={(text) => {
                                             //setStudentName(text)
                                             }}
                                        //value={''}
                                        placeholderTextColor={Colors.fullBlack}
                                        otherStyle={styles.__inputContainerStyle}
                                        />
                    <View style={styles.__cancelBox}>
                      <ButtonUI
                        title="Cancel"
                        width={HeightWidth.getResWidth(100)}
                        backgroundColor="#fff"
                        onPress={async () => {

                          //console.log(getItemState,'----------------------------->');
                        }}
                        style={styles.__buttonCancel}
                        borderRadius={5}
                        textStyles={{
                          color: '#000',
                          fontSize: HeightWidth.getResFontSize(18),
                          fontWeight: '400',
                        }}
                        otherStyle={{
                          borderWidth: 2,
                          borderColor: '#4C8BF5',
                          shadowColor: "#000",
                          shadowOffset: {
                            width: 0,
                            height: 1,
                          },
                          shadowOpacity: 0.22,
                          shadowRadius: 2.22,
                          elevation: 3,
                        }}
                      />
                      <ButtonUI
                        title="Save"
                        width={HeightWidth.getResWidth(100)}
                        backgroundColor={Colors.contentDocumentDisplayHeader}
                        onPress={async () => {
                          navigation.navigate('PdfContent', { courseDetails: route.params })
                          //console.log(getItemState,'----------------------------->');
                        }}
                        style={styles.__buttonCancel}
                        borderRadius={5}
                        textStyles={styles.__saveButtonText}
                        otherStyle={styles.__cancelButton}
                      />
                    </View>

                  </View>
                </ModalContent>
              </Modal>
            )}             
          </View>
         
          <TouchableOpacity style={styles.__touchBox}
            onPress={() => {
              navigation.navigate('PdfContentAdd', { courseDetails: route.params })
            }}
          >
            <View style={styles.__plusCircle}>
              <Text
                style={styles.__plusText}>+</Text>
            </View>
          </TouchableOpacity>
        </View>



        {/* <View>
          <FlatList
            keyExtractor={(_: any, index: number) => index.toString()}
            data={contentDetails}
            renderItem={({ item, index }) => {
              return (
                <View
                  style={{
                    padding: HeightWidth.getResWidth(10),
                    margin: HeightWidth.getResWidth(10),
                    backgroundColor: Colors.screenBG,
                  }}>
                  <TouchableOpacity
                    activeOpacity={0.5}
                    key={index}
                    onPress={() =>
                      alert('onclick pdf will get open on seprate screen')
                    }
                    style={{}}>
                    <Text numberOfLines={1} style={{}}>
                      {item.contentname}
                    </Text>
                  </TouchableOpacity>
                </View>
              );
            }}
          // style={styles.__flatList}
          />
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            padding: HeightWidth.getResWidth(10),
          }}>
          <ButtonUI
            title={'Add New Pdf'}
            backgroundColor={Colors.primaryBlue}
            borderRadius={5}
            elevation={5}
            textStyles={{ fontSize: 17, color: Colors.fullWhite }}
            width={width / 3}
            height={HeightWidth.getResWidth(43)}
            onPress={() =>
              navigation.navigate('PdfContentAdd', { courseDetails: route.params })
            }
          // disabled={loading}
          // activityIndicator={loading}
          />
        </View> */}
      </ScrollView>
    </SafeAreaView>
  );
};

export default PdfContent;

const styles = StyleSheet.create({
  __container: {
    flex: 1,
  },
  __VerticleLine: {
    width: HeightWidth.getResWidth(3),
    height: HeightWidth.getResHeight(18),
    alignSelf: 'center',
    marginRight: HeightWidth.getResWidth(10)
  },
  __bodyContainer: {
    width: Dimensions.get('window').width,
    paddingVertical: HeightWidth.getResHeight(20),
    backgroundColor: Colors.fullWhite,
    paddingHorizontal: HeightWidth.getResWidth(10),
    height: HeightWidth.getResHeight(700),
    position:'relative'
  },
  __titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#4C8BF5',
    borderBottomWidth: HeightWidth.getResHeight(2),
    paddingBottom: HeightWidth.getResHeight(20),
  },
  __chapterContainer: {
    flexDirection: 'row',
    width: '60%',
    paddingRight: HeightWidth.getResWidth(5)
  },
  __chapterName: {
    fontSize: HeightWidth.getResFontSize(15),
    color: Colors.ChapterNameGold
  },
  __subjectContainer: {
    width: '40%',
    height: '100%'
  },
  __subjectBox: {
    backgroundColor: Colors.ChapterNameGold,
    borderRadius: HeightWidth.getResWidth(5),
    padding: HeightWidth.getResWidth(5),
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'center'
  },
  __documnetContainer: {
    //width: '100%',
    paddingVertical: HeightWidth.getResHeight(20),
    flexWrap: 'wrap',
    flexDirection: 'row',
    minHeight: HeightWidth.getResHeight(700),
  },
  __documnetContainerBox: {
    paddingVertical: HeightWidth.getResHeight(20),
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: Colors.fullWhite,
    //backgroundColor:'green'
  },
  __documentBox: {
    marginVertical: HeightWidth.getResHeight(10),
    width: '100%',
    //backgroundColor:'red'
  },
  __countItem: {

    height: HeightWidth.getResHeight(50),
    //float:'right',
    //backgroundColor:'yellow',
    //right:0,
    //alignItems:'baseline'

  },
  __countItemContent: {
    width: HeightWidth.getResWidth(80),
    minHeight: HeightWidth.getResHeight(20),
    backgroundColor: Colors.countItemDocumentColor,
    paddingVertical: HeightWidth.getResHeight(5),
    paddingHorizontal: HeightWidth.getResWidth(10),
    borderRadius: HeightWidth.getResWidth(5),
    //marginLeft: '75%',
    //justifyContent:'flex-end',
    left: '78%',
    marginBottom: HeightWidth.getResHeight(5),
    height: '100%'
  },
  __documnetTypeCircle: {
    borderRadius: HeightWidth.getResWidth(40),
    width: HeightWidth.getResWidth(50),
    height: '100%',
    backgroundColor: Colors.documentTypeColor,
    //textAlign:'center',
    //alignSelf:'center',
    //color:'#fff',
    //top:'50%'
    alignItems: 'center',
    //paddingTop:10
    justifyContent: 'center',
  },
  __plusCircle: {
    borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width * 0.15,
        height: Dimensions.get('window').width * 0.15,
    backgroundColor: Colors.circlePlusGreen,
    //textAlign:'center',
    //alignSelf:'center',
    //color:'#fff',
    //top:'50%'
    alignItems: 'center',
    //paddingTop:10
    justifyContent: 'center',
  },
  __documentBoxType: {
    borderColor: Colors.documentTypeColor,
    borderWidth: HeightWidth.getResWidth(1),
    height: '100%',
    //width: HeightWidth.getResWidth(325),
    //width:'max-content',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: HeightWidth.getResWidth(20),
    borderBottomLeftRadius: HeightWidth.getResWidth(20),
    flexDirection: 'row',
    left: -15,
    padding: HeightWidth.getResWidth(5),
    paddingLeft: HeightWidth.getResWidth(20)
  },
  __documentFileName: {
    fontWeight: '400',
    fontSize: HeightWidth.getResFontSize(13),
    paddingLeft: 5,
    //width:'100%'
    width: HeightWidth.getResWidth(87)
  },
  _documentUpdateStatusContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: HeightWidth.getResWidth(200)
  },
  _deleteDocumentContainer: {
    width: HeightWidth.getResWidth(300),
    height: HeightWidth.getResHeight(500),
    alignItems: 'center',
    justifyContent: 'space-around',
    //marginVertical: HeightWidth.getResHeight(20),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: HeightWidth.getResHeight(8),
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: HeightWidth.getResHeight(16),
  },
  _deleteDocumentContent: {
    fontSize: HeightWidth.getResFontSize(25),
    fontWeight: '500',
    color: Colors.fullBlack,
    textAlign: 'center',
  },
  _renameDocumentContent: {
    fontSize: HeightWidth.getResFontSize(25),
    width: HeightWidth.getResWidth(300),
    fontWeight: '500',
    color: Colors.fullBlack,
    //textAlign: 'center',  
    borderBottomColor: '#4C8BF5',
    borderBottomWidth: HeightWidth.getResHeight(2),
    paddingBottom: HeightWidth.getResHeight(5)
  },
  _deleteHorizontalLine: {
    // marginHorizontal:'30%',
    // marginVertical:'5%',
    // borderBottomWidth:2,
    // borderBottomColor:'rgba(76, 139, 245, 0.29)'
    borderBottomWidth: HeightWidth.getResHeight(2),
    borderBottomColor: 'rgba(76, 139, 245, 0.29)',
    width: HeightWidth.getResWidth(250)
  },
  __buttonCancel: {
    borderWidth: HeightWidth.getResWidth(1),
    borderColor: '#4C8BF5'
  },
  __yesDeleteContent: {
    fontSize: HeightWidth.getResFontSize(20),
    fontWeight: 'bold',
    color: '#4C8BF5'
  },
  __cancelBox: {
    flexDirection: 'row',
    marginVertical: HeightWidth.getResHeight(40),
    width: '100%',
    justifyContent: 'space-between'
  },
  __textInput: {
    marginTop: HeightWidth.getResHeight(18),
    alignSelf: "center",
    fontSize: HeightWidth.getResFontSize(25),
    fontWeight: '500',
    color: Colors.fullBlack,
},
__inputContainerStyle: {
    borderBottomColor: Colors.documentTypeColor,
    borderBottomWidth: HeightWidth.getResWidth(2),
    paddingBottom: HeightWidth.getResHeight(5),
    fontSize: HeightWidth.getResFontSize(25),
    width: HeightWidth.getResWidth(300),
    fontWeight: '500',
    color: Colors.fullBlack,
},

    __modalContainer:{
        borderColor:Colors.contentDocumentDisplayHeader,
        borderWidth: HeightWidth.getResWidth(3),
        //backgroundColor:'red',
        borderRadius:5
    },
    __touchDocument:{
      flexDirection: 'row',
       marginBottom: HeightWidth.getResWidth(20), 
       maxHeight: HeightWidth.getResHeight(100), 
       height: HeightWidth.getResHeight(60)
    },
    __pdfText:{
      color: '#fff', 
      fontSize: HeightWidth.getResFontSize(18) 
    },
    __editIcon:{
      textAlign: 'center',
       width: HeightWidth.getResWidth(30),
        height: HeightWidth.getResHeight(30)
    },
    __cancelButton:{
      borderWidth: HeightWidth.getResWidth(2),
      borderColor: '#4C8BF5',
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: HeightWidth.getResHeight(1),
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      elevation: HeightWidth.getResHeight(3),
    },
    __saveButtonText:{
      color: '#fff',
      fontSize: HeightWidth.getResFontSize(18),
      fontWeight: '400',
    },
    __touchBox:{
      position:'absolute',
      right:HeightWidth.getResWidth(10),
      bottom:HeightWidth.getResWidth(10)
    },
    __plusText:{
      color: '#fff', 
      fontSize: HeightWidth.getResFontSize(25) 
    }
});
