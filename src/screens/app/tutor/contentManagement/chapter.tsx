import React, { useEffect, useState } from 'react'
import { StyleSheet, TouchableOpacity, Text, View, SafeAreaView, FlatList, ToastAndroid, ActivityIndicator } from 'react-native'

//components
import BaseHeader from '../../../../components/Header/BaseHeader';
import CustomStatusBar from '../../../../components/Header/custom_status_bar';
import ContentCard from '../../../../components/Card/ContentCard';

//utilities
import { width, Colors, images } from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';
import { fetchCourseChaptersList } from '../../../../common/api/academics';

interface Props {
    navigation: any;
    route: any;
}

interface Courses {
    board: string;
    className: string;
    subject: string;
}

const Chapters = ({ navigation, route }: Props) => {

    useEffect(() => {
        getChapters();
    }, [])

    const [chapterList, setChapterList] = useState([]);
    const [loading, setLoading] = useState(false);

    const getChapters = async () => {
        let { courseContent } = route.params;
        setLoading(true);
        try {
            //console.log(courseContent, '--------------------------');

            const getChapterList = await fetchCourseChaptersList(courseContent);
            await setChapterList(getChapterList.masterChapters)
            setLoading(false);
            //console.log("Chapter List------------", getChapterList);
        } catch (error) {
            if (error.response?.status === 401) {
                ToastAndroid.show("Login Again", ToastAndroid.LONG);
                setLoading(false);
            }
        }
    }

    const renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity>
                <ContentCard
                    title={item.chapter.name}
                    backgroundColor={Colors.primaryBlue}
                    textColor={Colors.fullWhite}
                    contentHeight={HeightWidth.getResWidth(70)}
                    showRightIcon={true}
                    leftIconName={images.content}
                    onPress={
                        () =>{
                    //console.log(item.board, item,'------------',index)
                    navigation.navigate('AllContentType', {
                      boardName: item.boardName,
                      className: item.className,
                      subjectName: item.subjectName,
                      chapterName: item.chapter.name,
                    })
                    }}
                />
            </TouchableOpacity>
        )
    }

    return (
        <SafeAreaView style={styles.__container}>
            <CustomStatusBar
                backgroundColor={Colors.primaryBlue}
                barStyle="light-content"
            />
            <BaseHeader
                onPress={() => {
                    navigation.goBack();
                }}
                title={'Chapters'}
                onMenuPress={() => {
                    navigation.openDrawer();
                }}
            />
            {!loading ? <View style={styles.__innerContainer} >
                <FlatList
                    data={chapterList}
                    keyExtractor={(_, index) => index.toString()}
                    renderItem={renderItem}
                />
            </View> : <View style={styles.__activityIndicator}>
                <ActivityIndicator size={"large"} color={Colors.primaryBlue} />
            </View>}
        </SafeAreaView >
    )
}

export default Chapters;

const styles = StyleSheet.create({
    __container: {
        flex: 1,
        backgroundColor: Colors.fullWhite
    },
    __innerContainer: {
        paddingTop: HeightWidth.getResWidth(10),
        flex: 1
    },
    __activityIndicator: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1
    },
})
