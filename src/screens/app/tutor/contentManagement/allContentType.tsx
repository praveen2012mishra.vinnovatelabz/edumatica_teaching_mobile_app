// All content Type screen
import React, { useEffect, FunctionComponent } from 'react';
import { StyleSheet, Image, View, SafeAreaView, TouchableOpacity, ScrollView, Text, Dimensions } from 'react-native';
import {
  fetchContentDetails,
  fetchContents,
} from '../../../../common/api/academics';
import ButtonUI from '../../../../components/UI/Button';
import Icon from 'react-native-vector-icons/Feather';
import { Colors, width, images } from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';

interface Props {
  navigation: any;
  route: any;
}

const documentTypeNumber = ['Documents', 'Images', 'Videos', 'Links', 'Quiz']

const AllContentType: FunctionComponent<Props> = ({ navigation, route }) => {
  useEffect(() => {
    // getCourses();
    //console.log('navigation-----', navigation);
  }, []);

  const getNavigationContentType =(type)=>{
    if(type=='Documents'){
      navigation.navigate('PdfContent', {
        boardName: route.params.boardName,
        className: route.params.className,
        subjectName: route.params.subjectName,
        chapterName: route.params.chapterName,
      })
    }
    if(type=='Images'){
      navigation.navigate('ImageContent', {
        boardName: route.params.boardName,
        className: route.params.className,
        subjectName: route.params.subjectName,
        chapterName: route.params.chapterName,
      })
    }
    if(type=='Videos'){
      navigation.navigate('VideoContent', {
        boardName: route.params.boardName,
        className: route.params.className,
        subjectName: route.params.subjectName,
        chapterName: route.params.chapterName,
      })
    }
  }

  const documentsDisplay = (item, id) => (
    <View style={styles.__documentBox} key={id}>
      <View style={styles.__countItem}>
        <Text style={styles.__countItemContent}>2 Items</Text>
      </View>
      <TouchableOpacity style={styles.__documentTypeContainer}
        onPress={() =>getNavigationContentType(item)}
      >
        <Text style={styles.__documentType}>{item}</Text>
        <Text style={styles.__sharedFileName}>Shared with A1, B1, B4</Text>
      </TouchableOpacity>
    </View>
  )

  return (
    <SafeAreaView style={styles.__container}>
      <ScrollView>
        <View style={styles.__documentDisplayHeaderContainer}>
          <View style={styles.__contentHeaderContainer}>
          <Icon name={'arrow-left'} color={Colors.fullWhite} size={30} />
            <Image source={images.VerticalLine} style={styles.__LargeVerticleLine} />
            <Text style={styles.__headerContent}>{route.params.boardName}</Text>
            <Image source={images.VerticalLine} style={styles.VerticleLine} />
            <Text style={styles.__headerContent}>{route.params.className}</Text>
          </View>
          <View style={styles.__spaceHeaderContainer}></View>
        </View>
        <View style={styles.__bodyContainer}>
          <View style={styles.__titleContainer}>          
            <View style={styles.__chapterContainer} > 
            {/* <Image source={images.GoldVerticalLine} style={styles.VerticleLine} />              */}
              <Text style={styles.chapterName}>{route.params.chapterName}</Text>
            </View>
            <View style={styles.__subjectContainer}>
              <Text style={styles.__subjectBox}>{route.params.subjectName}</Text>
            </View>
          </View>
          <View style={styles.__documnetContainer}>
            {documentTypeNumber.map((item, id) => documentsDisplay(item, id))}

          </View>
        </View>

        {/* <View
          style={{ justifyContent: 'center', alignItems: 'center', padding: 10 }}>
          <ButtonUI
            title={'All Pdf'}
            backgroundColor={Colors.fullWhite}
            borderRadius={5}
            elevation={5}
            textStyles={{ fontSize: 17, color: Colors.primaryBlue }}
            width={width - 20}
            height={HeightWidth.getResWidth(43)}
            onPress={() =>
              navigation.navigate('PdfContent', {
                boardName: route.params.boardName,
                className: route.params.className,
                subjectName: route.params.subjectName,
                chapterName: route.params.chapterName,
              })
            }
          // disabled={loading}
          // activityIndicator={loading}
          />
        </View>
        <View
          style={{ justifyContent: 'center', alignItems: 'center', padding: 10 }}>
          <ButtonUI
            title={'All docs'}
            backgroundColor={Colors.fullWhite}
            borderRadius={5}
            elevation={5}
            textStyles={{ fontSize: 17, color: Colors.primaryBlue }}
            width={width - 20}
            height={HeightWidth.getResWidth(43)}
            onPress={() => navigation.navigate('')}
          // disabled={loading}
          // activityIndicator={loading}
          />
        </View>
        <View
          style={{ justifyContent: 'center', alignItems: 'center', padding: 10 }}>
          <ButtonUI
            title={'All images'}
            backgroundColor={Colors.fullWhite}
            borderRadius={5}
            elevation={5}
            textStyles={{ fontSize: 17, color: Colors.primaryBlue }}
            width={width - 20}
            height={HeightWidth.getResWidth(43)}
            onPress={() => navigation.navigate('')}
          // disabled={loading}
          // activityIndicator={loading}
          />
        </View>
        <View
          style={{ justifyContent: 'center', alignItems: 'center', padding: 10 }}>
          <ButtonUI
            title={'embdded links'}
            backgroundColor={Colors.fullWhite}
            borderRadius={5}
            elevation={5}
            textStyles={{ fontSize: 17, color: Colors.primaryBlue }}
            width={width - 20}
            height={HeightWidth.getResWidth(43)}
            onPress={() => navigation.navigate('')}
          // disabled={loading}
          // activityIndicator={loading}
          />
        </View>
        <View
          style={{ justifyContent: 'center', alignItems: 'center', padding: 10 }}>
          <ButtonUI
            title={'Quiz'}
            backgroundColor={Colors.fullWhite}
            borderRadius={5}
            elevation={5}
            textStyles={{ fontSize: 17, color: Colors.primaryBlue }}
            width={width - 20}
            height={HeightWidth.getResWidth(43)}
            onPress={() => navigation.navigate('')}
          // disabled={loading}
          // activityIndicator={loading}
          />
        </View> */}
      </ScrollView>
    </SafeAreaView>
  );
};

export default AllContentType;

const styles = StyleSheet.create({
  __sharedFileName: {
    fontWeight: '400',
    fontSize: HeightWidth.getResFontSize(12),
  },
  __documentType: {
    backgroundColor: Colors.documentTypeColor,
    borderRadius: 5,
    fontSize: HeightWidth.getResFontSize(16),
    width: HeightWidth.getResWidth(110),
    paddingHorizontal:HeightWidth.getResWidth(10),
    paddingVertical:HeightWidth.getResHeight(5),
    marginBottom: HeightWidth.getResHeight(5)
  },
  __documentTypeContainer: {
    backgroundColor: Colors.documentTypeContainer,
    //width:HeightWidth.getResWidth(180),
    borderWidth: 2,
    borderColor: 'rgba(76, 139, 245, 0.53)',
    //width:HeightWidth.getResWidth(180),
    paddingVertical:HeightWidth.getResHeight(5),
    paddingHorizontal: HeightWidth.getResWidth(10),
    borderRadius: 10
  },
  __countItemContent: {
    width: HeightWidth.getResWidth(80),
    minHeight: HeightWidth.getResHeight(20),
    backgroundColor: Colors.countItemDocumentColor,
    paddingVertical: HeightWidth.getResHeight(5),
    paddingHorizontal: HeightWidth.getResWidth(10),
    borderRadius: 5,
    marginLeft: '50%',
    marginBottom: HeightWidth.getResWidth(5)
  },
  __documentBox: {
    marginVertical: HeightWidth.getResHeight(10),
    width: HeightWidth.getResWidth(160),
    //marginRight:10
  },
  __countItem: {

    minHeight: HeightWidth.getResHeight(50),
    //float:'right'

  },
  __documnetContainer: {
    width: '100%',
    marginVertical: HeightWidth.getResHeight(20),
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  __subjectBox: {
    //width: HeightWidth.getResWidth(100),
    //height: HeightWidth.getResWidth(22),
    backgroundColor: Colors.ChapterNameGold,
    borderRadius: 5,
    padding: HeightWidth.getResWidth(5),
    fontSize: HeightWidth.getResFontSize(18),
    textAlign:'center'
  },
  chapterName: {
    fontSize: HeightWidth.getResFontSize(15),
    color: Colors.ChapterNameGold
  },
  __chapterContainer: {
    flexDirection: 'row',
    width: '60%'
  },
  __subjectContainer: {
    width: '40%',
    height:'100%'
  },
  __titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#4C8BF5',
    borderBottomWidth: HeightWidth.getResHeight(2),
    paddingBottom: HeightWidth.getResHeight(20),
  },
  __bodyContainer: {
    width: Dimensions.get('window').width,
    paddingVertical: HeightWidth.getResHeight(20),
    backgroundColor: Colors.fullWhite,
    paddingHorizontal: HeightWidth.getResWidth(5),
  },
  __headerContent: {
    color: Colors.fullWhite,
    fontSize: HeightWidth.getResFontSize(20),
    fontWeight: '500'
  },
  __LargeVerticleLine: {
    width: HeightWidth.getResWidth(3),
    height: HeightWidth.getResHeight(28),

  },
  VerticleLine: {
    width: HeightWidth.getResWidth(3),
    //height: HeightWidth.getResWidth(18),
    alignSelf: 'center',
    marginRight: HeightWidth.getResWidth(10)
  },
  __contentHeaderContainer: {
    width: '70%',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  __spaceHeaderContainer: {
    width: '30%'
  },
  __documentDisplayHeaderContainer: {
    width: Dimensions.get('window').width,
    height: HeightWidth.getResHeight(100),
    backgroundColor: Colors.contentDocumentDisplayHeader,
    paddingTop: HeightWidth.getResHeight(40),
    justifyContent: 'center'
  },
  __container: {
    flex: 1,
  },
});
