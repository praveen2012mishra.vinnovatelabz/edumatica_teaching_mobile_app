import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, SafeAreaView, FlatList, ToastAndroid, ActivityIndicator } from 'react-native'

//components
import BaseHeader from '../../../../components/Header/BaseHeader';
import CustomStatusBar from '../../../../components/Header/custom_status_bar';
import ContentCard from '../components/ContentCard';

//utilities
import { width, Colors } from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';
import {
    fetchCustomChaptersList,
    fetchTutorCoursesList,
    fetchCourseChaptersList,
  } from '../../../../common/api/academics';

interface Props {
    navigation: any;
    route: any;
}

interface Courses {
    board: string;
    className: string;
    subject: string;
}

const Course = ({ navigation, route }: Props) => {

    useEffect(() => {
        getCourses();
    }, [])

    const [courseList, setCourseList] = useState<Courses[]>([]);
    const [loading, setLoading] = useState(false);

    const getCourses = async () => {
        setLoading(true);
        try {
            const getCourseList = await fetchTutorCoursesList();
            //console.log(getCourseList,'---------------------');
            
            await setCourseList(getCourseList)
            setLoading(false);
            //console.log("Course List------------", getCourseList);
        } catch (error) {
            if (error.response?.status === 401) {
                ToastAndroid.show("Login Again", ToastAndroid.LONG);
                setLoading(false);
            }
        }
    }

    const renderItem = ({ item, index }) => {
        //console.log("Course Item", item)
        let structuredCourse = {
            boardname: item.board,
            classname: item.className,
            subjectname: item.subject
        }
        return (
            <ContentCard
                index={`${index + 1}.`}
                title={`${item.className} - ${item.subject} - ${item.board}`}
                backgroundColor={Colors.primaryBlue}
                textColor={Colors.fullWhite}
                contentHeight={HeightWidth.getResWidth(45)}
                onPress={() => { navigation.navigate('Chapters', { courseContent: structuredCourse }) }}
            />
        )
    }

    return (
        <SafeAreaView style={styles.__container}>
            <CustomStatusBar
                backgroundColor={Colors.primaryBlue}
                barStyle="light-content"
            />
            <BaseHeader
                onPress={() => {
                    navigation.goBack();
                }}
                title={'Courses'}
                onMenuPress={() => {
                    navigation.openDrawer();
                }}
            />
            {!loading ? <View style={styles.__innerContainer} >
                <FlatList
                    data={courseList}
                    keyExtractor={(_, index) => index.toString()}
                    renderItem={renderItem}
                />
            </View> : <View style={styles.__activityIndicator}>
                <ActivityIndicator size={"large"} color={Colors.primaryBlue} />
            </View>}
        </SafeAreaView >
    )
}

export default Course;

const styles = StyleSheet.create({
    __container: {
        flex: 1,
        backgroundColor: Colors.fullWhite
    },
    __innerContainer: {
        paddingTop: HeightWidth.getResWidth(10),
        flex: 1
    },
    __activityIndicator: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
    },
})
