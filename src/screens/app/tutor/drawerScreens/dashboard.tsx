import React, { FunctionComponent } from 'react';
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native';

//utilities
import HeightWidth from '../../../../utility/HeightWidth';
import { Colors } from '../../../../utility/utilities';

interface Props {
  navigation: any;
  route: any;
}

const Dashboard: FunctionComponent<Props> = ({ route, navigation }) => {
  return (
    <View style={styles.__container}>
      <Text>Not visible</Text>
      <TouchableOpacity
        onPress={() => {
          navigation.openDrawer();
        }}
        style={styles.__menuButton}>
        <Text style={styles.__menuTitle}>Menu</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  __container: {
    flex: 1,
  },
  __menuButton: {
    padding: HeightWidth.getResWidth(15),
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.veryDarkDesaturatedBlue,
    marginVertical: HeightWidth.getResWidth(20),
    marginHorizontal: HeightWidth.getResWidth(10),
  },
  __menuTitle: {
    fontSize: 18,
    color: Colors.fullWhite,
  },
});

export default Dashboard;
