import {connect} from 'react-redux';
import React, {FunctionComponent, useEffect, useState} from 'react';
import {User} from '../../../../common/contracts/user';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
const Tab = createMaterialTopTabNavigator();
import Dashboard from './dashboard';
import PersonalDetails from '../profile/personalDetails';
import CourseDetails from '../profile/courseDetails';
import OtherInformation from '../profile/others';
// import ProfileKyc from '../../../common/profilekyc';
import ProfileKyc from '../profile/profilekyc';
import HeightWidth from '../../../../utility/HeightWidth';
import CustomStatusBar from '../../../../components/Header/custom_status_bar';
import {Colors} from '../../../../utility/utilities';

interface Props {
  authUser: User;
}
const tabBarOptions = {
  activeTintColor: 'white',
  inactiveTintColor: 'white',
  indicatorStyle: {
    backgroundColor: '#e1ad01',
    height: '85%',
    width: '22%',
    borderRadius: 12,
    marginVertical: 5,
    marginHorizontal: 5,
  },
  pressOpacity: 0.1,
  style: {
    backgroundColor: '#4C8BF5',
    borderRadius: 12,
    marginVertical: 5,
  },
  labelStyle: {
    fontSize: HeightWidth.getResFontSize(12),
    paddingTop: HeightWidth.getResHeight(12),
  },
  // tabStyle: {
  //   backgroundColor: '#8eb4f5',
  //   borderRadius: 12,
  //   marginVertical: 5,
  //   marginHorizontal: 5,
  // },
};
const Profile: FunctionComponent<Props> = ({authUser}) => {
  return (
    <>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />
      <Tab.Navigator tabBarOptions={tabBarOptions}>
        <Tab.Screen name="Personal Info" component={PersonalDetails} />
        <Tab.Screen name="Course Details" component={CourseDetails} />
        <Tab.Screen name="Others" component={OtherInformation} />
        <Tab.Screen name="KYC" component={ProfileKyc} />
      </Tab.Navigator>
    </>
  );
};

export default Profile;
