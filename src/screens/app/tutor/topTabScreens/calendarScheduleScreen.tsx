import React,{FunctionComponent} from 'react';
import {Text,TouchableOpacity,Dimensions, StyleSheet,Image, View,ScrollView,ImageBackground} from 'react-native';
import { Colors, images } from '../../../../utility/utilities';
import { Icon } from "react-native-elements";
import HeightWidth from '../../../../utility/HeightWidth';
import TextInputUI from '../../../../components/UI/TextInput'
import CalendarSchedule from '../components/CalenderSchedule'
import { Modal, ModalContent } from 'react-native-modals';
import ButtonUI from '../../../../components/UI/Button';
import {fetchSchedulesList,deleteTutorSchedule} from '../../../../common/api/academics'

interface memberProps {
  navigation?: any;
}

const calendarScheduleScreen: FunctionComponent<memberProps> = ({navigation}) => {
  const [sheduleStatus, setSceduleStatus] = React.useState(true);
  const [getEditStatus, setEditStatus] = React.useState(false)
  const [getStatus, setStatus] = React.useState(false);
  const [getScheduleTypeResponse, setScheduleTypeResponse] = React.useState(false);
  const [getScheduleBatchResponse, setScheduleBatchResponse] = React.useState('A2');
 const [getItemState,setItemState]=React.useState('');
 const [getDeletedStatus,setDeletedStatus]=React.useState(false);
 const [getAllScheduleList,setAllScheduleList]=React.useState([]);
  
  return (
  <ScrollView>
        <View>
          <View><ImageBackground source={images.sliderHeader} style={styles._backgroundImage}>
            <View style={styles.calenderHeaderContainer}>
              <Icon
                iconStyle={{}}
                name="angle-left"
                color={'#000'}
                size={54}
                type="font-awesome"
              />
              <View><Image source={images.searchIcon} style={styles.searchIcon} /></View>
              <TextInputUI
                width='sm'
                style={styles.input}
                other
                name="orgCode"
                placeholder="Search"
                onChangeText={(value) => {
                }}
                value={"Search"}
                otherStyle={{ backgroundColor: Colors.fullWhite, borderRadius: 10, paddingLeft: 40 }}
              />
              <View><Image source={images.sliderHeaderTeacher} style={styles.__profileImage} /></View>
              <View><Image source={images.messageImage} style={styles.messageImage} /></View>
            </View>
          </ImageBackground>
          </View>
          <View style={styles.bodyContainer}>
            <View style={styles.bodyHeadingButton}>
              <TouchableOpacity
              onPress={() => {
                navigation.navigate('Schedule Calender')
                
              }}
              >
                <Text style={styles.schedule}>Schedule</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => {
                navigation.navigate('AddScheduleScreen',{
                  getAllScheduleList:getAllScheduleList.length>0?getAllScheduleList:[],
                  addScheduleData:getStatus?getItemState:'',
                  batchNameUpdate:(e)=>{setScheduleBatchResponse(e)},
                  call:(e)=>{setSceduleStatus(e);},
                  response:(e)=>setScheduleTypeResponse(true),
                  status:'Add'
                })
                {/* { !sheduleStatus && (<AddScheduleScreen 
      //getAllScheduleList={getAllScheduleList} 
      //batchNameUpdate={(e)=>{setScheduleBatchResponse(e)}} 
      //addScheduleData={getStatus?getItemState:''} 
      //call={(e: any) => { setSceduleStatus(e); }} 
      //response={(e)=>setScheduleTypeResponse(true)}
      />)} */}
                //alert('pressing button')
                //setSceduleStatus(false);
              }}>
                <Text style={styles.addSchedule}>Add schedule</Text>
              </TouchableOpacity>
            </View>
            <CalendarSchedule 
            scheduleList={(list)=>{
              //console.log(list,'----------------');
              
              setAllScheduleList(list)
            }} 
            call={(e: any) => { setEditStatus(e) }} 
            itemState={(e: any)=>{setItemState(e)}}
            />
          </View>
        </View>
        

      {/* { !sheduleStatus && (<AddScheduleScreen 
      //getAllScheduleList={getAllScheduleList} 
      //batchNameUpdate={(e)=>{setScheduleBatchResponse(e)}} 
      //addScheduleData={getStatus?getItemState:''} 
      //call={(e: any) => { setSceduleStatus(e); }} 
      //response={(e)=>setScheduleTypeResponse(true)}
      />)} */}
     
      {getEditStatus && (
      <Modal
        visible={getEditStatus}
        onTouchOutside={() => {
          
          
          setEditStatus(false)
          //dispatch(getCalenderScreenTypeAction(false))
        }}
      >

        <ModalContent>
          <View style={styles.buttonContainer}>
            <ButtonUI
              title="Delete"
              width={HeightWidth.getResWidth(100)}
              backgroundColor="#fff"
              onPress={async () => {
                setEditStatus(false)
                //dispatch(getCalenderScreenTypeAction(false))
                let deletedItem= {dayname: getItemState.dayname, fromhour: getItemState.fromhour}
                await deleteTutorSchedule(deletedItem)
                
                navigation.navigate('AddScheduleScreen',{
                  getAllScheduleList:getAllScheduleList.length>0?getAllScheduleList:[],
                  addScheduleData:getStatus?getItemState:'',
                  batchNameUpdate:(e)=>{setScheduleBatchResponse(e)},
                  call:(e)=>{setSceduleStatus(e);},
                  response:(e)=>{setDeletedStatus(true);setScheduleTypeResponse(true)},
                  status:'Delete'
                })
                //console.log(getItemState,'----------------------------->');
              }}
              style={styles.buttonCancel}
              borderRadius={5}
              textStyles={{
                color: '#000',
                fontSize: HeightWidth.getResFontSize(18),
                fontWeight: '400',
              }}
              otherStyle={{ borderWidth: 1, borderColor: '#000' }}
            />
            <ButtonUI
              title="Edit"
              width={HeightWidth.getResWidth(100)}
              backgroundColor="rgba(76, 139, 245, 1)"
              onPress={() => {
                //handleAddSchedule()
                setSceduleStatus(false)
                setStatus(true)
                setEditStatus(false)
                //dispatch(getCalenderScreenTypeAction(false))
                navigation.navigate('AddScheduleScreen',{
                  getAllScheduleList:getAllScheduleList.length>0?getAllScheduleList:[],
                  addScheduleData:getStatus?getItemState:'',
                  batchNameUpdate:(e)=>{setScheduleBatchResponse(e)},
                  call:(e)=>{setSceduleStatus(e);},
                  response:(e)=>setScheduleTypeResponse(true),
                  status:'Edit'
                })
              }}
              style={styles.buttonCancel}
              borderRadius={5}
              textStyles={{
                color: '#fff',
                fontSize: HeightWidth.getResFontSize(18),
                fontWeight: '400',
              }}
            />
          </View>
        </ModalContent>
      </Modal>
      )} 
      {getScheduleTypeResponse && !getDeletedStatus && (
      <Modal
        visible={getScheduleTypeResponse}
        onTouchOutside={() => {
          setScheduleTypeResponse(false)
        }}
      >
        <ModalContent>
          <View style={styles.ResponseContainer}>
          <Icon
                iconStyle={{backgroundColor:'green',borderRadius:30}}
                name="check"
                color={'#fff'}
                size={54}
                type="font-awesome"
              />
            <Text style={styles.responseTest}>You have successfully added schedule for Batch {getScheduleBatchResponse}</Text>
          </View>
        </ModalContent>
      </Modal>
      )} 
      {getDeletedStatus && ( 
      <Modal
        visible={getDeletedStatus}
        onTouchOutside={() => {
          setDeletedStatus(false)
          navigation.navigate('Schedule Calender');
          //window.location.reload(true)
        }}
      >
        <ModalContent>
          <View style={styles.ResponseContainer}>
            <Text style={styles.responseTest}>You have successfully Deleted schedule for Batch {getScheduleBatchResponse}</Text>
          </View>
        </ModalContent>
      </Modal>
      )} 
    </ScrollView>
  );
};

export default calendarScheduleScreen;

const styles = StyleSheet.create({
  responseTest:{
    fontSize:HeightWidth.getResFontSize(15),
    textAlign:'center',
    paddingTop:5
  },
  ResponseContainer:{
    width:HeightWidth.getResWidth(250),
    justifyContent:'center'
  },
  buttonCancel: {
    borderWidth: 1,
    borderColor: 'grey'
  },
  buttonContainer: {
    width:HeightWidth.getResWidth(250),
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: HeightWidth.getResHeight(50)
  },
  _backgroundImage: {
    width: Dimensions.get('window').width,
    height: 150
  },
  calenderHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: HeightWidth.getResHeight(110),
    marginBottom: HeightWidth.getResHeight(40),
    alignItems: 'center',
    marginHorizontal: HeightWidth.getResHeight(Dimensions.get('window').width * .0625)
  },
  input: {
    color: 'rgba(33, 33, 33, 0.5)',
    fontSize: 12,
    overflow: 'hidden'
  },
  __profileImage: {
    width: HeightWidth.getResWidth(34),
    height: HeightWidth.getResWidth(34),
    borderRadius: 100,
    // resizeMode: 'contain'
  },
  messageImage: {
    width: HeightWidth.getResWidth(32),
    height: HeightWidth.getResWidth(32),
  },
  searchIcon: {
    width: HeightWidth.getResWidth(18),
    height: HeightWidth.getResWidth(18),
    //color: 'grey',
    backgroundColor: '#fff',
    //position:'absolute',
    left: HeightWidth.getResWidth(40),
    zIndex: 1000
  },
  bodyContainer: {
    width: HeightWidth.getResWidth(Math.round(Dimensions.get('window').width)),
    //height:,
    overflow:'scroll',
    backgroundColor: '#fff'
  },
  schedule: {
    fontWeight: '500',
    fontSize: HeightWidth.getResFontSize(18),
    //lineHeight:21.09,
    color: '#000',
    fontFamily: 'Roboto'
  },
  bodyHeadingButton: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginHorizontal: HeightWidth.getResWidth(20),
    marginTop: HeightWidth.getResWidth(20),
    paddingBottom: HeightWidth.getResWidth(20),
    alignItems: 'center',
    borderBottomWidth: 2,
    borderBottomColor: 'rgba(33, 33, 33, 0.5)'
  },
  addSchedule: {
    fontSize: HeightWidth.getResFontSize(12),
    fontWeight: 'bold',
    //lineHeight:14,
    color: 'rgba(76, 139, 245, 1)'
  }
})

