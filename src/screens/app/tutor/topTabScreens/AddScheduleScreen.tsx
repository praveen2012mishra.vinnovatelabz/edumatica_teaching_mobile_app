import React, { FunctionComponent, useState,useEffect } from 'react'
import { Text, StyleSheet, TextInput, Image, View, ImageBackground, Dimensions } from 'react-native'
import { Colors, images } from '../../../../utility/utilities';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Icon } from "react-native-elements";
import { createStackNavigator } from '@react-navigation/stack';
import TextInputUI from '../../../../components/UI/TextInput'
import HeightWidth from '../../../../utility/HeightWidth';
import DropdownPicker from '../../../../components/Other/dropdown_picker';
import RNPickerSelect from "react-native-picker-select";
import DatePickerUI from '../../../../components/UI/DatePicker'
import DateTimePicker from '@react-native-community/datetimepicker';
import ButtonUI from '../../../../components/UI/Button'
import {updateTutorSchedule,createTutorSchedule,fetchBatchesList} from '../../../../common/api/academics'
import {connect, useDispatch} from 'react-redux';
import {getCalenderAddUpdateResponseAction,getCalenderScreenTypeAction,getBatchNameUpdateAction} from '../../../../screens/auth/store/actions'
var moment = require('moment');
interface Props {
  
  addScheduleData?:any,
  response?:any,
  batchNameUpdate?:any,
  getAllScheduleList?:any,
  getAllScheduleListProp?:any,
  navigation?: any;
  route?:any
}

interface FormData {
//   batchenddate:String,
//       batchfriendlyname:String,
//       batchicon:String,
//       batchstartdate:String,
//       boardname:String,
//       classname:String,
//       students:String,
//       subjectname:String,
//       tutor:String,
// }

  batchfriendlyname:String,
  schedules:[{
    dayname:String,
    fromhour:String,
    tohour:String
  }]
}

const items = [
        { label: 'A2', value: 'A2' },
        { label: 'A3', value: 'A3' },
        { label: 'A4', value: 'A4' },
      ]

const day = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday',]

const AddScheduleScreen: FunctionComponent<Props> = ({route, addScheduleData,response,batchNameUpdate,getAllScheduleList,navigation }) => {
  //console.log(route.params.addScheduleData,'========================>');
  
  const dispatch = useDispatch();
  const [batchName, setBatchName] = React.useState(route.params.addScheduleData?route.params.addScheduleData.batchfriendlyname:'',);
  const inputRefs = {
    firstTextInput: null,
    batchName: null,
    favSport1: null,
    lastTextInput: null,
    favSport5: null,
  };
  const [date, setDate] = useState(new Date(1598051730000));
  const [mode, setMode] = useState('date');
  const [getShow, setShow] = useState(false);
  const [getScheduleType, setScheduleType] = useState(route.params.addScheduleData?'Edit Schedule':'Add schedule');
  const [getDay, setDay] = useState(route.params.addScheduleData?route.params.addScheduleData.dayname:'Monday');
  const [batchList,setBatchList]=useState([]);
  const [timeStartValidation, setTimeStartValidation] = useState(false);
  //alert(batchList+'------------------->');  
  //console.log(route.params,'---------------------->');
  
  useEffect(()=>{
    //console.log('-------------------->',route.params.getAllScheduleList);
    const getBatchList = async () =>{
      let batch=await fetchBatchesList()
      //console.log(batch,'----------------------');
      //alert(batch+'------------------->');
      let batchArray=[];
      batch.forEach((item)=>{
        //alert(item+'------------------->');
        //batchArray.push(item.batchfriendlyname)
        batchArray.push({ label: item.batchfriendlyname, value: item.batchfriendlyname })
      })
      //console.log(batch,'----------------------',batchArray);
      setBatchList(batchArray)
      //return batchArray
    }
    getBatchList()
  },[])

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(false);
    setDate(currentDate);
    let d = new Date(currentDate);
    let n = d.getUTCDay();
    //console.log(selectedDate, n,'------------------>',day[n]);
    setDay(day[n])
  };

  const showMode = (currentMode) => {
    setShow(true);
  };

  const showDatepicker = () => {
    showMode('date');
  };
  const [timeStart, setTimeStart] = useState(route.params.addScheduleData?route.params.addScheduleData.fromhour:'00:00');

  const showTimeStartpicker=()=>{
    showModeStart();
  }

  const [getShowStart, setShowStart] = useState(false);

  const showModeStart=()=>{
    setShowStart(true);
  }

  const onChangeStart=(event, selectedTime)=>{
    //route.params.getAllScheduleList
    
    setShowStart(false)
    let time=new Date(selectedTime);
    let timeStack=time.getHours() + ':'+time.getMinutes()
    route.params.getAllScheduleList.forEach((ele,id)=>{
      let eleTime=moment(ele.fromhour, "HH:mm:ss")
      let itemTime=moment(timeStack, "HH:mm:ss")
      console.log(eleTime.diff(itemTime),eleTime,itemTime);
      
      if(eleTime.diff(itemTime)==0 && ele.dayname == getDay && route.params.status =='Add'){
        setTimeStartValidation(true)
      }
      else{
        setTimeStartValidation(false)
      }
    })
    setTimeStart(timeStack)
    
    
  }

  const [timeEnd, setTimeEnd] = useState(route.params.addScheduleData?route.params.addScheduleData.tohour:'00:00');

  const showTimeEndpicker=()=>{
    showModeEnd();
  }

  const [getShowEnd, setShowEnd] = useState(false);
  const [updateId, setUpdateId] = useState(route.params.addScheduleData?route.params.addScheduleData.id:'');
  const showModeEnd=()=>{
    setShowEnd(true);
  }

  const onChangeEnd=(event, selectedTime)=>{
    setShowEnd(false)
    let time=new Date(selectedTime);
    let timeStack=time.getHours() + ':'+time.getMinutes()
    setTimeEnd(timeStack); 
    console.log(timeStack,'-------------------->');   
  }

  const handleAddSchedule = async ()=>{
    const addScheduleDataItem={batchfriendlyname: batchName, schedules: [{fromhour: timeStart, tohour: timeEnd, dayname: getDay}]};
    
    //console.log(addScheduleDataItem,'---------------------------->',!addScheduleData);
    if(!route.params.addScheduleData){
      //console.log(addScheduleDataItem,'---------------------------->');
      await createTutorSchedule(addScheduleDataItem); 
    }
    if(route.params.addScheduleData.id){
      
      
      const updateScheduleData={scheduleId: updateId, dayname: getDay, fromhour: timeStart, tohour: timeEnd}
     
      await updateTutorSchedule(updateScheduleData);
    }
    
    route.params.batchNameUpdate(batchName); 
    route.params.response(true);
    route.params.call(true);         
  }

  return (
    <View>
      <View><ImageBackground source={images.addScheduleImage} style={styles._backgroundImage}>
        <View style={styles.calenderHeaderContainer}>
          <Icon
            iconStyle={{}}
            name="angle-left"
            color={'#000'}
            size={54}
            type="font-awesome"
            onPress={() => {
              route.params.call(true);
              navigation.navigate('Schedule Calender');
              //dispatch(getCalenderScreenTypeAction(true)) 
            }}
          />
        </View>
      </ImageBackground>
      </View>
      <View style={styles.addScheduleContainer}>
        <View ><Text style={styles.addScheduleHeading}>{route.params.status}{' '}Schedule</Text></View>
        <View>
          <Text style={styles.batchname}>Batch Name</Text>
          <View style={{ borderBottomColor: 'grey', borderBottomWidth: 1 }}>
            <RNPickerSelect
              placeholder={{
                label: 'Please select batch list',
                value: null,
              }}
              items={batchList}
              onValueChange={(item) => {
                //console.log(item,'---------------------------');
                
                setBatchName(item)}}
              onUpArrow={() => {
                inputRefs.firstTextInput.focus();
              }}
              onDownArrow={() => {
                inputRefs.favSport1.togglePicker();
              }}
              style={pickerSelectStyles}
              value={batchName}
              ref={el => {
                inputRefs.batchName = el;
              }}
            />
          </View>
        </View>
        <View style={styles.dayContainer}>
          <Text style={styles.batchname}>Day</Text>
          <Icon
            iconStyle={{ right: 0 }}
            name="calendar"
            color={'#000'}
            size={20}
            type="font-awesome"
            onPress={showDatepicker}
          />
        </View>
        {getShow && (
          <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode={'date'}
            is24Hour={true}
            display="default"
            onChange={onChange}
            dayOfWeekFormat={'{dayofweek.abbreviated(2)}'}
          />
        )}
        {getDay && (<Text style={styles._showDay}>{getDay}</Text>)}
        <View style={styles.timeContainer}>
          <View>
            <Text style={styles._timeContent}>Session Start Time</Text>
            <View style={styles._timeDropDownBox}>
              <Text style={styles._timeValue}>{timeStart}</Text>
              <View style={styles.dropTimeIcon}>
                <Icon
                  iconStyle={{ }}
                  name="caret-down"
                  color={'#000'}
                  size={25}
                  type="font-awesome"
                  onPress={showTimeStartpicker}
                />
              </View>

            </View>
            {timeStartValidation && (<Text style={styles._timeContentValidation}>All ready availabte scedule</Text>)}
          </View>
          <View>
            <Text style={styles._timeContent}>Session End Time</Text>
            <View style={styles._timeDropDownBox}>
              <Text style={styles._timeValue}>{timeEnd}</Text>
              <View style={styles.dropTimeIcon}>
                <Icon
                  iconStyle={{ }}
                  name="caret-down"
                  color={'#000'}
                  size={35}
                  type="font-awesome"
                  onPress={showTimeEndpicker}
                />
              </View>

            </View>
          </View>
        </View>
        {getShowStart && (
          <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode={'time'}
            is24Hour={true}
            display="default"
            onChange={onChangeStart}
            dayOfWeekFormat={'{dayofweek.abbreviated(2)}'}
          />
        )}
         {getShowEnd && (
          <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode={'time'}
            is24Hour={true}
            display="default"
            onChange={onChangeEnd}
            dayOfWeekFormat={'{dayofweek.abbreviated(2)}'}
          />
        )}

        <View style={styles.buttonContainer}>
        <ButtonUI
                title="Cancel"
                width={HeightWidth.getResWidth(100)}
                backgroundColor="#fff"
                // onPress={() => {
                //   const values = {
                //     mobileNumber: mobileNumber,
                //     orgCode: orgCode,
                //     password: password,
                //   };
                //   handleSubmit(values);
                // }}
                style={styles.buttonCancel}
                borderRadius={5}
                textStyles={{
                  color: '#000',
                  fontSize: HeightWidth.getResFontSize(18),
                  fontWeight: '400',
                }}
                otherStyle={{borderWidth:1,borderColor:'#000'}}
              />
              <ButtonUI
                title={route.params.status}
                width={HeightWidth.getResWidth(100)}
                backgroundColor="rgba(76, 139, 245, 1)"
                onPress={() => {
                  handleAddSchedule()
                }}
                style={styles.buttonCancel}
                borderRadius={5}
                textStyles={{
                  color: '#fff',
                  fontSize: HeightWidth.getResFontSize(18),
                  fontWeight: '400',
                }}
              />
        </View>
      </View>


      {/* <CalendarHome /> */}
    </View>
  );
};

const mapStateToProps = (state) => ({
  getAllScheduleListProp:state.auth.getScheduleListReducer
});
//export default AddScheduleScreen;
export default connect(mapStateToProps)(AddScheduleScreen);
const pickerSelectStyles = StyleSheet.create({

  inputAndroid: {
    fontSize: 16,
    paddingTop: 13,
    //paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: 'gray',
    //borderRadius: 4,
    backgroundColor: '#fff',
    color: 'black',
    borderBottomWidth: 1,
    borderBottomColor: '#000'
  },
  placeholder: {
    color: 'black',
  },
});

const styles = StyleSheet.create({
  buttonContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    marginVertical:HeightWidth.getResHeight(50)
  },
  buttonCancel:{
    borderWidth:1,
    borderColor:'grey'
  },
  dropTimeIcon: {
    width: '30%'
  },
  _timeValue: {
    width: '70%',
    fontSize: HeightWidth.getResFontSize(20),
    color: '#000',
    fontWeight: '500',
    paddingLeft:5
  },
  _timeDropDownBox: {
    borderWidth: 1,
    borderColor: '#212121',
    borderRadius: 5,
    width: HeightWidth.getResWidth(100),
    marginTop: HeightWidth.getResHeight(20),
    height: HeightWidth.getResHeight(40),
    flexDirection: 'row'
    //justifyContent:'space-between'
  },
  _timeContent: {
    fontSize: HeightWidth.getResFontSize(10),
    color: 'rgba(33, 33, 33, 0.6)',
    fontWeight: '500'
  },
  _timeContentValidation: {
    fontSize: HeightWidth.getResFontSize(10),
    color: 'red',
    fontWeight: '500'
  },
  calenderHeaderContainer: {
    flexDirection: 'row',
    //justifyContent: 'space-around',
    marginTop: HeightWidth.getResHeight(140),
    alignItems: 'center',
    marginHorizontal: HeightWidth.getResHeight(Dimensions.get('window').width * .225)
  },
  _backgroundImage: {
    width: Dimensions.get('window').width,
    height: 220
  },
  addScheduleContainer: {
    width: Dimensions.get('window').width,
    height: 550,
    backgroundColor: '#fff',
    paddingHorizontal: HeightWidth.getResHeight(25)
  },
  addScheduleHeading: {
    fontSize: HeightWidth.getResFontSize(18),
    color: '#000',
    fontWeight: 'bold',
    marginVertical: HeightWidth.getResHeight(25),
    paddingBottom: HeightWidth.getResHeight(18),
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(33, 33, 33, 0.5)'
  },
  input: {
    color: 'rgba(33, 33, 33, 0.5)',
    fontSize: 12,
    overflow: 'hidden',
    paddingBottom: 0
  },
  batchname: {
    color: 'rgba(33, 33, 33, 0.5)'
  }
  ,
  editIcon: {
    width: HeightWidth.getResWidth(20),
    height: HeightWidth.getResHeight(40),
  },
  dayContainer: {
    marginVertical: HeightWidth.getResHeight(25),
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  timeContainer: {
    marginVertical: HeightWidth.getResHeight(25),
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  _showDay: {
    fontSize: HeightWidth.getResFontSize(18),
    color: '#000',
    fontWeight: 'bold',
    marginVertical: HeightWidth.getResHeight(25),
    paddingBottom: HeightWidth.getResHeight(18),
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(33, 33, 33, 0.5)'
  }
})


