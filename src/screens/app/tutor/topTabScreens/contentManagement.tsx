import React, {useEffect, FunctionComponent, useState} from 'react';
import {Text, StyleSheet, View, SafeAreaView, Alert} from 'react-native';
import {
  fetchCustomChaptersList,
  fetchTutorCoursesList,
  fetchCourseChaptersList,
} from '../../../../common/api/academics';
import ButtonUI from '../../../../components/UI/Button';

import Picker from '../../../../components/UI/Picker';
import {Colors, width} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';

interface Props {
  navigation: any;
}

const ContentManagement: FunctionComponent<Props> = ({navigation}) => {
  useEffect(() => {
    getCourses();
  }, []);

  const [courseList, setCourseList] = useState([]);
  const [structuredAllCourses, setStructuredAllCourses] = useState([]);
  const [selectedCourse, setSelectedCourse] = useState(null);
  const [chapterList, setChapterList] = useState([]);
  const [structuredAllChapters, setStructuredAllChapters] = useState([]);
  const [selectedChapters, setSelectedChapters] = useState(null);
  const [selectedDataObject, setSelectedDataObject] = useState({});
  const [selectedDataObjectChapter, setSelectedDataObjectChapter] = useState(
    {},
  );

  const getCourses = async () => {
    try {
      const getCourseList = await fetchTutorCoursesList();
      console.log('getCourseList----->', getCourseList);
      setCourseList(getCourseList);

      const structuredCoursesList = getCourseList.map((course, index) => ({
        label: `${course.board}, ${course.className}, ${course.subject}`,
        value: index,
      }));
      setStructuredAllCourses(structuredCoursesList);
    } catch (error) {
      // if (error.response?.status === 401) {
      //   setRedirectTo('/login');
      // } else {
      //   alert('Service not available in this area');
      //   setPinCode('');
      // }
      console.log('Error----->', error);
    }
  };

  const getChaptersOfCourse = async (item: any) => {
    console.log('ddaaaataaaitem', item);
    setSelectedCourse(item);
    const myData = courseList
      .filter((item, index) => index == selectedCourse)
      .map((item) => item);

    setSelectedDataObject(myData);
    try {
      console.log('myData------->', myData[0].board);
      const getChapterList = await fetchCourseChaptersList({
        boardname: myData[0].board,
        classname: myData[0].className,
        subjectname: myData[0].subject,
      });
      console.log('getChapterList----->', getChapterList);
      setChapterList(getChapterList.masterChapters);
      let allChapterList = getChapterList.masterChapters;
      const structuredChaptersList = allChapterList.map(
        (chapterItem, index) => ({
          label: `${chapterItem.chapter.name}`,
          value: index,
        }),
      );
      setStructuredAllChapters(structuredChaptersList);
    } catch (error) {
      console.log('Error----->', error);
    }
  };

  const setChapterObject = (item: any) => {
    setSelectedChapters(item);
    const myChapter = chapterList
      .filter((item, index) => index == selectedChapters)
      .map((item) => item);
    setSelectedDataObjectChapter(myChapter);
    console.log('myChapter---------', myChapter);
  };

  return (
    <SafeAreaView style={styles.__container}>
      <View style={{paddingTop: 2}}>
        <Picker
          label="Select Course"
          labelSize={16}
          labelColor={Colors.lightGrayColor}
          data={structuredAllCourses}
          rightIconName=""
          rightIconSize=""
          rightIconColor=""
          value={selectedCourse}
          docTypeHandler={async (item) => {
            getChaptersOfCourse(item);
          }}
          onRightIconPress=""
          showLabel={true}
          //   otherStyle={{marginHorizontal: 4}}
        />
      </View>

      <View style={{paddingTop: 2}}>
        <Picker
          label="Select Course"
          labelSize={16}
          labelColor={Colors.lightGrayColor}
          data={structuredAllChapters}
          rightIconName=""
          rightIconSize=""
          rightIconColor=""
          value={selectedChapters}
          docTypeHandler={async (item) => {
            setChapterObject(item);
          }}
          onRightIconPress=""
          showLabel={true}
          //   otherStyle={{marginHorizontal: 4}}
        />
      </View>

      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <ButtonUI
          title={'Continue'}
          backgroundColor={Colors.primaryBlue}
          borderRadius={5}
          elevation={5}
          textStyles={{fontSize: 17, color: Colors.fullWhite}}
          width={(2 * width) / 5}
          height={HeightWidth.getResWidth(43)}
          onPress={() =>
            navigation.navigate('AllContentType', {
              boardName: selectedDataObject[0].board,
              className: selectedDataObject[0].className,
              subjectName: selectedDataObject[0].subject,
              chapterName: selectedDataObjectChapter[0].chapter.name,
            })
          }
          // onPress={() => Alert.alert(JSON.stringify(selectedDataObject))}
          // disabled={loading}
          // activityIndicator={loading}
        />
      </View>
    </SafeAreaView>
  );
};

export default ContentManagement;

const styles = StyleSheet.create({
  __container: {
    flex: 1,
  },
});
