import {connect} from 'react-redux';
import React, {FunctionComponent, useEffect, useState} from 'react';
import {
  Link,
  Route,
  Redirect,
  withRouter,
  RouteComponentProps,
  NativeRouter,
  Switch,
} from 'react-router-native';
import {
  StyleSheet,
  Button,
  Text,
  View,
  ImageBackground,
  Dimensions,
} from 'react-native';
import {User} from '../../../../common/contracts/user';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
const Tab = createMaterialTopTabNavigator();
import Dashboard from './dashboard';
interface Props {
  authUser: User;
}

const Profile: FunctionComponent<Props> = ({authUser}) => {
  return (
    <View>
      <Tab.Navigator>
        <Tab.Screen name="PersonalDetails" component={Dashboard} />
        <Tab.Screen name="CourseDetails" component={Dashboard} />
        <Tab.Screen name="Others" component={Dashboard} />
        <Tab.Screen name="KYC" component={Dashboard} />
      </Tab.Navigator>
    </View>
  );
};

//   const mapStateToProps = (state: Auth) => ({
//     authUser: state.authReducer.authUser as User,
//   });

//   export default connect(mapStateToProps)(Navbar);

export default Profile;
