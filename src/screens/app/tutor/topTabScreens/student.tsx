import React from 'react';
import {Text, StyleSheet, View} from 'react-native';

const Student = () => {
  return (
    <View>
      <Text> This is Student </Text>
    </View>
  );
};

export default Student;

// import React from 'react';
// import {Text, StyleSheet, View} from 'react-native';
// import Latex from 'react-native-latex';

// const appStackStudent = () => {
//   return (
//     <Latex
//       style={
//         {
//           // width: '100%',
//           // height: 100,
//         }
//       }>
//       {'\\frac{1}{2\\pi}\\int_{-\\infty}^{\\infty}e^{-\\frac{x^2}{2}}dx'}
//     </Latex>
//   );
// };

// export default appStackStudent;
