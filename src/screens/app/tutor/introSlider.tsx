import React, {FunctionComponent} from 'react';

import {Text, StyleSheet, View, Image, SafeAreaView} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import PropTypes from 'prop-types';

//Utilities
import {width, Colors, images} from '../../../utility/utilities';

//Components
import CustomStatusBar from '../../../components/Header/custom_status_bar';
import IntroSliderHeader from './components/IntroSliderHeader';
import IntroSliderBody from './components/IntroSliderBody';
import ButtonUI from '../../../components/UI/Button';

const EdumeticaAddContentHome: FunctionComponent<{title?: string}> = ({
  title = 'Praveen',
}) => {
  return (
    <SafeAreaView style={styles.__container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />
      <IntroSliderHeader />
      <IntroSliderBody />
    </SafeAreaView>
  );
};

EdumeticaAddContentHome.propTypes = {
  title: PropTypes.string,
};

EdumeticaAddContentHome.defaultProps = {
  title: null,
};

const styles = StyleSheet.create({
  __container: {
    flex: 1,
    backgroundColor: Colors.fullWhite,
  },
});

export default EdumeticaAddContentHome;
