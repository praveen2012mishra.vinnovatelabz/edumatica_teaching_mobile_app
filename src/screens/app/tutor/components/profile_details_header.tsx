import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Colors, width, height} from '../../../../utility/utilities';
// import Icon from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

const profile_details_header = ({iconBackName, iconName, title, onBack}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        // marginHorizontal: '5%',
      }}>
      {iconBackName ? (
        <TouchableOpacity
          style={[styles.titleIcon, {position: 'absolute', left: 0}]}
          onPress={onBack}>
          <Icon size={40} color={Colors.darkColor} name={iconBackName} />
        </TouchableOpacity>
      ) : (
        <View></View>
      )}
      <View style={[styles.titleIcon, {backgroundColor: Colors.darkColor}]}>
        <Icon size={20} color={Colors.fullWhite} name={iconName} />
      </View>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
};

export default profile_details_header;

const styles = StyleSheet.create({
  titleIcon: {
    borderRadius: 25,
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: '5%',
  },
  title: {
    fontWeight: '400',
    fontSize: 18,
    textAlign: 'center',
    marginVertical: 8,
    color: Colors.darkColor,
  },
});
