import React, {useState, FunctionComponent, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {Checkbox} from 'react-native-paper';
import Picker from '../../../../components/UI/Picker';
import Button from '../../../../components/UI/Button';
import {ScrollView} from 'react-native-gesture-handler';
import CustomStatusBar from '../../../../components/Header/custom_status_bar';
import {Tutor} from '../../../../common/contracts/user';
import {BoardClassSubjectsMap} from '../../../../common/academics/contracts/board_class_subjects_map';
import {
  fetchBoardsList,
  fetchClassesList,
  fetchSubjectsList,
} from '../../../../common/api/academics';
import CourseTable from '../../../../components/table/course_table';
import * as Animatable from 'react-native-animatable';

// const data = [
//   {label: 'Nagpur', value: 'nagpur'},
//   {label: 'Delhi', value: 'delhi'},
// ];

import {Course} from '../../../../common/academics/contracts/course';
import {Board} from '../../../../common/academics/contracts/board';
import {Standard} from '../../../../common/academics/contracts/standard';

//utilities
import {
  width,
  Colors,
  height,
  images,
  strings,
} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';

interface Props {
  user: Tutor;
  nextButtonHandler: any;
  saveUser: (data: Tutor) => any;
}

const TutorCourseInformation: FunctionComponent<Props> = ({
  user,
  saveUser,
  nextButtonHandler,
}) => {
  const data = [];
  const data2 = [];
  const [tutorSubjects, setTutorSubjects] = useState<Course[]>([]);
  const [board, setBoard] = useState('0');
  const [standard, setStandard] = useState('0'); // Standard describes Class of the Tutor.
  const [subjectsList, setSubjectsList] = useState<
    {name: string; checked: boolean}[]
  >([]);
  const [boards, setBoards] = useState<Board[]>([]);
  const [classes, setClasses] = useState<Standard[]>([]);
  const [boardError, setBoardError] = useState('');
  const [standardError, setStandardError] = useState('');
  const [submitError, setSubmitError] = useState('');

  useEffect(() => {
    (async () => {
      try {
        const [boardsList] = await Promise.all([fetchBoardsList()]);
        //creating boards list for picker

        for (var i = 0; i < boardsList.length; i++) {
          data.push({
            label: boardsList[i].boardName,
            value: boardsList[i].boardName,
          });
        }
        setBoards(data);
      } catch (error) {
        if (error.response?.status === 401) {
          // setRedirectTo('/login');
        }
      }
    })();
  }, []);

  const validateBoard = (value) => {
    if (value != 0) {
      setBoardError('');
    }
  };
  const validateStandard = (value) => {
    if (value != 0) {
      setStandardError('');
    }
  };

  const setBoardAndFetchClasses = async (board: string) => {
    try {
      await setBoard(board);
      console.log(board);
      if (board.length > 1) {
        const classListResponse = await fetchClassesList({boardname: board});
        console.log('we are coming here');
        console.log(classListResponse);
        for (var i = 0; i < classListResponse.length; i++) {
          data2.push({
            label: classListResponse[i].className,
            value: classListResponse[i].className,
          });
        }
        console.log(data2);
        setClasses(data2);
      } else {
        setClasses([]);
      }
    } catch (error) {
      if (error.response?.status === 401) {
        // setRedirectTo('/');
      }
    }
  };

  const setClassAndFetchSubjects = async (standard: string) => {
    try {
      await setStandard(standard);
      console.log(standard);
      if (board.length > 1 && standard.length > 1) {
        const response = await fetchSubjectsList({
          boardname: board,
          classname: standard,
        });
        // dispatch(setSubjects(response));
        const structuredSubjectsList = response.map((subject) => ({
          name: subject.subjectName,
          checked: false,
        }));
        setSubjectsList(structuredSubjectsList);
        console.log(structuredSubjectsList);
      } else {
        setSubjectsList([]);
      }
    } catch (error) {
      if (error.response?.status === 401) {
        // setRedirectTo('/');
      }
    }
  };
  const handleChangeInSubjectCheckbox = (index: number) => {
    const subjects = subjectsList.map((subject, sIndex) => {
      if (index !== sIndex) return subject;
      return {...subject, checked: !subject.checked};
    });

    setSubjectsList(subjects);
  };

  const removeTutorSubjects = (map: BoardClassSubjectsMap) => {
    const mapSubjects = map.subjects.map((subject) => subject.toLowerCase());

    const tutorSubjectsMap = tutorSubjects.filter(
      (subjectItem) =>
        subjectItem.board !== map.boardname ||
        subjectItem.className.toString().toLowerCase() !==
          map.classname.toString().toLowerCase() ||
        mapSubjects.indexOf(subjectItem.subject.toLowerCase()) === -1,
    );

    setTutorSubjects(tutorSubjectsMap);
  };

  const submitCourseInformation = () => {
    if (tutorSubjects.length < 1) return;
    saveUser({...user, courseDetails: tutorSubjects});
  };

  const boardClassSubjectsMap: BoardClassSubjectsMap[] = [];

  for (let i = 0; i < tutorSubjects.length; ++i) {
    const subject = tutorSubjects[i];

    let boardClassSubjectIndex = boardClassSubjectsMap.findIndex(
      (boardClassSubject) =>
        boardClassSubject.boardname.toLowerCase() ===
          subject.board.toLowerCase() &&
        boardClassSubject.classname.toString().toLowerCase() ===
          subject.className.toString().toLowerCase(),
    );

    if (boardClassSubjectIndex === -1) {
      boardClassSubjectsMap.push({
        boardname: subject.board,
        classname: subject.className,
        subjects: [],
      });

      boardClassSubjectIndex = boardClassSubjectsMap.length - 1;
    }

    boardClassSubjectsMap[boardClassSubjectIndex].subjects.push(
      subject.subject,
    );
  }

  const [isSelected, setSelection] = useState(['']);

  const SubjectsCheckboxes = () => (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'stretch',
        flex: 0.5,
        padding: '5%',
      }}>
      {subjectsList.map((subject, index) => (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: '2%',
          }}>
          <Text key={index}>{subject.name}</Text>
          <View
            style={{
              borderWidth: 0.5,
              alignItems: 'stretch',
              borderRadius: 8,
              padding: '2%',
            }}>
            <Checkbox
              key={index}
              status={subject.checked ? 'checked' : 'unchecked'}
              onPress={() => {
                handleChangeInSubjectCheckbox(index);
              }}
            />
          </View>
          {/* </TouchableOpacity> */}
        </View>
      ))}
    </View>
  );

  const addSubjectsToClass = () => {
    console.log('board', board);
    console.log('standard', standard);
    if (board == null) return;
    if (standard == null) return;

    const map: Course[] = [];
    console.log(subjectsList);
    subjectsList
      .filter((subject) => subject.checked)
      .filter((subject) => {
        const subjectExists = tutorSubjects.find(
          (tutorSubject) =>
            tutorSubject.board.toLowerCase() === board.toLowerCase() &&
            tutorSubject.className.toString().toLowerCase() ===
              standard.toString().toLowerCase() &&
            tutorSubject.subject.toLowerCase() === subject.name.toLowerCase(),
        );

        return subjectExists === undefined;
      })
      .forEach((subject) => {
        map.push({board, className: standard, subject: subject.name});
      });

    setTutorSubjects([...tutorSubjects, ...map]);
    setSubjectsList([]);
  };

  const handelSubmit = () => {
    if (board == '0' || standard == '0') {
      setSubmitError('All fields are required field');
      setBoardError('Board must be selected');
      setStandardError('Standard must be selected');
    } else if (boardError.length > 0 || standardError.length > 0) {
      setSubmitError('All fields are required field');
    } else {
      submitCourseInformation();
      nextButtonHandler();
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />
      <View style={styles.body}>
        <View style={styles.form}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Picker
                label="Boards"
                labelSize={16}
                labelColor={Colors.lightGrayColor}
                data={boards}
                rightIconName=""
                rightIconSize=""
                rightIconColor=""
                value={board}
                docTypeHandler={async (item) => {
                  validateBoard(item);
                  try {
                    console.log(item);
                    if (item != null) {
                      setBoardAndFetchClasses(item.toString());
                    }
                  } catch (error) {}
                }}
                onRightIconPress=""
                showLabel={true}
              />
              {boardError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>{boardError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <Picker
                label="class"
                labelSize={16}
                labelColor={Colors.lightGrayColor}
                data={classes}
                rightIconName=""
                rightIconSize=""
                rightIconColor=""
                value={standard}
                // docTypeHandler=  {(item)=>{
                //   setStandard(item)
                //   // if(item.length>0){
                //   //   setClassAndFetchSubjects(item.toString())
                //   // }
                // }}
                docTypeHandler={(value) => {
                  validateStandard(value);
                  try {
                    console.log(value);
                    if (value.length > 0) {
                      setClassAndFetchSubjects(value.toString());
                    }
                  } catch (error) {}
                }}
                onRightIconPress=""
                showLabel={true}
              />
              {standardError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>
                    {standardError}
                  </Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}
              <Text style={styles.text}>Select your subjects</Text>

              <View
                style={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  width: width,
                  paddingHorizontal: 15,
                }}>
                <SubjectsCheckboxes />
              </View>

              <View style={{marginLeft: '75%'}}>
                <TouchableOpacity
                  onPress={() => {
                    addSubjectsToClass();
                  }}>
                  <Text style={styles.button1}>ADD</Text>
                </TouchableOpacity>
              </View>
              {tutorSubjects && tutorSubjects.length > 0 && (
                <View>
                  <View>
                    <CourseTable
                      boardClassSubjectsMap={boardClassSubjectsMap}
                      handleRemoveItem={removeTutorSubjects}
                    />
                  </View>
                </View>
              )}
            </ScrollView>
            <Button
              backgroundColor={'#4C8BF5'}
              size="lg"
              borderRadius={5}
              title="Next"
              textStyles={{
                fontSize: 18,
                fontStyle: 'normal',
                fontWeight: '400',
                color: '#fff',
              }}
              onPress={() => {
                handelSubmit();
              }}
            />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default TutorCourseInformation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginLeft: HeightWidth.getResWidth(2),
    marginRight: HeightWidth.getResWidth(2),
  },
  input: {
    padding: 10,
    margin: 10,
    fontSize: 12,
    overflow: 'hidden',
  },
  title: {
    fontWeight: '500',
    fontSize: HeightWidth.getResFontSize(24),
    textAlign: 'center',
    marginVertical: 8,
    paddingBottom: '5%',
  },
  title2: {
    fontWeight: '400',
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'left',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footer: {
    flex: 0.2,
    // marginVertical: 8,
    alignItems: 'center',
    // marginBottom: 50,
  },
  body: {},
  form: {
    borderRadius: 18,
    backgroundColor: 'white',
    //  padding:20,
    textAlign: 'center',
    //  paddingTop:"10%",
    //  marginTop:'10%'
  },
  logo: {
    width: 50,
    height: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
  link1: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(12),
    backgroundColor: 'white',
  },
  link2: {
    fontWeight: '400',
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(16),
    paddingTop: '5%',
    paddingBottom: '8%',
    marginLeft: '60%',
    borderRadius: 5,
    backgroundColor: 'white',
  },
  link3: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(18),
    paddingVertical: 10,
    backgroundColor: 'white',
    // paddingHorizontal: 20,
  },
  text: {
    fontSize: HeightWidth.getResFontSize(14),
    color: 'grey',
  },
  btn: {
    backgroundColor: '#f5c242',
    borderRadius: 8,
    padding: 20,
    margin: 20,
    textAlign: 'center',
  },
  button: {
    fontWeight: '400',
    backgroundColor: '#4C8BF5',
    borderRadius: 5,
    color: 'white',
    fontSize: HeightWidth.getResFontSize(18),
    overflow: 'hidden',
    padding: '5%',
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  img: {
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  linknav: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button1: {
    backgroundColor: '#4C8BF5',
    // borderColor: 'black',
    // borderWidth: 1,
    borderRadius: 5,
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
    overflow: 'hidden',
    padding: 12,
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
});
