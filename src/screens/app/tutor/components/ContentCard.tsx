import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

//utilities
import { Colors } from '../../../../utility/colors';
import HeightWidth from '../../../../utility/HeightWidth';
import Icon from 'react-native-vector-icons/Feather';

interface ContentCardProps {
    index?: number | string;
    title?: string;
    contentHeight?: number;
    backgroundColor?: string;
    textColor?: string;
    onPress?: any;
    showRightIcon?: boolean;
    leftIconName?: any
}

const ContentCard = ({
    index,
    title,
    contentHeight = HeightWidth.getResWidth(40),
    backgroundColor = Colors.fullWhite,
    textColor = Colors.primaryBlue,
    onPress,
    showRightIcon = false,
    leftIconName
}: ContentCardProps) => {
    return (
        <TouchableOpacity activeOpacity={0.7} onPress={onPress} key={index} style={[
            styles.__container,
            { height: contentHeight, backgroundColor: backgroundColor }
        ]}>
            {leftIconName && <Image source={leftIconName} style={styles.__leftContentIcon} />}
            <View style={styles.__innerContainer}>
                {index && <Text style={[styles.__leftContentText, { color: textColor }]}>{index}</Text>}
                <Text numberOfLines={2} style={[styles.__rightContentText, { color: textColor }]}>{title}</Text>
            </View>
            {showRightIcon && <View style={styles.__rightContentIcon}>
                <Icon name={'chevron-right'} color={Colors.fullWhite} size={30} />
            </View>}
        </TouchableOpacity>
    )

     
}

export default ContentCard;

const styles = StyleSheet.create({
    __container: {
        paddingHorizontal: HeightWidth.getResWidth(5),
        backgroundColor: Colors.fullWhite,
        marginVertical: HeightWidth.getResWidth(5),
        flexDirection: "row",
        marginHorizontal: HeightWidth.getResWidth(10),
        borderRadius: 10,
        alignItems: "center",
    },
    __innerContainer: {
        flexDirection: "row",
        flex: 1
    },
    __leftContentIcon: {
        marginHorizontal: HeightWidth.getResWidth(5),
        alignSelf: "center",
        width: HeightWidth.getResWidth(30),
        height: HeightWidth.getResWidth(30),
    },
    __leftContentText: {
        width: HeightWidth.getResWidth(30),
        marginLeft: HeightWidth.getResWidth(5),
        fontSize: 17,
        color: Colors.primaryBlue
    },
    __rightContentIcon: {
        marginHorizontal: HeightWidth.getResWidth(10),
    },
    __rightContentText: {
        fontSize: 17,
        color: Colors.primaryBlue,
        flex: 1,
        paddingHorizontal: HeightWidth.getResWidth(5)
    },
})
