import React, {useState, FunctionComponent} from 'react';
import {View, Text, StyleSheet, Dimensions, Alert} from 'react-native';
import Button from '../../../../components/UI/Button';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import DocumentPicker from 'react-native-document-picker';
import Picker from '../../../../components/UI/Picker';
import DatePicker from 'react-native-datepicker';
import {Tutor} from '../../../../common/contracts/user';
import AsyncStorage from '@react-native-community/async-storage';
import TextInput from '../../../../components/UI/TextInput';
import {
  AADHAAR_PATTERN,
  PAN_PATTERN,
  IFSC_PATTERN,
  ACCOUNT_NO_PATTERN,
} from '../../../../common/validations/patterns';

//api
import {DocumentType} from '../../../../common/enums/document_type';
import {
  fetchUploadUrlForKycDocument,
  uploadKycDocument,
} from '../../../../common/api/document';
import {KycDocument} from '../../../../common/contracts/kyc_document';

//utilities
import {
  width,
  Colors,
  height,
  images,
  strings,
} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';

const data = [
  {label: 'PAN Card', value: DocumentType.PAN},
  {label: 'Aadhar Card', value: DocumentType.AADHAR},
  {label: 'BANK DETAILS', value: DocumentType.BANK},
];

interface Props {
  user: Tutor;
  nextButtonHandler: any;
  saveUser: (data: Tutor) => any;
}

const TutorOtherInformation: FunctionComponent<Props> = ({
  user,
  saveUser,
  nextButtonHandler,
}) => {
  const [documentType, setDocumentType] = useState(DocumentType.AADHAR);
  const [document, setDocument] = useState('');
  const [dob, setDob] = useState('09-10-2020');
  const [formData1, setFormData] = useState(new FormData());
  const [droppedFiles, setDroppedFiles] = useState<File[]>([]);
  const [dropzoneKey, setDropzoneKey] = useState(0);
  const [documents, setDocuments] = useState<KycDocument[]>(
    user.kycDetails || [],
  );
  const [documentNumber, setDocumentNumber] = useState('');
  const [aadhaar, setAadhaar] = useState('');
  const [pan, setPan] = useState('');
  const [bankAccount, setBankAccount] = useState('');
  const [bankIfsc, setBankIfsc] = useState('');
  const [currentBankDetails, setCurrentBankDetails] = useState({
    accountNo: '',
    ifsc: '',
  });
  const [touchedDoc, setTouchedDoc] = useState(false);
  const [touchedIfsc, setTouchedIfsc] = useState(false);
  const [touchedBank, setTouchedBank] = useState(false);
  const docTypeHandler = (e) => {
    setDocumentType(e);
    console.log('Doc type ====>', e);
  };

  const fileUploader = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      let localUri = res.uri;
      let filename = localUri.split('/').pop();
      let match = /\.(\w+)$/.exec(filename);
      let type = match ? `image/${match[1]}` : `image`;
      console.log(
        'local uri is=========>' +
          localUri +
          '   ' +
          'type is ==========>' +
          type +
          '   ' + // mime type
          'name is ========>' +
          filename +
          'size is ========>' +
          res.size,
      );

      //   let formData = new FormData();
      // // Assume "photo" is the name of the form field the server expects
      //   formData.append('photo', { uri: localUri, name: filename, type });
      setDocument(res.name);
    } catch (err) {
      setDocument(null);
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
        Alert.alert('Upload Cancel');
      } else {
        throw err;
      }
    }
  };
  const addDocument = async () => {
    if (droppedFiles.length < 1) return;
    // console.log("we are adding document now ",formData1);
    // console.log("formdata name",formData1._parts[0][1].name)
    // console.log("formdata name",formData1._parts[0][1].type)
    // console.log("formdata name",formData1._parts[0][1].type)
    //Validations
    if (aadhaar !== '') {
      if (
        !AADHAAR_PATTERN.test(
          aadhaar
            .replace(/[^\dA-Z]/g, '')
            .replace(/(.{4})/g, '$1 ')
            .trim(),
        )
      ) {
        alert(
          'documentNumber' +
            'Invalid aadhaar number' +
            'Invalid aadhaar number',
        );
        return;
      } else {
      }
    }
    if (pan !== '') {
      if (!PAN_PATTERN.test(pan)) {
        alert('documentNumber' + 'Invalid pan number' + 'Invalid pan number');
        return;
      } else {
      }
    }
    if (documentType === DocumentType.BANK) {
      if (!ACCOUNT_NO_PATTERN.test(bankAccount)) {
        alert(
          'bankAccount' + 'Invalid account number' + 'Invalid account number',
        );
        return;
      } else {
      }
      if (!IFSC_PATTERN.test(bankIfsc)) {
        alert('bankIfsc' + 'Invalid IFSC code' + 'Invalid IFSC code');
        return;
      } else {
      }
    }
    /******** */
    const file = formData1._parts[0][1];
    const clonedDocuments = [...documents];

    const documentIndex = clonedDocuments.findIndex(
      (document) =>
        document.kycDocType.toLowerCase() === documentType.toLowerCase(),
    );

    if (documentIndex > -1) {
      clonedDocuments.splice(documentIndex, 1);
    }

    // console.log(file.type)
    // console.log(documentType)
    // console.log(file.name)

    setDocuments([
      ...clonedDocuments,
      {
        kycDocFormat: file.type,
        kycDocType: documentType,
        kycDocLocation: file.name,
      },
    ]);
    setDroppedFiles([]);
    setDocumentNumber('');
    if (bankAccount && bankIfsc) {
      setCurrentBankDetails({accountNo: bankAccount, ifsc: bankIfsc});
    }
    setBankAccount('');
    setBankIfsc('');
    setDropzoneKey(dropzoneKey + 1);

    const formData = formData1;

    // formData.append('document', file);

    try {
      const awsBucket = await fetchUploadUrlForKycDocument({
        fileName: file.name,
        contentType: file.type,
        contentLength: file.size,
      });

      await uploadKycDocument(awsBucket.url, formData);
    } catch (error) {
      console.log('cathing here ', error);
      if (error.response?.status === 401) {
        //
      }
    }
  };

  const handelSubmit = () => {
    if (documents.length < 1) return;
    saveUser({
      ...user,
      kycDetails: documents,
      dob: dob.toString(),
      aadhaar: aadhaar ? aadhaar : undefined,
      pan: pan ? pan : undefined,
      bankAccount: currentBankDetails.accountNo
        ? currentBankDetails.accountNo
        : undefined,
      bankIfsc: currentBankDetails.ifsc ? currentBankDetails.ifsc : undefined,
    });
    nextButtonHandler();
  };

  const handelSkip = () => {
    nextButtonHandler();
    saveUser({...user});
  };

  return (
    <View style={styles.container}>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          justifyContent: 'space-between',
          flexDirection: 'column',
        }}>
        <View>
          <Text style={styles.text}>Date Of Birth :</Text>
          <View>
            <DatePicker
              style={styles.dateInput}
              date={dob} // Initial date from state
              mode="date" // The enum of date, datetime and time
              placeholder="select date"
              format="DD-MM-YYYY"
              useNativeDriver="true"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              onDateChange={(dob) => {
                setDob(dob);
                console.log(dob);
              }}
            />
          </View>
          <Text style={styles.text}> Upload Documents </Text>

          <Picker
            showLabel={false}
            label="Document Type"
            labelSize={16}
            labelColor={Colors.lightGrayColor}
            data={data}
            rightIconSize={25}
            rightIconColor={Colors.lightGrayColor}
            rightIconName="attach"
            onRightIconPress={fileUploader}
            docTypeHandler={docTypeHandler}
            value={documentType}
          />
        </View>
        <View>
          {documentType === DocumentType.BANK ? (
            <TouchableOpacity
              style={styles.button2}
              onPress={() => {
                fileUploader();
              }}>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={styles.title2}> Upload cancelled cheque leaf</Text>
                {/* <MaterialCommunityIcons name="cloud-upload-outline" color={'white'} size={50} /> */}
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={styles.button2}
              onPress={() => {
                fileUploader();
              }}>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={styles.title2}> Upload {documentType} Card</Text>
                {/* <MaterialCommunityIcons name="cloud-upload-outline" color={'white'} size={50} /> */}
              </View>
            </TouchableOpacity>
          )}
        </View>
        {documentType !== DocumentType.BANK && (
          <TextInput
            width="lg"
            otherStyle={{
              borderBottomColor: Colors.verylightGrey,
              borderBottomWidth: 1,
            }}
            name="documentNumber"
            style={styles.input}
            placeholder={'Your ' + documentType + ' Number'}
            onChangeText={() => {}}
            onBlur={() => {
              setTouchedDoc(true);
              // handleBlur('documentNumber');
              setDocumentNumber(documentNumber);
            }}
            value={documentNumber}
          />
        )}

        {documentType === DocumentType.BANK && (
          <View>
            <TextInput
              name="bankAccount"
              style={styles.input}
              placeholder="Your bank account number"
              // onChangeText={handleChange('bankAccount')}
              onBlur={() => {
                setTouchedBank(true);

                setBankAccount(bankAccount);
              }}
              value={bankAccount}
            />

            <TextInput
              name="bankIfsc"
              style={styles.input}
              placeholder="Your bank IFSC code"
              onChangeText={() => {}}
              onBlur={() => {
                setTouchedIfsc(true);
                // handleBlur('bankIfsc')
                setBankIfsc(bankIfsc);
              }}
              value={bankIfsc}
            />
          </View>
        )}
        <View>
          <View style={{marginLeft: '75%'}}>
            <TouchableOpacity
              onPress={() => {
                addDocument();
              }}>
              <Text style={styles.button1}>ADD</Text>
            </TouchableOpacity>
          </View>

          {documents.map((document, index) => (
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
              key={index}
              // onPress={(index) => { removeDocument(index) }}
            >
              <Text style={styles.title2}> {document.kycDocType} </Text>
              {/* <MaterialCommunityIcons name="minus" color={'grey'} size={30} /> */}
            </TouchableOpacity>
          ))}
        </View>

        <View style={[styles.bottomBtn]}>
          <Button
            backgroundColor={'#4C8BF5'}
            size="lg"
            borderRadius={5}
            title="Next"
            textStyles={{
              fontSize: 18,
              fontStyle: 'normal',
              fontWeight: '400',
              color: '#fff',
            }}
            onPress={() => {
              handelSubmit();
            }}
            // onPress={console.log('pressed buttton')}
          />
          <Button
            backgroundColor={'#fff'}
            size="lg"
            borderRadius={5}
            title="Skip"
            textStyles={{
              fontSize: 18,
              fontStyle: 'normal',
              fontWeight: '400',
              color: '#4C8BF5',
            }}
            onPress={() => {
              handelSkip();
            }}
            // onPress={console.log('pressed buttton')}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default TutorOtherInformation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginLeft: HeightWidth.getResWidth(2),
    marginRight: HeightWidth.getResWidth(2),
  },
  input: {
    padding: 10,
    margin: 10,
    fontSize: 12,
    overflow: 'hidden',
  },
  title: {
    fontWeight: '500',
    fontSize: HeightWidth.getResFontSize(24),
    textAlign: 'center',
    marginVertical: 8,
    paddingBottom: '5%',
  },
  title2: {
    fontWeight: '400',
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'left',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footer: {
    flex: 0.2,
    // marginVertical: 8,
    alignItems: 'center',
    // marginBottom: 50,
  },
  body: {},
  form: {
    borderRadius: 18,
    backgroundColor: 'white',
    //  padding:20,
    textAlign: 'center',
    //  paddingTop:"10%",
    //  marginTop:'10%'
  },
  logo: {
    width: 50,
    height: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
  link1: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(12),
    backgroundColor: 'white',
  },
  link2: {
    fontWeight: '400',
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(16),
    paddingTop: '5%',
    paddingBottom: '8%',
    marginLeft: '60%',
    borderRadius: 5,
    backgroundColor: 'white',
  },
  link3: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(18),
    paddingVertical: 10,
    backgroundColor: 'white',
    // paddingHorizontal: 20,
  },
  text: {
    fontSize: HeightWidth.getResFontSize(14),
    color: 'grey',
  },
  btn: {
    backgroundColor: '#f5c242',
    borderRadius: 8,
    padding: 20,
    margin: 20,
    textAlign: 'center',
  },
  button: {
    fontWeight: '400',
    backgroundColor: '#4C8BF5',
    borderRadius: 5,
    color: 'white',
    fontSize: HeightWidth.getResFontSize(18),
    overflow: 'hidden',
    padding: '5%',
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  img: {
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  linknav: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button1: {
    backgroundColor: '#4C8BF5',
    // borderColor: 'black',
    // borderWidth: 1,
    borderRadius: 5,
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
    overflow: 'hidden',
    padding: 12,
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  dateInput: {
    padding: 2,
    margin: 2,
    fontSize: 12,
    overflow: 'hidden',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    marginTop: '2%',
    width: '100%',
  },
  bottomBtn: {
    flex: 1,
    justifyContent: 'flex-end',
    marginTop: '5%',
    alignItems: 'center',
  },
  addText: {
    fontSize: 16,
    fontWeight: '600',
    color: Colors.activeColor,
    flex: 1,
    textAlign: 'right',
    marginHorizontal: 5,
  },
  button2: {
    backgroundColor: '#1e90ff',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 24,
    // fontWeight: 'bold',
    // overflow: 'hidden',
    padding: 12,
    marginBottom: '2%',
    marginVertical: '2%',
    textAlign: 'center',
  },
});
