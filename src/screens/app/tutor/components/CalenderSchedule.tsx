import { element } from 'prop-types';
import React, { useState,FunctionComponent,useEffect } from 'react'
import { Text, StyleSheet, ScrollView, TextInput, TouchableOpacity, Image, View, ImageBackground, Dimensions } from 'react-native'
import { colors } from 'react-native-elements';
import { fetchSchedulesList } from '../../../../common/api/academics'
import HeightWidth from '../../../../utility/HeightWidth';
//import Moment from 'react-moment';
import {connect, useDispatch} from 'react-redux';
import { useFocusEffect } from '@react-navigation/native';
//import {getClickedScheduleItemAction,getScheduleListAction,getCalenderScreenTypeAction} from '../../screens/auth/store/actions'
const weekDaysTitle =  ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday',]
const tableTitle = ['Morning', 'Afternoon', 'Evening']
const tableData = [  
    ['1','2','3'], ['1','2','3'], ['1','2','3'], ['1','2','3'], ['1','2','3'], ['1','2','3'],['1','2','3'],
]

interface Props {
  call?: any;
  itemState?:any;
  scheduleList?:any;
  getScheduleData?:any;
}

interface setArr{
  sundayListSortTimeWise:[]
}

var moment = require('moment');

const CalendarSchedule: FunctionComponent<Props> = ({ getScheduleData,call,itemState,scheduleList }) => {  
  const [getScheduleList,setScheduleList]=useState([]);
  const [getColumnNumberInRow,setColumnNumberInRow]=useState(null);
  const [getRowData,setRowData]=useState([]);
  const dispatch = useDispatch();

  useFocusEffect(
    React.useCallback(() => {
      const getValue=async ()=>{
        let value=await fetchSchedulesList();
        setScheduleArray(value)
        }
      getValue()
    }, [])
  );
  //console.log(call,itemState,scheduleList,'----------------------');
  
  const setScheduleArray=(setArray)=>{
   let getScheduleLists=[];
   //console.log(setArray,'--------------------------');
   
   
   setArray.forEach(element => {
    getScheduleLists.push({
      id:element._id,
      fromhour:element.fromhour,
      tohour:element.tohour,
      batchfriendlyname:element.batch.batchfriendlyname,
      boardname:element.batch.boardname,
      classname:element.batch.classname,
      subjectname:element.batch.subjectname,
      dayname:element.dayname,
    })
      
   });
   scheduleList(getScheduleLists);
   //dispatch(getScheduleListAction(getScheduleLists));
   setScheduleListArray(getScheduleLists)
  }

  const displayScheduleItem=(item,x,y,z)=>(
    item?(<View>
      <Text style={styles._timeSchedule}>
        {item.fromhour}{' '}{'-'}{item.tohour}{','}{item.batchfriendlyname}{','}{item.boardname}
      </Text>
      <Text style={styles._className}>{item.classname}{'-'}{item.subjectname}</Text>
    </View>):<Text></Text>
  )

  const weekDaysScheduleClass = (data,ids) => (
    <View key={ids}>
      <View  style={styles.rowDaysContainer}><View style={styles.titleColumn}><Text style={styles.weekTitleName}>{tableTitle[ids]}</Text></View>
      <View style={styles.rowDataContainer}>
        {tableData.map((ele, index1) => (<View style={styles.shedulePerTime} key={index1}>
          {ele.map((element,index)=>(<TouchableOpacity  
          key={index}
            style={[styles.dataRowText,{backgroundColor:index ==0?'#ADE394':'rgba(151, 223, 254, 1)'}]} 
            onPress={()=>sheduleBoxContainer(getScheduleList[ids][index1][index])}>
          <View>
              {displayScheduleItem(getScheduleList[ids][index1][index],index,index1,ids)}
              </View>
            </TouchableOpacity>))}
        </View>))}</View></View>
    
      
    </View>
  )

  const setScheduleListArray=(list)=>{
    //console.log(list,'--------------------->');
  let morningList=[];
  let afternoonList=[];
  let eveningList=[];
  let startMorningTime=moment('00:01', "HH:mm:ss")
  let startAfterNoonTime=moment('12:00', "HH:mm:ss")
  let startEveningTime=moment('16:00', "HH:mm:ss");
  let getScheduleListItem=[]
  
  list.forEach((ele,id)=>{
    let eleTime=moment(ele.fromhour, "HH:mm:ss")
    let differenceTime=eleTime.diff(startAfterNoonTime)
    if(eleTime.diff(startMorningTime) > 0 && eleTime.diff(afternoonList) <0){
      morningList.push(ele)
    }
    if(eleTime.diff(afternoonList) > 0 && eleTime.diff(startEveningTime) <0){
      afternoonList.push(ele)
    }
    if(eleTime.diff(startEveningTime) > 0){
      eveningList.push(ele)
    }
  })
  let dayWiseMorningList=getFilterDayWiseList(morningList)
  let dayWiseAfternoonList=getFilterDayWiseList(afternoonList)
  let dayWiseEveningList=getFilterDayWiseList(eveningList)
  getScheduleListItem.push(dayWiseMorningList,dayWiseAfternoonList,dayWiseEveningList)
  
  setScheduleList(getScheduleListItem)      
  setTableRow(getColumnNumberInRow)
  }

  const getFilterDayWiseList=(list)=>{
    let daysList=[];
    let sundayList=[]
    let mondayList=[]
    let tuesdayList=[]
    let wednesdayList=[]
    let thoursdayList=[]
    let fridayList=[]
    let saturdayList=[]
    list.forEach((ele,id)=>{
      if(ele.dayname == weekDaysTitle[0]){
        mondayList.push(ele)
      }
      if(ele.dayname == weekDaysTitle[1]){
        tuesdayList.push(ele)
      }
      if(ele.dayname == weekDaysTitle[2]){
        wednesdayList.push(ele)
      }
      if(ele.dayname == weekDaysTitle[3]){
        thoursdayList.push(ele)
      }
      if(ele.dayname == weekDaysTitle[4]){
        fridayList.push(ele)
      }
      if(ele.dayname == weekDaysTitle[5]){
        saturdayList.push(ele)
      }
      if(ele.dayname == weekDaysTitle[6]){
        sundayList.push(ele)
      }
    })
    let sundayListSortTimeWise=[]
    let mondayListSortTimeWise=[]
    let tuesdayListSortTimeWise=[]
    let wednesdayListSortTimeWise=[]
    let thoursdayListSortTimeWise=[]
    let fridayListSortTimeWise=[]
    let saturdayListSortTimeWise=[]
    sundayListSortTimeWise=setSortingTimeWise(sundayList);
    mondayListSortTimeWise=setSortingTimeWise(mondayList);
    tuesdayListSortTimeWise=setSortingTimeWise(tuesdayList);
    wednesdayListSortTimeWise=setSortingTimeWise(wednesdayList);
    thoursdayListSortTimeWise=setSortingTimeWise(thoursdayList);
    fridayListSortTimeWise=setSortingTimeWise(fridayList);
    saturdayListSortTimeWise=setSortingTimeWise(saturdayList);
    daysList.push(      
      mondayListSortTimeWise,
      tuesdayListSortTimeWise,
      wednesdayListSortTimeWise,
      thoursdayListSortTimeWise,
      fridayListSortTimeWise,
      saturdayListSortTimeWise,
      sundayListSortTimeWise,
      )
     return  daysList
  }

  const setTableRow=(getColumn)=>{
    //console.log(getColumn,'===================',getColumn > 0);
    let arr=[];
    if(getColumn > 0){
      
      for (let i; i < getColumn; i++){
        arr.push('1')
        //console.log(arr,'===================',getColumn > 0);
        //console.log(arr,getColumn,'===================');
      }
    }
   
    
    else{
      for (let i; i < 2; i++){
        arr.push('1')
        //console.log(arr,getColumn,'---------------');
      }
    }
    //console.log(arr,getColumn);
    let getRows=[]
    for (let k; k < 7; k++){
      getRows.push(arr)
    }
    if(getRows.length == 0){
      const tableData = [  
        ['1','2','3'], ['1','2','3'], ['1','2','3'], ['1','2','3'], ['1','2','3'], ['1','2','3'],['1','2','3'],
    ]
    setRowData(tableData);
    }
    else{
      setRowData(getRows);
    }
  }

  const setSortingTimeWise=(list)=>{
    if(getColumnNumberInRow < list.length){
      setColumnNumberInRow(list.length)
    }
    if(list.length>1){
      list.sort((a,b)=>{
        let aTime=moment(a.fromhour, "HH:mm:ss")
        let bTime=moment(b.fromhour, "HH:mm:ss")
        
        return aTime.diff(bTime)
      })
    }
    return list
  }

 

  const weekDaysContainer = (data) => (
    <Text style={styles.weekDaysContainer}>{data}</Text>
  )

  const sheduleBoxContainer=(e)=>{
    itemState(e);
    call(true);
  }

  return getScheduleList.length>0 ?(
    <ScrollView horizontal={true} contentContainerStyle={styles.bodyContainer}>
     <View style={styles.wraper}>
        <View style={styles.headRow}>
        </View>
        <View style={styles.weekDaysName}>{weekDaysTitle.map((item, id) => <View key={id}>{weekDaysContainer(item)}</View>)}</View>
      </View>
      {(<View style={styles.rowDataBox}>
        {tableTitle.map((item, id) => (<View key={id} style={{ marginTop: 20 }}>{weekDaysScheduleClass(item,id)}</View>))}
      </View>)}
    </ScrollView>
  ):(<Text style={{color:'red',width:"100%",height:300,margin:100,alignItems:'center'}}>Unable to fetch data</Text>)
}

const mapStateToProps = (state) => ({
});

//export default CalendarSchedule;
export default connect(mapStateToProps)(CalendarSchedule);

const styles = StyleSheet.create({
  _className:{
    fontSize: HeightWidth.getResFontSize(14),
    fontWeight:'bold'
  },
  _timeSchedule:{
    fontSize: HeightWidth.getResFontSize(10),
  },
  bodyContainer: {
    width: HeightWidth.getResWidth(Math.round(Dimensions.get('window').width * 4)),
    height: HeightWidth.getResHeight(900),
    overflow: 'scroll',
    backgroundColor: '#fff',
    padding: HeightWidth.getResWidth(20),
    flexDirection: 'column',
  },
  wraper: {
    flexDirection: 'row',
    width: '100%',
    height: HeightWidth.getResHeight(72)
  },
  headRow: {
    width: '5%'
  },
  weekDaysName: {
    width: '95%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  weekDaysContainer: {
    height: HeightWidth.getResHeight(50),
    width: HeightWidth.getResWidth(168),
    borderColor: 'grey',
    borderRadius: 5,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    paddingHorizontal: 5,
    textAlign: 'center',
    fontWeight: '700',
    fontSize: HeightWidth.getResFontSize(14),
    color: '#000',
    fontFamily: 'Roboto'
  },
  dataRowText: {
    //height: '48%',
    height: HeightWidth.getResHeight(65),
    //minHeight:HeightWidth.getResHeight(40),
    maxHeight:HeightWidth.getResHeight(90),
    width: HeightWidth.getResWidth(168),
    justifyContent: 'center',
    borderColor: 'grey',
    borderRadius: 5,
    borderWidth: 1,
    paddingHorizontal: 5,
    paddingVertical: 5,
    textAlign: 'center',
    fontWeight: '700',
    fontSize: HeightWidth.getResFontSize(14),
    color: '#000',
    fontFamily: 'Roboto'
  },
  shedulePerTime: {
    flexDirection: 'column',
    //height: HeightWidth.getResHeight(150),
    justifyContent: 'space-between',
    //backgroundColor:'red'
  },

  weekTitleName: {
    height: HeightWidth.getResHeight(60),
    width: HeightWidth.getResWidth(150),
    //width:'100%',
    borderColor: 'grey',
    borderRadius: 5,
    borderWidth: 1,
    padding: 5,
    textAlign: 'center',
    fontWeight: '700',
    fontSize: HeightWidth.getResFontSize(14),
    color: '#000',
    fontFamily: 'Roboto',

  },
  rowDataBox: {
    flexDirection: 'column',
    width: '100%',
    height: HeightWidth.getResHeight(700),
    //backgroundColor:'green'
  },
  rowDaysContainer: {
    flexDirection: 'row',
    width: '100%',
    //height:'100%',
    minHeight:HeightWidth.getResHeight(200),
    maxHeight:HeightWidth.getResHeight(300),
    //backgroundColor:'red'
  },
  titleColumn: {
    width: '5%',
    transform: [{ rotate: '270deg' }],
    marginTop: 85,
    //height:HeightWidth.getResHeight(50);
  },
  rowDataContainer: {
    width: '95%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
})
