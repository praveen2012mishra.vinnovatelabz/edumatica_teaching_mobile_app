import React, {useState, FunctionComponent, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {Checkbox} from 'react-native-paper';
import Picker from '../../../../../components/UI/Picker';
import Button from '../../../../../components/UI/Button';
import {ScrollView} from 'react-native-gesture-handler';
import {Tutor, User} from '../../../../../common/contracts/user';
import {BoardClassSubjectsMap} from '../../../../../common/academics/contracts/board_class_subjects_map';
import {
  fetchBoardsList,
  fetchClassesList,
  fetchSubjectsList,
} from '../../../../../common/api/academics';
import {Chip} from 'react-native-paper';
import TextInput from '../../../../../components/UI/TextInput';
import {Switch} from 'react-native-paper';
import {updateTutor} from '../../../../../common/api/profile';

import CourseTable from '../../../../../components/table/course_table';
import * as Animatable from 'react-native-animatable';
import {connect, useDispatch} from 'react-redux';

import {Course} from '../../../../../common/academics/contracts/course';
import {Board} from '../../../../../common/academics/contracts/board';
import {Standard} from '../../../../../common/academics/contracts/standard';

//utilities
import {width, Colors} from '../../../../../utility/utilities';
import HeightWidth from '../../../../../utility/HeightWidth';
import {setAuthUser} from '../../../../auth/store/actions';

interface Props {
  user: Tutor;
  saveUser: (data: Tutor) => any;
  navigation: any;
}

const EditCourseInformation: FunctionComponent<Props> = ({
  user,
  saveUser,
  navigation,
}) => {
  const data = [];
  const data2 = [];
  const [tutorSubjects, setTutorSubjects] = useState<Course[]>(
    user.courseDetails || [],
  );
  const [board, setBoard] = useState('');
  const [standard, setStandard] = useState(''); // Standard describes Class of the Tutor.
  const [subjectsList, setSubjectsList] = useState<
    {name: string; checked: boolean}[]
  >([]);
  const [boards, setBoards] = useState<Board[]>([]);
  const [subject, setSubject] = useState<Board[]>([]);

  const [classes, setClasses] = useState<Standard[]>([]);
  const [boardError, setBoardError] = useState('');
  const [standardError, setStandardError] = useState('');
  const [submitError, setSubmitError] = useState('');
  const [customCourse, setCustomCourse] = useState(false);
  const [chip, setChip] = useState('');
  const dispatch = useDispatch();

  const color = [
    '#66CCFF',
    '#FFCC00',
    '#1C9379',
    '#8A7BA7',
    '#66CCFF',
    '#FFCC00',
    '#1C9379',
    '#8A7BA7',
  ];
  const randomColor = () => {
    let col = color[Math.floor(Math.random() * color.length)];
    return col;
  };

  useEffect(() => {
    (async () => {
      try {
        const [boardsList] = await Promise.all([fetchBoardsList()]);
        //creating boards list for picker

        for (var i = 0; i < boardsList.length; i++) {
          data.push({
            label: boardsList[i].boardName,
            value: boardsList[i].boardName,
          });
        }
        setBoards(data);
      } catch (error) {
        if (error.response?.status === 401) {
          // setRedirectTo('/login');
        }
      }
    })();
  }, []);

  const validateBoard = (value) => {
    if (value != 0) {
      setBoardError('');
    }
  };
  const validateStandard = (value) => {
    if (value != 0) {
      setStandardError('');
    }
  };

  const setBoardAndFetchClasses = async (board: string) => {
    try {
      await setBoard(board);
      console.log(board);
      if (board.length > 1) {
        const classListResponse = await fetchClassesList({boardname: board});
        console.log('we are coming here');
        console.log(classListResponse);
        for (var i = 0; i < classListResponse.length; i++) {
          data2.push({
            label: classListResponse[i].className,
            value: classListResponse[i].className,
          });
        }
        console.log(data2);
        setClasses(data2);
      } else {
        setClasses([]);
      }
    } catch (error) {
      if (error.response?.status === 401) {
        // setRedirectTo('/');
      }
    }
  };

  const setClassAndFetchSubjects = async (standard: string) => {
    try {
      await setStandard(standard);
      console.log(standard);
      if (board.length > 1 && standard.length > 1) {
        const response = await fetchSubjectsList({
          boardname: board,
          classname: standard,
        });
        // dispatch(setSubjects(response));
        const structuredSubjectsList = response.map((subject) => ({
          name: subject.subjectName,
          checked: false,
        }));
        setSubjectsList(structuredSubjectsList);
        console.log(structuredSubjectsList);
      } else {
        setSubjectsList([]);
      }
    } catch (error) {
      if (error.response?.status === 401) {
        // setRedirectTo('/');
      }
    }
  };
  const handleChangeInSubjectCheckbox = (index: number) => {
    const subjects = subjectsList.map((subject, sIndex) => {
      if (index !== sIndex) return subject;
      return {...subject, checked: !subject.checked};
    });

    setSubjectsList(subjects);
  };

  const removeTutorSubjects = (map: BoardClassSubjectsMap) => {
    const mapSubjects = map.subjects.map((subject) => subject.toLowerCase());

    const tutorSubjectsMap = tutorSubjects.filter(
      (subjectItem) =>
        subjectItem.board !== map.boardname ||
        subjectItem.className.toString().toLowerCase() !==
          map.classname.toString().toLowerCase() ||
        mapSubjects.indexOf(subjectItem.subject.toLowerCase()) === -1,
    );

    setTutorSubjects(tutorSubjectsMap);
  };

  const saveSubjectInformation = async (data: Tutor) => {
    const User = Object.assign({}, user, data);

    try {
      dispatch(setAuthUser(User));
      await updateTutor(User);
      alert('Added Successfully');
    } catch (error) {
      alert('Unable to Add');
      if (error.response?.status === 401) {
        alert("setRedirectTo('/login');");
      } else {
        dispatch(setAuthUser(User));
      }
    }
  };

  const submitCourseInformation = () => {
    if (tutorSubjects.length < 1) return;
    saveSubjectInformation({...user, courseDetails: tutorSubjects});
  };

  const boardClassSubjectsMap: BoardClassSubjectsMap[] = [];

  for (let i = 0; i < tutorSubjects.length; ++i) {
    const subject = tutorSubjects[i];

    let boardClassSubjectIndex = boardClassSubjectsMap.findIndex(
      (boardClassSubject) =>
        boardClassSubject.boardname.toLowerCase() ===
          subject.board.toLowerCase() &&
        boardClassSubject.classname.toString().toLowerCase() ===
          subject.className.toString().toLowerCase(),
    );

    if (boardClassSubjectIndex === -1) {
      boardClassSubjectsMap.push({
        boardname: subject.board,
        classname: subject.className,
        subjects: [],
      });

      boardClassSubjectIndex = boardClassSubjectsMap.length - 1;
    }

    boardClassSubjectsMap[boardClassSubjectIndex].subjects.push(
      subject.subject,
    );
  }

  const [isSelected, setSelection] = useState(['']);

  const SubjectsCheckboxes = () => (
    <View style={styles.__subjectCheckbox}>
      {subjectsList.map((subject, index) => (
        <View style={styles.__checkboxTitle}>
          <Text key={index}>{subject.name}</Text>
          <View style={styles.__checkbox}>
            <Checkbox
              key={index}
              status={subject.checked ? 'checked' : 'unchecked'}
              onPress={() => {
                handleChangeInSubjectCheckbox(index);
              }}
            />
          </View>
          {/* </TouchableOpacity> */}
        </View>
      ))}
    </View>
  );

  const addSubjectsToClass = () => {
    console.log('board', board);
    console.log('standard', standard);
    if (board == null) return;
    if (standard == null) return;

    const map: Course[] = [];
    console.log(subjectsList);
    subjectsList
      .filter((subject) => subject.checked)
      .filter((subject) => {
        const subjectExists = tutorSubjects.find(
          (tutorSubject) =>
            tutorSubject.board.toLowerCase() === board.toLowerCase() &&
            tutorSubject.className.toString().toLowerCase() ===
              standard.toString().toLowerCase() &&
            tutorSubject.subject.toLowerCase() === subject.name.toLowerCase(),
        );

        return subjectExists === undefined;
      })
      .forEach((subject) => {
        map.push({board, className: standard, subject: subject.name});
      });

    setTutorSubjects([...tutorSubjects, ...map]);
    setSubjectsList([]);
  };

  const handelSubmit = () => {
    if (board == '0' || standard == '0') {
      setSubmitError('All fields are required field');
      setBoardError('Board must be selected');
      setStandardError('Standard must be selected');
    } else if (boardError.length > 0 || standardError.length > 0) {
      setSubmitError('All fields are required field');
    } else {
      submitCourseInformation();
    }
  };

  const handleCustomSubjectsChange = (sub) => {
    const obj1 = {name: sub, checked: true};
    const subjects = [...subjectsList, obj1];

    setSubjectsList(subjects);
    console.log(subjects);
  };

  const addChip = () => {
    handleCustomSubjectsChange(chip);
    setChip('');
  };

  return (
    <SafeAreaView>
      <ScrollView showsVerticalScrollIndicator={false} style={styles.__pad}>
        <View style={styles.__custom}>
          <Switch
            value={customCourse}
            onValueChange={(e) => {
              // setStandard("")
              // setBoard("")
              setCustomCourse(!customCourse);
            }}
          />
          <Text style={styles.__title}>Custom Course</Text>
        </View>
        {/* {!customCourse ? (
          <Text>Not custom</Text>
        ) : (
          <Text>Custom</Text>
        )} */}
        {!customCourse ? (
          <Picker
            label="Boards"
            labelSize={13}
            labelColor={Colors.primaryBlue}
            data={boards}
            rightIconName=""
            rightIconSize=""
            rightIconColor=""
            value={board}
            docTypeHandler={async (item) => {
              validateBoard(item);
              try {
                console.log(item);
                if (item != null) {
                  setBoardAndFetchClasses(item.toString());
                }
              } catch (error) {}
            }}
            onRightIconPress=""
            showLabel={true}
          />
        ) : (
          <View>
            <Text style={styles.__text}>Boards</Text>
            <TextInput
              width="lg"
              otherStyle={{
                borderBottomColor: Colors.verylightGrey,
                borderBottomWidth: 1,
              }}
              name="Board"
              placeholder={'Enter Board Name'}
              onChangeText={(item) => {
                setBoard(item);
                // handleBlur('documentNumber');
              }}
              onBlur={() => {}}
              value={board}
            />
          </View>
        )}
        {boardError.length > 1 ? (
          <Animatable.View
            animation="fadeInLeft"
            duration={500}
            style={{alignSelf: 'flex-start'}}>
            <Text style={{color: 'red', fontSize: 15}}>{boardError}</Text>
          </Animatable.View>
        ) : (
          <View></View>
        )}

        {!customCourse ? (
          <Picker
            label="class"
            labelSize={13}
            labelColor={Colors.primaryBlue}
            data={classes}
            rightIconName=""
            rightIconSize=""
            rightIconColor=""
            value={standard}
            docTypeHandler={(value) => {
              validateStandard(value);
              try {
                console.log(value);
                if (value.length > 0) {
                  setClassAndFetchSubjects(value.toString());
                }
              } catch (error) {}
            }}
            onRightIconPress=""
            showLabel={true}
          />
        ) : (
          <View>
            <Text style={styles.__text}>Class</Text>
            <TextInput
              width="lg"
              otherStyle={{
                borderBottomColor: Colors.verylightGrey,
                borderBottomWidth: 1,
              }}
              name="class"
              placeholder={'Enter class Name'}
              onChangeText={(item) => {
                setStandard(item);
                // handleBlur('documentNumber');
              }}
              onBlur={() => {}}
              value={standard}
            />
          </View>
        )}

        {standardError.length > 1 ? (
          <Animatable.View
            animation="fadeInLeft"
            duration={500}
            style={{alignSelf: 'flex-start'}}>
            <Text style={{color: 'red', fontSize: 15}}>{standardError}</Text>
          </Animatable.View>
        ) : (
          <View></View>
        )}

        {!customCourse ? (
          <View>
            <Text style={styles.__text}>Select your subjects</Text>
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                width: width,
                paddingHorizontal: 15,
              }}>
              <SubjectsCheckboxes />
            </View>
          </View>
        ) : (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'stretch',
            }}>
            <View style={{flex: 0.4}}>
              <Text style={styles.__text}>Select your subjects</Text>
              <TextInput
                name="subject"
                placeholder="Enter subject here ..."
                onChangeText={(item) => {
                  setChip(item);
                }}
                value={chip}
              />
            </View>
            <View style={{flex: 0.2}}>
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  addChip();
                }}>
                <Text>Add</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  setSubjectsList([]);
                }}>
                <Text>Clear</Text>
              </TouchableOpacity>
            </View>
            <View style={{flex: 0.3}}>
              {subjectsList.map((item) => {
                return (
                  <View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                      }}>
                      {/* <Text key={index}>{item.studentName}</Text> */}
                      <Chip
                        style={{backgroundColor: randomColor()}}
                        icon="delete"
                        onPress={() => {}}>
                        {item.name}
                      </Chip>
                    </View>
                  </View>
                );
              })}
            </View>
          </View>
        )}

        <View style={styles.__button2}>
          <Button
            title="Add+"
            size="sm"
            width={HeightWidth.getResWidth(80)}
            height={HeightWidth.getResHeight(45)}
            backgroundColor={Colors.primaryBlue}
            onPress={() => {
              addSubjectsToClass();
            }}
            borderRadius={5}
            textStyles={{
              fontSize: 14,
              fontStyle: 'normal',
              fontWeight: '400',
              color: Colors.veryLightGray,
            }}
          />
        </View>

        {tutorSubjects && tutorSubjects.length > 0 && (
          <View>
            <View style={styles.__pad}>
              <CourseTable
                boardClassSubjectsMap={boardClassSubjectsMap}
                handleRemoveItem={removeTutorSubjects}
              />
            </View>
          </View>
        )}
        <View style={styles.__pad}></View>

        <View style={styles.__buttonBot}>
          <Button
            backgroundColor={Colors.primaryBlue}
            size="sm"
            borderRadius={10}
            title="Save changes"
            textStyles={{
              fontSize: 14,
              fontStyle: 'normal',
              fontWeight: '400',
              color: Colors.veryLightGray,
            }}
            onPress={() => {
              handelSubmit();
            }}
          />
        </View>

        <View style={styles.__pad}></View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  __title: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(16),
    textAlign: 'center',
    marginVertical: 12,
  },
  __text: {
    fontSize: HeightWidth.getResFontSize(13),
    color: Colors.primaryBlue,
    paddingHorizontal: HeightWidth.getResWidth(8),
    paddingVertical: HeightWidth.getResHeight(8),
  },
  __custom: {flexDirection: 'row', justifyContent: 'flex-end'},
  button: {
    fontWeight: '400',
    backgroundColor: Colors.primaryBlue,
    borderRadius: 5,
    color: 'white',
    fontSize: HeightWidth.getResFontSize(18),
    overflow: 'hidden',
    padding: '5%',
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  button1: {
    backgroundColor: Colors.primaryBlue,
    borderRadius: 5,
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
    overflow: 'hidden',
    padding: 12,
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  __checkboxTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 12,
    margin: 12,
  },
  __checkbox: {
    borderWidth: 0.5,
    alignItems: 'stretch',
    borderRadius: 8,
    margin: 5,
  },
  __subjectCheckbox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  __button2: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    padding: 10,
  },
  __pad: {padding: 5, margin: 5},
  __buttonBot: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const mapStateToProps = (state) => ({
  user: state.auth.authUser as User,
});

export default connect(mapStateToProps)(EditCourseInformation);
