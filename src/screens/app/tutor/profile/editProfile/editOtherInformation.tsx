import React, {useState, FunctionComponent, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Alert,
  ToastAndroid,
  PermissionsAndroid,
} from 'react-native';
import {connect, useDispatch} from 'react-redux';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import DocumentPicker from 'react-native-document-picker';
import Picker from '../../../../../components/UI/Picker';
import DatePicker from 'react-native-datepicker';
import {Tutor, User} from '../../../../../common/contracts/user';
import AsyncStorage from '@react-native-community/async-storage';
import TextInput from '../../../../../components/UI/TextInput';
import {
  AADHAAR_PATTERN,
  PAN_PATTERN,
  IFSC_PATTERN,
  ACCOUNT_NO_PATTERN,
} from '../../../../../common/validations/patterns';
//image picker
import {Icon} from 'react-native-elements';

import {updateTutor} from '../../../../../common/api/profile';

import CustomModel from '../../../../../components/Other/customModal';

import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

//api
import {DocumentType} from '../../../../../common/enums/document_type';
import {
  fetchUploadUrlForKycDocument,
  uploadKycDocument,
} from '../../../../../common/api/document';
import {KycDocument} from '../../../../../common/contracts/kyc_document';

//utilities
import {
  width,
  Colors,
  height,
  images,
  strings,
} from '../../../../../utility/utilities';
import HeightWidth from '../../../../../utility/HeightWidth';
import TextInputUI from '../../../../../components/UI/TextInput';
import ButtonUI from '../../../../../components/UI/Button';
import {setAuthUser} from '../../../../auth/store/actions';
const data = [
  {label: 'PAN Card', value: DocumentType.PAN},
  {label: 'Aadhar Card', value: DocumentType.AADHAR},
  {label: 'BANK DETAILS', value: DocumentType.BANK},
];

interface Props {
  user: Tutor;
  saveUser: (data: Tutor) => any;
  navigation: any;
}

const EditOtherInformation: FunctionComponent<Props> = ({
  user,
  saveUser,
  navigation,
}) => {
  useEffect(() => {
    if (pan.length > 0) {
      setShowPan(true);
    }
  }, []);
  const [documentType, setDocumentType] = useState(DocumentType.AADHAR);
  const [document, setDocument] = useState('');
  //   const [dob, setDob] = useState('09-10-2020');
  const [formData2, setFormData] = useState(new FormData());
  const [droppedFiles, setDroppedFiles] = useState<File[]>([]);
  const [dropzoneKey, setDropzoneKey] = useState(0);
  const [documents, setDocuments] = useState<KycDocument[]>([]);
  const [documentNumber, setDocumentNumber] = useState('');
  const [aadhaar, setAadhaar] = useState(user.aadhaar || '');
  const [pan, setPan] = useState(user.pan || '');
  const [bankAccount, setBankAccount] = useState(user.bankAccount || '');
  const [showSelectImageSourceModel, setShowSelectImageSourceModel] = useState(
    false,
  );
  const [bankIfsc, setBankIfsc] = useState(user.bankIfsc || '');
  const [currentBankDetails, setCurrentBankDetails] = useState({
    accountNo: '',
    ifsc: '',
  });
  const [touchedDoc, setTouchedDoc] = useState(false);
  const [touchedIfsc, setTouchedIfsc] = useState(false);
  const [touchedBank, setTouchedBank] = useState(false);
  const docTypeHandler = (e) => {
    setDocumentType(e);
    console.log('Doc type ====>', e);
  };
  const [showAadhar, setShowAadhar] = useState(false);
  const [showPan, setShowPan] = useState(false);
  const [showBank, setShowBank] = useState(false);
  const dispatch = useDispatch();
  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'App Camera Permission',
          message: 'App needs access to your camera ',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Camera permission given');
      } else {
        console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };
  const openCamera = () => {
    requestCameraPermission();
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: true,
        maxHeight: 2000,
        maxWidth: 2000,
      },
      (response) => {
        if (response.fileName != null || response.fileName != undefined) {
          // console.log("Image Capture Response---", response)
          setShowSelectImageSourceModel(false);
          // console.log(response.uri);
          // console.log(response.fileName);
          // console.log(response.base64);
          let localUri = response.uri;
          let filename = localUri.split('/').pop();
          let match = /\.(\w+)$/.exec(filename);
          let type = match ? `image/${match[1]}` : `image`;
          let formData = new FormData();
          formData.append('photo', {uri: localUri, name: filename, type});
          setFormData(formData);
          setDroppedFiles(localUri);
          // console.log(formData._parts[0][1]);
        } else {
          setShowSelectImageSourceModel(false);
          ToastAndroid.show('Cancelled Adding Image', ToastAndroid.LONG);
        }
      },
    );
  };
  const openImageLibrary = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: true,
        maxWidth: 2000,
        maxHeight: 2000,
      },
      (response) => {
        if (response.fileName != null || response.fileName != undefined) {
          setShowSelectImageSourceModel(false);
          let localUri = response.uri;
          let filename = localUri.split('/').pop();
          let match = /\.(\w+)$/.exec(filename);
          let type = match ? `image/${match[1]}` : `image`;
          let formData = new FormData();
          formData.append('photo', {uri: localUri, name: filename, type});
          setFormData(formData);
          setDroppedFiles(localUri);
          // console.log(formData._parts[0][1]);
        } else {
          setShowSelectImageSourceModel(false);
          ToastAndroid.show('Cancelled Adding Image', ToastAndroid.LONG);
        }
        // console.log("Image Response---", response)
      },
    );
  };

  const addDocument = async () => {
    console.log(droppedFiles);
    if (droppedFiles.length < 1) return;
    if (aadhaar !== '') {
      // if (
      //
      //AADHAAR_PATTERN.test(
      //     aadhaar
      //       .replace(/[^\dA-Z]/g, '')
      //       .replace(/(.{4})/g, '$1 ')
      //       .trim(),
      //   )
      // ) {
      //   alert(
      //     'documentNumber' +
      //       'Invalid aadhaar number' +
      //       'Invalid aadhaar number',
      //   );
      //   return;
      // } else {
      // }
    }
    if (pan !== '') {
      // if (!PAN_PATTERN.test(pan)) {
      //   alert('documentNumber' + 'Invalid pan number' + 'Invalid pan number');
      //   return;
      // } else {
      // }
    }
    if (documentType === DocumentType.BANK) {
      if (!ACCOUNT_NO_PATTERN.test(bankAccount)) {
        alert(
          'bankAccount' + 'Invalid account number' + 'Invalid account number',
        );
        return;
      } else {
      }
      if (!IFSC_PATTERN.test(bankIfsc)) {
        alert('bankIfsc' + 'Invalid IFSC code' + 'Invalid IFSC code');
        return;
      } else {
      }
    }
    /******** */
    const file = formData2._parts[0][1];
    const clonedDocuments = [...documents];

    const documentIndex = clonedDocuments.findIndex(
      (document) =>
        document.kycDocType.toLowerCase() === documentType.toLowerCase(),
    );

    if (documentIndex > -1) {
      clonedDocuments.splice(documentIndex, 1);
    }
    // console.log(file.type)
    // console.log(documentType)
    // console.log(file.name)

    setDocuments([
      ...clonedDocuments,
      {
        kycDocFormat: file.type,
        kycDocType: documentType,
        kycDocLocation: file.name,
      },
    ]);
    setDroppedFiles([]);
    setDocumentNumber('');
    if (bankAccount && bankIfsc) {
      setCurrentBankDetails({accountNo: bankAccount, ifsc: bankIfsc});
    }
    setBankAccount('');
    setBankIfsc('');
    setDropzoneKey(dropzoneKey + 1);

    const formData = formData2;

    // formData.append('document', file);

    try {
      const awsBucket = await fetchUploadUrlForKycDocument({
        fileName: file.name,
        contentType: file.type,
        contentLength: file.size,
      });
      await uploadKycDocument(awsBucket.url, formData);
      handelSubmit();
    } catch (error) {
      console.log('cathing here ', error);
      if (error.response?.status === 401) {
        //
      }
    }
  };

  const saveOtherInformation = async (data: Tutor) => {
    const User = Object.assign({}, user, data);

    try {
      dispatch(setAuthUser(User));
      await updateTutor(User);
      alert('Updated Successfully');
    } catch (error) {
      alert('Unable to Update');
      console.log('getting error in updating tutor', error);
      if (error.response?.status === 401) {
        // setRedirectTo('/login');
      } else {
        dispatch(setAuthUser(User));
      }
    }
  };

  const handelSubmit = () => {
    console.log(documents.length);
    if (documents.length < 1) return;
    saveOtherInformation({
      ...user,
      kycDetails: documents,
      //   dob: dob.toString(),
      aadhaar: aadhaar ? aadhaar : undefined,
      pan: pan ? pan : undefined,
      bankAccount: currentBankDetails.accountNo
        ? currentBankDetails.accountNo
        : undefined,
      bankIfsc: currentBankDetails.ifsc ? currentBankDetails.ifsc : undefined,
    });
  };

  return (
    <View style={styles.__container}>
      <ScrollView>
        <View>
          <Text style={styles.__title}>Bank account details</Text>
          <View style={styles.__box}>
            <TextInput
              width="lg"
              otherStyle={{
                borderBottomColor: Colors.primaryBlue,
                borderBottomWidth: 1,
              }}
              name="bankAccount"
              placeholder="Your bank account number"
              // onChangeText={handleChange('bankAccount')}
              onChangeText={(item) => {
                setTouchedBank(true);
                setBankAccount(item);
              }}
              value={bankAccount}
            />
            <TextInput
              width="lg"
              otherStyle={{
                borderBottomColor: Colors.primaryBlue,
                borderBottomWidth: 1,
              }}
              name="bankIfsc"
              placeholder="Your bank IFSC code"
              onChangeText={(item) => {
                setTouchedIfsc(true);
                setBankIfsc(item);
              }}
              value={bankIfsc}
            />
            <View style={styles.__row}>
              <View style={styles.__rowText}>
                <Text>Upload cancelled cheque leaf</Text>
              </View>
              <View style={styles.__column}>
                <Text style={styles.__columnText}>
                  Drag and drop a file here or click
                </Text>
                <TouchableOpacity
                  style={styles.__button}
                  onPress={() => {
                    setDocumentType(DocumentType.BANK);
                    setShowSelectImageSourceModel(true);
                  }}>
                  <Icon
                    name="upload"
                    type="antdesign"
                    color={Colors.veryLightGray}
                    size={20}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.__button2}>
            <ButtonUI
              title="Add+"
              size="sm"
              width={HeightWidth.getResWidth(80)}
              height={HeightWidth.getResHeight(45)}
              backgroundColor={Colors.primaryBlue}
              onPress={() => {
                addDocument();
              }}
              borderRadius={5}
              textStyles={{
                fontSize: 14,
                fontStyle: 'normal',
                fontWeight: '400',
                color: Colors.veryLightGray,
              }}
            />
          </View>

          <Text style={styles.__title}>Addhar Details</Text>
          <View style={styles.__box}>
            <TextInput
              width="lg"
              otherStyle={{
                borderBottomColor: Colors.primaryBlue,
                borderBottomWidth: 1,
              }}
              name="aadhar"
              placeholder={'Your aadhar Number'}
              onChangeText={(item) => {
                setTouchedDoc(true);
                // handleBlur('documentNumber');
                setAadhaar(item);
              }}
              onBlur={() => {}}
              value={aadhaar}
            />
            <View style={styles.__row}>
              <View style={styles.__rowText}>
                <Text>Upload Aadhar Card</Text>
              </View>
              <View style={styles.__column}>
                <Text style={styles.__columnText}>
                  Drag and drop a file here or click
                </Text>
                <TouchableOpacity
                  style={styles.__button}
                  onPress={() => {
                    setDocumentType(DocumentType.AADHAR);
                    setShowSelectImageSourceModel(true);
                  }}>
                  <Icon
                    name="upload"
                    type="antdesign"
                    color={Colors.veryLightGray}
                    size={20}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.__button2}>
            <ButtonUI
              title="Add+"
              size="sm"
              width={HeightWidth.getResWidth(80)}
              height={HeightWidth.getResHeight(45)}
              backgroundColor={Colors.primaryBlue}
              onPress={() => {
                addDocument();
              }}
              borderRadius={5}
              textStyles={{
                fontSize: 14,
                fontStyle: 'normal',
                fontWeight: '400',
                color: Colors.veryLightGray,
              }}
            />
          </View>

          <Text style={styles.__title}>Pan Details</Text>

          <View style={styles.__box}>
            <TextInput
              width="lg"
              otherStyle={{
                borderBottomColor: Colors.primaryBlue,
                borderBottomWidth: 1,
              }}
              name="PAN"
              placeholder={'Your PAN Number'}
              onChangeText={(item) => {
                setTouchedDoc(true);
                // handleBlur('documentNumber');
                setPan(item);
              }}
              onBlur={() => {}}
              value={pan}
            />
            <View style={styles.__row}>
              <View style={styles.__rowText}>
                <Text>Upload Pan Card</Text>
              </View>
              <View style={styles.__column}>
                <Text style={styles.__columnText}>
                  Drag and drop a file here or click
                </Text>
                <TouchableOpacity
                  style={styles.__button}
                  onPress={() => {
                    setDocumentType(DocumentType.PAN);
                    setShowSelectImageSourceModel(true);
                  }}>
                  <Icon
                    name="upload"
                    type="antdesign"
                    color={Colors.veryLightGray}
                    size={20}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.__button2}>
            <ButtonUI
              title="Add+"
              size="sm"
              width={HeightWidth.getResWidth(80)}
              height={HeightWidth.getResHeight(45)}
              backgroundColor={Colors.primaryBlue}
              onPress={() => {
                addDocument();
              }}
              borderRadius={5}
              textStyles={{
                fontSize: 14,
                fontStyle: 'normal',
                fontWeight: '400',
                color: Colors.veryLightGray,
              }}
            />
          </View>
        </View>
        <CustomModel
          isModalVisible={showSelectImageSourceModel}
          onBackButtonPress={() => {
            setShowSelectImageSourceModel(false);
          }}>
          <View style={{}}>
            <Text style={{}}>Select Image From</Text>
            <View style={{}}>
              <ButtonUI
                title={'Camera'}
                borderRadius={50}
                backgroundColor={Colors.lightBlue}
                leftIconName={'camera'}
                leftIconSize={20}
                leftIconColor={Colors.white}
                elevation={10}
                textStyles={{
                  fontSize: 16,
                  color: Colors.white,
                  fontFamily: 'Montserrat-Medium',
                  textAlign: 'center',
                }}
                width={width / 2 - 50}
                height={50}
                onPress={() => {
                  openCamera();
                }}
              />
              <ButtonUI
                title={'Image Library'}
                borderRadius={50}
                backgroundColor={Colors.lightBlue}
                leftIconName={'image'}
                leftIconSize={20}
                leftIconColor={Colors.white}
                elevation={10}
                textStyles={{
                  fontSize: 16,
                  color: Colors.white,
                  fontFamily: 'Montserrat-Medium',
                  textAlign: 'center',
                }}
                width={width / 2 - 30}
                height={50}
                onPress={() => {
                  openImageLibrary();
                }}
              />
            </View>
          </View>
        </CustomModel>
        {/* <View style={[styles.bottomBtn]}>
          <ButtonUI
            backgroundColor={'#4C8BF5'}
            size="lg"
            borderRadius={5}
            title="Save"
            textStyles={{
              fontSize: 18,
              fontStyle: 'normal',
              fontWeight: '400',
              color: '#fff',
            }}
            onPress={() => {
              handelSubmit();
            }}
            // onPress={console.log('pressed buttton')}
          />
        </View> */}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  __container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginHorizontal: HeightWidth.getResWidth(2),
    marginBottom: HeightWidth.getResHeight(12),
    paddingBottom: HeightWidth.getResHeight(12),
  },
  __title: {
    color: Colors.primaryBlue,
    fontWeight: '400',
    fontSize: HeightWidth.getResFontSize(16),
    marginVertical: 12,
  },
  __box: {
    borderWidth: 1,
    borderColor: Colors.pureCyanBlue,
    // marginVertical: HeightWidth.getResWidth(5),
    // marginHorizontal: HeightWidth.getResHeight(5),
    padding: 8,
    borderRadius: 16,
  },
  __button2: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    padding: 10,
  },
  __button: {
    justifyContent: 'center',
    padding: 10,
    backgroundColor: Colors.primaryBlue,
    width: HeightWidth.getResWidth(150),
    borderRadius: 10,
  },
  __row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: HeightWidth.getResHeight(10),
  },
  __rowText: {
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(16),
  },
  __column: {
    flexDirection: 'column',
  },
  __columnText: {
    color: Colors.primaryGrey,
    fontSize: HeightWidth.getResFontSize(12),
    textAlign: 'center',
    // marginVertical: 12,
  },
});

const mapStateToProps = (state) => ({
  user: state.auth.authUser as User,
});

export default connect(mapStateToProps)(EditOtherInformation);
