import React, {useEffect, useState, FunctionComponent} from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Button from '../../../../../components/UI/Button';
import TextInput from '../../../../../components/UI/TextInput';
import Picker from '../../../../../components/UI/Picker';
import {Tutor, User} from '../../../../../common/contracts/user';
import {updateTutor} from '../../../../../common/api/profile';
import {connect, useDispatch} from 'react-redux';

import {
  EMAIL_PATTERN,
  PIN_PATTERN,
} from '../../../../../common/validations/patterns';
import CustomStatusBar from '../../../../../components/Header/custom_status_bar';
import * as Animatable from 'react-native-animatable';

import {
  fetchQualificationsList,
  fetchCitySchoolsList,
  fetchCitiesByPinCode,
} from '../../../../../common/api/academics';

//utilities
import {Colors} from '../../../../../utility/utilities';
import HeightWidth from '../../../../../utility/HeightWidth';
import {setAuthUser} from '../../../../auth/store/actions';

interface Props {
  user: Tutor;
  //   saveUser: (data: Tutor) => any;
  navigation: any;
}

const EditPersonalDetails: FunctionComponent<Props> = ({
  user,
  //   saveUser,
  navigation,
}) => {
  const [name, setName] = useState(user.tutorName || '');
  const [email, setEmail] = useState(user.emailId || '');
  const [pinCode, setPinCode] = useState(user.pinCode || '');
  const [qualification, setQualification] = useState(0);
  const [school, setSchool] = useState(0);
  const [cityName, setCityName] = useState(user.cityName || 'City Name');
  const [stateName, setStateName] = useState(user.stateName || 'State Name');
  const [qualificationsList, setQualificationsList] = useState([
    {label: '', value: ''},
  ]);
  const [schoolsList, setSchoolsList] = useState([]);
  const [redirectTo, setRedirectTo] = useState('');

  // validation states
  const [nameError, setNameError] = useState('');
  const [pinError, setPinError] = useState('');
  const [emailError, setEmailError] = useState('');
  const [schoolError, setSchoolError] = useState('');
  const [qualificationError, setQualificationError] = useState('');
  const [submitError, setSubmitError] = useState('');
  const dispatch = useDispatch();

  useEffect(() => {
    (async () => {
      try {
        const qualificationsListResponse = await fetchQualificationsList();
        const structuredQualificationsList = qualificationsListResponse.map(
          (qualification) => ({
            label: `${qualification.degree} (${qualification.subjectName})`,
            value: qualification.degree,
          }),
        );
        setQualificationsList([
          ...structuredQualificationsList,
          {label: 'Other', value: 'Other'},
        ]);
      } catch (error) {
        if (error.response?.status === 401) {
          // alert("");
          //   navigation.navigate('SplashScreen');
        }
      }
    })();
  }, [pinCode]);

  const savePersonalInformation = async (data: Tutor) => {
    const User = Object.assign({}, user, data);

    try {
      dispatch(setAuthUser(User));
      await updateTutor(User);
    } catch (error) {
      if (error.response?.status === 401) {
        alert("setRedirectTo('/login');");
      } else {
        dispatch(setAuthUser(User));
      }
    }
  };

  // validation
  const validateName = (value) => {
    if (value.length > 4) {
      setNameError('');
    } else if (value.lenth == 0) {
      setNameError('Name field cannot be empty');
    } else {
      setNameError('Name must contains 5 characters');
    }
  };

  const validateEmail = (value) => {
    if (EMAIL_PATTERN.test(value)) {
      setEmailError('');
    } else if (value.lenth == 0) {
      setEmailError('Email field cannot be empty');
    } else {
      setEmailError('Email is invalid');
    }
  };
  const validateSchool = (value) => {
    if (value != 0) {
      setSchoolError('');
    }
  };
  const validateQualification = (value) => {
    if (value != 0) {
      setQualificationError('');
    }
  };

  const validatePin = (value) => {
    if (PIN_PATTERN.test(value)) {
      setPinError('');
    } else if (value.lenth == 0) {
      setPinError('PIN field cannot be empty');
    } else {
      setPinError('Invalid PIN');
    }
  };

  const submitPersonalInformation = async (values) => {
    console.log([qualification.toString()]);
    try {
      await savePersonalInformation({
        ...user,
        tutorName: name,
        emailId: email,
        qualifications: qualification ? [qualification.toString()] : [],
        schoolName: school ? school.toString() : '',
        pinCode: pinCode,
        cityName: cityName,
        stateName: stateName,
      });
    } catch (error) {
      alert(error);
    }
  };

  const onPinCodeChange = async (pin: string) => {
    setPinCode(pin);
    if (PIN_PATTERN.test(pin)) {
      try {
        const getCityArr = await fetchCitiesByPinCode({pinCode: pin});
        setCityName(getCityArr[0].cityName);
        setStateName(getCityArr[0].stateName);

        const schoolsListResponse = await fetchCitySchoolsList({
          cityName: getCityArr[0].cityName,
        });

        const structuredSchoolsList = schoolsListResponse.map((school) => ({
          label: `${school.schoolName} (${school.schoolAddress})`,
          value: school.schoolName,
        }));

        setSchool(0);
        setSchoolsList([
          ...structuredSchoolsList,
          {label: 'Other', value: 'Other'},
        ]);
      } catch (error) {
        if (error.response?.status === 401) {
          setRedirectTo('/login');
        } else {
          alert('Service not available in this area');
          setPinCode('');
        }
      }
    } else {
      setCityName('');
      setStateName('');
    }
  };
  const handelSubmit = (values) => {
    if (
      pinCode.length == 0 ||
      email.length == 0 ||
      name.length == 0 ||
      qualification == 0 ||
      school == 0
    ) {
      setSubmitError('All fields are required field');
      setNameError('Name field cannot be empty');
      setEmailError('Email field cannot be empty');
      setPinError('PIN cannot be empty');
      setSchoolError('must select a school');
      setQualificationError('must select your qualification');
    } else if (
      emailError.length > 0 ||
      nameError.length > 0 ||
      pinError.length > 0
    ) {
      setSubmitError('All fields are required field');
    } else {
      submitPersonalInformation(values);
    }
  };

  // const handelSubmit = (values)=>{

  // }
  return (
    <SafeAreaView style={styles.__container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />
      <View>
        <View style={styles.__form}>
          <View>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Text style={styles.__text}>Tutor Name</Text>
              <TextInput
                width="lg"
                labelColor={Colors.veryLightGray}
                placeholder="Enter Your Name"
                autoFocus={true}
                value={name}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setName(value);
                  validateName(value);
                }}
              />
              {nameError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={styles.__error}>
                  <Text style={styles.__errorText}>{nameError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <View style={styles.__gap}></View>
              <Text style={styles.__text}>Tutor Email</Text>
              <TextInput
                width="lg"
                labelSize={20}
                labelColor={Colors.veryLightGray}
                placeholder="Enter Your Email Address"
                value={email}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setEmail(value);
                  validateEmail(value);
                }}
              />
              {emailError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={styles.__error}>
                  <Text style={styles.__errorText}>{emailError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <View style={styles.__gap}></View>

              <Text style={styles.__text}>PIN Code</Text>
              <TextInput
                width="lg"
                // label="Email Address"
                labelColor={Colors.veryLightGray}
                placeholder="PIN CODE"
                value={pinCode}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                onChangeText={(value) => {
                  setPinCode(value);
                  validatePin(value);
                  if (value.length > 5) {
                    onPinCodeChange(value);
                  }
                  // validatePassword(value)
                }}
              />
              {pinError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={styles.__error}>
                  <Text style={styles.__errorText}>{pinError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}
              <View style={styles.__gap}></View>
              <Picker
                label="Qualification"
                labelSize={HeightWidth.getResFontSize(13)}
                labelColor={Colors.primaryBlue}
                data={qualificationsList}
                rightIconName=""
                rightIconSize=""
                rightIconColor=""
                value={qualification}
                docTypeHandler={(item) => {
                  setQualification(item);
                  validateQualification(item);
                }}
                onRightIconPress=""
                showLabel={true}
              />

              {qualificationError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={styles.__error}>
                  <Text style={styles.__errorText}>{qualificationError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}
              <View style={styles.__gap}></View>

              <Picker
                label="School"
                labelSize={13}
                labelColor={Colors.primaryBlue}
                data={schoolsList}
                rightIconName=""
                rightIconSize=""
                rightIconColor=""
                value={school}
                docTypeHandler={(item) => {
                  setSchool(item);
                  validateSchool(item);
                }}
                onRightIconPress=""
                showLabel={true}
              />
              {schoolError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={styles.__error}>
                  <Text style={styles.__errorText}>{schoolError}</Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}
              <View style={styles.__gap}></View>
              <Text style={styles.__text}>City</Text>

              <TextInput
                width="lg"
                labelColor={Colors.veryLightGray}
                placeholder="City"
                value={cityName}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
              />
              <View style={styles.__gap}></View>
              <Text style={styles.__text}>State</Text>

              <TextInput
                width="lg"
                labelColor={Colors.veryLightGray}
                placeholder="State"
                value={stateName}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
              />
              <View style={styles.__gap}></View>
              <View style={styles.__gapBottom}>
                <Button
                  backgroundColor={'#4C8BF5'}
                  size="sm"
                  borderRadius={10}
                  title="Save changes"
                  textStyles={{
                    fontSize: 14,
                    fontStyle: 'normal',
                    fontWeight: '400',
                    color: '#fff',
                  }}
                  onPress={() => {
                    const values = {
                      name: name,
                      email: email,
                      pinCode: pinCode,
                      qualification: qualification,
                      school: school,
                      city: cityName,
                      state: stateName,
                    };
                    handelSubmit(values);
                  }}
                />
                <Button
                  backgroundColor={'#fff'}
                  size="sm"
                  borderRadius={10}
                  width={HeightWidth.getResWidth(100)}
                  title="Clear ?"
                  textStyles={{
                    fontSize: 14,
                    fontStyle: 'normal',
                    fontWeight: '400',
                    // color: '#fff',
                    color: '#4C8BF5',
                  }}
                  onPress={() => {
                    setName(''),
                      setEmail(''),
                      setPinCode(''),
                      setQualification(0);
                    setCityName(''), setStateName('');
                  }}
                />
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  __container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginLeft: HeightWidth.getResWidth(2),
    marginRight: HeightWidth.getResWidth(2),
    height: '100%',
  },
  __form: {
    borderRadius: 18,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    textAlign: 'center',
  },
  __errorText: {color: 'red', fontSize: HeightWidth.getResFontSize(13)},
  __gap: {
    paddingVertical: HeightWidth.getResHeight(1),
    marginVertical: HeightWidth.getResHeight(1),
  },
  __text: {
    fontSize: HeightWidth.getResFontSize(13),
    color: Colors.primaryBlue,
    paddingHorizontal: HeightWidth.getResWidth(8),
  },
  __gapBottom: {
    paddingBottom: HeightWidth.getResHeight(15),
    marginBottom: HeightWidth.getResHeight(15),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  __error: {alignSelf: 'flex-start'},
});

const mapStateToProps = (state) => ({
  user: state.auth.authUser as User,
});

export default connect(mapStateToProps)(EditPersonalDetails);
