import React, {FunctionComponent, useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  Image,
  View,
  Linking,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
  ScrollView,
  Alert,
} from 'react-native';
import RNFS from 'react-native-fs';
import {
  fetchUploadUrlForKycDocument,
  uploadAadhaarZip,
  uploadKycDocument,
} from '../../../../common/api/document';
import {
  Link,
  Route,
  Redirect,
  NativeRouter,
  Switch,
  withRouter,
  RouteComponentProps,
} from 'react-router-native';
import {Icon} from 'react-native-elements';
import {
  width,
  Colors,
  height,
  images,
  strings,
} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';
import TextInputUI from '../../../../components/UI/TextInput';
import Button from '../../../../components/UI/Button';
import CustomStatusBar from '../../../../components/Header/custom_status_bar';
import * as Animatable from 'react-native-animatable';
import {connect, useDispatch} from 'react-redux';
import {setAuthUser} from '../../.././auth/store/actions';
// import { RootState } from "../../../store";
import {Tutor, Organization, User} from '../../../../common/contracts/user';
import {isTutor, isOrganization} from '../../../../common/helpers';
import {updateTutor} from '../../../../common/api/profile';
import Dropzone from '../../../../common/components/dropzone/dropzone';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Picker} from '@react-native-picker/picker';
import * as yup from 'yup';
import DocumentPicker from 'react-native-document-picker';
// import * as FileSystem from "expo-file-system";
// import OrganizationLayout from "..c/components/organization_layout";
import {DocumentType} from '../../../../common/enums/document_type';
import {KycDocument} from '../../../../common/contracts/kyc_document';
import {color} from 'react-native-reanimated';
// import {
//   fetchUploadUrlForKycDocument,
//   uploadAadhaarZip,
//   uploadKycDocument,
// } from "../../common/api/document";
interface Props extends RouteComponentProps<{username: string}> {
  authUser: Tutor | Organization;
}
const ValidationSchema = yup.object().shape({
  aadhaarKycShareCode: yup.string().required('Share code is a required field'),

  lastDigit: yup.string().required('Last digits is a required field'),
});

const ProfileKyc: FunctionComponent<Props> = ({authUser}) => {
  const [formData1, setFormData] = useState(new FormData());
  const [droppedFiles, setDroppedFiles] = useState<File[]>([]);
  // const [aadhaarKycShareCode, setAadhaarKycShareCode] = useState('');
  const [aadhaarKycStatus, setAadhaarKycStatus] = useState(
    authUser.aadhaarKycStatus || '',
  );
  const [document, setDocument] = useState([]);
  const [dropzoneKey, setDropzoneKey] = useState(0);
  const [redirectTo, setRedirectTo] = useState('');
  const [url, setUrl] = useState('');
  const [mobile, setMobile] = useState('');
  const [lastDigit, setLastDigit] = useState('');
  const [sharedCode, setSharedCode] = useState('');
  const [base64, setBase64] = useState('');
  const dispatch = useDispatch();

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const createFormData = (user: Tutor | Organization, file: File) => {
    const formData = new FormData();
    const toExclude = [
      'kycDetails',
      'courseDetails',
      'package',
      'scheduleList',
      'batchList',
      'studentList',
      'tutorList',
    ];

    for (const [k, v] of Object.entries(user)) {
      if (!toExclude.includes(k)) {
        formData.append(k, v);
      }
    }
    formData.append('shareCode', sharedCode);
    formData.append('mobile', mobile);
    formData.append('lastDigit', lastDigit);
    file.type = 'application/zip';
    formData.append('files', file);
    return formData;
  };

  const processZip = async () => {
    console.log('we are coming here HERE BITCH');
    RNFS.readFile(url, 'base64').then(async (res) => {
      setBase64(res);
      console.log('started process');
      const file = formData1._parts[0][1];
      console.log(file);
      const user = Object.assign({}, authUser, {
        aadhaarKycZip: 'data:application/zip; base64,' + res,
        aadhaarKycShareCode: sharedCode,
      });
      const profileUpdated = (profile: Tutor | Organization) =>
        dispatch(setAuthUser(profile));
      const formData = createFormData(user, file);
      await uploadAadhaarZip(formData).then(async (value) => {
        user.aadhaarKycStatus = value.message;
        profileUpdated(user);
        setAadhaarKycStatus(value.message);
      });
      // .catch((err) => {
      //   if (err.response?.status === 401) setRedirectTo('/login');
      //   setAadhaarKycStatus('Rejected');
      // });
    });
  };

  const handleFormSubmit = async () => {
    if (droppedFiles.length < 1) {
      alert(
        'No zip file selected' +
          {
            variant: 'error',
          },
      );
      return;
    }

    // Process Zip in backend and then save the user
    processZip();
  };

  async function takeAndUploadPhotoAsync() {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      let localUri = res.uri;
      let filename = localUri.split('/').pop();
      let match = /\.(\w+)$/.exec(filename);
      let type = match ? `application/${match[1]}` : `zip`;
      // console.log(res);
      // console.log(
      //   'local uri is=========>' +
      //     localUri +
      //     '   ' +
      //     'type is ==========>' +
      //     type +
      //     '   ' + // mime type
      //     'name is ========>' +
      //     filename +
      //     'size is ========>' +
      //     res.size,
      // );
      console.log(filename);
      setUrl(localUri);
      let formData = new FormData();
      RNFS.readFile(url, 'base64').then((res) => {
        // console.log(res);
      });
      formData.append('aadhaarKycZip', {
        uri: localUri,
        name: filename,
        data: type,
      });

      setFormData(formData);
      console.log(formData._parts[0][1]);
      setDroppedFiles(localUri);
      //   let formData = new FormData();
      // // Assume "photo" is the name of the form field the server expects
      //   formData.append('photo', { uri: localUri, name: filename, type });
      // setDocument(res.name);
    } catch (err) {
      setDocument(null);
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
        Alert.alert('Upload Cancel');
      } else {
        throw err;
      }
    }
  }

  const alertKycStatus = () => {
    return (
      <View>
        <View style={styles.__box}>
          <View style={styles.__row}>
            <View>
              <Icon
                name={
                  aadhaarKycStatus === 'Verified' ? 'verified' : 'unverified'
                }
                size={60}
                type="Octicons"
                color={Colors.primaryGreen}
              />
              {/* verified */}
            </View>
            <View style={styles.__column}>
              <Text style={styles.__title}>{aadhaarKycStatus}</Text>
              <Text style={styles.__title2}>
                Your KYC status is {aadhaarKycStatus}
              </Text>
            </View>
          </View>

          {aadhaarKycStatus === 'Verified' ? (
            <View></View>
          ) : (
            <View>
              <Text>Enter correct details !!</Text>
            </View>
          )}
        </View>
        {aadhaarKycStatus === 'Rejected' && (
          <View>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                setAadhaarKycStatus('');
              }}>
              <Text> Redo KYC process </Text>
            </TouchableOpacity>
          </View>
        )}
        <View style={styles.__buttonBot}>
          <Button
            backgroundColor={Colors.primaryBlue}
            size="sm"
            borderRadius={10}
            title="Redo KYC Process"
            textStyles={{
              fontSize: 16,
              fontStyle: 'normal',
              fontWeight: '400',
              color: Colors.veryLightGray,
            }}
            onPress={() => {
              setAadhaarKycStatus('');
            }}
          />
        </View>
      </View>
    );
  };
  return (
    <SafeAreaView style={styles.__container}>
      {aadhaarKycStatus === '' ? (
        <View>
          <View
            style={{
              height: '30%',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Image source={images.kyc} style={styles.__smallLogo} />
            <Text style={styles.__title3}>Let's do KYC together!</Text>
          </View>
          <ScrollView style={styles.__box}>
            <View style={styles.__row}>
              <View style={styles.__rowText}>
                <Text>Upload cancelled cheque leaf</Text>
              </View>
              <View style={styles.__column}>
                <Text style={styles.__columnText}>
                  Drag and drop a file here or click
                </Text>
                <TouchableOpacity
                  style={styles.__button}
                  onPress={() => {
                    takeAndUploadPhotoAsync();
                  }}>
                  <Icon
                    name="upload"
                    type="antdesign"
                    color={Colors.veryLightGray}
                    size={20}
                  />
                </TouchableOpacity>
              </View>
            </View>

            <TextInputUI
              width="lg"
              style={styles.input}
              other
              name="aadhaarKycShareCode"
              placeholder="Enter Share Code"
              onChangeText={(value) => {
                setSharedCode(value);
                // validateSharedCode(value);
              }}
              value={sharedCode}
              otherStyle={{
                borderBottomColor: Colors.verylightGrey,
                borderBottomWidth: 1,
              }}
            />
            <TextInputUI
              width="lg"
              style={styles.input}
              other
              name="mobileNumber"
              placeholder="Enter mobile number"
              onChangeText={(value) => {
                setMobile(value);
                // validateSharedCode(value);
              }}
              value={mobile}
              otherStyle={{
                borderBottomColor: Colors.verylightGrey,
                borderBottomWidth: 1,
              }}
            />
            <TextInputUI
              width="lg"
              style={styles.input}
              other
              name="lastDigit"
              placeholder="Enter last digit of aadhar card"
              onChangeText={(value) => {
                setLastDigit(value);
                // validateSharedCode(value);
              }}
              value={lastDigit}
              otherStyle={{
                borderBottomColor: Colors.verylightGrey,
                borderBottomWidth: 1,
              }}
            />

            {/* <TouchableOpacity
              style={styles.button2}
              onPress={() => {
                takeAndUploadPhotoAsync();
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text style={styles.title2}> Upload Aadhaar Kyc Zip File </Text>
                <MaterialCommunityIcons
                  name="cloud-upload-outline"
                  color={'white'}
                  size={50}
                />
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.button}
              onPress={(event) => {
                event.persist();
                handleFormSubmit();
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <MaterialCommunityIcons
                  name="content-save"
                  color={'white'}
                  size={50}
                />
                <Text style={styles.title2}> Save Changes</Text>
              </View>
            </TouchableOpacity> */}
          </ScrollView>
          <View style={styles.__buttonBotRight}>
            <Button
              backgroundColor={Colors.primaryBlue}
              size="sm"
              borderRadius={10}
              title="Save Changes"
              textStyles={{
                fontSize: 16,
                fontStyle: 'normal',
                fontWeight: '400',
                color: Colors.veryLightGray,
              }}
              onPress={() => {
                event.persist();
                handleFormSubmit();
              }}
            />
          </View>
        </View>
      ) : (
        <View>
          <Image source={images.kyc} style={styles.__defaultLogo} />
          {aadhaarKycStatus !== '' && alertKycStatus()}
        </View>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  __container: {
    paddingHorizontal: HeightWidth.getResWidth(5),
    marginHorizontal: HeightWidth.getResWidth(5),
  },
  __defaultLogo: {
    resizeMode: 'contain',
    width: width,
    height: '60%',
  },
  __smallLogo: {
    resizeMode: 'contain',
    width: '50%',
    height: '100%',
  },
  __title: {
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
    marginVertical: 12,
    color: 'green',
  },
  __title2: {
    fontSize: 13,
    textAlign: 'left',
    marginVertical: 8,
    padding: 4,
  },
  __title3: {
    fontSize: 20,
    textAlign: 'left',
    marginVertical: 4,
    padding: 4,
    color: Colors.primaryMustard,
  },
  __box: {
    borderRadius: 10,
    marginTop: 12,
    borderColor: Colors.primaryBlue,
    borderWidth: 1,
    padding: 10,
  },
  __buttonBot: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  __buttonBotRight: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    padding: 10,
  },
  __button: {
    justifyContent: 'center',
    padding: 10,
    backgroundColor: Colors.primaryBlue,
    width: HeightWidth.getResWidth(150),
    borderRadius: 10,
  },
  __row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: HeightWidth.getResHeight(10),
  },
  __rowText: {
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(16),
  },
  __column: {
    flexDirection: 'column',
  },
  __columnText: {
    color: Colors.primaryGrey,
    fontSize: HeightWidth.getResFontSize(12),
    textAlign: 'center',
    // marginVertical: 12,
  },
  input: {
    padding: 10,
    margin: 10,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 12,
    color: 'black',
    fontSize: 18,
    overflow: 'hidden',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 30,
    textAlign: 'center',
    marginVertical: 12,
  },
  title2: {
    fontSize: 20,
    textAlign: 'left',
    marginVertical: 8,
    padding: 4,
  },
  title3: {
    fontSize: 16,
    textAlign: 'center',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  footer: {
    flex: 0.2,
    marginVertical: 8,
    alignItems: 'center',
    marginBottom: 50,
  },
  body: {
    padding: 20,
    margin: 20,
    marginTop: 0,
    paddingTop: 0,
    flex: 0.8,
    marginVertical: 8,
    borderBottomColor: '#737373',
    width: Dimensions.get('screen').width * 1,
  },
  form: {
    borderRadius: 18,
    backgroundColor: 'white',
    padding: 8,
    textAlign: 'center',
  },
  logo: {
    width: 50,
    height: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
  link1: {
    color: 'blue',
    fontSize: 12,
  },
  link2: {
    color: 'blue',
    fontSize: 18,
    padding: 20,
  },
  text: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
    margin: 5,
    alignItems: 'center',
  },
  btn: {
    backgroundColor: '#1e90ff',
    borderRadius: 8,
    padding: 20,
    margin: 20,
    textAlign: 'center',
  },
  button: {
    backgroundColor: '#1e90ff',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  button2: {
    backgroundColor: '#1e90ff',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  img: {
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  pin: {
    color: 'blue',
  },
  input_role: {
    paddingBottom: 1,
    marginBottom: 10,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 12,
    color: 'white',
    fontSize: 18,
  },
  lnktxt: {
    fontSize: 20,
  },
  input_role_body: {},
});

const mapStateToProps = (state) => ({
  authUser: state.auth.authUser as User,
});

export default connect(mapStateToProps)(ProfileKyc);
