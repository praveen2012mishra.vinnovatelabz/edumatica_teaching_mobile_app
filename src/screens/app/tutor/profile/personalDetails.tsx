import React, {Component, useState, useEffect, FunctionComponent} from 'react';
import {
  Link,
  Route,
  Redirect,
  NativeRouter,
  RouteComponentProps,
} from 'react-router-native';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {connect, useDispatch} from 'react-redux';
//utilities
import {Colors} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';
import TextInputUI from '../../../../components/UI/TextInput';

import {Tutor, User} from '../../../../common/contracts/user';
import {updateTutor} from '../../../../common/api/profile';
import {setAuthUser} from '../../../auth/store/actions';
import {Icon} from 'react-native-elements';

interface Props {
  profile: Tutor;
  profileUpdated: (user: Tutor) => any;
  navigation: any;
}
const PersonalDetails: FunctionComponent<Props> = ({
  profile,
  profileUpdated,
  navigation,
}) => {
  const [openModal, setOpenModal] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const savePersonalInformation = async (data: Tutor) => {
    const user = Object.assign({}, profile, data);

    try {
      profileUpdated(user);
      await updateTutor(user);
    } catch (error) {
      if (error.response?.status === 401) {
        setRedirectTo('/login');
      } else {
        profileUpdated(profile);
      }
    }
  };
  const dispatch = useDispatch();
  return (
    <View>
      <SafeAreaView style={styles.__container}>
        <View style={styles.__header}>
          <View style={{flex: 0.8}}>
            <View style={styles.__insideBox2}>
              <Icon
                name="person-outline"
                size={25}
                type="material"
                color={Colors.primaryMustard}
              />
              <Text style={styles.__title}>Personal Details</Text>
            </View>
          </View>
          <View style={styles.__header2}>
            <TouchableOpacity
              style={styles.__icon}
              onPress={() => {
                navigation.navigate('EditPersonalInfo');
              }}>
              <Icon
                name="pencil-circle"
                size={40}
                type="material-community"
                color={Colors.primaryBlue}
              />
            </TouchableOpacity>

            <Text style={styles.__title3}>
              View &amp; Edit Your Personal Details
            </Text>
          </View>
        </View>

        <ScrollView>
          <View style={styles.__box}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: 20,
              }}>
              <View>
                <View style={styles.__insideBox2}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>Your Name</Text>
                </View>
                <Text style={styles.__title3}>{profile.tutorName}</Text>
              </View>
              <View>
                <View style={styles.__insideBox2}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>Enrollment Id</Text>
                </View>
                <Text style={styles.__title3}>
                  {profile.enrollmentId ? profile.enrollmentId : '-'}
                </Text>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: 20,
              }}>
              <View>
                <View style={styles.__insideBox2}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>Email Address</Text>
                </View>
                <Text style={styles.__title3}>{profile.emailId}</Text>
              </View>
              <View>
                <View style={styles.__insideBox2}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>Phone Number</Text>
                </View>
                <Text style={styles.__title3}>{profile.mobileNo}</Text>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: 20,
              }}>
              <View>
                <View style={styles.__insideBox2}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>Date of Birth</Text>
                </View>
                <Text style={styles.__title3}>
                  {profile.dob && profile.dob.length > 0
                    ? new Date(profile.dob).toDateString()
                    : '-'}
                </Text>
              </View>
            </View>
          </View>

          <View style={styles.__box}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: 20,
              }}>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: 3,
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>Qualifications</Text>
                </View>
                <Text style={styles.__title3}>
                  {profile.qualifications && profile.qualifications.join(', ')}
                </Text>
              </View>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: 3,
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>School / University</Text>
                </View>
                <Text style={styles.__title3}>{profile.schoolName}</Text>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: 20,
              }}>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: 3,
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>Package</Text>
                </View>
                <Text style={styles.__title3}>
                  {profile.package && profile.package.name}
                </Text>
              </View>
            </View>
          </View>

          <View style={styles.__box}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: 20,
              }}>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: 3,
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>City</Text>
                </View>
                <Text style={styles.__title3}>{profile.cityName}</Text>
              </View>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: 3,
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>Pin Code</Text>
                </View>
                <Text style={styles.__title3}>{profile.pinCode}</Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: 20,
              }}>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: 3,
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="md-person-circle-sharp"
                    type="ionicon"
                    color={Colors.primaryBlue}
                  />
                  <Text style={styles.__title2}>State</Text>
                </View>
                <Text style={styles.__title3}>{profile.stateName}</Text>
              </View>
            </View>
          </View>
          <View style={{padding: '15%'}}></View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};
const styles = StyleSheet.create({
  __container: {
    paddingHorizontal: HeightWidth.getResWidth(5),
    marginHorizontal: HeightWidth.getResWidth(5),
  },
  __title: {
    color: Colors.primaryMustard,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(16),
    textAlign: 'center',
    marginVertical: 12,
  },
  __title2: {
    color: Colors.veryDarkGray,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(12),
    textAlign: 'center',
    // marginVertical: 12,
  },
  __title3: {
    color: Colors.primaryGrey,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(12),
    textAlign: 'center',
    // marginVertical: 12,
  },
  __header: {
    flexDirection: 'row',
    padding: 3,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  __header2: {
    flexDirection: 'column',
    padding: 3,
    alignItems: 'center',
  },
  __icon: {
    alignSelf: 'flex-end',
  },
  __box: {
    borderWidth: 1,
    borderColor: Colors.pureCyanBlue,
    borderRadius: 5,
    marginVertical: HeightWidth.getResWidth(5),
    marginHorizontal: HeightWidth.getResHeight(5),
  },
  __insideBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
  },
  __insideBox2: {
    flexDirection: 'row',
    padding: 3,
    alignItems: 'center',
  },
});

const mapStateToProps = (state) => ({
  profile: state.auth.authUser as User,
});

export default connect(mapStateToProps)(PersonalDetails);
