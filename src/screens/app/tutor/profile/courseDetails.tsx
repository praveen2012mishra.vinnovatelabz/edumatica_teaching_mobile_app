import {connect} from 'react-redux';
import React, {FunctionComponent, useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {Redirect} from 'react-router-native';
import {Colors} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {DataTable} from 'react-native-paper';
import ButtonUI from '../../../../components/UI/Button';
import {Tutor, User} from '../../../../common/contracts/user';
import {updateTutor} from '../../../../common/api/profile';
import {Icon} from 'react-native-elements';
import {getStudent} from '../../../../common/api/tutor';
interface BoardClassSubjectsMap {
  boardname: string;
  classname: string;
  subjects: string[];
}

interface Props {
  profile: Tutor;
  navigation: any;
}

const CourseDetails: FunctionComponent<Props> = ({profile, navigation}) => {
  const [openModal, setOpenModal] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const saveSubjectInformation = async (data: Tutor) => {
    const user = Object.assign({}, profile, data);

    try {
      await updateTutor(user);
      // profileUpdated(user);
    } catch (error) {
      if (error.response?.status === 401) {
        setRedirectTo('/login');
      } else {
        //profileUpdated(profile);
      }
    }
  };

  useEffect(() => {}, [profile]);

  const TutorSubjects = [...profile.courseDetails];

  const boardClassSubjectsMap: BoardClassSubjectsMap[] = [];

  for (let i = 0; i < TutorSubjects.length; ++i) {
    const subject = TutorSubjects[i];

    let boardClassSubjectIndex = boardClassSubjectsMap.findIndex(
      (boardClassSubject) =>
        boardClassSubject.boardname.toLowerCase() ===
          subject.board.toLowerCase() &&
        boardClassSubject.classname.toString().toLowerCase() ===
          subject.className.toString().toLowerCase(),
    );

    if (boardClassSubjectIndex === -1) {
      boardClassSubjectsMap.push({
        boardname: subject.board,
        classname: subject.className,
        subjects: [],
      });

      boardClassSubjectIndex = boardClassSubjectsMap.length - 1;
    }

    boardClassSubjectsMap[boardClassSubjectIndex].subjects.push(
      subject.subject,
    );
  }

  const boards = Array.from(
    new Set(TutorSubjects.map((subject) => subject.board)),
  );
  return (
    <SafeAreaView style={styles.__container}>
      <View style={styles.__header}>
        <View style={{flex: 0.8}}>
          <View style={styles.__insideBox2}>
            <Icon
              name="person-outline"
              size={25}
              type="material"
              color={Colors.primaryMustard}
            />
            <Text style={styles.__title}>Course Details</Text>
          </View>
        </View>
        <View style={styles.__header2}>
          <TouchableOpacity
            style={styles.__icon}
            onPress={() => {
              navigation.navigate('EditCourseInfo');
            }}>
            <Icon
              name="pencil-circle"
              size={40}
              type="material-community"
              color={Colors.primaryBlue}
            />
          </TouchableOpacity>

          <Text style={styles.__title3}>
            View &amp; Edit Your Class &amp; Subject
          </Text>
        </View>
      </View>
      <ScrollView>
        <View>
          <View style={styles.__header3}>
            <Text style={styles.__title4}>Course</Text>
            <ButtonUI
              title="Add+"
              width={HeightWidth.getResWidth(80)}
              height={HeightWidth.getResHeight(50)}
              backgroundColor={Colors.primaryBlue}
              onPress={() => {
                navigation.navigate('EditCourseInfo');
              }}
              borderRadius={5}
              textStyles={{
                color: 'white',
                fontSize: HeightWidth.getResFontSize(18),
                fontWeight: '400',
              }}
            />
          </View>

          {boards &&
            boards.length > 0 &&
            boards.map((board) => (
              <View>
                <Text style={styles.__titleBoard}>{board}</Text>
                <View>
                  {/* <DataTable>
                    <DataTable.Header style={styles.__row}>
                      <DataTable.Title>
                        {' '}
                        <Text style={styles.__rowText}>Classes</Text>{' '}
                      </DataTable.Title>
                      <DataTable.Title>
                        <Text style={styles.__rowText}>Subjects</Text>
                      </DataTable.Title>
                    </DataTable.Header>

                    {boardClassSubjectsMap
                      .filter(
                        (boardClassSubjectMap) =>
                          boardClassSubjectMap.boardname === board,
                      )
                      .map((boardClassSubjectMap) => (
                        <DataTable.Row>
                          <DataTable.Title>
                            {boardClassSubjectMap.classname}
                          </DataTable.Title>
                          <DataTable.Title>
                            {boardClassSubjectMap.subjects.join(', ')}
                          </DataTable.Title>
                        </DataTable.Row>
                      ))}
                  </DataTable> */}
                  <View style={styles.__tableBox}>
                    <View style={styles.__headerBox}>
                      <Text style={styles.__title2}>Classes</Text>
                    </View>
                    <View style={styles.__headerBox}>
                      <Text style={styles.__title2}>Subject</Text>
                    </View>
                  </View>
                  {boardClassSubjectsMap
                    .filter(
                      (boardClassSubjectMap) =>
                        boardClassSubjectMap.boardname === board,
                    )
                    .map((boardClassSubjectMap) => (
                      <View style={styles.__tableBox}>
                        <View style={styles.__bodyBox}>
                          <Text style={styles.__title3}>
                            {boardClassSubjectMap.classname}
                          </Text>
                        </View>
                        <View style={styles.__bodyBox}>
                          <Text style={styles.__title3}>
                            {boardClassSubjectMap.subjects.join(', ')}
                          </Text>
                        </View>
                      </View>
                    ))}
                </View>
              </View>
            ))}
        </View>
        <View style={{padding: '15%'}}></View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  __container: {
    paddingHorizontal: HeightWidth.getResWidth(5),
    marginHorizontal: HeightWidth.getResWidth(5),
  },
  __title: {
    color: Colors.primaryMustard,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(16),
    textAlign: 'center',
    marginVertical: 12,
  },
  __title2: {
    color: Colors.veryDarkGray,
    fontSize: HeightWidth.getResFontSize(12),
    textAlign: 'center',
    // marginVertical: 12,
  },
  __title3: {
    color: Colors.primaryGrey,
    fontSize: HeightWidth.getResFontSize(12),
    textAlign: 'center',
    // marginVertical: 12,
  },
  __title4: {
    color: Colors.veryDarkGray,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'center',
    marginVertical: 12,
  },
  __title5: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'center',
    marginVertical: HeightWidth.getResWidth(12),
  },
  __titleBoard: {
    color: Colors.primaryBlue,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(16),
    marginVertical: HeightWidth.getResWidth(12),
    borderLeftWidth: HeightWidth.getResWidth(2),
    borderColor: Colors.primaryBlue,
    marginHorizontal: HeightWidth.getResWidth(12),
    paddingHorizontal: HeightWidth.getResWidth(14),
  },
  __box: {
    borderWidth: 1,
    borderColor: Colors.pureCyanBlue,
    borderRadius: 5,
    marginVertical: HeightWidth.getResWidth(5),
    marginHorizontal: HeightWidth.getResHeight(5),
  },
  __header: {
    flexDirection: 'row',
    padding: 3,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  __header2: {
    flexDirection: 'column',
    padding: 3,
    alignItems: 'center',
  },
  __header3: {
    flexDirection: 'row',
    padding: 3,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.primaryMustard,
    borderRadius: 5,
    paddingHorizontal: 12,
    paddingVertical: 5,
  },
  __icon: {
    alignSelf: 'flex-end',
  },
  __insideBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
  },
  __tableBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  __headerBox: {
    flex: 0.5,
    justifyContent: 'center',
    borderWidth: 1,
    // borderRadius: 5,
    backgroundColor: Colors.lightBlue,
    height: 50,
  },
  __bodyBox: {
    flex: 0.5,
    justifyContent: 'center',
    borderWidth: 1,
    // borderRadius: 5,
    borderColor: Colors.primaryBlue,
    height: 50,
  },
  __insideBox2: {
    flexDirection: 'row',
    padding: 3,
    alignItems: 'center',
  },
  __row: {
    backgroundColor: Colors.primaryBlue,
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderWidth: 1,
    width: '100%',
    borderRadius: 5,
  },
  __rowText: {
    color: Colors.veryDarkGray,
    fontSize: HeightWidth.getResFontSize(12),
    textAlign: 'center',
    marginHorizontal: 15,
  },
});

const mapStateToProps = (state) => ({
  profile: state.auth.authUser as User,
});

export default connect(mapStateToProps)(CourseDetails);
