import React, {FunctionComponent, useState, useEffect} from 'react';

import {
  StyleSheet,
  Text,
  TextInput,
  Image,
  View,
  Linking,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
  ScrollView,
} from 'react-native';
import {
  Link,
  Route,
  Redirect,
  NativeRouter,
  Switch,
  withRouter,
  RouteComponentProps,
} from 'react-router-native';
//utilities
import {Colors} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';
import TextInputUI from '../../../../components/UI/TextInput';
import ButtonUI from '../../../../components/UI/Button';
import {Create as CreateIcon} from '@material-ui/icons';
import {Tutor, User} from '../../../../common/contracts/user';
import {updateTutor} from '../../../../common/api/profile';
// import IdCard from '../../../../assets/svgs/id-card.svg';
// import Layout from '../tutor_layout';
// import TutorOtherInformationModal from '../modals/tutor_other_information_modal';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {connect, useDispatch} from 'react-redux';
import {Icon} from 'react-native-elements';

interface OtherInformationProps {
  profile: Tutor;
  navigation: any;
}

const OtherInformation: FunctionComponent<OtherInformationProps> = ({
  profile,
  navigation,
}) => {
  useEffect(() => {}, [profile]);
  if (
    (profile.dob && profile.dob.length > 0) ||
    (profile.kycDetails && profile.kycDetails.length > 0)
  ) {
    return (
      <View style={styles.__box}>
        <View>
          <View style={styles.__row}>
            <Text style={styles.__title}>Documents :</Text>
            <Text style={styles.__title2}>
              {profile.kycDetails && profile.kycDetails.length}
            </Text>
          </View>

          <View>
            {profile.kycDetails &&
              profile.kycDetails.map((document, index) => (
                <View style={styles.__list}>
                  <Text style={styles.__title4}>{document.kycDocType}</Text>
                </View>
              ))}
          </View>
        </View>
      </View>
    );
  }

  return (
    <View>
      <Text>No data available on other details.</Text>
    </View>
  );
};

interface Props {
  profile: Tutor;
  profileUpdated: (user: Tutor) => any;
  navigation: any;
}

const TutorOtherInformation: FunctionComponent<Props> = ({
  profile,
  profileUpdated,
  navigation,
}) => {
  const [openModal, setOpenModal] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const saveOtherInformation = async (data: Tutor) => {
    const user = Object.assign({}, profile, data);

    try {
      profileUpdated(user);
      await updateTutor(user);
    } catch (error) {
      if (error.response?.status === 401) {
        console.log('setRedirectTo login');
      } else {
        profileUpdated(profile);
      }
    }
  };

  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.__header}>
          <View>
            <View style={styles.__insideBox2}>
              <Icon
                name="person-outline"
                size={25}
                type="material"
                color={Colors.primaryMustard}
              />
              <Text style={styles.__titleHead}>Other Information</Text>
            </View>
          </View>
          <View style={styles.__header2}>
            <TouchableOpacity
              style={styles.__icon}
              onPress={() => {
                navigation.navigate('EditOtherInformation');
              }}>
              <Icon
                name="pencil-circle"
                size={40}
                type="material-community"
                color={Colors.primaryBlue}
              />
            </TouchableOpacity>

            <Text style={styles.__title3}>
              View &amp; Edit Your Other Information
            </Text>
          </View>
        </View>

        <OtherInformation profile={profile} navigation />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  __titleHead: {
    color: Colors.primaryMustard,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(16),
    textAlign: 'center',
    marginVertical: 12,
  },
  __title: {
    color: Colors.veryDarkGray,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(20),
    textAlign: 'center',
    marginVertical: 12,
  },
  __title2: {
    color: Colors.veryLightGray,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(20),
    textAlign: 'center',
    backgroundColor: Colors.primaryBlue,
    width: HeightWidth.getResWidth(25),
    borderRadius: 50,
  },
  __title3: {
    color: Colors.primaryGrey,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(10),
    textAlign: 'center',
    // marginVertical: 12,
  },
  __title4: {
    color: Colors.veryLightGray,
    fontWeight: 'bold',
    fontSize: HeightWidth.getResFontSize(16),
    textAlign: 'center',
    // marginVertical: 12,
  },
  __header: {
    flexDirection: 'row',
    padding: 3,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  __header2: {
    flexDirection: 'column',
    padding: 3,
    alignItems: 'center',
  },
  __icon: {
    alignSelf: 'flex-end',
  },
  __box: {
    borderWidth: 1,
    borderColor: Colors.pureCyanBlue,
    borderRadius: 5,
    marginVertical: HeightWidth.getResWidth(5),
    marginHorizontal: HeightWidth.getResHeight(5),
  },
  __insideBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
  },
  __insideBox2: {
    flexDirection: 'row',
    padding: 3,
    alignItems: 'center',
  },
  __row: {
    flexDirection: 'row',
    padding: 3,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 5,
  },
  __list: {
    backgroundColor: Colors.primaryBlue,
    margin: 10,
    padding: 3,
    width: '50%',
    borderRadius: 5,
  },
});

const mapStateToProps = (state) => ({
  profile: state.auth.authUser as User,
});

export default connect(mapStateToProps)(TutorOtherInformation);
