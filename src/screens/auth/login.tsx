import React, {FunctionComponent, useState} from 'react';
import {useDispatch} from 'react-redux';
import {
  StyleSheet,
  Text,
  View,
  Linking,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import TextInputUI from '../../components/UI/TextInput';
import ButtonUI from '../../components/UI/Button';
import CustomStatusBar from '../../components/Header/custom_status_bar';
import * as Animatable from 'react-native-animatable';
import {BASE_ROUTE} from '@env';
import * as yup from 'yup';

import {loginUser} from '../../common/api/auth';
import {getStudent, getTutor, getParent} from '../../common/api/tutor';
import {getOrganization} from '../../common/api/organization';
import {getAdmin} from '../../common/api/admin';
import {setAuthToken, setAuthUser, setAuthUserRole} from './store/actions';
import {
  PHONE_PATTERN,
  PASSWORD_PATTERN,
} from '../../common/validations/patterns';
import {Role} from '../../common/enums/role';

//utilities
import {Colors} from '../../utility/utilities';
import HeightWidth from '../../utility/HeightWidth';
import AsyncStorage from '@react-native-community/async-storage';
interface Props {
  navigation: any;
}

const Login: FunctionComponent<Props> = ({navigation}) => {
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const [isLoginButtonDisabled, setLoginButtonDisabled] = useState(true);
  const [redirectTo, setRedirectTo] = useState('');
  const dispatch = useDispatch();
  const [username, setUsername] = useState('');
  const [mobileNumber, setMobileNumber] = useState('8396828281');
  const [password, setPassword] = useState('Upes@123');
  const [orgCode, setOrgCode] = useState('Tutor1');
  const [roles, setRoles] = useState<Role[]>([]);
  const [width, setWidth] = useState(Dimensions.get('screen').width * 1);

  const [orgCodeError, setOrgCodeError] = useState('');
  const [mobileNumberError, setMobileNumberError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [submitError, setSubmitError] = useState('');

  // validation
  const validateOrgCode = (value) => {
    if (value.length > 4) {
      setOrgCodeError('');
    } else if (value.lenth == 0) {
      setOrgCodeError('Org Code field cannot be empty');
    } else {
      setOrgCodeError('Org Code must contains 5 characters');
    }
  };

  const validateMobileNumber = (value) => {
    if (PHONE_PATTERN.test(value)) {
      setMobileNumberError('');
    } else if (value.lenth == 0) {
      setMobileNumberError('mobile Number field cannot be empty');
    } else {
      setMobileNumberError('Mobile Number is invalid');
    }
  };

  const validatePassword = (value) => {
    if (PASSWORD_PATTERN.test(value)) {
      setPasswordError('');
    } else if (value.lenth == 0) {
      setPasswordError('password field cannot be empty');
    } else {
      setPasswordError(
        'Password must contain uppercase, lowercase, alphanumeric, & special characters',
      );
    }
  };

  const handleSubmit = (values) => {
    if (
      orgCode.length == 0 ||
      mobileNumber.length == 0 ||
      password.length == 0
    ) {
      setSubmitError('All fields are required field');
      setMobileNumberError('mobile Number field cannot be empty');
      setOrgCodeError('Org Code field cannot be empty');
      setPasswordError('password field cannot be empty');
    } else if (orgCodeError.length > 0 || mobileNumberError.length > 0) {
      setSubmitError('All fields are required field');
    } else {
      handleLogin(values);
    }
  };

  const handleLogin = async ({orgCode, mobileNumber, password}) => {
    console.log(orgCode + '|' + mobileNumber, password);

    try {
      const response = await loginUser(orgCode + '|' + mobileNumber, password);
      console.log('this is res we are getting', response.data);
      const user = response.data;
      setUsername(user.username);
      // await dispatch();
      // setAuthToken(user.accessToken)
      dispatch(setAuthToken(user.accessToken));
      console.log(username);

      if (user.roles.length > 1) {
        setRoles(user.roles);
        return;
      }
      const role = user.roles[0];
      dispatch(setAuthUserRole(role));
      AsyncStorage.setItem('userRole', role);
      if (role === Role.ADMIN) {
        try {
          const admin = await getAdmin();
          dispatch(setAuthUser(admin));
          AsyncStorage.setItem('authUser', JSON.stringify(admin));
          alert('send to admin dashboard');
          //   setRedirectTo(`/profile/${username}/dashboard`);
        } catch (error) {}
        return;
      } else if (role === Role.ORGANIZATION) {
        try {
          const organization = await getOrganization();
          dispatch(setAuthUser(organization));
          navigation.reset({
            index: 0,
            routes: [{name: 'appStackOrg'}],
          });

          //   setRedirectTo(`/profile/${user.username}/dashboard`);
        } catch (error) {
          dispatch(setAuthUser({mobileNo: mobileNumber}));
          // Redirect the user to complete his profile, if he has recently
          // registered or not yet filled his profile up.
          navigation.reset({
            index: 0,
            routes: [{name: 'createProfileDetailsStackOrg'}],
          });
        }

        return;
      } else if (role === Role.TUTOR || role === Role.ORG_TUTOR) {
        try {
          //check if user already have data or not
          const tutor = await getTutor();
          console.log('tutor--->', tutor);
          dispatch(setAuthUser(tutor));
          navigation.reset({
            index: 0,
            routes: [{name: 'appStackTutor'}],
          });

          return;
        } catch (error) {
          //if no data is present send to process pages
          dispatch(setAuthUser({mobileNo: mobileNumber}));
          console.log('error--->', error);
          navigation.reset({
            index: 0,
            routes: [{name: 'createProfileDetailsStackTutor'}],
          });

          // navigation.navigate('ProfileDetailsScreen',{authUser:authUser.mobileNumber})
          //   setRedirectTo(`/profile/${user.username}/process/1`);
        }
        return;
      } else if (role === Role.STUDENT) {
        const student = await getStudent();
        dispatch(setAuthUser(student));
        navigation.reset({
          index: 0,
          routes: [{name: 'appStackStudent'}],
        });
        // setRedirectTo(`/profile/${user.username}/dashboard`);
      } else if (role === Role.PARENT) {
        const parent = await getParent();
        dispatch(setAuthUser(parent));
        navigation.reset({
          index: 0,
          routes: [{name: 'appStackParent'}],
        });
        // setRedirectTo(`/profile/${user.username}/dashboard`);
      }
    } catch (error) {
      alert('login failed' + '' + error);
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />

      <View style={styles.body}>
        <View>
          <View style={styles.form}>
            <Text style={styles.title}> Login </Text>
            <Text style={styles.title2}> Please login to your account </Text>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <TextInputUI
                width="lg"
                style={styles.input}
                other
                name="orgCode"
                placeholder="Institute / Tutor Code"
                onChangeText={(value) => {
                  setOrgCode(value);
                  validateOrgCode(value);
                }}
                value={orgCode}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
              />

              {orgCodeError.length > 1 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>
                    {orgCodeError}
                  </Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <View style={{paddingTop: HeightWidth.getResHeight(20)}}></View>

              <TextInputUI
                width="lg"
                style={styles.input}
                name="mobileNumber"
                placeholder="Mobile Number"
                onChangeText={(value) => {
                  setMobileNumber(value);
                  validateMobileNumber(value);
                }}
                value={mobileNumber}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                keyboardType={'numeric'}
                maxLength={10}
              />
              {mobileNumberError.length > 0 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>
                    {mobileNumberError}
                  </Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <View style={{paddingTop: HeightWidth.getResHeight(20)}}></View>

              <TextInputUI
                width="lg"
                style={styles.input}
                name="password"
                placeholder="Password"
                onChangeText={(value) => {
                  setPassword(value);
                  validatePassword(value);
                }}
                password={true}
                value={password}
                otherStyle={{
                  borderBottomColor: Colors.verylightGrey,
                  borderBottomWidth: 1,
                }}
                borderRadius={5}
              />
              {passwordError.length > 0 ? (
                <Animatable.View
                  animation="fadeInLeft"
                  duration={500}
                  style={{alignSelf: 'flex-start'}}>
                  <Text style={{color: 'red', fontSize: 15}}>
                    {passwordError}
                  </Text>
                </Animatable.View>
              ) : (
                <View></View>
              )}

              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('ForgotPassword');
                }}>
                <Text style={styles.link2}>Forget Password?</Text>
              </TouchableOpacity>

              <ButtonUI
                title="Log In"
                width={width * 0.95}
                backgroundColor="#4C8BF5"
                onPress={() => {
                  const values = {
                    mobileNumber: mobileNumber,
                    orgCode: orgCode,
                    password: password,
                  };
                  handleSubmit(values);
                }}
                borderRadius={5}
                textStyles={{
                  color: 'white',
                  fontSize: HeightWidth.getResFontSize(18),
                  fontWeight: '400',
                }}
              />

              <View style={{paddingTop: HeightWidth.getResHeight(40)}}></View>

              <View style={styles.fixToText}>
                <Text style={styles.text}>
                  {' '}
                  By tapping Login, you agree with our{' '}
                </Text>

                <Text
                  style={styles.link1}
                  onPress={() =>
                    Linking.openURL(
                      BASE_ROUTE.substr(0, BASE_ROUTE.length - 3) +
                        'terms-conditions',
                    )
                  }>
                  Terms & Conditions
                </Text>
                <Text style={styles.text}>, Learn</Text>
              </View>

              <View style={styles.fixToText}>
                <Text style={styles.text}>
                  how we process your data in our{' '}
                </Text>
                <Text
                  style={styles.link1}
                  onPress={() =>
                    Linking.openURL(
                      BASE_ROUTE.substr(0, BASE_ROUTE.length - 3) +
                        'privacy-policy',
                    )
                  }>
                  Privacy Policy
                </Text>
                <Text style={styles.text}>, and </Text>
                <Text
                  style={styles.link1}
                  onPress={() =>
                    Linking.openURL(
                      BASE_ROUTE.substr(0, BASE_ROUTE.length - 3) +
                        'privacy-policy',
                    )
                  }>
                  Cookies Policy
                </Text>
              </View>
              <View style={{paddingTop: HeightWidth.getResHeight(40)}}></View>
              <View style={styles.fixToText}>
                <View style={styles.linknav}>
                  <Text style={styles.text}>New User? </Text>

                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate('PreRegister');
                    }}>
                    <Text style={styles.link1}>Sign up</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginLeft: HeightWidth.getResWidth(2),
    marginRight: HeightWidth.getResWidth(2),
  },
  input: {
    padding: 10,
    margin: 10,
    fontSize: 12,
    overflow: 'hidden',
  },
  title: {
    fontWeight: '500',
    fontSize: HeightWidth.getResFontSize(24),
    textAlign: 'center',
    marginVertical: 8,
    paddingBottom: '5%',
  },
  title2: {
    fontWeight: '400',
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'left',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footer: {
    flex: 0.2,
    // marginVertical: 8,
    alignItems: 'center',
    // marginBottom: 50,
  },
  body: {},
  form: {
    borderRadius: 18,
    backgroundColor: 'white',
    //  padding:20,
    textAlign: 'center',
    //  paddingTop:"10%",
    //  marginTop:'10%'
  },
  logo: {
    width: 50,
    height: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
  link1: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(12),
    backgroundColor: 'white',
  },
  link2: {
    fontWeight: '400',
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(16),
    paddingTop: '5%',
    paddingBottom: '8%',
    marginLeft: '60%',
    borderRadius: 5,
    backgroundColor: 'white',
  },
  link3: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(18),
    paddingVertical: 10,
    backgroundColor: 'white',
    // paddingHorizontal: 20,
  },
  text: {
    fontSize: HeightWidth.getResFontSize(14),
    color: 'grey',
  },
  btn: {
    backgroundColor: '#f5c242',
    borderRadius: 8,
    padding: 20,
    margin: 20,
    textAlign: 'center',
  },
  button: {
    fontWeight: '400',
    backgroundColor: '#4C8BF5',
    borderRadius: 5,
    color: 'white',
    fontSize: HeightWidth.getResFontSize(18),
    overflow: 'hidden',
    padding: '5%',
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  img: {
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  linknav: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default Login;
