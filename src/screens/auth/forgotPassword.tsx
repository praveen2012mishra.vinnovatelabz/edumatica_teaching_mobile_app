import {Navigation} from '@material-ui/icons';
import React, {FunctionComponent, useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  Image,
  View,
  Linking,
  Button,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import {Link, Route, Redirect, NativeRouter, Switch} from 'react-router-native';
import * as yup from 'yup';
import TextInputUI from '../../components/UI/TextInput';
import ButtonUI from '../../components/UI/Button';
import CustomStatusBar from '../../components/Header/custom_status_bar';
import * as Animatable from 'react-native-animatable';

import {generateOTP, verifyOTP} from '../../common/api/auth';
import {OTP_PATTERN, PHONE_PATTERN} from '../../common/validations/patterns';
import {width, Colors, height, images, strings} from '../../utility/utilities';
import HeightWidth from '../../utility/HeightWidth';

interface Props {
  navigation: any;
}

interface FormData {
  orgCode: string;
  mobileNumber: string;
  otp: string;
}

const ValidationSchema = yup.object().shape({
  orgCode: yup.string().required('Code is a required field'),
  mobileNumber: yup
    .string()
    .required('mobile number is a required field')
    .matches(PHONE_PATTERN, 'mobile number is invalid'),
  otp: yup.string().required().matches(OTP_PATTERN, 'otp is invalid'),
});

const ForgotPassword: FunctionComponent<Props> = ({navigation}) => {
  const [width, setWidth] = useState(Dimensions.get('screen').width * 1);
  const [orgCode, setOrgCode] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [otp, setOtp] = useState('');

  const [orgCodeError, setOrgCodeError] = useState('');
  const [mobileNumberError, setMobileNumberError] = useState('');
  const [otpError, setOtpError] = useState('');
  const [submitError, setSubmitError] = useState('');

  //validation
  const validateOrgCode = (value) => {
    if (value.length > 4) {
      setOrgCodeError('');
    } else if (value.lenth == 0) {
      setOrgCodeError('Org Code field cannot be empty');
    } else {
      setOrgCodeError('Org Code must contains 5 characters');
    }
  };

  const validateMobileNumber = (value) => {
    if (PHONE_PATTERN.test(value)) {
      setMobileNumberError('');
    } else if (value.lenth == 0) {
      setMobileNumberError('mobile Number field cannot be empty');
    } else {
      setMobileNumberError('Mobile Number is invalid');
    }
  };

  const validateOtp = (value) => {
    if (OTP_PATTERN.test(value)) {
      setOtpError('');
    } else if (value.lenth == 0) {
      setOtpError('OTP field cannot be empty');
    } else {
      setOtpError('OTP is invalid');
    }
  };

  const handleSubmit = (values) => {
    if (orgCode.length == 0 || mobileNumber.length == 0 || otp.length == 0) {
      setMobileNumberError('mobile Number field cannot be empty');
      setOrgCodeError('Org Code field cannot be empty');
      setOtpError('OTP field cannot be empty');
    } else if (
      orgCodeError.length > 0 ||
      mobileNumberError.length > 0 ||
      otpError.length > 0
    ) {
      setSubmitError('All fields are required field');
    } else {
      handleVerifyOTP({orgCode, mobileNumber, otp});
    }
  };

  const handleVerifyOTP = async ({orgCode, mobileNumber, otp}: FormData) => {
    try {
      await verifyOTP(orgCode, mobileNumber, otp, '');
      navigation.navigate('ResetPassword', {orgCode, mobileNumber, otp});
    } catch (err) {
      alert('otp' + 'matches' + 'otp is invalid');
    }
  };

  const handleGenerateOTP = async (mobileNumber) => {
    // const mobileNumber = getValues('mobileNumber');

    if (!mobileNumber) {
      return alert(
        'mobileNumber' + 'required' + 'mobile number is a required field',
      );
    }

    try {
      const response = await generateOTP('+91', mobileNumber);

      // TODO: Remove this in production.
      alert('your otp is :' + response.data.otp);
    } catch (err) {
      alert('mobileNumber' + 'matches' + 'mobile number is invalid');
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />

      <View style={styles.body}>
        <View style={styles.form}>
          <Text style={styles.title}> Forgot Password </Text>
          <Text style={styles.title2}> Please fill up the details</Text>

          <TextInputUI
            width="lg"
            style={styles.input}
            name="orgCode"
            placeholder="Institute / Tutor Code"
            onChangeText={(value) => {
              setOrgCode(value);
              validateOrgCode(value);
            }}
            value={orgCode}
            otherStyle={{
              borderBottomColor: Colors.verylightGrey,
              borderBottomWidth: 1,
            }}
          />
          {orgCodeError.length > 1 ? (
            <Animatable.View
              animation="fadeInLeft"
              duration={500}
              style={{alignSelf: 'flex-start'}}>
              <Text style={{color: 'red', fontSize: 15}}>{orgCodeError}</Text>
            </Animatable.View>
          ) : (
            <View></View>
          )}

          <View style={{paddingTop: HeightWidth.getResHeight(20)}}></View>
          <TextInputUI
            width="lg"
            style={styles.input}
            name="mobileNumber"
            placeholder="Mobile Number"
            onChangeText={(value) => {
              setMobileNumber(value);
              validateMobileNumber(value);
            }}
            value={mobileNumber}
            otherStyle={{
              borderBottomColor: Colors.verylightGrey,
              borderBottomWidth: 1,
            }}
            keyboardType={'numeric'}
          />
          {mobileNumberError.length > 0 ? (
            <Animatable.View
              animation="fadeInLeft"
              duration={500}
              style={{alignSelf: 'flex-start'}}>
              <Text style={{color: 'red', fontSize: 15}}>
                {mobileNumberError}
              </Text>
            </Animatable.View>
          ) : (
            <View></View>
          )}

          <View style={{paddingTop: HeightWidth.getResHeight(20)}}></View>
          <TextInputUI
            width="lg"
            style={styles.input}
            name="otp"
            placeholder="OTP"
            onChangeText={(value) => {
              setOtp(value);
              validateOtp(value);
            }}
            password={true}
            value={otp}
            otherStyle={{
              borderBottomColor: Colors.verylightGrey,
              borderBottomWidth: 1,
            }}
            keyboardType={'numeric'}
          />
          {otpError.length > 0 ? (
            <Animatable.View
              animation="fadeInLeft"
              duration={500}
              style={{alignSelf: 'flex-start'}}>
              <Text style={{color: 'red', fontSize: 15}}>{otpError}</Text>
            </Animatable.View>
          ) : (
            <View></View>
          )}

          <View style={{paddingTop: HeightWidth.getResHeight(12)}}></View>
          <TouchableOpacity
            onPress={() => {
              handleGenerateOTP(mobileNumber);
            }}>
            <Text style={styles.link2}>Click here to get OTP</Text>
          </TouchableOpacity>
          <ButtonUI
            title="Submit"
            width={width * 0.95}
            backgroundColor="#4C8BF5"
            onPress={() => {
              const values = {
                mobileNumber: mobileNumber,
                orgCode: orgCode,
                otp: otp,
              };
              handleSubmit(values);
            }}
            borderRadius={5}
            textStyles={{
              color: 'white',
              fontSize: HeightWidth.getResFontSize(18),
              fontWeight: '400',
            }}
          />

          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Login');
            }}>
            <Text style={styles.link1}>Login page</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  input: {
    padding: 10,
    margin: 10,
    fontSize: 12,
    overflow: 'hidden',
  },
  title: {
    fontWeight: '500',
    fontSize: HeightWidth.getResFontSize(24),
    textAlign: 'center',
    marginVertical: 8,
    paddingBottom: '5%',
  },
  title2: {
    fontWeight: '400',
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'left',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footer: {
    flex: 0.2,
    // marginVertical: 8,
    alignItems: 'center',
    // marginBottom: 50,
  },
  body: {},
  form: {
    borderRadius: 18,
    backgroundColor: 'white',
    //  padding:20,
    textAlign: 'center',
    //  paddingTop:"10%",
    //  marginTop:'10%'
  },
  logo: {
    width: 50,
    height: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
  link1: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(12),
    backgroundColor: 'white',
    paddingTop: '20%',
    textAlign: 'center',
  },
  link2: {
    fontWeight: '400',
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(16),
    paddingTop: '5%',
    paddingBottom: '8%',
    // paddingLeft:"55%",
    paddingLeft: HeightWidth.getResWidth(200),
    borderRadius: 5,
    backgroundColor: 'white',
  },
  link3: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(18),
    paddingVertical: 10,
    backgroundColor: 'white',
    // paddingHorizontal: 20,
  },
  text: {
    fontSize: HeightWidth.getResFontSize(14),
    color: 'grey',
  },
  btn: {
    backgroundColor: '#f5c242',
    borderRadius: 8,
    padding: 20,
    margin: 20,
    textAlign: 'center',
  },
  button: {
    fontWeight: '400',
    backgroundColor: '#4C8BF5',
    borderRadius: 5,
    color: 'white',
    fontSize: 18,
    overflow: 'hidden',
    padding: '5%',
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  img: {
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  linknav: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default ForgotPassword;
