import React, {FunctionComponent, useState, useEffect} from 'react';
import {
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  Image,
  ImageBackground,
  ScrollView,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import CustomStatusBar from '../../components/Header/custom_status_bar';
import {width, Colors, height, images, strings} from '../../utility/utilities';
import {Role} from '../../common/enums/role';

//redux
import {connect} from 'react-redux';
import * as actions from '../../store/actions/index';

//utilities
import HeightWidth from '../../utility/HeightWidth';

interface Props {
  navigation: any;
  // setTokenData(value): any
  tokenValue: string;
}

const SplashScreen: FunctionComponent<Props> = ({
  navigation,
  // setTokenData,
  tokenValue,
}) => {
  const [authUser, setAuthUser] = useState({});
  useEffect(() => {
    setTimeout(async () => {
      try {
        const authUser = await AsyncStorage.getItem('authUser');
        setAuthUser(authUser);
      } catch (error) {}
      //ToDo: checkUserData();
      checkUserData();

      // navigation.reset({
      //   index: 0,
      //   routes: [{name: 'Login'}],
      // });
    }, 1500);
  }, []);

  // const getToken = async () =>{
  //    const token = await AsyncStorage.getItem('accessToken')
  //    setTokenData(token)
  // }

  const checkUserData = async () => {
    const userToken = await AsyncStorage.getItem('accessToken');

    const userRole = await AsyncStorage.getItem('userRole');
    console.log('userToken', userToken, userRole);
    // const completedRegistration = await AsyncStorage.getItem('completeReg');
    console.log('data------', userToken, userRole);

    // completedRegistration will be set to true once process is finished
    let completedRegistration = true;

    // after reopening app check for userToken and userRole if present send to homescreen else send to process/Login.
    if (userToken != undefined || userToken != null) {
      // check for userRole not null
      if (userRole == null || userRole == undefined) {
        navigation.reset({
          index: 0,
          routes: [{name: 'Login'}],
        });
      }
      //User Already present
      if (userRole === Role.TUTOR) {
        if (completedRegistration) {
          navigation.reset({
            index: 0,
            routes: [{name: 'appStackTutor'}],
          });
        } else {
          //process forms

          navigation.reset({
            index: 0,
            routes: [{name: 'createProfileDetailsStack', authUser: authUser}],
          });

          // navigation.navigate('createProfileDetailsStack');
        }
      } else if (userRole === Role.STUDENT) {
        navigation.reset({
          index: 0,
          routes: [{name: 'appStackStudent'}],
        });
      } else if (userRole === Role.ORGANIZATION) {
        navigation.reset({
          index: 0,
          routes: [{name: 'appStackOrg'}],
        });
      } else if (userRole === Role.PARENT) {
        navigation.reset({
          index: 0,
          routes: [{name: 'appStackParent'}],
        });
      }
    } else {
      //send to register
      console.log('we are coming here too');
      navigation.reset({
        index: 0,
        routes: [{name: 'Login'}],
      });
    }
  };

  return (
    <View style={styles.__contianer}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />
      <Image source={images.sampleBack} style={styles.__defaultLogo} />
      <View
        style={{
          flex: 1,
          justifyContent: 'flex-end',
          marginBottom: HeightWidth.getResHeight(50),
          paddingVertical: 50,
        }}>
        <View style={{alignSelf: 'center'}}>
          <Text style={{}}>Welcome to Edumatica</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  __contianer: {
    flex: 1,
    backgroundColor: Colors.fullWhite,
  },
  __defaultLogo: {
    resizeMode: 'contain',
    width: width,
    height: '60%',
  },
  __iconBg: {
    height: 80,
    width: 80,
    backgroundColor: Colors.white,
    elevation: 6,
    borderRadius: 40,
    shadowColor: '#470000',
    shadowOffset: {width: 0, height: 6},
    shadowOpacity: 0.2,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = (state) => {
  return {
    tokenValue: state.auth.setTokenValue,
  };
};

// const mapDispatchToProps = dispatch => {
//     return {
//         setTokenData: (setTokenValue) => dispatch(actions.setToken(setTokenValue)),
//         // setUserType: (setUserType) => dispatch(actions.setUser(setUserType))
//     }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
export default SplashScreen;
