import React, {FunctionComponent, useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  Image,
  View,
  Linking,
  Button,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
  ToastAndroid,
} from 'react-native';
import {
  Link,
  Route,
  Redirect,
  NativeRouter,
  RouteComponentProps,
} from 'react-router-native';
import * as yup from 'yup';
import * as Animatable from 'react-native-animatable';
import SmsRetriever from 'react-native-sms-retriever';

// Import the commons
import TextInputUI from '../../components/UI/TextInput';
import ButtonUI from '../../components/UI/Button';
import CustomStatusBar from '../../components/Header/custom_status_bar';
import {width, Colors, height, images, strings} from '../../utility/utilities';
import {generateOTP, verifyOTP, checkOrgCode} from '../../common/api/auth';
import {
  OTP_PATTERN,
  PHONE_PATTERN,
  PASSWORD_PATTERN,
} from '../../common/validations/patterns';
import HeightWidth from '../../utility/HeightWidth';

interface FormData {
  orgCode: string;
  mobileNumber: string;
  otp: string;
  accountType: string;
  serverError: string;
}

const ValidationSchema = yup.object().shape({
  //   orgCode: yup
  //     .string()
  //     .required('Code is a required field'),
  mobileNumber: yup
    .string()
    .required('mobile number is a required field')
    .matches(PHONE_PATTERN, 'mobile number is invalid'),
  //   otp: yup.string().required().matches(OTP_PATTERN, 'otp is invalid'),
  password: yup
    .string()
    .required()
    .matches(
      PASSWORD_PATTERN,
      'password must contain uppercase, lowercase, alphanumeric, & special characters',
    ),
});

interface Props {
  navigation: any;
  route: any;
}

const Register2: FunctionComponent<Props> = ({route, navigation}) => {
  const [role, setRole] = useState(route.params.role);
  const [mobileNumber, setMobileNumber] = useState('');
  const [password, setPassword] = useState('');
  const [orgCode, setOrgCode] = useState('');
  const [roles] = useState<string[]>([
    'STUDENT',
    'TUTOR',
    'PARENT',
    'INSTITUTE',
  ]);
  const [redirectTo, setRedirectTo] = useState('');
  const [width, setWidth] = useState(Dimensions.get('screen').width * 1);
  // const [_isLoadingPhoneNo, _setisLoadingPhoneNo] = useState(false);
  const [orgCodeError, setOrgCodeError] = useState('');
  const [mobileNumberError, setMobileNumberError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [submitError, setSubmitError] = useState('');

  useEffect(() => {
    setTimeout(() => {
      fetchPhoneNoFromDevice();
    }, 300);
  }, []);

  //Fetch Phone Number
  const fetchPhoneNoFromDevice = async () => {
    try {
      var phoneNumber = await SmsRetriever.requestPhoneNumber();
      let ExtractedPhoneNo = phoneNumber.substr(3);
      setMobileNumber(ExtractedPhoneNo);
    } catch (error) {
      console.log(JSON.stringify(error));
    }
  };

  // validation
  const validateOrgCode = (value) => {
    if (value.length > 4) {
      setOrgCodeError('');
    } else if (value.lenth == 0) {
      setOrgCodeError('Org Code field cannot be empty');
    } else {
      setOrgCodeError('Org Code must contains 5 characters');
    }
  };

  const validateMobileNumber = (value) => {
    if (PHONE_PATTERN.test(value)) {
      setMobileNumberError('');
    } else if (value.lenth == 0) {
      setMobileNumberError('mobile Number field cannot be empty');
    } else {
      setMobileNumberError('Mobile Number is invalid');
    }
  };

  const validatePassword = (value) => {
    if (PASSWORD_PATTERN.test(value)) {
      setPasswordError('');
    } else if (value.lenth == 0) {
      setPasswordError('password field cannot be empty');
    } else {
      setPasswordError(
        'password must contain uppercase, lowercase, alphanumeric, & special characters',
      );
    }
  };

  const handleSubmit = (values) => {
    if (
      orgCode.length == 0 ||
      mobileNumber.length == 0 ||
      password.length == 0
    ) {
      setSubmitError('All fields are required field');
      setMobileNumberError('mobile Number field cannot be empty');
      setOrgCodeError('Org Code field cannot be empty');
      setPasswordError('password field cannot be empty');
    } else if (orgCodeError.length > 0 || mobileNumberError.length > 0) {
      setSubmitError('All fields are required field');
    } else {
      handleGenerateOTP(mobileNumber);
      navigation.navigate('OTP', {orgCode, mobileNumber, password, role});
    }
  };

  const handleGenerateOTP = async (mobileNumber) => {
    try {
      const response = await generateOTP('+91', mobileNumber);
      // TODO: Remove this in production.
      alert('otp is ' + response.data.otp);
    } catch (error) {
      if (error.response?.status === 401) {
        navigation.navigate('Login');
      } else {
        alert('error in getting mobile number');
      }
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />

      <View style={styles.body}>
        <View style={styles.form}>
          <Text style={styles.title}>Register</Text>
          <Text style={styles.title2}>Please fill details</Text>

          {/* <View>
      {role=='STUDENT' ?(<View >
                 <TouchableOpacity style={{backgroundColor:"#ADE394", padding:"5%", borderRadius:5,flexDirection: 'row', justifyContent: 'space-between'}} > 
                  <Text style={styles.title}>{role}</Text>
                  </TouchableOpacity>
                  </View>):(<View></View>)}
      {role=='TUTOR' ?(<View >
                 <TouchableOpacity style={{backgroundColor:"#FEB4B4", padding:"5%", borderRadius:5,flexDirection: 'row', justifyContent: 'space-between'}} > 
                  <Text style={styles.title}>{role}</Text>
                  </TouchableOpacity>
                  </View>):(<View></View>)}
      {role=='INSTITUTE' ?(<View >
                 <TouchableOpacity style={{backgroundColor:"#97DFFE", padding:"5%", borderRadius:5,flexDirection: 'row', justifyContent: 'space-between'}} > 
                  <Text style={styles.title}>{role}</Text>
                  </TouchableOpacity>
                  </View>):(<View></View>)}
      {role=='PARENT' ?(<View >
                 <TouchableOpacity style={{backgroundColor:"#AF8AFE", padding:"5%", borderRadius:5,flexDirection: 'row', justifyContent: 'space-between'}} > 
                  <Text style={styles.title}>{role}</Text>
                  </TouchableOpacity>
                  </View>):(<View></View>)}
    </View> */}

          <TextInputUI
            width="lg"
            style={styles.input}
            name="orgCode"
            placeholder="Institute / Tutor Code"
            onChangeText={(value) => {
              setOrgCode(value);
              validateOrgCode(value);
            }}
            value={orgCode}
            otherStyle={{
              borderBottomColor: Colors.verylightGrey,
              borderBottomWidth: 1,
            }}
          />
          {orgCodeError.length > 1 ? (
            <Animatable.View
              animation="fadeInLeft"
              duration={500}
              style={{alignSelf: 'flex-start'}}>
              <Text style={{color: 'red', fontSize: 15}}>{orgCodeError}</Text>
            </Animatable.View>
          ) : (
            <View></View>
          )}

          <View style={{paddingTop: HeightWidth.getResHeight(20)}}></View>
          <TextInputUI
            width="lg"
            style={styles.input}
            name="mobileNumber"
            placeholder="Mobile Number"
            onChangeText={(value) => {
              setMobileNumber(value);
              validateMobileNumber(value);
            }}
            value={mobileNumber}
            otherStyle={{
              borderBottomColor: Colors.verylightGrey,
              borderBottomWidth: 1,
            }}
            keyboardType={'numeric'}
          />
          {mobileNumberError.length > 0 ? (
            <Animatable.View
              animation="fadeInLeft"
              duration={500}
              style={{alignSelf: 'flex-start'}}>
              <Text style={{color: 'red', fontSize: 15}}>
                {mobileNumberError}
              </Text>
            </Animatable.View>
          ) : (
            <View></View>
          )}

          <View style={{paddingTop: HeightWidth.getResHeight(12)}}></View>
          <TextInputUI
            width="lg"
            style={styles.input}
            name="password"
            placeholder="Password"
            onChangeText={(value) => {
              setPassword(value);
              validatePassword(value);
            }}
            password={true}
            value={password}
            otherStyle={{
              borderBottomColor: Colors.verylightGrey,
              borderBottomWidth: 1,
            }}
          />
          {passwordError.length > 0 ? (
            <Animatable.View
              animation="fadeInLeft"
              duration={500}
              style={{alignSelf: 'flex-start'}}>
              <Text style={{color: 'red', fontSize: 15}}>{passwordError}</Text>
            </Animatable.View>
          ) : (
            <View></View>
          )}

          <View style={{paddingTop: HeightWidth.getResHeight(40)}}></View>

          <ButtonUI
            title="Sign Up"
            width={width * 0.95}
            backgroundColor="#4C8BF5"
            onPress={() => {
              const values = {
                mobileNumber: mobileNumber,
                orgCode: orgCode,
                password: password,
              };
              handleSubmit(values);
            }}
            borderRadius={5}
            textStyles={{
              color: 'white',
              fontSize: HeightWidth.getResFontSize(18),
              fontWeight: '400',
            }}
          />

          <View style={{paddingTop: HeightWidth.getResHeight(40)}}></View>
          <View style={styles.fixToText}>
            <Text style={styles.text}>
              {' '}
              By tapping Sign Up, you agree with our{' '}
            </Text>
            <Text
              style={styles.link1}
              onPress={() =>
                Linking.openURL('http://65.0.71.62/terms-conditions')
              }>
              Terms & Conditions
            </Text>
            <Text style={styles.text}>, Learn</Text>
          </View>

          <View style={styles.fixToText}>
            <Text style={styles.text}>how we process your data in our </Text>
            <Text
              style={styles.link1}
              onPress={() =>
                Linking.openURL('http://65.0.71.62/privacy-policy')
              }>
              Privacy Policy
            </Text>
            <Text style={styles.text}>, and </Text>
            <Text
              style={styles.link1}
              onPress={() =>
                Linking.openURL('http://65.0.71.62/privacy-policy')
              }>
              Cookies Policy
            </Text>
          </View>
          <View style={{paddingTop: HeightWidth.getResHeight(40)}}></View>

          <View style={styles.fixToText}>
            <View style={styles.linknav}>
              <Text style={styles.text3}>Already have account? </Text>

              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Login');
                }}>
                <Text style={styles.link1}> Sign In</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  input: {
    padding: 10,
    margin: 10,
    fontSize: HeightWidth.getResFontSize(12),
    overflow: 'hidden',
  },
  title: {
    fontWeight: '500',
    fontSize: HeightWidth.getResFontSize(24),
    textAlign: 'center',
    marginVertical: 8,
    paddingBottom: '5%',
  },
  title2: {
    fontWeight: '400',
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'left',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footer: {
    flex: 0.2,
    // marginVertical: 8,
    alignItems: 'center',
    // marginBottom: 50,
  },
  body: {},
  form: {
    borderRadius: 18,
    backgroundColor: 'white',
    // padding:20,
    textAlign: 'center',
    // paddingTop:"10%",
    // marginTop:'10%'
  },
  logo: {
    width: 50,
    height: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    alignContent: 'center',
    alignItems: 'center',
  },

  link1: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(12),
  },
  link2: {
    fontWeight: '500',
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(14),
    paddingVertical: '5%',
    paddingLeft: '60%',
  },
  link3: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(18),
    paddingVertical: 10,
    // paddingHorizontal: 20,
  },
  text: {
    fontSize: HeightWidth.getResFontSize(14),
    color: 'grey',
  },
  text2: {
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'left',
    color: 'black',
    fontWeight: '400',
  },
  text3: {
    fontSize: HeightWidth.getResFontSize(12),
    textAlign: 'right',
    color: 'black',
    fontWeight: '400',
  },
  btn: {
    backgroundColor: '#f5c242',
    borderRadius: 8,
    padding: 20,
    margin: 20,
    textAlign: 'center',
  },
  button: {
    fontWeight: '400',
    backgroundColor: '#4C8BF5',
    borderRadius: 5,
    color: 'white',
    fontSize: 18,
    overflow: 'hidden',
    padding: '5%',
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  img: {
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  linknav: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default Register2;
