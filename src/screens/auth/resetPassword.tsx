import React, {FunctionComponent, useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  Image,
  View,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import {Redirect, RouteComponentProps} from 'react-router-native';
import * as yup from 'yup';
import TextInputUI from '../../components/UI/TextInput';
import ButtonUI from '../../components/UI/Button';
import CustomStatusBar from '../../components/Header/custom_status_bar';
import * as Animatable from 'react-native-animatable';

import {PASSWORD_PATTERN} from '../../common/validations/patterns';
import {setPassword} from '../../common/api/auth';
import {width, Colors, height, images, strings} from '../../utility/utilities';
import HeightWidth from '../../utility/HeightWidth';

interface Props {
  navigation: any;
  route: any;
}

interface FormData {
  password: string;
  passwordConfirmation: string;
}

const ValidationSchema = yup.object().shape({
  password: yup
    .string()
    .required()
    .matches(
      PASSWORD_PATTERN,
      'password must contain uppercase, lowercase, alphanumeric, & special characters',
    ),
  passwordConfirmation: yup
    .string()
    .oneOf([yup.ref('password')], 'passwords must match'),
});

const ResetPassword: FunctionComponent<Props> = ({route, navigation}) => {
  const orgCode = route.params.orgCode;
  const mobileNumber = route.params.mobileNumber;
  const otp = route.params.otp;
  const [passwordError, setPasswordError] = useState('');
  const [password2Error, setPassword2Error] = useState('');
  const [submitError, setSubmitError] = useState('');

  const [width, setWidth] = useState(Dimensions.get('screen').width * 1);
  const [pass, setPass] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const [
    isPasswordConfirmationVisible,
    setIsPasswordConfirmationVisible,
  ] = useState(false);

  const validatePassword = (value) => {
    if (PASSWORD_PATTERN.test(value)) {
      setPasswordError('');
    } else if (value.lenth == 0) {
      setPasswordError('password field cannot be empty');
    } else {
      setPasswordError(
        'password must contain uppercase, lowercase, alphanumeric, & special characters',
      );
    }
  };

  const validatePassword2 = (value) => {
    if (PASSWORD_PATTERN.test(value)) {
      setPassword2Error('');
    } else if (value.lenth == 0) {
      setPassword2Error('password field cannot be empty');
    } else if (pass !== value) {
      setPassword2Error('Passwords must match');
    } else {
      setPassword2Error(
        'password must contain uppercase, lowercase, alphanumeric, & special characters',
      );
    }
  };

  if (!orgCode || !mobileNumber || !otp) {
    // setRedirectTo('/forgot-password');
    navigation.navigate('ForgotPassword');
  }

  const handleSubmit = (values) => {
    if (pass.length == 0 || passwordConfirmation.length == 0) {
      setSubmitError('All fields are required field');
      setPasswordError('password field cannot be empty');
      setPassword2Error('password field cannot be empty');
    } else if (pass !== passwordConfirmation) {
      setSubmitError('Passwords must match');
      setPassword2Error('Passwords must match');
    } else {
      handleResetPassword(values);
    }
  };

  const handleResetPassword = async ({password}) => {
    try {
      await setPassword(
        orgCode as string,
        mobileNumber as string,
        otp as string,
        password,
      );

      navigation.navigate('Login');
      alert('password changed successfully');
    } catch (error) {
      alert('password' + 'invalid' + error.response.data.message);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />

      <View style={styles.body}>
        <View style={styles.form}>
          <Text style={styles.title}>New Password </Text>
          <Text style={styles.title2}>New Password Setup</Text>
          <TextInputUI
            width="lg"
            style={styles.input}
            name="pass"
            placeholder="Password"
            onChangeText={(value) => {
              setPass(value);
              validatePassword(value);
            }}
            password={true}
            value={pass}
            otherStyle={{
              borderBottomColor: Colors.verylightGrey,
              borderBottomWidth: 1,
            }}
          />
          {passwordError.length > 0 ? (
            <Animatable.View
              animation="fadeInLeft"
              duration={500}
              style={{alignSelf: 'flex-start'}}>
              <Text style={{color: 'red', fontSize: 15}}>{passwordError}</Text>
            </Animatable.View>
          ) : (
            <View></View>
          )}

          <View style={{paddingTop: HeightWidth.getResHeight(20)}}></View>
          <TextInputUI
            width="lg"
            style={styles.input}
            name="Password Confirmation"
            placeholder="Password"
            onChangeText={(value) => {
              setPasswordConfirmation(value);
              validatePassword2(value);
            }}
            password={true}
            value={passwordConfirmation}
            otherStyle={{
              borderBottomColor: Colors.verylightGrey,
              borderBottomWidth: 1,
            }}
          />
          {password2Error.length > 0 ? (
            <Animatable.View
              animation="fadeInLeft"
              duration={500}
              style={{alignSelf: 'flex-start'}}>
              <Text style={{color: 'red', fontSize: 15}}>{password2Error}</Text>
            </Animatable.View>
          ) : (
            <View></View>
          )}

          <View style={{paddingTop: HeightWidth.getResHeight(18)}}></View>

          <ButtonUI
            title="Log In"
            width={width * 0.95}
            backgroundColor="#4C8BF5"
            onPress={() => {
              const values = {password: pass};
              handleSubmit(values);
            }}
            borderRadius={5}
            textStyles={{
              color: 'white',
              fontSize: HeightWidth.getResFontSize(18),
              fontWeight: '400',
            }}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  input: {
    padding: 10,
    margin: 10,
    fontSize: 12,
    overflow: 'hidden',
  },
  title: {
    fontWeight: '500',
    fontSize: HeightWidth.getResFontSize(24),
    textAlign: 'center',
    marginVertical: 8,
    paddingBottom: '5%',
  },
  title2: {
    fontWeight: '400',
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'left',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footer: {
    flex: 0.2,
    // marginVertical: 8,
    alignItems: 'center',
    // marginBottom: 50,
  },
  body: {},
  form: {
    borderRadius: 18,
    backgroundColor: 'white',
    //  padding:20,
    textAlign: 'center',
    //  paddingTop:"10%",
    //  marginTop:'10%'
  },
  logo: {
    width: 50,
    height: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
  link1: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(12),
    backgroundColor: 'white',
    paddingTop: '20%',
    textAlign: 'center',
  },
  link2: {
    fontWeight: '400',
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(16),
    paddingTop: '5%',
    paddingBottom: '8%',
    paddingLeft: '55%',
    borderRadius: 5,
    backgroundColor: 'white',
  },
  link3: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(18),
    paddingVertical: 10,
    backgroundColor: 'white',
    // paddingHorizontal: 20,
  },
  text: {
    fontSize: HeightWidth.getResFontSize(14),
    color: 'grey',
  },
  btn: {
    backgroundColor: '#f5c242',
    borderRadius: 8,
    padding: 20,
    margin: 20,
    textAlign: 'center',
  },
  button: {
    fontWeight: '400',
    backgroundColor: '#4C8BF5',
    borderRadius: 5,
    color: 'white',
    fontSize: 18,
    overflow: 'hidden',
    padding: '5%',
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  img: {
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  linknav: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default ResetPassword;
