import React, { FunctionComponent, useState, useEffect, createRef, useRef } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
  Platform,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import RNOtpVerify from 'react-native-otp-verify';
import OTPInputView from '@twotalltotems/react-native-otp-input';

//components
import TextInputUI from '../../components/UI/TextInput';
import ButtonUI from '../../components/UI/Button';
import CustomStatusBar from '../../components/Header/custom_status_bar';

//utilities
import { width, Colors, height, images, strings } from '../../utility/utilities';
import HeightWidth from '../../utility/HeightWidth';
import { registerUser } from '../../common/api/auth';

interface Props {
  navigation: any;
  route: any;
}

import { generateOTP, verifyOTP, checkOrgCode } from '../../common/api/auth';
import { OTP_PATTERN, PHONE_PATTERN } from '../../common/validations/patterns';
import { Role } from '../../common/enums/role';
import BaseHeader from '../../components/Header/BaseHeader';

const Otp_verification: FunctionComponent<Props> = ({ route, navigation }) => {
  const [password, setPassword] = useState(route.params.password);
  const [role, setRole] = useState(route.params.role);
  const [mobileNumber, setMobileNumber] = useState(route.params.mobileNumber);
  const [orgCode, setOrgCode] = useState(route.params.orgCode);
  const [autoFocusOn, setAutoFocusOn] = useState(false);
  const [otp, setOtp] = useState('');
  const [roles] = useState<string[]>([
    'STUDENT',
    'TUTOR',
    'PARENT',
    'INSTITUTE',
  ]);
  const [counter, setCounter] = useState(120);
  const [width, setWidth] = useState(Dimensions.get('screen').width * 1);
  const [otpError, setOtpError] = useState('');
  const [submitError, setSubmitError] = useState('');

  useEffect(() => {
    const timer =
      counter > 0 && setInterval(() => setCounter(counter - 1), 1000);
    return () => clearInterval(timer);
  }, [counter]);

  const validateOtp = (value) => {
    if (OTP_PATTERN.test(value)) {
      setOtpError('');
    } else if (value.lenth == 0) {
      setOtpError('OTP field cannot be empty');
    } else {
      console.log("val otp is invalid", value)
      setOtpError('OTP is invalid');
    }
  };

  const handleSubmit = async (otp) => {
    setOtpError('');
    await validateOtp(otp);
    if (orgCode.length == 0 || mobileNumber.length == 0 || otp.length == 0) {
      setOtpError('OTP field cannot be empty');
    } else if (otpError != '') {
      // console.log(orgCode, mobileNumber, otp, otpError)
      // setSubmitError('All fields are required field');
      setOtpError(otpError);
    } else {
      console.log("handleVerifyOTP")
      handleVerifyOTP(orgCode, mobileNumber, otp);
    }
  };

  const handleVerifyOTP = async (orgCode, mobileNumber, otp) => {
    if (!role) {
      alert('account type error');
    } else
      try {
        let userType = Role.TUTOR;
        switch (role) {
          case 'STUDENT':
            console.log('student processing');
            userType = Role.STUDENT;
            break;
          case 'TUTOR':
            console.log('Tutor processing');
            userType = Role.TUTOR;
            break;
          case 'INSTITUTE':
            console.log('institute processing');
            userType = Role.ORGANIZATION;
            break;
          case 'PARENT':
            console.log('parent processing');
            userType = Role.PARENT;
            break;
          default:
            userType = Role.TUTOR;
        }
        var response = await verifyOTP(orgCode, mobileNumber, otp, userType);
        //   console.log("this is response",response.data.message);
        if (response.data.message == 'Success') {
          alert(response.data.message);
          try {
            await registerUser(
              orgCode as string,
              mobileNumber as string,
              otp as string,
              password,
              userType,
            );
            navigation.navigate('Login');
            alert('account created successfully');
          } catch (error) {
            if (error.response?.status === 401) {
              navigation.navigate('Login');
            } else {
              alert(
                'serverError' + 'Invalid Data' + error.response?.data.message,
              );
            }
          }
        }
      } catch (error) {
        if (error.response?.status === 401) {
          navigation.navigate('Login');
        } else {
          alert('please enter correct otp');
          setOtpError('OTP is invalid');
        }
      }
  };

  const handleGenerateOTP = async (mobileNumber) => {
    try {
      const response = await generateOTP('+91', mobileNumber);
      // TODO: Remove this in production.
      alert('otp is ' + response.data.otp);
    } catch (error) {
      if (error.response?.status === 401) {
        navigation.navigate('Login');
      } else {
        alert('error in getting mobile number');
      }
    }
  };

  useEffect(() => {
    getHash();
    startListeningForOtp();
    return () => {
      RNOtpVerify.removeListener();
    };
  }, []);

  const getHash = () => {
    RNOtpVerify.getHash().then(console.log).catch(console.log);
  };

  const startListeningForOtp = () => {
    RNOtpVerify.getOtp()
      .then((p) => {
        RNOtpVerify.addListener(otpHandler);
        setAutoFocusOn(true);
      })
      .catch((p) => console.log(p));
  };

  const otpHandler = (message) => {
    setOtpError('');
    if (message === null) {
      RNOtpVerify.removeListener();
    } else if (message.length > 0) {
      try {
        console.log("message------", message)
        let otpCode = /(\d{6})/g.exec(message)[1];
        setOtp(otpCode);
        handleSubmit(otpCode);
        RNOtpVerify.removeListener();
        console.log('OTPCode', otpCode);
      } catch (err) {
        console.log('error', err);
      }
    }
  };

  const autoFillOTP = () => {
    if (otp != null) {
      return otp;
    }
  };

  return (
    <SafeAreaView style={styles.__container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />
      <BaseHeader
        onPress={() => {
          navigation.goBack();
        }}
        title={'EDUMATICA'}
      />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 50 : 0}
        style={[styles.__container]}>
        <ScrollView
          keyboardDismissMode={'on-drag'}
          keyboardShouldPersistTaps="always">
          <View style={styles.__innerContent}>
            <Text style={styles.__innterContentHeader}>OTP Verification</Text>
            <Text style={styles.__innterContentSubHeader}>
              Please verify yourself by providing 6 digit OTP sent to your
              mobile number ******{mobileNumber.substring(5, 10)}
            </Text>
            <View style={styles.__textInput}>
              <OTPInputView
                style={styles.__otpInputView}
                pinCount={6}
                autoFocusOnLoad={autoFocusOn}
                code={autoFillOTP()}
                onCodeChanged={(code) => {
                  setOtp(code);
                }}
                codeInputFieldStyle={styles.__underlineStyleBase}
                codeInputHighlightStyle={styles.__underlineStyleHighLighted}
              />
              {otpError.length > 0 ? (
                <Animatable.View animation="fadeInLeft" duration={500} style={styles.__errorView}>
                  <Text style={styles.__errorText}>{otpError}</Text>
                </Animatable.View>
              ) :
                null
              }
            </View>
            <Text style={styles.__counterMsgText}>
              Request otp again in: {counter} seconds
            </Text>
            <View style={styles.__inputButton}>
              <ButtonUI
                title="Verify"
                width={width * 0.85}
                height={HeightWidth.getResWidth(55)}
                textStyles={{ fontSize: 18, color: Colors.fullWhite }}
                backgroundColor={Colors.primaryBlue}
                onPress={() => {
                  handleSubmit(otp);
                }}
                borderRadius={5}
              />
            </View>
            <TouchableOpacity
              disabled={counter === 0 ? false : true}
              onPress={() => {
                setCounter(120);
                handleGenerateOTP(mobileNumber);
              }}>
              <Text style={[styles.__resendOTPText, { color: counter === 0 ? Colors.primaryBlue : Colors.veryLightGray, }]}>
                Resend OTP?
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Register', { role: role });
              }}
              style={styles.__changeMobileNoView}
            >
              <Text style={styles.__changeMobileNoText}> Change Mobile Number </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  __container: {
    flex: 1,
    backgroundColor: 'white',
  },
  __innerContent: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: HeightWidth.getResWidth(30)
  },
  __innterContentHeader: {
    fontWeight: '700',
    fontSize: HeightWidth.getResFontSize(26),
    textAlign: 'center',
    alignItems: 'center',
    marginVertical: HeightWidth.getResWidth(30),
  },
  __innterContentSubHeader: {
    fontSize: HeightWidth.getResFontSize(14),
    color: 'black',
    fontWeight: '400',
    alignItems: 'center',
    textAlign: 'center',
    marginHorizontal: HeightWidth.getResWidth(35),
  },
  __textInput: {
    marginVertical: HeightWidth.getResWidth(20),
    justifyContent: 'center',
    alignItems: 'center',
  },
  __errorView: {
    marginVertical: HeightWidth.getResWidth(6),
    alignSelf: 'flex-start',
  },
  __errorText: {
    color: Colors.primaryRed,
    fontSize: HeightWidth.getResFontSize(15),
  },
  __inputButton: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: HeightWidth.getResWidth(25),
  },
  __counterMsgText: {
    alignSelf: 'flex-end',
    marginHorizontal: HeightWidth.getResWidth(15),
    fontSize: HeightWidth.getResFontSize(14),
    color: Colors.veryGrayishBlack,
  },
  __resendOTPText: {
    fontWeight: "bold",
    fontSize: HeightWidth.getResFontSize(16),
    marginVertical: HeightWidth.getResWidth(15),
    alignItems: 'center',
    textAlign: 'center',
  },
  __changeMobileNoView: {
    marginVertical: HeightWidth.getResWidth(25)
  },
  __changeMobileNoText: {
    color: Colors.primaryBlue,
    fontSize: HeightWidth.getResFontSize(14),
    fontWeight: '500',
    alignItems: 'center',
    textAlign: 'center',
  },
  __otpInputView: {
    height: HeightWidth.getResWidth(65),
    alignSelf: 'center',
    width: HeightWidth.getResWidth(width - 40),
  },
  __underlineStyleBase: {
    width: HeightWidth.getResWidth(40),
    height: HeightWidth.getResWidth(65),
    borderWidth: 0,
    borderBottomColor: Colors.veryDarkGray,
    borderBottomWidth: 1,
    color: Colors.primaryBlue,
    backgroundColor: Colors.fullWhite,
    fontSize: 25,
  },
  __underlineStyleHighLighted: {
    borderColor: Colors.pureCyanBlue,
  },
});

export default Otp_verification;
