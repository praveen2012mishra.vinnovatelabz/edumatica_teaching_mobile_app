import React, {FunctionComponent, useState} from 'react';
import {useDispatch} from 'react-redux';
import {
  StyleSheet,
  Text,
  TextInput,
  Image,
  View,
  Linking,
  Button,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import HeightWidth from '../../utility/HeightWidth';

import {Role} from '../../common/enums/role';
import CustomStatusBar from '../../components/Header/custom_status_bar';
import {width, Colors, height, images, strings} from '../../utility/utilities';
interface Props {
  navigation: any;
}

const PreRegister: FunctionComponent<Props> = ({navigation}) => {
  const [redirectTo, setRedirectTo] = useState('');
  const dispatch = useDispatch();
  const [username, setUsername] = useState('');
  const [roles, setRoles] = useState<Role[]>([]);

  return (
    <SafeAreaView style={styles.container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />

      <View style={styles.body}>
        <View>
          <View style={styles.form}>
            <Text style={styles.title}> Register as </Text>
            {/* <Text style={styles.title2}> Please login to your account </Text>   */}

            <View style={{flexDirection: 'column', alignItems: 'center'}}>
              <View
                style={{
                  flexDirection: 'row',
                  padding: '2%',
                  alignItems: 'center',
                }}>
                <View>
                  <TouchableOpacity
                    style={{
                      backgroundColor: '#ADE394',
                      padding: '15%',
                      borderRadius: 5,
                      alignItems: 'center',
                      width: HeightWidth.getResWidth(150),
                      height: HeightWidth.getResHeight(170),
                    }}
                    onPress={() => {
                      navigation.navigate('Register', {role: 'STUDENT'});
                    }}>
                    <Image
                      style={styles.stretch}
                      source={require('../../images/rolesPng/student.png')}
                    />

                    <Text
                      style={{
                        color: 'white',
                        fontSize: 14,
                        alignItems: 'center',
                        textAlign: 'center',
                        paddingTop: '5%',
                      }}>
                      Student
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{padding: '2%'}}></View>
                <View>
                  <TouchableOpacity
                    style={{
                      backgroundColor: '#FEB4B4',
                      padding: '15%',
                      borderRadius: 5,
                      alignItems: 'center',
                      width: HeightWidth.getResWidth(150),
                      height: HeightWidth.getResHeight(170),
                    }}
                    onPress={() => {
                      navigation.navigate('Register', {role: 'TUTOR'});
                    }}>
                    <Image
                      style={styles.stretch}
                      source={require('../../images/rolesPng/tutor.png')}
                    />

                    <Text
                      style={{
                        color: 'white',
                        fontSize: 14,
                        alignItems: 'center',
                        textAlign: 'center',
                        paddingTop: '5%',
                      }}>
                      Tutor
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  padding: '2%',
                  alignItems: 'center',
                }}>
                <View>
                  <TouchableOpacity
                    style={{
                      backgroundColor: '#AF8AFE',
                      padding: '15%',
                      borderRadius: 5,
                      alignItems: 'center',
                      width: HeightWidth.getResWidth(150),
                      height: HeightWidth.getResHeight(170),
                    }}
                    onPress={() => {
                      navigation.navigate('Register', {role: 'PARENT'});
                    }}>
                    <Image
                      style={styles.stretch}
                      source={require('../../images/rolesPng/parent.png')}
                    />

                    <Text
                      style={{
                        color: 'white',
                        fontSize: 14,
                        alignItems: 'center',
                        textAlign: 'center',
                        paddingTop: '5%',
                      }}>
                      Gauradian
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{padding: '2%'}}></View>
                <View>
                  <TouchableOpacity
                    style={{
                      backgroundColor: '#97DFFE',
                      padding: '15%',
                      borderRadius: 5,
                      alignItems: 'center',
                      width: HeightWidth.getResWidth(150),
                      height: HeightWidth.getResHeight(170),
                    }}
                    onPress={() => {
                      navigation.navigate('Register', {role: 'INSTITUTE'});
                    }}>
                    <Image
                      style={styles.stretch}
                      source={require('../../images/rolesPng/org.png')}
                    />

                    <Text
                      style={{
                        color: 'white',
                        fontSize: 14,
                        alignItems: 'center',
                        textAlign: 'center',
                        paddingTop: '5%',
                      }}>
                      institute
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Login');
          }}>
          <Text style={styles.link3}> Already have a account </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginLeft: HeightWidth.getResWidth(2),
    marginRight: HeightWidth.getResWidth(2),
  },
  input: {
    padding: 10,
    margin: 10,
    fontSize: 12,
    overflow: 'hidden',
  },
  title: {
    fontWeight: '500',
    fontSize: HeightWidth.getResFontSize(24),
    textAlign: 'center',
    marginVertical: 8,
    paddingBottom: '5%',
  },
  title2: {
    fontWeight: '400',
    fontSize: HeightWidth.getResFontSize(18),
    textAlign: 'left',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  footer: {
    flex: 0.2,
    // marginVertical: 8,
    alignItems: 'center',
    // marginBottom: 50,
  },
  body: {},
  form: {
    borderRadius: 18,
    backgroundColor: 'white',
    //  padding:20,
    textAlign: 'center',
    //  paddingTop:"10%",
    //  marginTop:'10%'
  },
  logo: {
    width: 50,
    height: 50,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
  link1: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(12),
    backgroundColor: 'white',
  },
  link2: {
    fontWeight: '400',
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(16),
    paddingTop: '5%',
    paddingBottom: '8%',
    paddingLeft: '60%',
    borderRadius: 5,
    backgroundColor: 'white',
  },
  link3: {
    color: '#4C8BF5',
    fontSize: HeightWidth.getResFontSize(18),
    paddingVertical: 10,
    backgroundColor: 'white',
    // paddingHorizontal: 20,
  },
  text: {
    fontSize: HeightWidth.getResFontSize(14),
    color: 'grey',
  },
  btn: {
    backgroundColor: '#f5c242',
    borderRadius: 8,
    padding: 20,
    margin: 20,
    textAlign: 'center',
  },
  button: {
    fontWeight: '400',
    backgroundColor: '#4C8BF5',
    borderRadius: 5,
    color: 'white',
    fontSize: HeightWidth.getResFontSize(18),
    overflow: 'hidden',
    padding: '5%',
    marginBottom: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  img: {
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  linknav: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  stretch: {
    width: HeightWidth.getResWidth(80),
    height: HeightWidth.getResHeight(80),
    resizeMode: 'contain',
  },
});

export default PreRegister;
