import {combineReducers, createReducer} from '@reduxjs/toolkit';
import {getCalenderAddUpdateResponseAction,getBatchNameUpdateAction,getClickedScheduleItemAction,getCalenderScreenTypeAction,getScheduleListAction,setAuthToken, setAuthUser, setAuthUserRole} from './actions';
import {User} from '../../../common/contracts/user';

const INITIAL_AUTH_TOKEN = '';
const authToken = createReducer(INITIAL_AUTH_TOKEN, {
  [setAuthToken.type]: (_, action) => action.payload,
});

const INITIAL_AUTH_USER: User | {} = {};
const authUser = createReducer(INITIAL_AUTH_USER, {
  [setAuthUser.type]: (_, action) => action.payload,
});

const AUTH_USER_ROLE = '';
const authUserRole = createReducer(AUTH_USER_ROLE, {
  [setAuthUserRole.type]: (_, action) => action.payload,
});

//get schedule list
const GET_SCHEDULE_LIST = '';
const getScheduleListReducer = createReducer(GET_SCHEDULE_LIST, {
  [getScheduleListAction.type]: (_, action) => action.payload,
});

//get schedule list
const GET_CALENDER_SCREEN_TYPE = '';
const getCalenderScreenTypeReducer = createReducer(GET_CALENDER_SCREEN_TYPE, {
  [getCalenderScreenTypeAction.type]: (_, action) => action.payload,
});

//get schedule list
const GET_CLICKED_SCHEDULE_ITEM = '';
const getClickedScheduleItemReducer = createReducer(GET_CLICKED_SCHEDULE_ITEM, {
  [getClickedScheduleItemAction.type]: (_, action) => action.payload,
});

//get schedule list
const GET_BATCH_NAME_UPDATE = '';
const getBatchNameUpdateReducer = createReducer(GET_BATCH_NAME_UPDATE, {
  [getBatchNameUpdateAction.type]: (_, action) => action.payload,
});

//get schedule list
const GET_CALENDER_ADD_UPDATE_RESPONSE = '';
const getCalenderAddUpdateResponseReducer = createReducer(GET_CALENDER_ADD_UPDATE_RESPONSE, {
  [getCalenderAddUpdateResponseAction.type]: (_, action) => action.payload,
});

export const authReducer = combineReducers({
  authToken,
  authUser,
  authUserRole,
  getScheduleListReducer,
  getCalenderScreenTypeReducer,
  getClickedScheduleItemReducer,
  getBatchNameUpdateReducer,
  getCalenderAddUpdateResponseReducer
});
