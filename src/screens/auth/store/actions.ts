import {createAction} from '@reduxjs/toolkit';
import {Role} from '../../../common/enums/role';
import {User} from '../../../common/contracts/user';
import AsyncStorage from '@react-native-community/async-storage';

export const setAuthToken = createAction(
  'SET_AUTH_TOKEN',
  (accessToken: string) => {
    // We'll retain the access token in the local storage in case the
    // browser tab is refreshed then we'd still have the token to fetch the
    // user details of the authenticated user from it.
    AsyncStorage.setItem('accessToken', accessToken);

    return {
      payload: accessToken,
    };
  },
);

export const setAuthUser = createAction('SET_AUTH_USER', (user: User | {}) => {
  // We'll retain the authenticated user in the local storage in case the
  // browser tab is refreshed then we'd still have the details to fetch
  // the user details of the authenticated user from it.
  AsyncStorage.setItem('authUser', JSON.stringify(user));

  return {
    payload: user,
  };
});

export const setAuthUserRole = createAction(
  'SET_AUTH_USER_ROLE',
  (role: Role | '') => {
    // We'll retain the authenticated user role in the local storage in case
    // the browser tab is refreshed then we'd still have the details to
    // fetch the user details of the authenticated user from it.
    AsyncStorage.setItem('authUserRole', role);

    return {
      payload: role,
    };
  },
);

export const getScheduleListAction = createAction(
  'GET_SCHEDULE_LIST',
  (items: Object | []) => {
    //Get all calender schedule list
    return {
      payload: items,
    };
  },
);

export const getCalenderScreenTypeAction = createAction(
  'GET_CALENDER_SCREEN_TYPE',
  (items: Boolean | false) => {
    //Get all calender schedule list
    return {
      payload: items,
    };
  },
);

export const getClickedScheduleItemAction = createAction(
  'GET_CLICKED_SCHEDULE_ITEM',
  (items: Object | {}) => {
    //Get all calender schedule list
    return {
      payload: items,
    };
  },
);

export const getBatchNameUpdateAction = createAction(
  'GET_BATCH_NAME_UPDATE',
  (items: String | 'Add Schedule') => {
    //Get all calender schedule list
    return {
      payload: items,
    };
  },
);

export const getCalenderAddUpdateResponseAction = createAction(
  'GET_CALENDER_ADD_UPDATE_RESPONSE',
  (items: Boolean | false) => {
    //Get all calender schedule list
    return {
      payload: items,
    };
  },
);
