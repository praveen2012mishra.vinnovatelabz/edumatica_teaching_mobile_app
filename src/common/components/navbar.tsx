import React, { FunctionComponent, useEffect, useState } from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { Link , Route, Redirect ,withRouter, RouteComponentProps ,NativeRouter,Switch} from 'react-router-native';
import { StyleSheet,Button, Text, View, ImageBackground ,Dimensions} from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CreateScheduleOrg from '../../academics/containers/schedule_org'
import CreateScheduleTutor from '../../academics/containers/schedule_tutor'
import { RootState } from '../../../store';
import { isAdminTutor, isStudent, isOrganization, isOrgTutor, isAdmin } from '../helpers';
import { User } from '../../common/contracts/user';
import Schedules from '../../academics/containers/schedules'
import ProfileSubjectInformation from '../../dashboard/containers/profile_subject_information'
import ProfileOtherInformation from '../../dashboard/containers/profile_other_information'
import ProfilePersonalInformation from '../../dashboard/containers/profile_personal_information'




import ProfileKyc from '../../dashboard/containers/profile_kyc'
// import { useFocusEffect } from '@react-navigation/native';
import Tutors from '../../enrollment/containers/tutors'
import Students from '../../enrollment/containers/students';
import TutorsEnrollment from '../../enrollment/containers/tutors_enrollment'
import StudentsEnrollment from '../../enrollment/containers/students_enrollment';
import CreateBatchTutor from '../../academics/containers/batch_tutor'
import CreateBatchOrg from '../../academics/containers/batch_org';
import ProfileSecurity from '../../dashboard/containers/profile_security'
interface Props {
  authUser: User;
}


function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <View>
        <Text>Hello Welcome </Text>
      </View>
    </View>
  );
}

function NotificationsScreen({ navigation }) {

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Button onPress={() => navigation.goBack()} title="Go back home" />
    </View>
  );
}
// function StudentsBar({ navigation }) {
  
//     return <Redirect to={`/profile/students`} />;

// }
function Profile({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <View><Text>Hello, you are in PROFILE screen </Text></View>
    </View>
  );
}

function Tab1({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <View><Text>Hello, you are in Tab1 </Text></View>
    </View>
  );
}
function Tab2({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <View><Text>Hello, you are in Tab2 </Text></View>
    </View>
  );
}
function LogoutScreen({ navigation }) {

  return (
     <Redirect to={'/'} />
  );
}
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

// =========================================================================================================>
// Student navbar
function StudentTaberProfile (){
  return (
      <Tab.Navigator>
        <Tab.Screen 
        name="Profile" 
        component={ProfilePersonalInformation} 
        options={{
          tabBarLabel: 'Personal Info',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="information-variant" color={color} size={size} />
          ),
          // tabBarBadge: 0,
        }}
        />
        
        <Tab.Screen 
        name="Security" 
        component={ProfileSecurity} 
        options={{
          tabBarLabel: 'Security',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="security" color={color} size={size} />
          ),
        }}
        />
        
      </Tab.Navigator>
  );
}

const StudentNav: FunctionComponent<Props> = ({ authUser }) => {
  const [navigation, setNavigation] = useState({authUser});
  
  return (
    <NavigationContainer >
      <Drawer.Navigator initialRouteName="Home" >
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="Schedule Calender" component={Schedules} />
        <Drawer.Screen name="Notes" component={HomeScreen}  />
        <Drawer.Screen name="Assessments" component={HomeScreen} />
        <Drawer.Screen name="Profile" component={StudentTaberProfile} />
        <Drawer.Screen name="Logout" component={LogoutScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
//=========================================================================================================>


// =========================================================================================================>
// Organisation navbar
function OrgTaberTutor (){
  return (
      <Tab.Navigator>
        <Tab.Screen 
        name="Tutors" 
        component={Tutors} 
        options={{
          tabBarLabel: 'STUDENTS LIST',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="human-male-male" color={color} size={size} />
          ),
          tabBarBadge: 3,
        }}
        />
        <Tab.Screen 
        name="AddTutor" component={TutorsEnrollment} 
        options={{
          tabBarLabel: 'ADD STUDENTS',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account-multiple-plus" color={color} size={size} />
          ),
        }}
        />
      </Tab.Navigator>
  );
}

function OrgTaberSchedule (){
  return (
      <Tab.Navigator>
        <Tab.Screen 
        name="Schedule" 
        component={Schedules} 
        options={{
          tabBarLabel: 'Schedules',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="human-male-male" color={color} size={size} />
          ),
          tabBarBadge: 3,
        }}
        />
        <Tab.Screen 
        name="AddSchedule" component={CreateScheduleOrg} 
        options={{
          tabBarLabel: 'ADD Schedule',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account-multiple-plus" color={color} size={size} />
          ),
        }}
        />
      </Tab.Navigator>
  );
}


function OrgTaberStudent (){
  return (
      <Tab.Navigator>
        <Tab.Screen 
        name="Students" 
        component={Students} 
        options={{
          tabBarLabel: 'STUDENTS LIST',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="human-male-male" color={color} size={size} />
          ),
          tabBarBadge: 3,
        }}
        />
        <Tab.Screen 
        name="AddStudent" component={StudentsEnrollment} 
        options={{
          tabBarLabel: 'ADD STUDENTS',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account-multiple-plus" color={color} size={size} />
          ),
        }}
        />
      </Tab.Navigator>
  );
}


function OrgTaberProfile (){
  return (
      <Tab.Navigator>
        <Tab.Screen 
        name="Profile" 
        component={ProfilePersonalInformation} 
        options={{
          tabBarLabel: 'Personal Info',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="information-variant" color={color} size={size} />
          ),
          // tabBarBadge: 0,
        }}
        />
        <Tab.Screen 
        name="courseDetails" 
        component={ProfileSubjectInformation} 
        options={{
          tabBarLabel: 'Course Details',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="book-open" color={color} size={size} />
          ),
        }}
        />
        <Tab.Screen 
        name="Others" 
        component={ProfileOtherInformation} 
        options={{
          tabBarLabel: 'Business Details',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="gift" color={color} size={size} />
          ),
        }}
        />
        <Tab.Screen 
        name="Security" 
        component={ProfileSecurity} 
        options={{
          tabBarLabel: 'Security',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="security" color={color} size={size} />
          ),
        }}
        />
        <Tab.Screen 
        name="KYC" 
        component={ProfileKyc} 
        options={{
          tabBarLabel: 'KYC',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="file-document" color={color} size={size} />
          ),
        }}
        />
      </Tab.Navigator>
  );
}



const OrgNav: FunctionComponent<Props> = ({ authUser }) => {
  const [navigation, setNavigation] = useState({authUser});
  
  return (
    <NavigationContainer >
      <Drawer.Navigator initialRouteName="Home" >
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="Tutors" component={OrgTaberTutor}  />
        <Drawer.Screen name="Students" component={OrgTaberStudent}  />
        <Drawer.Screen name="Batches" component={CreateBatchOrg} />
        <Drawer.Screen name="Assessments" component={HomeScreen} />
        <Drawer.Screen name="Schedule Calender" component={OrgTaberSchedule} />
        <Drawer.Screen name="Content" component={HomeScreen} />
        <Drawer.Screen name="Profile" component={OrgTaberProfile} />
        <Drawer.Screen name="Logout" component={LogoutScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

// =========================================================================================================>



// =========================================================================================================>
//Tutor navbar
function TutorTaberStudent (){
  return (
      <Tab.Navigator>
        <Tab.Screen 
        name="Students" 
        component={Students} 
        options={{
          tabBarLabel: 'STUDENTS LIST',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="human-male-male" color={color} size={size} />
          ),
          tabBarBadge: 3,
        }}
        />
        <Tab.Screen 
        name="AddStudent" component={StudentsEnrollment} 
        options={{
          tabBarLabel: 'ADD STUDENTS',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account-multiple-plus" color={color} size={size} />
          ),
        }}
        />
      </Tab.Navigator>
  );
}

function TutorTaberSchedule (){
  return (
      <Tab.Navigator>
        <Tab.Screen 
        name="Schedule" 
        component={Schedules} 
        options={{
          tabBarLabel: 'Schedules',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="human-male-male" color={color} size={size} />
          ),
          tabBarBadge: 3,
        }}
        />
        <Tab.Screen 
        name="AddSchedule" component={CreateScheduleTutor} 
        options={{
          tabBarLabel: 'ADD Schedule',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account-multiple-plus" color={color} size={size} />
          ),
        }}
        />
      </Tab.Navigator>
  );
}


function TutorTaberProfile (){
  return (
      <Tab.Navigator>
        <Tab.Screen 
        name="Profile" 
        component={ProfilePersonalInformation} 
        options={{
          tabBarLabel: 'Personal Info',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="information-variant" color={color} size={size} />
          ),
          tabBarBadge: 3,
        }}
        />
        <Tab.Screen 
        name="courseDetails" 
        component={ProfileSubjectInformation} 
        options={{
          tabBarLabel: 'Course Details',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="book-open" color={color} size={size} />
          ),
        }}
        />
        <Tab.Screen 
        name="Others" 
        component={ProfileOtherInformation} 
        options={{
          tabBarLabel: 'Others',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="gift" color={color} size={size} />
          ),
        }}
        />
        <Tab.Screen 
        name="Security" 
        component={ProfileSecurity} 
        options={{
          tabBarLabel: 'Security',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="security" color={color} size={size} />
          ),
        }}
        />
        <Tab.Screen 
        name="KYC" 
        component={ProfileKyc} 
        options={{
          tabBarLabel: 'KYC',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="file-document" color={color} size={size} />
          ),
        }}
        />
      </Tab.Navigator>
  );
}

const TutorNav: FunctionComponent<Props> = ({ authUser }) => {
  const [navigation, setNavigation] = useState({authUser});
  
  return (
    <NavigationContainer >
      <Tab.Navigator initialRouteName="Home" >
        <Tab.Screen name="Add" component={HomeScreen} options={{
          tabBarLabel: 'Add',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="plus" color={color} size={40} />
          ),
        }}/>
        <Tab.Screen name="Schedules" component={HomeScreen}  options={{
          tabBarLabel: 'Schedules',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="calendar" color={color} size={40} />
          ),
        }}/>
        <Tab.Screen name="Home" component={TutorTaberProfile}  options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home" color={color} size={40} />
          ),
        }}/>
        <Tab.Screen name="Students" component={HomeScreen}  options={{
          tabBarLabel: 'Students',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="school-outline" color={color} size={40} />
          ),
        }}/>
        <Tab.Screen name="Notification" component={LogoutScreen}  options={{
          tabBarLabel: 'Notification',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="bell-outline" color={color} size={40} />
          ),
        }}/>
       
        {/* <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="Students" component={TutorTaberStudent}  />
        <Drawer.Screen name="Batches" component={CreateBatchTutor} />
        <Drawer.Screen name="Assessments" component={NotificationsScreen} />
        <Drawer.Screen name="Schedule Calender" component={TutorTaberSchedule} />
        <Drawer.Screen name="Content" component={NotificationsScreen} />
        <Drawer.Screen name="Profile" component={TutorTaberProfile} />
        <Drawer.Screen name="Logout" component={LogoutScreen} /> */}
      </Tab.Navigator>
    </NavigationContainer>
  );
}
// =========================================================================================================>

const Navbar: FunctionComponent<Props> = ({ authUser }) => {

  if (isOrganization(authUser)) {
    return (
      <View style={{width: Dimensions.get('screen').width*1, height: Dimensions.get('screen').height}}> 
      <OrgNav authUser={authUser}/>
      </View>
    );
  }

  if(isAdmin(authUser)){
    return (
      <View><Text>Hello guys we are in Admin tutor Dashboard </Text></View>
    );
  }

  if (isAdminTutor(authUser)) {
    return (
      <View style={{width: Dimensions.get('screen').width*1, height: Dimensions.get('screen').height}}> 
      <TutorNav authUser={authUser}/>
      </View>
    );
  }

  if (isOrgTutor(authUser)) {
    return (
      <View style={{width: Dimensions.get('screen').width*1, height: Dimensions.get('screen').height}}> 
      <TutorNav authUser={authUser}/>
      </View>
    );
  }
  
  if (isStudent(authUser)) {
    return (
      <View style={{width: Dimensions.get('screen').width*1, height: Dimensions.get('screen').height}}> 
      <StudentNav authUser={authUser}/>
      </View>
    );
  }

  return (
    <View><Text>Hello guys we are in None Dashboard</Text></View>
  );

};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
});

export default connect(mapStateToProps)(Navbar);
