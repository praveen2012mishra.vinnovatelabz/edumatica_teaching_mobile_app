import React, { FunctionComponent } from 'react';
import {
  Button as BaseButton,
  ButtonProps,
  createStyles,
  Theme,
} from '@material-ui/core';
import { withStyles, WithStyles } from '@material-ui/styles';

const getButtonColor = (
  color: string | undefined,
  variant: string | undefined,
  theme: Theme
) => {
  if (color === 'error') {
    return theme.palette.error.main;
  }

  if (variant !== 'contained') {
    return color;
  }

  switch (color) {
   
    default:
      return color;
  }
};

const styles = (theme: Theme) =>
  createStyles({
    root: {
      background: ({ color, variant }: ButtonProps) =>
        getButtonColor(color, variant, theme),
      borderRadius: '100px',
    },
  });

interface Props
  extends WithStyles<typeof styles>,
    Omit<ButtonProps, 'classes'> {
  component?: React.ReactNode;
  to?: string;
}

const Button: FunctionComponent<Props> = ({ classes, ...props }) => (
  <BaseButton {...props} className={classes.root} />
);

export default withStyles(styles)(Button);
