import React, { FunctionComponent } from 'react';
import { connect } from 'react-redux';
import { Link as RouterLink } from 'react-router-native';
import { StyleSheet, Text, TextInput, View,ImageBackground,Linking,SafeAreaView} from 'react-native';

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import {
  AppBar,
  Box,
  Link,
  Toolbar,
} from '@material-ui/core';
import { RootState } from '../../../store';
import { User } from '../contracts/user';

const styles = StyleSheet.create({
  container: {
    flexDirection:'row',
    flex: 1,
    justifyContent: 'space-around',
    marginHorizontal: 5,
    marginVertical: 5,
  },
  title: {
    textAlign: 'center',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  separator: {

    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  btn:{

     borderRadius: 8,
     padding:"20px",
     textAlign:"center"
  },
  logo:{
    width: 50,
    height: 50,
  },
  link1:{
    color: 'blue',
    fontSize: 8
  },
  link2:{
    color: '#00ced1',
    fontSize: 15,
  },
  main:{
    backgroundColor:'white',
    flexDirection:'row',
    flex: 0.3,
    justifyContent: 'space-around',
    paddingVertical: 5,
    
  }

});

interface Props {
  authUser: User;
}

const Footer: FunctionComponent<Props> = () => {

  
    return (
     <SafeAreaView style={styles.main}>
    
    <View style = {styles.container}>
            <Text style={styles.link2}
              onPress={() => Linking.openURL('http://65.0.71.62/user-guide')}>
              User Guide
            </Text>
            <Text style={styles.link2}
              onPress={() => Linking.openURL('http://65.0.71.62/legal-notice')}>
              Legal Notice
            </Text>
      
    </View>
  
     </SafeAreaView>
    );
  };
  
  const mapStateToProps = (state: RootState) => ({
    authUser: state.authReducer.authUser as User,
  });
  
  export default connect(mapStateToProps)(Footer);
  
 

  
