import React, { FunctionComponent } from 'react';
import { StyleSheet, Text, TextInput,Image, View,Linking,Button,TouchableOpacity,SafeAreaView,LogBox,Dimensions,ScrollView} from 'react-native';
import { createStyles, withStyles, WithStyles } from '@material-ui/styles';
import { Modal as BaseModal, Portal, Provider } from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';


interface Props {
  header?: React.ReactNode;
  open: boolean;
  handleClose: () => any;
}

const Modal: FunctionComponent<Props> = ({
  children,
  handleClose,
  header,
  open,
}) => {
  const [visible, setVisible] = React.useState(true);
  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  return (
    
    <Provider>
    <Portal>
      {/* <BaseModal visible={visible} onDismiss={hideModal}>
        <Text>Example Modal.  Click outside this area to dismiss.</Text>
      </BaseModal> */}
      <BaseModal visible={open} onDismiss={handleClose}>
      <View style={styles.container}>
        <View style={{flexDirection: 'row',backgroundColor:"skyblue",alignItems: 'center',justifyContent: 'space-between'}}>
          <View style={{flex:0.8}}>
          {header && <Text style={styles.title1} >{header}</Text>}
          </View>

          <View style={{flex:0.2}}>
            <TouchableOpacity onPress={handleClose}>
          <MaterialCommunityIcons name="close-circle" color={'grey'} size={50} />
            </TouchableOpacity>
          </View>
        </View>

        <View >
          {children}
        </View>
      </View>
    </BaseModal>
    </Portal>
  </Provider>
    
  );
};


const styles = StyleSheet.create({
  container: {

    width: "100%",
    backgroundColor:"white",
    borderWidth:1,
    borderRadius:8,
    marginBottom:"10%",
    paddingBottom:"5%",
    marginTop:"10%",
    height:"100%"
   
  },
  title1: {
    fontWeight: 'bold',
    fontSize: 24,
    textAlign: 'center',
    marginVertical: 8,
  },
  title2: {
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
    marginVertical: 8,
  },

});
export default Modal;
