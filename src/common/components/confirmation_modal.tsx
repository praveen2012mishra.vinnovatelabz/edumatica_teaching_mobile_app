import React, { FunctionComponent } from 'react';
import { StyleSheet, Text, TextInput,Image, View,Linking,Button,TouchableOpacity,SafeAreaView,LogBox,Dimensions,ScrollView} from 'react-native';
import Modal from './modal';

interface Props {
  header: string;
  helperText: string;
  openModal: boolean;
  onClose: () => any;
  handleDelete: () => any;
}

const ConfirmationModal: FunctionComponent<Props> = ({
  header,
  helperText,
  openModal,
  onClose,
  handleDelete
}) => {
  return (
    <Modal
      open={openModal}
      handleClose={onClose}
      header={
        <View >
            <Text style={styles.title1}>
              {header}
            </Text>
          
        </View>
      }
    >
      <View>
        <View>
          <Text style={styles.title2}>{helperText}</Text>
        </View>
        <View>
          <View style={{ flexDirection: 'row'}}>
            <TouchableOpacity onPress={onClose} style={{ flex:0.5}}>
              <Text style={styles.title1}>
              Cancel
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={handleDelete} style={{ flex:0.5}}>
              <Text style={styles.title1}>
              Ok
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('screen').width,
    backgroundColor:"#C1E3F2",
    marginTop:10,
    borderWidth:1,
    borderRadius:8
  },
  title1: {
    fontWeight: 'bold',
    fontSize: 24,
    textAlign: 'center',
    marginVertical: 8,
  },
  title2: {
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
    marginVertical: 8,
  },

});
export default ConfirmationModal;
