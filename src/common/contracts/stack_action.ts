import { StackActionType } from '../enums/stack_action_type';

interface StackActionPayload<AD, AT> {
  type: AD;
  data: AT;
}

export interface StackActionItem<AD = any, AT = any> {
  type: StackActionType;
  payload: StackActionPayload<AD, AT>;
}
