import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {
  GET_KYC_DOCUMENT_UPLOAD_URL,
  UPDATE_KYC_DOCUMENT_URL,
  UPLOAD_ZIP,
} from './routes';

interface Document {
  fileName: string;
  contentType: string;
  contentLength: number;
}

interface UploadUrlForKycDocumentResponse {
  url: string;
  uuid: string;
}

export const fetchUploadUrlForKycDocument = async (document: Document) => {
  const response = await axios.get<UploadUrlForKycDocumentResponse>(
    GET_KYC_DOCUMENT_UPLOAD_URL,
    {params: document},
  );

  return response.data;
};

export const uploadKycDocument = async (url: string, data: FormData) => {
  const response = await axios.put(url, data);

  return response.data;
};

export const updateKycDocument = async (
  data: {
    kycDocType: string;
    kycDocFormat: string;
    kycDocLocation: string;
  }[],
) => {
  const response = await axios.post(UPDATE_KYC_DOCUMENT_URL, data);

  return response.data;
};

export const uploadAadhaarZip = async (data: FormData) => {
  try {
    console.log(data);
    const response = await axios.post(UPLOAD_ZIP, data);
    console.log('This is response we are getting', response.data);
    return response.data;
  } catch (error) {
    console.log(
      'this is error in uploading ================++>>>>>>>>>>>>>>>>>>>>>>>>',
      error,
    );
  }
};
// {
//   headers: {'Content-Type': 'multipart/form-data'},
// }
// export const uploadAadhaarZip = async (fetchBodydata) => {
//   const authToken = await AsyncStorage.getItem('accessToken');
//   const myUrl = `${UPLOAD_ZIP}/`;

//   console.log('FetchBody--------->', fetchBodydata);
//   console.log('FetchURL---------->', myUrl);
//   console.log('token---------->', authToken);
//   let headers = null;
//   let fetchData = null;

//   headers = {
//     Accept: 'application/json',
//     'Content-Type': 'multipart/form-data',
//     Authorization: `Bearer ${authToken}`,
//   };
//   fetchData = fetchBodydata;

//   const fetchCall = await fetch(myUrl, {
//     method: 'POST',
//     headers: headers,
//     body: fetchData,
//   });
//   console.log('Header---------->', fetchCall);
//   const responseData = await fetchCall.json();
//   return responseData;
// };
