import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';



import {
  GET_TUTOR_SUBJECTS,
  GET_TUTOR_STUDENTS,
  GET_STUDENT,
  GET_TUTOR,
  GET_PARENT,
  UPDATE_TUTOR_STUDENTS,
} from './routes';
import { Student, TutorResponse } from '../../common/contracts/user';
// import { Course } from '../../academics/contracts/course';

export const getCoursesOfTutor = async () => {
  // const response = await axios.get<Course[]>(GET_TUTOR_SUBJECTS);

  // return response.data;
};

export const updateStudentsOfTutor = async (
  students: Student[]
) => {
  const response = await axios.post<Student[]>(
    UPDATE_TUTOR_STUDENTS,
    { studentList: students }
  );

  return response.data;
};

export const getStudentsOfTutor = async () => {
  const response = await axios.get<Student[]>(GET_TUTOR_STUDENTS);

  return response.data;
};

export const getStudent = async () => {
  const response = await axios.get(GET_STUDENT);

  return response.data.student;
};

export const getParent = async () => {
  const response = await axios.get(GET_PARENT);

  return response.data.parent;
};

export const getTutor = async () => {
  const response = await axios.get(GET_TUTOR);
  return response.data.tutor;
};






export const getFetch = async (fetchURL) => {
  // console.log("URL---------->", fetchURL);
  const authToken = await AsyncStorage.getItem('accessToken');
  // console.log("------------we are in tutor component---------------",authToken)
  let headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authToken}`
  };

  const fetchCall = await fetch(fetchURL, {
      method: "GET",
      headers: headers,
  })

  console.log("Header---------->", fetchCall);
  const responseData = await fetchCall.json();
  return responseData;
}

