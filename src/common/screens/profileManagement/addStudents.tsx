import React, { FunctionComponent, useState, useEffect } from 'react'
import { StyleSheet, Text, View, SafeAreaView } from 'react-native'

//components
import BaseHeader from '../../../components/Header/BaseHeader';
import CustomStatusBar from '../../../components/Header/custom_status_bar';

import ProfileTopTab from './profileTopTab';


//utilities
import { width, Colors, height, images, strings } from '../../../utility/utilities';
import HeightWidth from '../../../utility/HeightWidth';
import { getOrgStudentsList } from '../../../common/api/organization';

interface Props {
    route: any,
    navigation: any
}

const AddStudents: FunctionComponent<Props> = ({ route, navigation }) => {
    return (
        <SafeAreaView style={styles.__container}>
            <CustomStatusBar
                backgroundColor={Colors.primaryBlue}
                barStyle="light-content"
            />
            <BaseHeader
                onPress={() => {
                    navigation.goBack();
                }}
                title={'EDUMATICA'}
            />
            <ProfileTopTab />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    __container: {
        flex: 1
    },
})

export default AddStudents;