import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'

// Screens
import EnterStudentDetails from './enterStudentDetails';
import BulkUpload from './bulkUpload';

const ProfileTopTabs = createMaterialTopTabNavigator();

export default function ProfileTopTab() {
    return (
        <ProfileTopTabs.Navigator>
            <ProfileTopTabs.Screen name="Enter Details" component={EnterStudentDetails} initialParams={{ formType: "add" }} />
            <ProfileTopTabs.Screen name="Bulk Upload" component={BulkUpload} />
        </ProfileTopTabs.Navigator>
    )
}

const styles = StyleSheet.create({})
