import React, {
  FunctionComponent,
  useState,
  useEffect,
  useCallback,
} from 'react';

import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  ToastAndroid,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

//components
import BaseHeader from '../../../components/Header/BaseHeader';
import CustomStatusBar from '../../../components/Header/custom_status_bar';

//utilities
import {
  width,
  Colors,
  height,
  images,
  strings,
} from '../../../utility/utilities';
import HeightWidth from '../../../utility/HeightWidth';
import {
  getOrgStudentsList,
  deleteStudentsOfOrganization,
} from '../../api/organization';
import {deleteStudents} from '../../api/profile';
import AsyncStorage from '@react-native-community/async-storage';
import {Role} from '../../enums/role';
import {getStudentsOfTutor} from '../../api/tutor';

interface Props {
  navigation: any;
  route: any;
}

interface StudentData {
  item: {
    __v: number;
    _id: string;
    batches: string;
    boardName: string;
    cityName: string;
    className: string;
    mobileNo: string;
    ownerId: string;
    pinCode: string;
    schoolName: string;
    stateName: string;
    status: number;
    studentName: string;
    updatedby: string;
    updatedon: string;
  };
}

const StudentManagement: FunctionComponent<Props> = ({navigation, route}) => {
  const [studentList, setStudentList] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getStudentList();
    });
    return unsubscribe;
  }, []);

  const getStudentList = async () => {
    let role = await AsyncStorage.getItem('userRole');
    setLoading(true);
    let studList = [];
    try {
      if (role === Role.ORGANIZATION) {
        studList = await getOrgStudentsList();
        console.log('Student data-----------', studList);
      } else {
        studList = await getStudentsOfTutor();
        // let zxc = JSON.stringify(studList)
        console.log('Student data-----------', JSON.stringify(studList));
      }
      setStudentList(studList);
      setLoading(false);
    } catch (error) {
      ToastAndroid.show('Error Fetching Data', ToastAndroid.LONG);
    }
  };

  const deleteStudent = async (deleteId: string) => {
    let role = await AsyncStorage.getItem('userRole');
    try {
      if (role === Role.ORGANIZATION) {
        await deleteStudentsOfOrganization([deleteId]);
      } else {
        await deleteStudents([deleteId]);
      }
      getStudentList();
    } catch (error) {
      if (error.response?.status === 401) {
        ToastAndroid.show('Login Again', ToastAndroid.LONG);
      }
      if (error.response?.status === 422) {
        ToastAndroid.show('Error invalid input', ToastAndroid.LONG);
      }
    }
  };

  const renderItem = ({item}: StudentData) => {
    return (
      <View key={item._id} style={styles.__cardContainer}>
        <View style={styles.__leftCardContent}>
          <Text style={styles.__cardTitle}>{item.studentName}</Text>
          <View style={styles.__cardTopContent}>
            <Text style={styles.__cardTopTitle}>School Name:</Text>
            <Text numberOfLines={1} style={styles.__cardTopData}>
              {item.schoolName}
            </Text>
          </View>
          <View style={styles.__cardMiddleContent}>
            <View style={styles.__cardMiddleLeftContent}>
              <Text style={styles.__cardMiddleLeftTitle}>Class:</Text>
              <Text style={styles.__cardMiddleLeftData}>{item.className}</Text>
            </View>
            <View style={styles.__cardMiddleRightContent}>
              <Text style={styles.__cardMiddleRightTitle}>Board:</Text>
              <Text numberOfLines={1} style={styles.__cardMiddleRightData}>
                {item.boardName}
              </Text>
            </View>
          </View>
          <View style={styles.__cardBottomContent}>
            <View style={styles.__leftCardBottomContent}>
              <Text style={styles.__cardBottomContentTitle}>PhoneNo:</Text>
              <Text style={styles.__cardBottomContentData}>
                {item.mobileNo}
              </Text>
            </View>
            <View style={styles.__rightBottomCardContent}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Enter Details', {
                    formType: 'edit',
                    currentStudent: item,
                  });
                }}
                style={[
                  styles.__rightBottomCardContentButton,
                  {backgroundColor: Colors.primaryBlueOpacity},
                ]}>
                <Icon name="edit-2" size={16} color={Colors.fullWhite} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  Alert.alert(
                    'Delete',
                    'Are you sure you want to Delete a student?',
                    [
                      {
                        text: 'OK',
                        onPress: () => {
                          deleteStudent(item.mobileNo);
                        },
                      },
                      {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                      },
                    ],
                    {cancelable: true},
                  );
                }}
                style={[
                  styles.__rightBottomCardContentButton,
                  {backgroundColor: Colors.verySoftRed},
                ]}>
                <Icon name="trash" size={16} color={Colors.fullWhite} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.__container}>
      <CustomStatusBar
        backgroundColor={Colors.primaryBlue}
        barStyle="light-content"
      />
      <BaseHeader
        onPress={() => {
          navigation.goBack();
        }}
        title={'EDUMATICA'}
      />
      {!loading ? (
        <View style={styles.__container}>
          {studentList.length != 0 ? (
            <FlatList
              keyExtractor={(_: any, index: number) => index.toString()}
              renderItem={renderItem}
              data={studentList}
              style={styles.__flatList}
            />
          ) : (
            <Text style={styles.__noContentText}>No Data Present</Text>
          )}
        </View>
      ) : (
        <View style={styles.__activityIndicator}>
          <ActivityIndicator size={'large'} color={Colors.primaryBlue} />
        </View>
      )}
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('AddStudents');
        }}
        style={styles.__bottomAddButton}>
        <Icon name={'plus'} size={35} color={Colors.fullWhite} />
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  __container: {
    flex: 1,
  },
  __cardContainer: {
    marginHorizontal: HeightWidth.getResWidth(10),
    borderRadius: 10,
    marginVertical: HeightWidth.getResWidth(4),
    padding: HeightWidth.getResWidth(6),
    elevation: 4,
    backgroundColor: Colors.fullWhite,
    flexDirection: 'row',
  },
  __leftCardContent: {
    flex: 1,
  },
  __rightBottomCardContent: {
    paddingHorizontal: HeightWidth.getResWidth(5),
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    marginLeft: HeightWidth.getResWidth(10),
  },
  __cardTitle: {
    fontSize: 18,
    color: Colors.primaryBlue,
  },
  __cardTopContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  __cardTopTitle: {
    fontSize: 16,
    color: Colors.primaryBlue,
  },
  __cardTopData: {
    fontSize: 16,
    color: Colors.veryGrayishBlack,
    marginLeft: HeightWidth.getResWidth(10),
  },
  __cardMiddleContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: HeightWidth.getResWidth(4),
  },
  __cardMiddleLeftContent: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  __cardMiddleLeftTitle: {
    fontSize: 16,
    color: Colors.veryGrayishBlack,
  },
  __cardMiddleLeftData: {
    fontSize: 16,
    color: Colors.veryGrayishBlack,
    marginLeft: HeightWidth.getResWidth(10),
  },
  __cardMiddleRightContent: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  __cardMiddleRightTitle: {
    fontSize: 16,
    color: Colors.veryGrayishBlack,
  },
  __cardMiddleRightData: {
    fontSize: 16,
    color: Colors.veryGrayishBlack,
    marginLeft: HeightWidth.getResWidth(10),
  },
  __cardBottomContent: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: HeightWidth.getResWidth(4),
  },
  __leftCardBottomContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  __cardBottomContentTitle: {
    fontSize: 16,
    color: Colors.veryGrayishBlack,
  },
  __cardBottomContentData: {
    fontSize: 16,
    color: Colors.veryGrayishBlack,
    marginLeft: HeightWidth.getResWidth(10),
  },
  __bottomAddButton: {
    bottom: 0,
    right: HeightWidth.getResWidth(15),
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    backgroundColor: Colors.primaryBlue,
    elevation: 10,
    marginBottom: HeightWidth.getResWidth(15),
    width: HeightWidth.getResWidth(50),
    height: HeightWidth.getResWidth(50),
    borderRadius: 30,
  },
  __activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  __rightBottomCardContentButton: {
    borderRadius: 30,
    width: HeightWidth.getResWidth(30),
    height: HeightWidth.getResWidth(30),
    marginHorizontal: HeightWidth.getResWidth(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  __noContentText: {
    fontSize: 20,
    color: Colors.veryGrayishBlack,
    alignSelf: 'center',
    marginVertical: HeightWidth.getResWidth(40),
  },
  __flatList: {
    marginBottom: HeightWidth.getResWidth(15),
  },
});

export default StudentManagement;
