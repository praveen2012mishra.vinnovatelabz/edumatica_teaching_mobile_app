import React, { FunctionComponent, useState } from 'react'
import { StyleSheet, Text, View, SafeAreaView, ToastAndroid, TouchableOpacity, PermissionsAndroid, ActivityIndicator } from 'react-native'
import XLSX from 'xlsx';
import Icon from 'react-native-vector-icons/Feather';
import RNFS from 'react-native-fs';
import DocumentPicker from 'react-native-document-picker';
import AsyncStorage from '@react-native-community/async-storage';
import RNFetchBlob from 'rn-fetch-blob'


//utilities
import { fetchCitiesByPinCode } from '../../api/academics';
import { Colors, } from '../../../utility/utilities';
import HeightWidth from '../../../utility/HeightWidth';
import { addStudentsOfOrganization } from '../../api/organization';
import { Student } from '../../contracts/user';
import { EMAIL_PATTERN, PHONE_PATTERN, PIN_PATTERN } from '../../validations/patterns';
import { Role } from '../../enums/role';
import { updateStudentsOfTutor } from '../../api/tutor';
import { GET_STUDENT_ENROLLMENT_TEMPLATES } from '../../api/routes';

interface Props {
    route: any,
    navigation: any
}

interface FileProps {
    uri: string;
    type: string;
    name: string;
    size: number;
}

const BulkUpload: FunctionComponent<Props> = ({ route, navigation }) => {

    const [students, setStudents] = useState<Student[]>([]);
    const [fileName, setFileName] = useState('');
    const [loading, setLoading] = useState(false);

    const downloadTemplate = async () => {
        let dirs = RNFetchBlob.fs.dirs;
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
            title: "Storage",
            message: "This app would like to store some files on your phone",
            buttonPositive: "ok"
        }).then(async () => {
            setLoading(true);
            await RNFetchBlob
                .config({
                    fileCache: true,
                    addAndroidDownloads: {
                        notification: true,
                        useDownloadManager: true,
                        title: 'students.xlsx',
                        description: 'Student Enrollment Template',
                        mime: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        mediaScannable: true,
                        path: dirs.DownloadDir + '/students.xlsx',
                    }
                })
                .fetch('GET', GET_STUDENT_ENROLLMENT_TEMPLATES)
                .then((res) => {
                    console.log('The file saved to ', res.path())
                    setLoading(false)
                    ToastAndroid.show("Template Downloaded successfully", ToastAndroid.LONG);
                }).catch((error) => {
                    ToastAndroid.show("Error Downloading" + error, ToastAndroid.LONG);
                    setLoading(false)
                    console.log("error" + error)
                })
        })
    }

    const uploadFile = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.xlsx],
            });
            console.log(
                res.uri,
                res.type,
                res.name,
                res.size
            );
            await readExcel(res);
            setFileName(res.name)
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                ToastAndroid.show("Cancelled Picking Document", ToastAndroid.LONG)
            } else {
                throw err;
            }
        }
    }

    const isUniqueMobile = (mobile: string[]) => {
        let mobileNo,
            unique = true;
        mobile.sort().sort((a, b) => {
            if (a === b) {
                mobileNo = a;
                unique = false;
            }
            return 0;
        });
        return { mobileNo: mobileNo, isUnique: unique };
    };

    const validateStudents = (structuredStudents: Student[]) => {
        let uploadError = '';

        const studentMobile = isUniqueMobile(
            structuredStudents.map((student) => student.mobileNo)
        );
        const parentMobile = isUniqueMobile(
            structuredStudents.map((student) => student.parentMobileNo)
        );
        const uniqueMobile = isUniqueMobile(
            structuredStudents.map((student) => {
                if (student.mobileNo === student.parentMobileNo) {
                    return student.mobileNo
                }
            })
        );
        if (!studentMobile.isUnique) {
            uploadError = `Student mobile number "${studentMobile.mobileNo}" must be unique`;
        } else if (!parentMobile.isUnique) {
            uploadError = `Parent mobile number "${parentMobile.mobileNo}" must be unique`;
        } else if (!uniqueMobile.isUnique) {
            uploadError = `Parent mobile number "${uniqueMobile.mobileNo}" cannot be same as student mobile number`;
        }

        structuredStudents &&
            structuredStudents.every((student) => {
                if (uploadError === '') {
                    if (!student.studentName) {
                        uploadError = `Student name should not be empty`;
                        return false;
                    }
                    if (student.studentName == null || student.studentName.length < 5) {
                        uploadError = `Student name "${student.studentName}" cannot be less than 5 character`;
                        return false;
                    }
                    if (!student.mobileNo) {
                        uploadError = `Student mobile number should not be empty`;
                        return false;
                    }
                    if (
                        student.mobileNo == null ||
                        !student.mobileNo.toString().match(PHONE_PATTERN)
                    ) {
                        uploadError = `Student Mobile number "${student.mobileNo}" must be 10 digit number`;
                        return false;
                    }
                    if (
                        student.parentMobileNo &&
                        !student.parentMobileNo.toString().match(PHONE_PATTERN)
                    ) {
                        uploadError = `Parent mobile number "${student.parentMobileNo}" must be 10 digit number`;
                        return false;
                    }
                    if (student.parentMobileNo === student.mobileNo) {
                        uploadError = `Parent mobile number "${student.parentMobileNo}" cannot be same as student mobile number`;
                        return false;
                    }
                    if (!student.emailId) {
                        uploadError = 'Email cannot be empty';
                        return false;
                    }

                    if (!EMAIL_PATTERN.test(student.emailId.toLowerCase())) {
                        uploadError = 'Invalid Email';
                        return false;
                    }

                    if (!student.pinCode) {
                        uploadError = 'Pin Code cannot be empty';
                        return false;
                    }
                    if (!PIN_PATTERN.test(student.pinCode)) {
                        uploadError = 'Invalid pin code';
                        return false;
                    }

                    if (!student.cityName) {
                        uploadError = 'City cannot be empty';
                        return false;
                    }

                    if (!student.stateName) {
                        uploadError = 'State cannot be empty';
                        return false;
                    }

                    if (!student.schoolName) {
                        uploadError = 'School cannot be empty';
                        return false;
                    }

                    if (!student.boardName) {
                        uploadError = 'Board cannot be empty';
                        return false;
                    }

                    if (!student.className) {
                        uploadError = 'Class cannot be empty';
                        return false;
                    }

                    const filteredStudents = students.filter(function (el) {
                        return el.mobileNo === student.mobileNo;
                    });

                    if (filteredStudents.length > 0) {
                        uploadError = `Mobile number "${student.mobileNo}" already used`;
                        return false;
                    }
                }
                return true;
            });

        if (uploadError === '') {
            setStudents([...students, ...structuredStudents]);
        } else {
            setFileName('')
            ToastAndroid.show(uploadError, ToastAndroid.SHORT);
        }
    };

    const readExcel = (file: FileProps | null) => {
        RNFS.readFile(file.uri, 'ascii').then(async (res) => {
            const workbook = XLSX.read(res, { type: 'binary' });
            const wsname = workbook.SheetNames[0];
            const ws = workbook.Sheets[wsname];
            const studentsArr = XLSX.utils.sheet_to_json(ws);

            let structuredStudents: Student[] = await Promise.all(studentsArr.map(async (student: any) => {
                let citiesArr = await fetchCitiesByPinCode({ pinCode: student.Pin_Code });
                let studentObj = {
                    boardName: student.Board,
                    className: student.Class,
                    mobileNo: student.Student_Mobile,
                    parentMobileNo: student.Parent_Mobile,
                    emailId: student.Email,
                    schoolName: student.Schools,
                    studentName: student.Student_Name,
                    pinCode: student.Pin_Code,
                    cityName: citiesArr && citiesArr[0].cityName ? citiesArr[0].cityName : '',
                    stateName:
                        citiesArr && citiesArr[0].stateName
                            ? citiesArr[0].stateName
                            : '',
                    enrollmentId: student.Enrollment_Id
                }
                return studentObj;
            }))
            validateStudents(structuredStudents);
        });
    };

    const submitStudentList = async () => {
        let role = await AsyncStorage.getItem('userRole')
        setLoading(true)
        try {
            if (role === Role.ORGANIZATION) {
                await addStudentsOfOrganization(students);
            } else {
                await updateStudentsOfTutor(students);
            }
            setLoading(false)
            navigation.navigate('StudentManagement')
        } catch (error) {
            console.log("Error" + error)
            setLoading(false)
            if (error.response?.data.message) {
                console.log("error", error.response?.data.message)
                ToastAndroid.show('Error Fetching Data', ToastAndroid.LONG)
            }
        }
    }

    return (
        <SafeAreaView style={styles.__container}>
            <TouchableOpacity onPress={() => { downloadTemplate() }} style={styles.__templateButton}>
                <Text style={styles.__templateText}>Download Template</Text>
            </TouchableOpacity>
            <View style={styles.__innerContent}>
                <Text style={styles.__innerContentTitle}>Upload Students List</Text>
                <Text style={styles.__innerContentNotice}>Note: Upload file as per the given format (Template)</Text>
                <TouchableOpacity
                    onPress={() => { fileName != '' && students.length != 0 ? submitStudentList() : uploadFile() }} style={[styles.__uploadButton,
                    { backgroundColor: fileName === '' ? Colors.primaryBlue : Colors.verySoftRed }]}>
                    {!loading ? <>
                        {fileName === '' ?
                            <Icon name="upload" size={26} color={Colors.fullWhite} />
                            : null}
                    </> :
                        <ActivityIndicator size="small" color={Colors.fullWhite} />
                    }
                    <Text style={styles.__uploadText}>{fileName === '' ? "Upload" : "Submit"}</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    __container: {
        flex: 1
    },
    __templateButton: {
        borderRadius: 40,
        paddingHorizontal: HeightWidth.getResWidth(20),
        paddingVertical: HeightWidth.getResWidth(10),
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        backgroundColor: Colors.primaryBlue,
        marginVertical: HeightWidth.getResWidth(30),
    },
    __templateText: {
        fontSize: 17,
        color: Colors.fullWhite,
    },
    __uploadButton: {
        borderRadius: 20,
        padding: HeightWidth.getResWidth(30),
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        marginVertical: HeightWidth.getResWidth(50),
        backgroundColor: Colors.primaryBlue,
    },
    __uploadText: {
        fontSize: 17,
        color: Colors.fullWhite,
    },
    __innerContent: {
        padding: HeightWidth.getResWidth(15),
        borderRadius: 10,
        marginVertical: HeightWidth.getResWidth(15),
        marginHorizontal: HeightWidth.getResWidth(10),
        backgroundColor: Colors.fullWhite
    },
    __innerContentTitle: {
        alignSelf: "center",
        fontSize: 18,
        color: Colors.primaryBlue,
        marginVertical: HeightWidth.getResWidth(10)
    },
    __innerContentNotice: {
        alignSelf: "center",
        textAlign: "center",
        fontSize: 14,
        color: Colors.primaryRed
    },
})

export default BulkUpload;
