import React, { FunctionComponent, useState, useEffect } from 'react'
import { StyleSheet, Text, View, SafeAreaView, ToastAndroid, ActivityIndicator } from 'react-native'

//components
import TextInputUI from '../../../components/UI/TextInput';
import BaseHeader from '../../../components/Header/BaseHeader';
import CustomStatusBar from '../../../components/Header/custom_status_bar';
import CustomScrollView from '../../../components/Other/CustomScrollView';
import Picker from '../../../components/UI/Picker';


//utilities
import { width, Colors } from '../../../utility/utilities';
import HeightWidth from '../../../utility/HeightWidth';
import { fetchBoardsList, fetchCitiesByPinCode, fetchCitySchoolsList, fetchClassesList } from '../../api/academics';
import ButtonUI from '../../../components/UI/Button';
import { addStudentsOfOrganization } from '../../api/organization';
import { PHONE_PATTERN, EMAIL_PATTERN, PIN_PATTERN } from '../../validations/patterns';
import AsyncStorage from '@react-native-community/async-storage';
import { Role } from '../../enums/role';
import { updateStudentsOfTutor } from '../../api/tutor';


interface Props {
    route: any,
    navigation: any
}

const EnterStudentDetails: FunctionComponent<Props> = ({ route, navigation }) => {
    let { formType } = route.params;
    const [studentName, setStudentName] = useState('');
    const [enrollmentId, setEnrollmentId] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [parentMobileNo, setParentMobileNo] = useState('');
    const [emailId, setEmailId] = useState('');
    const [pinCode, setPinCode] = useState('');
    const [cityName, setCityName] = useState('');
    const [stateName, setStateName] = useState('');
    const [schoolName, setSchoolName] = useState('');
    const [citySchoolList, setCitySchoolList] = useState([]);
    const [boardName, setBoardName] = useState('');
    const [boardList, setBoardList] = useState([]);
    const [className, setStudentClassName] = useState('');
    const [studentClassList, setStudentClassList] = useState([]);
    const [showInput, setShowInput] = useState(false);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const unsubscribe = navigation.addListener('blur', () => {
            setStudentName('')
            setEnrollmentId('')
            setMobileNo('')
            setParentMobileNo('')
            setEmailId('')
            setPinCode('')
            setCityName('')
            setStateName('')
            setSchoolName('')
            setCitySchoolList([])
            setBoardName('')
            setBoardList([])
            setStudentClassName('')
            setStudentClassList([])
        });
        return unsubscribe;
    }, [navigation]);

    if (formType === 'add') {
        useEffect(() => {
            getBoard()
        }, [])
    }

    if (formType === 'edit') {
        let { currentStudent } = route.params;
        useEffect(() => {
            setLoading(true);
            (async () => {
                try {
                    await getBoard()
                    await getStudentClass(currentStudent.boardName);
                    await getCitiesFromPINCode(currentStudent.pinCode)
                    await getSchools(currentStudent.cityName)
                    Promise.all([
                        setStudentName(currentStudent.studentName || ''),
                        setEnrollmentId(currentStudent.enrollmentId || ''),
                        setMobileNo(currentStudent.mobileNo || ''),
                        setSchoolName(currentStudent.schoolName || ''),
                        setEmailId(currentStudent.emailId || ''),
                        setPinCode(currentStudent.pinCode || ''),
                        setCityName(currentStudent.cityName || ''),
                        setStateName(currentStudent.stateName || ''),
                        setBoardName(currentStudent.boardName || ''),
                        setStudentClassName(currentStudent.className || ''),
                    ])
                    setLoading(false);

                } catch (error) {
                    if (error.response?.status === 401) {
                        ToastAndroid.show("Login Again", ToastAndroid.LONG);
                        setLoading(false);
                    }
                }
            })();
        }, [currentStudent]);
    }

    const getCitiesFromPINCode = async (pinCode: string) => {
        try {
            const getStateCity = await fetchCitiesByPinCode({ pinCode });
            setCityName(getStateCity[0].cityName);
            setStateName(getStateCity[0].stateName);
            getSchools(getStateCity[0].cityName)
        } catch (error) {
            ToastAndroid.show('Error Fetching Data', ToastAndroid.LONG)

        }
    }

    const getSchools = async (city: string) => {
        try {
            const [getCitySchoolList] = await Promise.all([fetchCitySchoolsList({ cityName: city })]);
            const structuredSchoolsList = getCitySchoolList.map((school) => ({
                label: `${school.schoolName} (${school.schoolAddress})`,
                value: school.schoolName,
            }));
            const schoolListData = [
                { label: 'other', value: 'other' },
                ...structuredSchoolsList
            ]
            if (structuredSchoolsList.length === 0) {
                setShowInput(true)
            } else {
                setCitySchoolList(schoolListData)
                setShowInput(false)
            }
        } catch (error) {
            ToastAndroid.show('Error Fetching Data', ToastAndroid.LONG)

        }
    }

    const getBoard = async () => {
        try {
            const [getBoardList] = await Promise.all([fetchBoardsList()]);
            const structuredBoardList = getBoardList.map((board) => ({
                label: `${board.boardName} (${board.boardDescriptions})`,
                value: board.boardName,
            }));
            setBoardList(structuredBoardList)
        } catch (error) {
            if (error.response?.data.message) {
                ToastAndroid.show('Error Fetching Data', ToastAndroid.LONG)
            }
        }
    }

    const getStudentClass = async (boardName: string) => {
        try {
            const [getStudentClass] = await Promise.all([fetchClassesList({ boardname: boardName })]);
            const structuredClassList = getStudentClass.map((classData) => ({
                label: classData.className,
                value: classData.className,
            }));
            setStudentClassList(structuredClassList)
        } catch (error) {
            if (error.response?.data.message) {
                ToastAndroid.show(error.response?.data.message, ToastAndroid.LONG)
            }
        }
    }

    const postStudentDetails = async () => {
        let role = await AsyncStorage.getItem('userRole')
        if (studentName === '') {
            ToastAndroid.show("Please Enter studentName", ToastAndroid.LONG)
            return;
        } else if (studentName.length < 5) {
            ToastAndroid.show("Student name cannot be less than 5 character", ToastAndroid.LONG)
            return;
        }
        if (!PHONE_PATTERN.test(mobileNo)) {
            ToastAndroid.show("Please Enter Student Mobile No", ToastAndroid.LONG)
            return;
        } else if (mobileNo.length == 0) {
            ToastAndroid.show('mobile Number field cannot be empty', ToastAndroid.LONG)
            return;
        }
        if (!PHONE_PATTERN.test(parentMobileNo)) {
            ToastAndroid.show("Please Enter Parent Mobile No", ToastAndroid.LONG)
            return;
        } else if (parentMobileNo.length == 0) {
            ToastAndroid.show('mobile Number field cannot be empty', ToastAndroid.LONG)
            return;
        }
        if (!EMAIL_PATTERN.test(emailId.trim())) {
            ToastAndroid.show("Please Enter EmailId", ToastAndroid.LONG)
            return;
        } else if (emailId.length === 0) {
            ToastAndroid.show('Please Enter EmailId', ToastAndroid.LONG)
            return;
        }
        if (boardName === '') {
            ToastAndroid.show("Board is not selected", ToastAndroid.LONG)
            return;
        }
        if (className === '') {
            ToastAndroid.show("Class is not selected", ToastAndroid.LONG)
            return;
        }
        if (!PIN_PATTERN.test(pinCode)) {
            ToastAndroid.show("PIN Code is Invalid", ToastAndroid.LONG)
            return;
        } else if (pinCode.length === 0) {
            ToastAndroid.show("Please Enter PIN Code", ToastAndroid.LONG)
            return;
        }
        if (stateName === '') {
            ToastAndroid.show("State is not selected", ToastAndroid.LONG)
            return;
        }
        if (cityName === '') {
            ToastAndroid.show("City is not selected", ToastAndroid.LONG)
            return;
        }
        if (schoolName === '') {
            ToastAndroid.show("School is not selected", ToastAndroid.LONG)
            return;
        }

        let studentDetails = {
            studentName,
            mobileNo,
            parentMobileNo,
            emailId,
            boardName,
            className,
            pinCode,
            stateName,
            cityName,
            schoolName
        };

        let structuredStudentDetails = []

        if (enrollmentId === '') {
            structuredStudentDetails = [
                { ...studentDetails, }
            ]
        } else {
            structuredStudentDetails = [
                { ...studentDetails, enrollmentId }
            ]
        }

        try {
            let addStudentResponse = null;
            if (role === Role.ORGANIZATION) {
                addStudentResponse = await addStudentsOfOrganization(structuredStudentDetails);
            } else {
                addStudentResponse = await updateStudentsOfTutor(structuredStudentDetails);
            }
            navigation.navigate('StudentManagement');
        } catch (error) {
            console.log("Error" + error)
            if (error.response?.data.message) {
                console.log("error", error.response?.data.message)
                ToastAndroid.show(error.response?.data.message, ToastAndroid.LONG)
            }
        }
    }

    return (
        <SafeAreaView style={styles.__container}>
            {formType === 'edit' ? <>
                <CustomStatusBar
                    backgroundColor={Colors.primaryBlue}
                    barStyle="light-content"
                />
                <BaseHeader
                    onPress={() => {
                        navigation.goBack();
                    }}
                    title={'EDUMATICA'}
                />
            </> : null}
            {!loading ? <CustomScrollView>
                <View style={styles.__innerContent}>
                    <Text style={styles.__contentHeader}>Enroll Students</Text>
                    <View style={styles.__textInput}>
                        <TextInputUI
                            label={"Student Name:"}
                            labelStyle={styles.__inputLabelStyle}
                            width="lg"
                            style={styles.__textInput}
                            placeholder="Enter Student Name"
                            onChangeText={(text) => { setStudentName(text) }}
                            value={studentName}
                            otherStyle={styles.__inputContainerStyle}
                        />
                    </View>
                    <View style={styles.__textInput}>
                        <TextInputUI
                            label={"Enrollment ID:"}
                            labelStyle={styles.__inputLabelStyle}
                            width="lg"
                            style={styles.__textInput}
                            placeholder="Enter Enrollment ID"
                            onChangeText={(text) => { setEnrollmentId(text) }}
                            value={enrollmentId}
                            otherStyle={styles.__inputContainerStyle}
                        />
                    </View>
                    <View style={styles.__textInput}>
                        <TextInputUI
                            label={"Student Mobile No:"}
                            labelStyle={styles.__inputLabelStyle}
                            width="lg"
                            style={styles.__textInput}
                            placeholder="Enter Student Mobile No"
                            onChangeText={(text) => { setMobileNo(text) }}
                            value={mobileNo}
                            maxLength={10}
                            keyboardType={'numeric'}
                            otherStyle={styles.__inputContainerStyle}
                        />
                    </View>
                    <View style={styles.__textInput}>
                        <TextInputUI
                            label={"Parent Mobile No:"}
                            labelStyle={styles.__inputLabelStyle}
                            width="lg"
                            style={styles.__textInput}
                            placeholder="Enter Parent Mobile No"
                            onChangeText={(text) => { setParentMobileNo(text) }}
                            value={parentMobileNo}
                            maxLength={10}
                            keyboardType={'numeric'}
                            otherStyle={styles.__inputContainerStyle}
                        />
                    </View>
                    <View style={styles.__textInput}>
                        <TextInputUI
                            label={"Email Address"}
                            labelStyle={styles.__inputLabelStyle}
                            width="lg"
                            style={styles.__textInput}
                            placeholder="Enter Email Address"
                            onChangeText={(text) => { setEmailId(text.trim()) }}
                            value={emailId}
                            otherStyle={styles.__inputContainerStyle}
                        />
                    </View>
                    <View style={styles.__textInput}>
                        <TextInputUI
                            label={"PIN Code"}
                            labelStyle={styles.__inputLabelStyle}
                            width="lg"
                            style={styles.__textInput}
                            placeholder="Enter PIN Code"
                            onChangeText={(text) => {
                                setPinCode(text);
                                text.length > 5 ?
                                    getCitiesFromPINCode(text) :
                                    null
                            }}
                            value={pinCode}
                            maxLength={6}
                            keyboardType={'numeric'}
                            otherStyle={styles.__inputContainerStyle}
                        />
                    </View>
                    <View style={styles.__textContent}>
                        <View style={styles.__textLeftContent}>
                            <Text style={styles.__textHeader}>City</Text>
                            <Text style={styles.__textData}>{cityName === '' ? "None" : cityName}</Text>
                        </View>
                        <View style={styles.__divider} />
                        <View style={styles.__textRightContent}>
                            <Text style={styles.__textHeader}>State</Text>
                            <Text style={styles.__textData}>{stateName === '' ? "None" : stateName}</Text>
                        </View>
                    </View>
                    {!showInput ? < View style={styles.__dropDown}>
                        <Picker
                            label="School Name"
                            labelSize={16}
                            labelColor={Colors.lightGrayColor}
                            data={citySchoolList}
                            rightIconName=""
                            rightIconSize=""
                            rightIconColor=""
                            value={schoolName}
                            docTypeHandler={async (item) => {
                                setSchoolName(item);
                                item === 'other' ? setShowInput(true) : null;
                            }}
                            onRightIconPress=""
                            showLabel={true}
                            otherStyle={styles.__pickerContainerStyle}
                        />
                    </View> :
                        <View style={styles.__textInput}>
                            <TextInputUI
                                label={"School Name"}
                                labelStyle={styles.__inputLabelStyle}
                                width="lg"
                                style={styles.__textInput}
                                placeholder="Enter School Name"
                                onChangeText={(text) => {
                                    setSchoolName(text);
                                }}
                                value={schoolName}
                                otherStyle={styles.__inputContainerStyle}
                                rightIconSize={20}
                                rightIconColor={Colors.veryGrayishBlack}
                                rightIconName={"chevron-down"}
                                onRightIconPress={() => {
                                    setShowInput(false);
                                    getSchools(cityName)
                                }}
                            />
                        </View>}
                    <View style={styles.__dropDown}>
                        <Picker
                            label="Board"
                            labelSize={16}
                            labelColor={Colors.lightGrayColor}
                            data={boardList}
                            rightIconName=""
                            rightIconSize=""
                            rightIconColor=""
                            value={boardName}
                            docTypeHandler={async (item) => {
                                setBoardName(item)
                                formType === 'add' ? getStudentClass(item) : null
                            }}
                            onRightIconPress=""
                            showLabel={true}
                            otherStyle={styles.__pickerContainerStyle}
                        />
                    </View>
                    <View style={styles.__dropDown}>
                        <Picker
                            label="Class"
                            labelSize={16}
                            labelColor={Colors.lightGrayColor}
                            data={studentClassList}
                            rightIconName=""
                            rightIconSize=""
                            rightIconColor=""
                            value={className}
                            docTypeHandler={async (item) => {
                                setStudentClassName(item)
                            }}
                            onRightIconPress=""
                            showLabel={true}
                            otherStyle={styles.__pickerContainerStyle}
                        />
                    </View>
                    <View style={styles.__inputButton}>
                        <ButtonUI
                            title={"Submit"}
                            borderRadius={10}
                            backgroundColor={Colors.primaryBlue}
                            elevation={0}
                            textStyles={styles.__buttonTextStyle}
                            width={width / 2 - 70}
                            height={50}
                            onPress={() => { postStudentDetails() }}
                        />
                    </View>
                </View>
            </CustomScrollView> :
                <SafeAreaView style={styles.__activityIndicator}>
                    <ActivityIndicator size={"large"} color={Colors.primaryBlue} />
                </SafeAreaView>
            }
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    __container: {
        flex: 1
    },
    __innerContent: {
        marginHorizontal: HeightWidth.getResWidth(10),
        marginVertical: HeightWidth.getResWidth(15),
        borderRadius: 10,
        backgroundColor: Colors.fullWhite,
        padding: HeightWidth.getResWidth(10)
    },
    __contentHeader: {
        alignSelf: "center",
        fontWeight: "bold",
        fontSize: 18,
        color: Colors.veryDarkDesaturatedBlue
    },
    __textInput: {
        marginTop: HeightWidth.getResWidth(18),
        alignSelf: "center"
    },
    __textContent: {
        marginTop: HeightWidth.getResWidth(10),
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around",
        backgroundColor: Colors.primaryBlueOpacity,
        paddingVertical: HeightWidth.getResWidth(8),
        borderRadius: 10
    },
    __textLeftContent: {
        justifyContent: "space-around",
        alignItems: "center",
        marginHorizontal: HeightWidth.getResWidth(2),
    },
    __textRightContent: {
        justifyContent: "space-around",
        alignItems: "center",
        marginHorizontal: HeightWidth.getResWidth(2),
    },
    __textHeader: {
        fontSize: 16,
        color: Colors.veryDarkDesaturatedBlue,
        marginBottom: HeightWidth.getResWidth(8)
    },
    __textData: {
        fontSize: 17,
        color: Colors.veryDarkDesaturatedBlue
    },
    __divider: {
        marginVertical: HeightWidth.getResWidth(2),
        width: HeightWidth.getResWidth(1),
        backgroundColor: Colors.fullWhite,
        height: HeightWidth.getResWidth(28)
    },
    __dropDown: {
        marginTop: HeightWidth.getResWidth(8),
        alignSelf: "center"
    },
    __inputButton: {
        marginVertical: HeightWidth.getResWidth(20),
        alignSelf: "center",
        justifyContent: "center",
        alignItems: "center"
    },
    __activityIndicator: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1
    },
    __inputLabelStyle: {
        marginLeft: HeightWidth.getResWidth(6),
        color: Colors.veryDarkDesaturatedBlue
    },
    __inputContainerStyle: {
        borderBottomColor: Colors.primaryBlue,
        borderBottomWidth: 1,
    },
    __pickerContainerStyle: {
        marginHorizontal: HeightWidth.getResWidth(4)
    },
    __buttonTextStyle: {
        fontSize: 16,
        color: Colors.fullWhite
    }
})

export default EnterStudentDetails;
