import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Platform,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

//utilities
import {
  width,
  postFetch,
  Colors,
  images,
  getFetch,
} from '../../../../utility/utilities';
import HeightWidth from '../../../../utility/HeightWidth';
import {
  UploadPrescription,
  UploadReport,
  UserReportList,
} from '../../../../utility/Api';

//Components
import CustomStatusBar from '../../../../components/Header/CustomStatusBar';
import TextInputUI from '../../../../components/UI/TextInput';
import BasicHeader from '../../../../components/Header/BasicHeader';
import ButtonUI from '../../../../components/UI/Button';
import GlobalHeader from '../../../../components/Header/GlobalHeader';
import CustomModel from '../../../../components/Other/CustomModal';
import AsyncStorage from '@react-native-community/async-storage';

export default class AddPrescription extends Component {
  state = {
    prescriptionHeading: '',
    prescriptionNote: '',
    fileSource: '',
    fileName: '',
    filetype: 'image/JPEG',
    loading: false,
    _showSelectImageSourceModel: false,
    selectedImageBase: '',
  };

  onTextChange = (text, state) => {
    this.setState({[`${state}`]: text});
  };

  openCamera = () => {
    launchCamera(
      {
        mediaType: 'photo',
        includeBase64: true,
        maxHeight: 2000,
        maxWidth: 2000,
      },
      (response) => {
        if (response.fileName != null || response.fileName != undefined) {
          // console.log("Image Capture Response---", response)
          this.setState({
            _showSelectImageSourceModel: false,
            fileSource: response.uri,
            fileName: response.fileName,
            selectedImageBase: response.base64,
          });
        } else {
          this.setState({
            _showSelectImageSourceModel: false,
          });
          ToastAndroid.show('Cancelled Adding Image', ToastAndroid.LONG);
        }
      },
    );
  };
  openImageLibrary = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: true,
        maxWidth: 2000,
        maxHeight: 2000,
      },
      (response) => {
        if (response.fileName != null || response.fileName != undefined) {
          this.setState({
            _showSelectImageSourceModel: false,
            fileSource: response.uri,
            fileName: response.fileName,
            selectedImageBase: response.base64,
          });
        } else {
          this.setState({
            _showSelectImageSourceModel: false,
          });
          ToastAndroid.show('Cancelled Adding Image', ToastAndroid.LONG);
        }
        // console.log("Image Response---", response)
      },
    );
  };

  onDataSubmit = async () => {
    let {
      fileSource,
      prescriptionHeading,
      prescriptionNote,
      fileName,
      filetype,
    } = this.state;
    const userId = await AsyncStorage.getItem('userId');
    if (
      fileSource === '' &&
      prescriptionHeading === '' &&
      prescriptionNote === ''
    ) {
      ToastAndroid.show('Enter All Details', ToastAndroid.LONG);
      return;
    }
    if (prescriptionHeading === '') {
      ToastAndroid.show('Please Enter Prescription Heading', ToastAndroid.LONG);
      return;
    }
    if (prescriptionNote === '') {
      ToastAndroid.show('Please Enter Prescription Note', ToastAndroid.LONG);
      return;
    }
    if (fileSource === '') {
      ToastAndroid.show('Image is not selected', ToastAndroid.LONG);
      return;
    }
    try {
      this.setState({loading: true});
      const bodyData = new FormData();
      bodyData.append('image', {
        uri: fileSource,
        name: fileName,
        type: filetype,
      });
      bodyData.append('heading', prescriptionHeading);
      bodyData.append('p_desc', prescriptionNote);
      bodyData.append('u_id', userId);
      const responseData = await postFetch(
        `${UploadPrescription}/`,
        bodyData,
        'form',
      );
      console.log('Fetch category-----', responseData);
      if (responseData.status === 'TXN') {
        ToastAndroid.show('Uploaded Successfully!!', ToastAndroid.LONG);
        this.setState({loading: false});
        this.props.navigation.goBack();
      } else {
        ToastAndroid.show(responseData.error, ToastAndroid.LONG);
        this.setState({loading: false});
      }
    } catch (error) {
      ToastAndroid.show(`Error - ${error}`, ToastAndroid.LONG);
      this.setState({loading: false});
    }
  };

  render() {
    let {
      prescriptionHeading,
      prescriptionNote,
      fileName,
      loading,
      _showSelectImageSourceModel,
      selectedImageBase,
    } = this.state;
    return (
      <View style={styles.__container}>
        <CustomStatusBar
          backgroundColor={Colors.blendDarkBlue}
          barStyle="light-content"
        />
        <BasicHeader
          title={'Send Prescription'}
          onBackPress={() => this.props.navigation.goBack()}
        />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 50 : 0}
          style={[styles.__container]}>
          <ScrollView
            keyboardDismissMode={'on-drag'}
            keyboardShouldPersistTaps="always"
            style={[styles.__container]}>
            <View style={[styles.__detailsContainer]}>
              <Text style={styles.__detailsTitle}>Enter Details</Text>
              <View style={styles.__textInput}>
                <TextInputUI
                  label={'Prescription Heading'}
                  labelStyle={{fontSize: 16, color: Colors.lightDarkBlue}}
                  placeholder={'Enter Prescription Heading'}
                  placeholderTextColor={Colors.verylightGrey}
                  value={prescriptionHeading}
                  backgroundColor={Colors.white}
                  inputBorderR={20}
                  width={width - 30}
                  onChangeText={(text) =>
                    this.onTextChange(text, 'prescriptionHeading')
                  }
                  height={50}
                  maxLength={120}
                  textInputStyles={{
                    color: Colors.lightDarkBlue,
                    fontSize: 18,
                    fontFamily: 'Montserrat-Regular',
                  }}
                  otherStyle={{
                    borderColor: Colors.lightBlue,
                    borderWidth: 1,
                    marginTop: 10,
                    paddingHorizontal: 5,
                  }}
                  returnKeyTypes={'next'}
                />
              </View>
              <View style={styles.__textInput}>
                <TextInputUI
                  label={'Prescription Note'}
                  labelStyle={{fontSize: 16, color: Colors.lightDarkBlue}}
                  placeholder={'Enter Prescription Note'}
                  placeholderTextColor={Colors.verylightGrey}
                  value={prescriptionNote}
                  backgroundColor={Colors.white}
                  inputBorderR={20}
                  width={width - 30}
                  onChangeText={(text) =>
                    this.onTextChange(text, 'prescriptionNote')
                  }
                  multiline
                  maxLength={240}
                  textInputStyles={{
                    color: Colors.lightDarkBlue,
                    fontSize: 18,
                    fontFamily: 'Montserrat-Regular',
                  }}
                  otherStyle={{
                    borderColor: Colors.lightBlue,
                    borderWidth: 1,
                    marginTop: 10,
                    paddingHorizontal: 5,
                    height: 'auto',
                    maxHeight: 180,
                    minHeight: 80,
                  }}
                  returnKeyTypes={'next'}
                />
              </View>
              <Text style={[styles.__detailsTitle, {marginTop: 10}]}>
                Select Prescription Image
              </Text>
              <TouchableOpacity
                onPress={() =>
                  this.setState({_showSelectImageSourceModel: true})
                }
                style={styles.__inputBottomContent}>
                <Image
                  source={
                    selectedImageBase
                      ? {uri: `data:image/png;base64,${selectedImageBase}`}
                      : images.picture
                  }
                  style={[
                    styles.__fileContentImage,
                    selectedImageBase ? styles.__selectedImageStyle : {},
                  ]}
                />
                <Text numberOfLines={1} style={styles.__fileContentText}>
                  {fileName ? fileName : 'Upload Image'}
                </Text>
              </TouchableOpacity>
              <View style={styles.__inputButton}>
                <ButtonUI
                  title={'Submit'}
                  backgroundColor={Colors.lightDarkBlue}
                  borderRadius={50}
                  elevation={5}
                  textStyles={{
                    fontSize: 17,
                    color: Colors.white,
                    fontFamily: 'Montserrat-Regular',
                  }}
                  width={width / 2}
                  height={HeightWidth.getResWidth(50)}
                  onPress={() => this.onDataSubmit()}
                  activityIndicator={loading}
                  disabled={loading}
                  activityIndicatorColor={Colors.white}
                />
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
        <CustomModel
          isModalVisible={_showSelectImageSourceModel}
          onBackButtonPress={() => {
            this.setState({_showSelectImageSourceModel: false});
          }}>
          <View style={styles.__imageModalContainer}>
            <Text style={styles.__imageModalLabel}>Select Image From</Text>
            <View style={[styles.__inputButtonModel, {width: width - 40}]}>
              <ButtonUI
                title={'Camera'}
                borderRadius={50}
                backgroundColor={Colors.lightBlue}
                leftIconName={'camera'}
                leftIconSize={20}
                leftIconColor={Colors.white}
                elevation={10}
                textStyles={{
                  fontSize: 16,
                  color: Colors.white,
                  fontFamily: 'Montserrat-Medium',
                  textAlign: 'center',
                }}
                width={width / 2 - 50}
                height={50}
                onPress={() => {
                  this.openCamera();
                }}
              />
              <ButtonUI
                title={'Image Library'}
                borderRadius={50}
                backgroundColor={Colors.lightBlue}
                leftIconName={'image'}
                leftIconSize={20}
                leftIconColor={Colors.white}
                elevation={10}
                textStyles={{
                  fontSize: 16,
                  color: Colors.white,
                  fontFamily: 'Montserrat-Medium',
                  textAlign: 'center',
                }}
                width={width / 2 - 30}
                height={50}
                onPress={() => {
                  this.openImageLibrary();
                }}
              />
            </View>
          </View>
        </CustomModel>
      </View>
    );
  }
}
